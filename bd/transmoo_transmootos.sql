-- phpMyAdmin SQL Dump
-- version 4.9.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Dec 18, 2020 at 08:26 AM
-- Server version: 5.6.41-84.1
-- PHP Version: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `transmoo_transmootos`
--

-- --------------------------------------------------------

--
-- Table structure for table `address`
--

CREATE TABLE `address` (
  `id` int(11) NOT NULL,
  `zip_code` varchar(9) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `number` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `complement` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `neighborhood` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `city` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `state` varchar(2) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `address`
--

INSERT INTO `address` (`id`, `zip_code`, `address`, `number`, `complement`, `neighborhood`, `city`, `state`) VALUES
(1, '', '', '', '', '', '', ''),
(2, '', '', '', '', '', '', ''),
(3, '39550000', 'RUA PORTEIRINHA ', '519', '', 'SANTOS CRUZEIRO', 'Taiobeiras', 'MG'),
(4, '39550000', 'RUA CEDRO', '55', '', 'PLANALTO', 'PLANALTO', 'MG'),
(5, '39550000', 'TRANSMOOTOS', '143', '', 'CENTRO', 'Taiobeiras', 'MG'),
(6, '39560000', 'ELIZABETE', '230', '', 'NEWTON JUNIOR', 'NEWTON JUNIOR', 'MG'),
(7, '39550000', 'transmootos', '143', '', 'Centro', 'Taiobeiras', 'MG'),
(8, '39550000', 'rua bh', '20', '', 'centro', 'centro', 'MG'),
(9, '39550000', 'bom jardim', '1751', '', 'bom jardim', 'Taiobeiras', 'MG'),
(10, '', '', '', '', '', '', ''),
(11, '39550000', 'praça doutor jose americano mendes', '143', '', 'Centro', 'Taiobeiras', 'MG'),
(12, '39550000', 'turmalina', '301', '', '', '', 'MG'),
(13, '39550000', 'Santa Rita De Cásia ', '802', '', 'vila formosa ', 'Taiobeiras', 'MG'),
(14, '39550000', 'Avenida São João', '184', '', 'centro', 'centro', 'MG'),
(15, '39550000', 'Praça doutor Jose americano mendes', '143', '', 'Centro', 'Taiobeiras', 'MG'),
(16, '39550000', 'Sagrameno', '246', '', 'sagrada família', 'sagrada família', 'MG'),
(17, '39550000', 'Praça doutor Jose americano mendes', '143', '', 'Centro', 'Taiobeiras', 'MG'),
(18, '39550000', 'Sagrameno', '246', '', 'sagrada família', 'sagrada família', 'MG'),
(19, '39550000', 'Praça doutor Jose americano mendes', '143', '', 'Centro', 'Taiobeiras', 'MG'),
(20, '39550000', 'Sagrameno', '246', '', 'sagrada família', 'sagrada família', 'MG'),
(21, '39550000', 'Praça doutor Jose americano mendes', '143', '', 'Centro', 'Taiobeiras', 'MG'),
(22, '39550000', 'Sagrameno', '246', '', 'sagrada família', 'sagrada família', 'MG'),
(23, '39550000', 'Praça Doutor José Americano Mendes', '143', '', 'Centro', 'Taiobeiras', 'MG'),
(24, '39550000', 'Sagramento', '246', '', 'sagrada família', 'sagrada família', 'MG'),
(25, '39550000', 'Praça doutor Jose americano mendes', '143', '', 'Centro', 'Taiobeiras', 'MG'),
(26, '39550000', 'Sagrameno', '246', '', 'sagrada família', 'sagrada família', 'MG'),
(27, '39550000', 'Praça doutor Jose americano mendes', '143', '', 'Centro', 'Taiobeiras', 'MG'),
(28, '39550000', 'Venezuela', '558', '', 'Santo Cruzeiro', 'Santo Cruzeiro', 'MG'),
(29, '39550000', 'Praça doutor Jose americano mendes', '143', '', 'Centro', 'Taiobeiras', 'MG'),
(30, '39550000', 'Venezuela', '558', '', 'Santo Cruzeiro', 'Santo Cruzeiro', 'MG'),
(31, '39550000', 'E', '66', '', 'Fila Formosa', 'Taiobeiras', 'MG'),
(32, '39550000', 'Lagoa Grande', '', '', '', '', 'MG'),
(33, '39550000', 'rua são vicente de paula esquina com av são joão ', '', '', 'Centro', 'Taiobeiras', 'MG'),
(34, '39550000', 'rua rio pardo', '310', '', 'centro', 'centro', 'MG'),
(35, '39550-000', 'Lagoa Grande', '', '', '', 'Taiobeiras', 'MG'),
(36, '39550-000', 'E', '66', '', 'Fila Formosa', 'Fila Formosa', 'MG'),
(37, '39550-000', 'rua sao vicente de paula esquina com av sao joao', '', '', 'centro', 'Taiobeiras', 'MG'),
(38, '39550-000', 'rua rio pardo', '310', '', 'centro', 'centro', 'MG'),
(39, '39550-000', 'PRAÇA DOUTOR JOSE AMERICANO MENDES', '143', '', 'CENTRO', 'Taiobeiras', 'MG'),
(40, '39550-000', 'VENEZUELA', '226', '', 'SANTO CRUZEIRO', 'SANTO CRUZEIRO', 'MG'),
(41, '39550-000', 'PRAÇA DOUTOR JOSE AMERICANO MENDES', '143', '', 'CENTRO', 'Taiobeiras', 'MG'),
(42, '39550-000', 'VENEZUELA', '226', '', 'SANTO CRUZEIRO', 'SANTO CRUZEIRO', 'MG'),
(43, '39550-000', 'P', '143', '', 'CENTRO', 'Taiobeiras', 'MG'),
(44, '', '', '', '', '', '', ''),
(45, '39550-000', 'PRAÇA DOUTOR JOSE AMERICANO MENDES', '143', '', 'CENTRO', 'Taiobeiras', 'MG'),
(46, '39550-000', 'RUA RIO PARDO', '310', '', 'CENTRO', 'CENTRO', 'MG'),
(47, '39550-000', 'PRAÇA DOUTOR JOSE AMERICANO MENDES', '143', '', 'CENTRO', 'Taiobeiras', 'MG'),
(48, '', 'onibus ', '', '', '', '', ''),
(49, '39550-000', 'MANOS LANCHES', '', '', 'CENTRO', 'Taiobeiras', 'MG'),
(50, '39550-000', 'RUA SANTO ANTONIO', '135', '', 'SAGRADA FAMILIA', 'SAGRADA FAMILIA', 'MG'),
(51, '39550-000', '`sete lagoas ', '809', '', 'SANTO CRUZEIRO', 'Taiobeiras', 'MG'),
(52, '39550-000', 'DROGARIA BRITO', '', '', '', '', 'MG'),
(53, '39550-000', '`sete lagoas ', '809', '', 'SANTO CRUZEIRO', 'Taiobeiras', 'MG'),
(54, '39550-000', 'DROGARIA BRITO', '', '', '', '', 'MG'),
(55, '39550-000', 'governador valadares', '91 A', '', 'SANTO CRUZEIRO', 'Taiobeiras', 'MG'),
(56, '39550-000', 'arena belvedere', '', '', '', '', 'MG'),
(57, '39550-000', 'governador valadares', '91 A', '', 'SANTO CRUZEIRO', 'Taiobeiras', 'MG'),
(58, '39550-000', 'arena belvedere', '', '', '', '', 'MG'),
(59, '39550-000', 'RUA BOM JARDIM', 'S/N', '', 'SAGRADA FAMILIA', 'Taiobeiras', 'MG'),
(60, '39550-000', 'RUA BAHIA ', '593', '', 'PLANALTO', 'PLANALTO', 'MG'),
(61, '39550-000', 'POSTO COMPEÇAS 2', '', '', 'SANTOS CRUZEIRO', 'Taiobeiras', 'MG'),
(62, '39550-000', 'TRAVESSA MARTINHO REGO', '29', '', 'CENTRO', 'CENTRO', 'MG'),
(63, '39550-000', 'PRAÇA DOUTOR JOSE AMERICANO MENDES', '143', '', 'CENTRO', 'Taiobeiras', 'MG'),
(64, '39550-000', '', '40', '', '', '', 'MG'),
(65, '39550-000', 'PRAÇA DOUTOR JOSE AMERICANO MENDES', '143', '', 'CENTRO', 'Taiobeiras', 'MG'),
(66, '39550-000', 'monte azul', '40', '', '', '', 'MG'),
(67, '39550-000', 'RUA PIRAPORA', '601', '', 'SAGRADA FAMILIA', 'Taiobeiras', 'MG'),
(68, '39550-000', 'RUA BAHIA', '593', '', 'PLANALTO', 'PLANALTO', 'MG'),
(69, '', 'pirapora', '601', '', '', 'Taiobeiras', 'MG'),
(70, '', 'noe', '', '', '', '', ''),
(71, '', 'pirapora', '601', '', '', 'Taiobeiras', 'MG'),
(72, '', 'noe', '', '', '', '', ''),
(73, '39550-000', 'santos drumont', '1136', '', '', 'Taiobeiras', 'MG'),
(74, '39550-000', 'centro', '', '', '', '', 'MG'),
(75, '39550-000', 'santos drumont', '1136', '', '', 'Taiobeiras', 'MG'),
(76, '39550-000', 'centro', '', '', '', '', 'MG'),
(77, '39550-000', 'RUA BAHIA ', '593', '', 'PLANALTO', 'Taiobeiras', 'MG'),
(78, '39550-000', 'RUA PIRAPORA ', '601', '', 'SAGRADA FAMILIA', 'SAGRADA FAMILIA', 'MG'),
(79, '39550-000', 'PRAÇA DOUTOR JOSE AMERICANO MENDES', '143', '', 'CENTRO', 'Taiobeiras', 'MG'),
(80, '39550-000', 'CRISTALIA', '66', '', 'Fila Formosa', 'Fila Formosa', 'MG'),
(81, '39550-000', 'RUA TAMOIOS ', '210', '', 'NS FATIMA', 'Taiobeiras', 'MG'),
(82, '39550-000', 'RUA NANUQUE', '78A', '', 'SAGRADA FAMILIA', 'SAGRADA FAMILIA', 'MG'),
(83, '39550-000', 'RUA NANUQUE ', '78A', '', 'SAGRADA FAMILIA', 'Taiobeiras', 'MG'),
(84, '39550-000', 'RUA TAMOIOS', '210', '', 'NSA FATIMA', 'NSA FATIMA', 'MG'),
(85, '39550-000', 'UNAI', '334', '', '', 'Taiobeiras', 'MG'),
(86, '39550-000', 'LAGOA GRANDE', '', '', '', '', 'MG'),
(87, '39550000', 'RUA BAHIA', '593', '', 'PLANALTO', 'Taiobeiras', 'MG'),
(88, '39550000', 'RUA PORTEIRINHA(OFICINA RENATIM)', '', '', 'SANTOS CRUZEIRO', 'SANTOS CRUZEIRO', 'MG'),
(89, '39550000', 'POSTO ALE (VILA)', '', '', 'SANTOS CRUZEIRO', 'Taiobeiras', 'MG'),
(90, '39550000', 'TRAVESSA MARTINHO REGO', '29', '', 'CENTRO', 'CENTRO', 'MG'),
(91, '39550-000', '', '', '', '', 'Taiobeiras', 'MG'),
(92, '39560-000', 'HOTEL VALLEY HILUS', '', '', 'RODOVIA', 'RODOVIA', 'MG'),
(93, '39550-000', 'AV DO CONTORNO', '', '', '', 'Taiobeiras', 'MG'),
(94, '39560-000', 'HOTEL VALLEY HILUS', '', '', 'RODOVIA', 'RODOVIA', 'MG'),
(95, '39550-000', 'AV DO CONTORNO', '', '', 'NOSSA SENHORA DE FATIMA', 'Taiobeiras', 'MG'),
(96, '39560-000', 'HOTEL VALLEY HILUS', '', '', 'RODOVIA', 'RODOVIA', 'MG'),
(97, '39550-000', 'AV DO CONTORNO', '40', '', 'NOSSA SENHORA DE FATIMA', 'Taiobeiras', 'MG'),
(98, '39560-000', 'HOTEL VALLEY HILUS', '52', '', 'RODOVIA', 'RODOVIA', 'MG'),
(99, '39550-000', 'AV DO CONTORNO', '40', '', 'NOSSA SENHORA DE FATIMA', 'Taiobeiras', 'MG'),
(100, '39560-000', 'HOTEL VALLEY HILUS', '52', '', 'RODOVIA', 'RODOVIA', 'MG'),
(101, '39550-000', 'AV DO CONTORNO', '40', '', 'NOSSA SENHORA DE FATIMA', 'Taiobeiras', 'MG'),
(102, '39560-000', 'HOTEL VALLEY HILUS', '52', '', 'RODOVIA', 'RODOVIA', 'MG'),
(103, '39550-000', 'AV', '40', '', 'NOSSA SENHORA DE FATIMA', 'Taiobeiras', 'MG'),
(104, '39560-000', 'HOTEL VALLEY HILUS', '52', '', 'RODOVIA', 'RODOVIA', 'MG'),
(105, '39550-000', 'AV DO CONTORNO', '40', '', 'NOSSA SENHORA DE FATIMA', 'Taiobeiras', 'MG'),
(106, '39560-000', 'HOTEL VALLEY HILUS', '52', '', 'RODOVIA', 'RODOVIA', 'MG'),
(107, '39550-000', 'AV DO CONTORNO', '', '', '', 'Taiobeiras', 'MG'),
(108, '39560-000', 'HOTEL VALLEY HILUS', '', '', 'RODOVIA', 'RODOVIA', 'MG'),
(109, '39550-000', 'AV DO CONTORNO', '', '', '', 'Taiobeiras', 'MG'),
(110, '39560-000', 'HOTEL VALLEY HILUS', '', '', 'RODOVIA', 'RODOVIA', 'MG'),
(111, '39550-000', 'AV DO CONTORNO', '', '', '', 'Taiobeiras', 'MG'),
(112, '39560-000', 'HOTEL VALLEY HILUS', '', '', 'RODOVIA', 'RODOVIA', 'MG'),
(113, '39550-000', '', '', '', 'centro', 'Taiobeiras', 'MG'),
(114, '', 'pizza mais', '', '', '', '', ''),
(115, '39550-000', '', '', '', 'centro', 'Taiobeiras', 'MG'),
(116, '', 'pizza mais', '', '', '', '', ''),
(117, '39550-000', 'praça doutor Jose americano mendes', '143', '', 'centro', 'Taiobeiras', 'MG'),
(118, '39550-000', 'Alcides Apostolo', '290', '', '', '', 'MG'),
(119, '39550-000', 'praça doutor Jose americano mendes', '143', '', 'centro', 'Taiobeiras', 'MG'),
(120, '39550-000', 'Alcides Apostolo', '290', '', '', '', 'MG'),
(121, '39550-000', 'RUA SETE LAGOAS', '595', '', 'NOSSA SENHORA DE FATIMA', 'Taiobeiras', 'MG'),
(122, '39560000', 'RODOVIA SALINAS/RUBELITA', '431', '', 'ALTO SÃO JOÃO', 'ALTO SÃO JOÃO', 'MG'),
(123, '39550000', 'PRAÇA DOUTOR JOSE AMERICANOS MENDES ', '143', '', 'CENTRO', 'Taiobeiras', 'MG'),
(124, '39560000', '', '', '', '', '', 'MG'),
(125, '39550000', 'PONTO DO ACAI', '', '', 'CENTRO', 'Taiobeiras', 'MG'),
(126, '39550000', 'RUA SAO ROMAO ', '87', '', 'SAGRADA FAMILIA', 'SAGRADA FAMILIA', 'MG'),
(127, '39550000', 'RUA JURAMENTO', '26', '', 'CENTRO', 'Taiobeiras', 'MG'),
(128, '39550000', 'PONTO DO ACAI', '', '', 'CENTRO', 'CENTRO', 'MG'),
(129, '39550-000', 'rio de janeiro', '420', '', 'nossa senhora de fatima', 'Taiobeiras', 'MG'),
(130, '39550-000', 'bar do 500', '', '', '', '', 'MG'),
(131, '39550-000', 'rio de janeiro', '420', '', 'nossa senhora de fatima', 'Taiobeiras', 'MG'),
(132, '39550-000', 'bar do 500', '', '', '', '', 'MG'),
(133, '39550-000', 'praça doutor jose americano mendes', '143', '', 'centro', 'Taiobeiras', 'MG'),
(134, '39550-000', 'formiga', '367', '', '', '', 'MG'),
(135, '39550-000', 'praça doutor jose americano mendes', '143', '', 'centro', 'Taiobeiras', 'MG'),
(136, '39550-000', 'formiga', '367', '', '', '', 'MG'),
(137, '39550-000', 'praça doutor jose americano mendes', '143', '', 'centro', 'Taiobeiras', 'MG'),
(138, '39550-000', 'rio de janeiro', '420', '', 'nossa senhora de fatima', 'nossa senhora de fatima', 'MG'),
(139, '39550-000', 'praça doutor jose americano mendes', '143', '', 'centro', 'Taiobeiras', 'MG'),
(140, '39550-000', 'rio de janeiro', '420', '', 'nossa senhora de fatima', 'nossa senhora de fatima', 'MG'),
(141, '39550000', 'praça doutor jose americano mendes', '143', '', 'centro', 'Taiobeiras', 'MG'),
(142, '39568000', 'AV JOAO BERNADINO DE SOUZA', '879', '', 'CENTRO', 'CENTRO', 'MG'),
(143, '39550000', 'praça doutor jose americano mendes', '143', '', 'centro', 'Taiobeiras', 'MG'),
(144, '39530000', 'AV MARIO NASCIMENTO ', '747', '', 'CENTRO', 'CENTRO', 'MG'),
(145, '39550000', 'praça doutor jose americano mendes', '143', '', 'centro', 'Taiobeiras', 'MG'),
(146, '39569000', 'rua pedra azul', '75', '', 'centro', 'centro', 'MG'),
(147, '39550000', 'parque de eventos', '', '', '', 'Taiobeiras', 'MG'),
(148, '39550000', 'sete lagoas', '827', '', 'santo cruzeiro', 'santo cruzeiro', 'MG'),
(149, '39550000', 'escola gente pequena', '', '', 'centro', 'Taiobeiras', 'MG'),
(150, '39550000', 'rua bom jardim', '1751', '', 'nilton junior', 'nilton junior', 'MG'),
(151, '39550000', 'praça doutor jose americano mendes', '143', '', '', 'Taiobeiras', 'MG'),
(152, '', 'marry noivas', '', '', '', '', ''),
(153, '39550000', 'praça doutor jose americano mendes', '143', '', '', 'Taiobeiras', 'MG'),
(154, '', 'marry noivas', '', '', '', '', ''),
(155, '39550000', 'praça doutor jose americano mendes', '143', '', '', 'Taiobeiras', 'MG'),
(156, '39550000', 'lambari', '169', '', '', '', 'MG'),
(157, '39550000', 'praça doutor jose americano mendes', '143', '', 'centro', 'Taiobeiras', 'MG'),
(158, '39550000', '', '1342', '', '', '', 'MG'),
(159, '39550-000', 'DEOCLECIANO DAVID COSTA', '30', '', '', 'Taiobeiras', 'MG'),
(160, '39550-000', 'FORUM', '', '', '', '', 'MG'),
(161, '39550-000', 'PRACA JOSE AMERICANO MENDES ', '143', '', 'CENTRO', 'Taiobeiras', 'MG'),
(162, '39970000', 'PRACA DO VARANDAL', '', '', 'CENTRO', 'CENTRO', 'MG'),
(163, '39550-000', 'PÇ DOUTOR JOSE AMERICANO MENDES', '143', '', 'CENTRO', 'Taiobeiras', 'MG'),
(164, '39550-000', 'E M JOAO DA CRUZ SANTOS', '', '', '', '', 'MG'),
(165, '39550-000', 'PÇ DOUTOR JOSE AMERICANO MENDES', '143', '', 'CENTRO', 'Taiobeiras', 'MG'),
(166, '39550-000', 'SAO VICENTE D PAULA', '644', '', '', '', 'MG'),
(167, '39550-000', 'PÇ DOUTOR JOSE AMERICANO MENDES', '143', '', 'CENTRO', 'Taiobeiras', 'MG'),
(168, '39550-000', 'tamoios', '210', '', '', '', 'MG'),
(169, '39550-000', 'PÇ DOUTOR JOSE AMERICANO MENDES', '143', '', 'CENTRO', 'Taiobeiras', 'MG'),
(170, '39550-000', 'tamoios', '210', '', '', '', 'MG'),
(171, '', 'PÇ DOUTOR JOSE AMERICANO MENDES', '143', '', 'CENTRO', 'Taiobeiras', 'MG'),
(172, '39550-000', 'venezuela', '260', '', 'santo cruzeiro', 'santo cruzeiro', 'MG'),
(173, '39550-000', 'PÇ DOUTOR JOSE AMERICANO MENDES', '143', '', 'CENTRO', 'Taiobeiras', 'MG'),
(174, '39550-000', 'SAO ROMÃO', '269', '', '', '', 'MG'),
(175, '39550-000', 'BAHIA', '143', '', '', 'Taiobeiras', 'MG'),
(176, '39550-000', 'CENTRO', '', '', '', '', 'MG'),
(177, '39550-000', 'PÇ DOUTOR JOSE AMERICANO MENDES', '143', '', 'CENTRO', 'Taiobeiras', 'MG'),
(178, '39550-000', 'AV LIBERDADE', '527', '', '', '', 'MG'),
(179, '39550-000', 'PÇ DOUTOR JOSE AMERICANO MENDES', '143', '', 'CENTRO', 'Taiobeiras', 'MG'),
(180, '39550-000', 'AV LIBERDADE', '527', '', '', '', 'MG'),
(181, '39550-000', 'bom jardim', '', '', 'CENTRO', 'Taiobeiras', 'MG'),
(182, '39550-000', 'rio pardo', '', '', '', '', 'MG'),
(183, '39550-000', 'PÇ DOUTOR JOSE AMERICANO MENDES', '143', '', 'CENTRO', 'Taiobeiras', 'MG'),
(184, '', 'farmacia cariri', '', '', '', '', ''),
(185, '39550-000', 'PÇ DOUTOR JOSE AMERICANO MENDES', '143', '', 'CENTRO', 'Taiobeiras', 'MG'),
(186, '39550-000', 'SAO ROMAO ', '800', '', '', '', 'MG'),
(187, '39550-000', 'PARQUE DE EVENTOS', '', '', '', 'Taiobeiras', 'MG'),
(188, '39550-000', 'SETE LAGOAS', '827', '', 'santo cruzeiro', 'santo cruzeiro', 'MG'),
(189, '39550-000', 'PÇ DOUTOR JOSE AMERICANO MENDES', '143', '', 'CENTRO', 'Taiobeiras', 'MG'),
(190, '39550-000', 'CAETES', '405', '', '', '', 'MG'),
(191, '39550-000', 'PERNAMBUCO', '26', '', '', 'Taiobeiras', 'MG'),
(192, '39550-000', 'FARMACIA POPULAR', '', '', '', '', 'MG'),
(193, '39550-000', 'BOM JARDIM', '950', '', '', 'Taiobeiras', 'MG'),
(194, '39550-000', 'GRAO MOGOL ', '05', '', 'CENTRO', 'CENTRO', 'MG'),
(195, '39550-000', 'BOM JARDIM', '950', '', '', 'Taiobeiras', 'MG'),
(196, '39550-000', 'GRAO MOGOL ', '05', '', 'CENTRO', 'CENTRO', 'MG'),
(197, '39550-000', 'GENTE PEQUENA', '143', '', '', 'Taiobeiras', 'MG'),
(198, '39550-000', 'BOM JARDIM ', '1751', '', '', '', 'MG'),
(199, '39550-000', 'PÇ DOUTOR JOSE AMERICANO MENDES', '143', '', 'CENTRO', 'Taiobeiras', 'MG'),
(200, '39550-000', 'AV LIBERDADE - ZAAMP', '', '', '', '', 'MG'),
(201, '39550-000', 'rua formia', '167', '', '', 'Taiobeiras', 'MG'),
(202, '39550-000', 'vila', '', '', '', '', 'MG'),
(203, '39550-000', 'PÇ DOUTOR JOSE AMERICANO MENDES', '143', '', 'CENTRO', 'Taiobeiras', 'MG'),
(204, '39550-000', 'santa luzia', '913', '', '', '', 'MG'),
(205, '39550-000', 'PÇ DOUTOR JOSE AMERICANO MENDES', '143', '', 'CENTRO', 'Taiobeiras', 'MG'),
(206, '', '', '', '', '', '', ''),
(207, '39550-000', 'PÇ DOUTOR JOSE AMERICANO MENDES', '143', '', 'CENTRO', 'Taiobeiras', 'MG'),
(208, '39550-000', 'jose sarmento', '66', '', '', '', 'MG'),
(209, '39550-000', 'tamoios', '210', '', '', 'Taiobeiras', 'MG'),
(210, '39550-000', 'grao mogol', '118', '', '', '', 'MG'),
(211, '39550-000', 'tamoios', '210', '', '', 'Taiobeiras', 'MG'),
(212, '39550-000', 'GRAO MOGOL ', '118', '', '', '', 'MG'),
(213, '39550-000', 'andre petrone ', '36', '', 'santo cruzeiro', 'Taiobeiras', 'MG'),
(214, '39550-000', 'paraiba', '303', '', '', '', 'MG');

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `profile` int(11) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `note` varchar(1000) NOT NULL,
  `commission` float(10,1) NOT NULL,
  `img_profile` varchar(500) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `date_register` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `name`, `profile`, `email`, `password`, `note`, `commission`, `img_profile`, `status`, `date_register`) VALUES
(1, 'Admin', 1, 'admin@gmail.com', '$2y$10$vJzIyiufauvbZ4af99yi0eGfukbW4HIqON1kGmR0ObUwxYldmTML.', 'teste', 0.0, '', 1, '2020-05-02 17:00:58'),
(2, 'Maria', 2, 'maria@gmail.com', '$2y$10$spvu4A2OfcCdgZges9FWWutzAmyIgx91s6cYTwRkMQ8U1/FKsM/qS', '', 0.0, 'perfil.png', 1, '2020-08-10 12:41:43'),
(3, 'Leandro', 1, 'leandromiranda4101@gmail.com', '$2y$10$s4JJ6AOWX1m6.qgesDCsse.JfEarXKStLNArckuE3svCmf04YGBpi', '', 0.0, 'perfil.png', 1, '2020-08-10 13:27:03'),
(4, 'Gissely', 2, 'gisselyx@outlook.com', '$2y$10$z5QZmYqS.FuS6DZE1iov3egqshuUuzEonjS7bWy8E7rjzY8d8c7zu', '', 0.0, 'perfil.png', 0, '2020-08-10 13:28:07'),
(5, 'Kessy', 1, 'kessylo003@gmail.com', '$2y$10$s4YBI6fvZbOlxF3LjYWZJO0uPiQT1jL29s4NwcfAGjt7O2FrrRuKS', '', 0.0, 'perfil.png', 1, '2020-08-21 10:06:40');

-- --------------------------------------------------------

--
-- Table structure for table `bikers`
--

CREATE TABLE `bikers` (
  `id` int(11) NOT NULL,
  `cpf` varchar(20) NOT NULL,
  `name` varchar(200) NOT NULL,
  `surname` varchar(200) NOT NULL,
  `date_birthday` date NOT NULL,
  `phone` varchar(200) NOT NULL,
  `license` varchar(200) NOT NULL,
  `license_expiration` date DEFAULT NULL,
  `blood_type` int(11) NOT NULL,
  `commission` float(10,1) NOT NULL,
  `image` varchar(200) NOT NULL,
  `type_document` int(11) NOT NULL,
  `register` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bikers`
--

INSERT INTO `bikers` (`id`, `cpf`, `name`, `surname`, `date_birthday`, `phone`, `license`, `license_expiration`, `blood_type`, `commission`, `image`, `type_document`, `register`) VALUES
(1, '111.253.696-55', 'José Clecio', 'Mendes Malaquias (SAIU 16/09/2020)', '1991-10-07', '(38) 99181-9544', '05379060120', '2024-10-31', 0, 0.0, 'perfil.png', 0, '2020-08-10 13:56:40'),
(2, '169.674.666-31', 'Pedro', 'Henrique de Matos', '2001-02-03', '(38) 98388-617', '07411065578', '2021-01-28', 0, 20.0, '', 0, '2020-08-10 14:08:37'),
(3, '166.481.766-28', 'Paulo', 'Victor dos Santos Freitas', '2001-10-08', '(38) 98812-6984', '07427305439', '2021-03-02', 3, 20.0, '', 0, '2020-08-10 14:10:49'),
(4, '120.521.736-3', 'Heuton', 'Barbosa Sena', '1993-10-19', '(38) 99159-1550', '05750521595', '2023-10-18', 0, 20.0, 'perfil.png', 0, '2020-08-10 14:17:13'),
(5, '087.389.886-90', 'ELIEZER ', 'FERREIRA DOS SANTOS', '1987-02-01', '(38) 98808-9198', '04076819910', '2023-11-14', 5, 20.0, 'perfil.png', 0, '2020-08-12 18:30:27'),
(6, '106.268.276-98', 'WANDERSON ', 'LIMA FERREIRA', '1999-06-10', '(38) 99269-2962', '07011241123', '2023-03-21', 0, 40.0, 'perfil.png', 0, '2020-08-19 19:23:03'),
(7, '332.365.618-07', 'Renato Caldeira', 'Barros do Amaral', '1999-09-15', '(38) 99843-1471', 'AB', '2024-12-03', 0, 20.0, 'perfil.png', 0, '2020-09-16 17:00:07'),
(8, '127.835.076-41', 'Edson', 'Ribeiro dos Santos', '1995-01-19', '(38) 99860-9679', 'AB (N°REGISTRO 0848452710)', '2024-08-07', 0, 40.0, '5d080de885365dacd59de129afcd2fdc.jpg', 0, '2020-09-17 15:05:26'),
(9, '108.052.686-26', 'LEANDRO MIRANDA XAVIER', '', '1993-01-21', '(38) 99217-7267', '05194188667', '2024-09-30', 0, 0.0, 'perfil.png', 0, '2020-09-18 12:57:26'),
(10, '241.835.088-60', 'WARLEY ', ' DA CRUZ ROCHA', '2000-04-12', '(19) 99955-7818', '07262778277', '2020-05-15', 0, 20.0, 'perfil.png', 0, '2020-09-24 09:18:25'),
(11, '', 'WILSON', 'SANTOS DA SILVA', '1978-12-25', '', '', '0000-00-00', 0, 0.0, 'perfil.png', 0, '2020-09-25 17:15:37'),
(12, '131.529.496-69', 'Diego ', 'Alves Araujo', '1995-03-20', '(38) 98853-5837', '06681501449', '2022-06-20', 0, 40.0, 'perfil.png', 0, '2020-09-29 16:13:32'),
(13, '106.347.566-05', 'ROGERIO', 'PEREIRA GOMES ', '1987-03-06', '(34) 99238-1519', '06681512717', '2023-09-05', 0, 40.0, 'perfil.png', 0, '2020-09-30 09:34:42');

-- --------------------------------------------------------

--
-- Table structure for table `bills_pay`
--

CREATE TABLE `bills_pay` (
  `id` int(11) NOT NULL,
  `id_provider` int(11) NOT NULL,
  `name_bill` varchar(200) NOT NULL,
  `bill_value` float(10,2) NOT NULL,
  `type_bill` varchar(100) NOT NULL COMMENT 'Fixo, Variavel',
  `due_date` date NOT NULL,
  `paid_out` enum('0','1') NOT NULL COMMENT '0 == nao, 1 == sim',
  `register` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `bills_pay`
--

INSERT INTO `bills_pay` (`id`, `id_provider`, `name_bill`, `bill_value`, `type_bill`, `due_date`, `paid_out`, `register`) VALUES
(2, 2, 'INTERNET ', 64.90, 'Fixo', '2020-08-20', '1', '2020-09-21 10:00:15'),
(3, 3, 'ENERGIA', 69.76, 'Variável', '2020-08-06', '1', '2020-10-13 17:25:17'),
(4, 4, 'ALUGUEL IMPRESSORA', 50.00, 'Fixo', '2020-09-06', '1', '2020-09-18 17:13:17'),
(5, 1, 'ALUGUEL ', 600.00, 'Fixo', '2020-09-10', '1', '2020-09-02 16:50:02'),
(6, 5, 'PUBLICIDADE', 500.00, 'Variável', '2020-09-16', '1', '2020-10-02 16:22:20'),
(7, 6, 'SISTMEA ', 1000.00, 'Variável', '2020-08-30', '0', '2020-08-21 12:36:02'),
(8, 8, 'PEÇAS P/ UNO ', 819.55, 'Variável', '2020-09-22', '1', '2020-10-13 17:23:37'),
(9, 8, 'PEÇAS P/ UNO', 819.54, 'Variável', '2020-10-20', '0', '2020-08-21 12:58:22'),
(10, 9, 'HONORARIOS CONTABEIS', 232.00, 'Fixo', '2020-08-01', '1', '2020-09-25 13:37:48'),
(11, 3, 'ENERGIA', 48.27, 'Variável', '2020-09-06', '1', '2020-09-18 14:22:07'),
(12, 10, 'CAMISETAS TRANS MOTOS', 1203.00, 'Variável', '2020-08-24', '1', '2020-08-28 09:34:39'),
(13, 10, 'CAMISETAS TRANS MOTOS', 108.00, 'Variável', '2020-08-27', '1', '2020-08-28 14:25:43'),
(14, 11, 'concerto carro', 350.00, 'Variável', '2020-08-27', '1', '2020-08-28 10:20:04'),
(15, 12, 'HIGIENIZAÇÂO', 300.00, 'Variável', '2020-08-28', '1', '2020-08-28 14:54:40'),
(16, 13, 'alinhamento, balanceamento e cambagem', 140.00, 'Variável', '2020-08-29', '1', '2020-09-01 10:55:17'),
(17, 14, 'Publicidade', 100.00, 'Variável', '2020-09-01', '1', '2020-09-01 11:03:34'),
(18, 15, 'PEÇAS e MÃO DE OBRA', 740.00, 'Variável', '2020-09-01', '1', '2020-09-01 16:23:22'),
(19, 3, 'energia', 48.27, 'Variável', '2020-09-06', '0', '2020-09-08 15:12:01'),
(20, 17, 'AGUA', 93.95, 'Variável', '2020-09-28', '0', '2020-09-09 08:23:49'),
(21, 17, 'AGUA', 84.38, 'Variável', '2020-08-29', '1', '2020-09-18 14:21:54'),
(22, 3, 'energia', 46.92, 'Variável', '2020-10-06', '0', '2020-09-23 14:57:34'),
(23, 18, 'PLAFETOS', 150.00, 'Variável', '2020-09-23', '1', '2020-09-23 15:03:03'),
(24, 16, 'almoço salinas', 140.00, 'Variável', '2020-09-24', '1', '2020-09-24 16:37:14'),
(25, 19, 'CARREATA', 50.00, 'Variável', '2020-09-25', '1', '2020-09-25 16:26:44');

-- --------------------------------------------------------

--
-- Table structure for table `bills_payments`
--

CREATE TABLE `bills_payments` (
  `id` int(11) NOT NULL,
  `id_bill` int(11) NOT NULL,
  `date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `bills_payments`
--

INSERT INTO `bills_payments` (`id`, `id_bill`, `date`) VALUES
(1, 12, '2020-08-28 09:34:39'),
(2, 14, '2020-08-28 10:20:03'),
(3, 13, '2020-08-28 14:25:43'),
(4, 15, '2020-08-28 14:54:40'),
(5, 16, '2020-09-01 10:55:17'),
(6, 16, '2020-09-01 10:55:18'),
(7, 17, '2020-09-01 11:03:34'),
(8, 18, '2020-09-01 16:23:22'),
(9, 5, '2020-09-02 16:50:02'),
(10, 21, '2020-09-18 14:21:54'),
(11, 11, '2020-09-18 14:22:07'),
(12, 4, '2020-09-18 17:13:17'),
(13, 2, '2020-09-21 10:00:15'),
(14, 23, '2020-09-23 15:03:03'),
(15, 24, '2020-09-24 16:37:14'),
(16, 10, '2020-09-25 13:37:48'),
(17, 25, '2020-09-25 16:26:44'),
(18, 6, '2020-10-02 16:22:20'),
(19, 5, '2020-10-05 14:56:28'),
(20, 8, '2020-10-13 17:23:37'),
(21, 3, '2020-10-13 17:25:17');

-- --------------------------------------------------------

--
-- Table structure for table `bills_received`
--

CREATE TABLE `bills_received` (
  `id` int(11) NOT NULL,
  `id_client` int(11) NOT NULL,
  `amount` float(10,2) NOT NULL,
  `date_receipt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `user` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `bills_received`
--

INSERT INTO `bills_received` (`id`, `id_client`, `amount`, `date_receipt`, `user`) VALUES
(1, 3, 36.00, '2020-08-27 11:22:08', 1),
(2, 10, 18.00, '2020-08-27 11:28:31', 5),
(3, 2, 18.00, '2020-08-27 11:28:52', 5),
(4, 1, 645.00, '2020-08-27 11:30:24', 5),
(5, 11, 54.00, '2020-08-27 11:34:09', 5),
(6, 8, 70.00, '2020-08-27 11:34:52', 5),
(7, 5, 600.00, '2020-08-27 11:40:07', 5),
(8, 18, 327.00, '2020-08-27 11:41:25', 5),
(9, 1, 87.50, '2020-08-27 11:55:46', 5),
(10, 18, 525.00, '2020-09-02 15:39:52', 5),
(11, 14, 0.00, '2020-09-03 18:53:12', 3),
(12, 14, 408.00, '2020-09-04 17:25:27', 5),
(13, 20, 144.00, '2020-09-04 17:27:52', 5),
(14, 5, 303.00, '2020-09-04 17:47:36', 5),
(15, 19, 510.00, '2020-09-08 15:26:00', 5),
(16, 1, 730.00, '2020-09-09 09:03:12', 5),
(17, 16, 172.50, '2020-09-10 11:14:51', 5),
(18, 18, 477.00, '2020-09-09 17:05:23', 3),
(19, 8, 52.50, '2020-09-10 18:04:39', 5),
(20, 12, 12.00, '2020-09-11 12:48:28', 5),
(21, 3, 81.00, '2020-09-14 17:06:29', 5),
(22, 5, 249.00, '2020-09-15 17:17:11', 3),
(23, 18, 578.00, '2020-09-16 17:40:58', 5),
(24, 9, 192.50, '2020-09-18 14:05:33', 3),
(25, 9, 50.00, '2020-09-18 14:06:08', 3),
(26, 5, 348.00, '2020-09-18 18:16:07', 5),
(27, 27, 69.00, '2020-09-21 15:44:35', 5),
(28, 11, 0.00, '2020-09-21 18:05:50', 5),
(29, 11, 111.00, '2020-09-21 18:06:28', 5),
(30, 20, 225.00, '2020-09-21 18:08:45', 5),
(31, 14, 0.00, '2020-09-22 16:31:33', 1),
(32, 28, 6.00, '2020-09-23 10:18:33', 5),
(33, 18, 5.60, '2020-09-23 14:19:42', 5),
(34, 26, 33.00, '2020-09-23 15:34:09', 5),
(35, 18, 711.20, '2020-09-23 16:43:11', 5),
(36, 28, 3.00, '2020-09-24 11:06:38', 5),
(37, 19, 498.00, '2020-09-25 17:39:10', 5),
(38, 27, 81.00, '2020-09-28 17:38:49', 5),
(39, 28, 3.00, '2020-09-30 11:26:25', 5),
(40, 18, 613.20, '2020-09-30 16:33:38', 5),
(41, 24, 32.50, '2020-10-01 14:03:22', 5),
(42, 21, 84.00, '2020-10-01 14:06:06', 5),
(43, 20, 126.00, '2020-10-01 18:17:23', 5),
(44, 14, 510.00, '2020-10-01 18:18:35', 5),
(45, 26, 51.00, '2020-10-01 18:19:01', 5),
(46, 24, 42.50, '2020-10-02 14:27:00', 5),
(47, 9, 295.00, '2020-10-03 08:46:33', 5),
(48, 1, 810.00, '2020-10-03 08:47:37', 5),
(49, 31, 297.00, '2020-10-05 15:22:14', 5),
(50, 14, 183.00, '2020-10-08 10:53:00', 5),
(51, 27, 54.00, '2020-10-08 16:17:41', 5),
(52, 25, 327.00, '2020-10-08 16:17:55', 5),
(53, 16, 212.50, '2020-10-13 08:26:22', 5),
(54, 18, 786.80, '2020-10-13 08:27:07', 5),
(55, 23, 115.00, '2020-10-13 13:35:15', 5),
(56, 26, 63.00, '2020-10-14 14:18:44', 5),
(57, 1, 565.00, '2020-10-14 15:13:53', 5),
(58, 18, 840.00, '2020-10-14 15:50:13', 5),
(59, 31, 213.00, '2020-10-15 08:05:30', 5),
(60, 17, 65.00, '2020-10-15 08:05:42', 5),
(61, 28, 9.00, '2020-10-15 08:05:53', 5),
(62, 19, 228.00, '2020-10-15 16:36:17', 5),
(63, 8, 27.50, '2020-10-15 17:24:31', 5),
(64, 11, 66.00, '2020-10-15 17:40:59', 5),
(65, 24, 10.00, '2020-10-15 17:41:10', 5),
(66, 27, 57.00, '2020-10-16 16:58:00', 5),
(67, 30, 132.00, '2020-10-16 16:59:08', 5),
(68, 26, 15.00, '2020-10-16 17:28:49', 5),
(69, 20, 39.00, '2020-10-22 16:24:50', 5),
(70, 21, 24.00, '2020-10-26 14:19:29', 5),
(71, 18, 299.60, '2020-10-26 16:40:01', 5);

-- --------------------------------------------------------

--
-- Table structure for table `blood_type`
--

CREATE TABLE `blood_type` (
  `id` int(11) NOT NULL,
  `description` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `blood_type`
--

INSERT INTO `blood_type` (`id`, `description`) VALUES
(1, 'A+'),
(2, 'A-'),
(3, 'B+'),
(4, 'B-'),
(5, 'AB+'),
(6, 'AB-'),
(7, 'O+'),
(8, 'O-');

-- --------------------------------------------------------

--
-- Table structure for table `clients`
--

CREATE TABLE `clients` (
  `id` int(11) NOT NULL,
  `type_client` int(11) NOT NULL,
  `where_from` int(11) NOT NULL,
  `contract` tinyint(1) NOT NULL,
  `price_trip` float(10,2) NOT NULL,
  `image` varchar(200) NOT NULL,
  `observation` text NOT NULL,
  `status` int(11) NOT NULL,
  `register` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `clients`
--

INSERT INTO `clients` (`id`, `type_client`, `where_from`, `contract`, `price_trip`, `image`, `observation`, `status`, `register`) VALUES
(1, 2, 3, 1, 2.50, 'perfil.png', 'Dias de funcionamento: segunda a sabado das 18:00 as 00:00\r\ndomingo de 11:00 as 14:00', 1, '2020-08-10 16:47:33'),
(2, 2, 3, 1, 3.00, 'perfil.png', 'HORARIO DE FUNCIONAMENTO; DAS 19:00 AS 23:00\r\nNÃO FUNCIONA NA TERÇA \r\n', 1, '2020-08-10 17:10:41'),
(3, 2, 3, 1, 3.00, 'perfil.png', 'RUA RIO VERDE, 40 - PLANALTO\r\nHORARIO DE FUNCIONAMENTO ; DAS 19:30 AS 23:00\r\nNAO ATENDE TERÇA', 1, '2020-08-10 17:16:39'),
(4, 1, 5, 0, 0.00, 'perfil.png', '', 1, '2020-08-10 17:58:21'),
(5, 1, 3, 0, 3.00, 'perfil.png', 'NÃO TRABALHA SOMENTE NA SEGUNDA FEIRA.\r\nPILOTO: CLESIO \r\nENCERRADO MES SETEMBRO', 1, '2020-08-11 10:25:24'),
(6, 1, 3, 0, 0.00, 'perfil.png', 'QUARTA, QUINTA, SEXTA À PARTI 19:30 \r\nTER MUITO CUIDADO', 1, '2020-08-11 14:00:06'),
(7, 2, 3, 0, 0.00, 'perfil.png', 'DOMINGO DAS 19:00 AS 23:30', 1, '2020-08-11 18:10:14'),
(8, 1, 3, 1, 2.50, 'perfil.png', 'RUA NANUQUE, 49\r\nHORÁRIOS = QUARTA, QUINTA E DOMINGO DAS 19:00 AS 22:30 E NAS SEXTAS E NOS SÁBADOS DAS 19:00 AS 23:00', 1, '2020-08-11 18:15:43'),
(9, 2, 3, 1, 2.50, 'perfil.png', 'SEGUNDA A SEGUNDA DAS 17:00 AS 00:00 \r\nDURANTE O DIA ALMOÇO DAS 11:00 AS 14:', 1, '2020-08-11 18:19:19'),
(10, 1, 3, 1, 3.00, 'perfil.png', 'AV CAIÇARA, 1134 - SAGRADA FAMILIA\r\nENTREGAS AVULSAS', 1, '2020-08-11 18:24:59'),
(11, 2, 3, 1, 3.00, 'perfil.png', 'BLOCO Nº76 - 7501 AO 7600\r\nTAXA 3,00 POR ENTREGA', 1, '2020-08-12 10:01:14'),
(12, 1, 3, 1, 3.00, 'perfil.png', 'VALOR: 3,00 POR ENTREGA\r\nEND: RUA RIO GRANDE DO SUL Nº3', 1, '2020-08-12 11:29:19'),
(13, 1, 3, 0, 0.00, 'perfil.png', '', 1, '2020-08-12 15:26:41'),
(14, 2, 3, 1, 3.00, 'perfil.png', 'SEXTA E SABADO DAS 19:00 AS 23:00', 1, '2020-08-12 19:23:41'),
(15, 1, 3, 0, 0.00, 'perfil.png', '', 1, '2020-08-14 08:16:17'),
(16, 1, 3, 1, 2.50, 'perfil.png', 'AVENIDA DO CONTORNO , Nº 2921 PROXIMO A MARRY NOIVAS\r\nquarta e sexta 19:30 ás 21:30', 1, '2020-08-14 15:16:27'),
(17, 2, 3, 1, 2.50, 'perfil.png', 'SEG à SEX : 18:00 às 23:00\r\nSAB e DOMINGO: 16 às 18:00 avulso \r\nSEG à QUARTA 1 ENTREGADOR\r\nQUINTA à DOMINGO 2 ENTREGADORES', 1, '2020-08-17 10:04:40'),
(18, 2, 3, 1, 2.80, 'perfil.png', 'VALOR: 2,80\r\n13:30 ÁS 22:00 AVULSO\r\nGERENTE: LUCIANA ', 1, '2020-08-19 09:57:27'),
(19, 2, 3, 1, 3.00, 'perfil.png', '14:00 AS 22:00\r\nAVULSAS', 1, '2020-08-19 14:10:42'),
(20, 1, 2, 1, 3.00, 'perfil.png', 'RUA SANTOS DUMONT, 682- VILA FORMOSA\r\nSEGUNDA,TERÇA,QUINTA, SEXTA E SABADO DAS 13:00 AS 23:10\r\nQUARTA DAS 13:00 AS 18:10', 1, '2020-08-20 20:07:08'),
(21, 1, 3, 1, 3.00, 'perfil.png', 'SEG À SEX 13:30 ÀS 15:00  AV SÃO JOÃO 101 (PROXIMO AO PRONTO SOCORRO)\r\nSENDO 2 OU 3 VEZES POR SEMANA', 1, '2020-08-28 15:33:54'),
(22, 1, 3, 0, 0.00, 'perfil.png', '', 1, '2020-09-02 09:06:44'),
(23, 1, 3, 1, 2.50, 'perfil.png', 'RUA ESPINOSA 31 \r\nQUARTA-FEIRA', 1, '2020-09-02 16:16:46'),
(24, 2, 3, 1, 2.50, 'perfil.png', '', 1, '2020-09-03 12:13:21'),
(25, 1, 5, 1, 3.00, 'perfil.png', '', 1, '2020-09-09 12:57:57'),
(26, 2, 3, 1, 3.00, 'perfil.png', 'SEGUNDA à SEXTA 13:00 às 17:00 \r\nRua: SAO ROMAO n°800', 1, '2020-09-09 15:44:10'),
(27, 1, 3, 1, 3.00, 'perfil.png', 'sab', 1, '2020-09-18 14:32:31'),
(28, 1, 3, 1, 3.00, 'perfil.png', '8:00 ÀS 20:00\r\nCAIÇARA N°851', 1, '2020-09-23 09:21:44'),
(29, 2, 0, 0, 0.00, 'perfil.png', 'rua: jovina cruz 349, bairro raquel / SALINAS-MG', 1, '2020-09-24 18:15:48'),
(30, 1, 3, 1, 3.00, 'perfil.png', 'SEGUNDA A SABADO 10:00 A 21:00\r\nDOMINGO 12:00 AS 21:00', 1, '2020-09-28 09:15:52'),
(31, 2, 3, 1, 3.00, 'perfil.png', 'QUARTA A DOMINGO 18:00 as 00:00', 1, '2020-09-29 10:18:20');

-- --------------------------------------------------------

--
-- Table structure for table `contracts`
--

CREATE TABLE `contracts` (
  `id` int(11) NOT NULL,
  `id_client` int(11) NOT NULL,
  `contract` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `document_biker`
--

CREATE TABLE `document_biker` (
  `id` int(11) NOT NULL,
  `id_biker` int(11) NOT NULL,
  `document` varchar(200) NOT NULL,
  `type_document` int(11) NOT NULL,
  `register` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `gasoline_control`
--

CREATE TABLE `gasoline_control` (
  `id` int(11) NOT NULL,
  `id_motorcycle` int(11) NOT NULL,
  `odometer` float(10,1) NOT NULL,
  `quantity` float(10,2) NOT NULL,
  `amount` float(10,2) NOT NULL,
  `gas_station` varchar(200) NOT NULL,
  `register` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gasoline_control`
--

INSERT INTO `gasoline_control` (`id`, `id_motorcycle`, `odometer`, `quantity`, `amount`, `gas_station`, `register`) VALUES
(1, 1, 60689.0, 7.00, 37.00, 'POSTO TAIOBEIRAS', '2020-08-10 17:37:08'),
(2, 4, 14596.0, 12.00, 60.00, 'POSTO COMPEÇAS', '2020-08-10 17:43:44'),
(3, 7, 37057.0, 11.00, 55.00, 'POSTO COMPEÇAS', '2020-08-12 18:36:15'),
(4, 3, 13128.0, 12.00, 62.00, 'POSTO TAIOBEIRAS', '2020-08-12 19:01:09'),
(5, 2, 63904.0, 14.00, 70.00, 'POSTO COMPEÇAS', '2020-08-12 19:15:08'),
(6, 4, 14830.0, 8.00, 40.00, 'POSTO COMPEÇAS', '2020-08-17 18:14:20'),
(7, 1, 61116.0, 4.00, 20.00, 'POSTO PATATIVA', '2020-08-17 23:34:01'),
(8, 1, 61126.0, 8.00, 40.00, 'POSTO COMPEÇAS', '2020-08-18 18:02:10'),
(9, 5, 60369.0, 12.00, 60.00, 'POSTO COMPEÇAS', '2020-08-19 19:11:25'),
(10, 3, 13482.0, 8.00, 40.00, 'POSTO TAIOBERAS', '2020-08-20 19:17:41'),
(11, 6, 31762.0, 11.56, 56.00, 'POSTO PATATIVA', '2020-08-21 17:27:41'),
(12, 8, 11609.0, 2.00, 10.00, 'POSTO PATATIVA', '2020-08-21 22:33:14'),
(13, 7, 37431.0, 4.00, 20.00, 'POSTO PATATIVA', '2020-08-22 00:11:42'),
(14, 1, 61515.0, 4.00, 4.00, 'POSTO COMPEÇAS', '2020-08-24 08:44:29'),
(15, 8, 11699.0, 2.00, 10.00, 'POSTO PATATIVA', '2020-08-24 13:07:58'),
(16, 3, 13633.0, 4.00, 20.00, 'POSTO PATATIVA', '2020-08-24 13:09:50'),
(17, 7, 37527.0, 2.00, 10.00, 'POSTO PATATIVA', '2020-08-24 20:34:41'),
(18, 4, 15226.0, 2.00, 10.00, 'POSTO COMPEÇAS', '2020-08-25 00:07:41'),
(19, 3, 13723.0, 4.00, 20.00, 'POSTO COMPEÇAS', '2020-08-25 18:32:33'),
(20, 7, 37564.0, 4.00, 20.00, 'POSTO COMPEÇAS', '2020-08-25 18:38:02'),
(21, 4, 15265.0, 4.00, 20.00, 'POSTO COMPEÇAS', '2020-08-25 23:54:03'),
(22, 1, 61631.0, 4.00, 20.00, 'POSTO COMPEÇAS', '2020-08-26 11:08:53'),
(23, 5, 60672.0, 4.00, 20.00, 'POSTO TAIOBERAS', '2020-08-26 19:03:20'),
(24, 8, 11749.0, 2.00, 10.00, 'POSTO PATATIVA', '2020-08-26 22:33:39'),
(25, 3, 13821.0, 4.00, 20.00, 'POSTO COMPEÇAS', '2020-08-27 21:11:12'),
(26, 1, 61771.0, 4.00, 20.00, 'POSTO COMPEÇAS', '2020-08-28 00:09:06'),
(27, 5, 60740.0, 4.00, 20.00, 'POSTO TAIOBERAS', '2020-08-28 23:46:44'),
(28, 4, 15413.0, 4.00, 20.00, 'POSTO COMPEÇAS', '2020-08-29 09:10:51'),
(29, 9, 122.5, 6.00, 30.00, 'POSTO COMPEÇAS', '2020-08-29 09:19:56'),
(30, 7, 37778.0, 4.00, 20.00, 'POSTO PATATIVA', '2020-08-29 20:25:31'),
(31, 3, 13924.0, 4.00, 20.00, 'POSTO PATATIVA', '2020-08-29 20:26:33'),
(32, 1, 61892.0, 4.00, 20.00, 'POSTO COMPEÇAS', '2020-08-30 01:30:23'),
(33, 4, 15487.0, 4.00, 20.00, 'Mendes Rocha', '2020-08-31 08:18:46'),
(34, 3, 13969.0, 202.00, 10.00, 'POSTO PATATIVA', '2020-08-31 13:56:04'),
(35, 3, 14009.0, 4.00, 20.00, 'POSTO COMPEÇAS', '2020-08-31 14:28:59'),
(36, 5, 60958.0, 4.05, 20.00, 'POSTO PATATIVA', '2020-08-31 16:16:16'),
(37, 2, 64292.0, 4.00, 20.00, 'POSTO PATATIVA', '2020-09-01 21:25:10'),
(38, 5, 61075.0, 4.00, 20.00, 'Mendes Rocha', '2020-09-02 21:25:02'),
(39, 9, 122489.0, 30.00, 100.00, 'POSTO COMPEÇAS', '2020-09-03 08:53:10'),
(40, 4, 15661.0, 4.00, 20.00, 'POSTO COMPEÇAS', '2020-09-03 08:55:58'),
(41, 7, 37941.0, 4.00, 20.00, 'POSTO PATATIVA', '2020-09-03 21:26:54'),
(42, 1, 62066.0, 4.00, 20.00, 'POSTO COMPEÇAS', '2020-09-03 21:52:06'),
(43, 1, 62183.0, 2.00, 14.00, 'POSTO PATATIVA', '2020-09-04 00:03:32'),
(44, 4, 15776.0, 4.00, 20.00, 'POSTO JENIPAPO SALINAS', '2020-09-04 16:14:23'),
(45, 9, 1227440.0, 18.00, 60.00, 'POSTO JENIPAPO SALINAS', '2020-09-04 16:36:27'),
(46, 7, 38001.0, 10.00, 50.00, 'POSTO COMPEÇAS', '2020-09-04 19:07:08'),
(47, 3, 14156.0, 10.00, 50.00, 'POSTO COMPEÇAS', '2020-09-04 19:29:08'),
(48, 2, 64371.0, 10.00, 50.00, 'POSTO PATATIVA', '2020-09-04 19:49:37'),
(49, 5, 61177.0, 10.00, 50.00, 'Mendes Rocha', '2020-09-04 21:48:40'),
(50, 1, 62231.0, 10.00, 50.00, 'POSTO COMPEÇAS', '2020-09-05 00:29:49'),
(51, 4, 15823.0, 8.00, 40.00, 'POSTO COMPEÇAS', '2020-09-05 01:14:41'),
(52, 8, 11847.0, 2.00, 10.00, 'TAIOBEIRAS EMPRE. COMERCIAIS LTDA', '2020-09-08 09:31:23'),
(53, 4, 16003.0, 2.00, 10.00, 'POSTO COMPEÇAS', '2020-09-08 16:21:06'),
(54, 8, 11924.0, 2.00, 10.00, 'Mendes Rocha', '2020-09-08 20:01:31'),
(55, 3, 14431.0, 4.00, 20.00, 'POSTO COMPEÇAS', '2020-09-09 13:57:58'),
(56, 9, 122963.0, 15.00, 50.00, 'POSTO COMPEÇAS', '2020-09-09 19:46:53'),
(57, 9, 123171.0, 15.00, 50.00, 'POSTO PATATIVA', '2020-09-11 16:33:20'),
(58, 3, 14553.0, 2.00, 10.00, 'POSTO COMPEÇAS', '2020-09-11 18:05:12'),
(59, 7, 38356.0, 10.00, 50.00, 'POSTO COMPEÇAS', '2020-09-11 19:26:54'),
(60, 9, 123287.0, 6.00, 20.00, 'POSTO COMPEÇAS', '2020-09-11 21:23:17'),
(61, 3, 14561.0, 10.00, 50.00, 'Mendes Rocha', '2020-09-11 21:24:05'),
(62, 2, 64655.0, 10.00, 50.00, 'POSTO PATATIVA', '2020-09-12 00:07:29'),
(63, 4, 16359.0, 6.00, 30.00, 'POSTO COMPEÇAS', '2020-09-12 18:54:45'),
(64, 1, 62554.0, 6.00, 30.00, 'POSTO TAIOBEIRAS', '2020-09-12 18:56:16'),
(65, 5, 61523.0, 10.00, 50.00, 'POSTO TAIOBEIRAS', '2020-09-14 23:23:30'),
(66, 3, 14819.0, 4.00, 20.00, 'POSTO COMPEÇAS', '2020-09-15 20:06:38'),
(67, 9, 123616.0, 15.00, 50.00, 'POSTO COMPEÇAS', '2020-09-16 16:20:07'),
(68, 4, 16565.0, 6.00, 30.00, 'POSTO COMPEÇAS', '2020-09-17 00:01:49'),
(69, 3, 14925.0, 2.00, 10.00, 'POSTO COMPEÇAS', '2020-09-17 13:02:26'),
(70, 9, 123723.0, 15.00, 50.00, 'POSTO COMPEÇAS', '2020-09-17 16:21:13'),
(71, 4, 16624.0, 6.00, 30.00, '', '2020-09-18 17:59:05'),
(72, 4, 16896.0, 9.00, 47.00, 'POSTO COMPEÇAS', '2020-09-18 17:59:47'),
(73, 7, 38729.0, 10.00, 50.00, 'POSTO COMPEÇAS', '2020-09-18 19:47:06'),
(74, 2, 64909.0, 8.00, 40.00, 'POSTO COMPEÇAS', '2020-09-18 20:01:20'),
(75, 5, 61908.0, 10.00, 50.00, 'POSTO TAIOBEIRAS', '2020-09-18 21:27:16'),
(76, 1, 62752.0, 6.00, 30.00, 'POSTO COMPEÇAS', '2020-09-18 21:28:22'),
(77, 3, 14962.0, 10.00, 50.00, 'POSTO COMPEÇAS', '2020-09-18 21:48:44'),
(78, 8, 11988.0, 3.00, 19.00, 'posto patativa', '2020-09-19 12:23:46'),
(79, 9, 123940.0, 9.00, 30.00, 'posto patativa', '2020-09-21 09:14:27'),
(80, 9, 125933.0, 10.00, 50.00, 'posto patativa', '2020-09-21 15:52:38'),
(81, 1, 62911.0, 4.00, 20.00, 'POSTO COMPEÇAS', '2020-09-22 07:57:31'),
(82, 3, 15200.0, 4.00, 20.00, 'POSTO COMPEÇAS', '2020-09-23 14:09:32'),
(83, 9, 124164.0, 15.00, 50.00, 'posto patativa', '2020-09-23 17:02:52'),
(84, 4, 0.0, 4.00, 20.00, 'posto patativa', '2020-09-24 10:14:14'),
(85, 2, 65000.0, 4.00, 20.00, 'POSTO COMPEÇAS', '2020-09-24 17:58:01'),
(86, 3, 15295.0, 8.00, 40.00, 'MENDES ROCHA ', '2020-09-25 15:05:12'),
(87, 9, 124394.0, 10.00, 50.00, 'POSTO COMPEÇAS', '2020-09-25 16:24:43'),
(88, 7, 39078.0, 6.00, 30.00, 'posto patativa', '2020-09-26 08:36:48'),
(89, 5, 62263.0, 4.00, 20.00, 'POSTO COMPEÇAS', '2020-09-26 11:29:41'),
(90, 5, 62224.0, 7.00, 35.00, 'MENDES ROCHA ', '2020-09-26 11:30:25'),
(91, 5, 62443.0, 4.00, 20.00, 'posto patativa', '2020-09-26 12:01:55'),
(92, 3, 15537.0, 2.00, 10.00, '10,00', '2020-09-28 09:54:03'),
(93, 3, 15491.0, 2.00, 10.00, 'POSTO COMPEÇAS', '2020-09-28 09:54:35'),
(94, 9, 124535.0, 10.00, 50.00, 'POSTO COMPEÇAS', '2020-09-28 16:05:22'),
(95, 4, 17298.0, 4.00, 20.00, 'MENDES ROCHA ', '2020-09-30 09:25:46'),
(96, 2, 65372.0, 6.00, 29.00, 'POSTO COMPEÇAS', '2020-09-30 13:31:43'),
(97, 3, 15600.0, 4.00, 20.00, 'POSTO COMPEÇAS', '2020-09-30 14:52:00'),
(98, 1, 63083.0, 4.00, 20.00, 'POSTO COMPEÇAS', '2020-09-30 14:53:07'),
(99, 1, 62246.0, 3.00, 15.00, 'POSTO COMPEÇAS', '2020-09-30 14:54:02'),
(100, 5, 62856.0, 10.00, 50.00, 'MENDES ROCHA ', '2020-09-30 16:21:38'),
(101, 7, 39297.0, 2.00, 10.00, 'posto patativa', '2020-10-01 08:38:26'),
(102, 9, 12584.0, 9.00, 30.00, 'POSTO COMPEÇAS', '2020-10-01 16:22:13'),
(103, 9, 12584.0, 4.00, 20.00, 'POSTO COMPEÇAS', '2020-10-01 16:22:38'),
(104, 7, 39405.0, 8.00, 40.00, 'posto patativa', '2020-10-02 15:01:36'),
(105, 3, 15745.0, 4.00, 20.00, 'POSTO COMPEÇAS', '2020-10-02 17:38:37'),
(106, 3, 15841.0, 8.00, 40.00, 'POSTO COMPEÇAS', '2020-10-02 18:18:14'),
(107, 1, 63374.0, 8.00, 40.00, 'posto patativa', '2020-10-05 17:53:08'),
(108, 3, 16148.0, 2.00, 10.00, 'POSTO COMPEÇAS', '2020-10-07 14:30:03'),
(109, 3, 16052.0, 4.00, 20.00, 'POSTO COMPEÇAS', '2020-10-07 14:30:39'),
(110, 1, 63641.0, 4.00, 20.00, 'posto patativa', '2020-10-07 17:32:12'),
(111, 3, 16203.0, 4.00, 20.00, 'POSTO COMPEÇAS', '2020-10-08 13:52:45'),
(112, 3, 16281.0, 8.00, 40.00, 'POSTO COMPEÇAS', '2020-10-09 14:23:41'),
(113, 5, 0.0, 5.00, 25.00, 'posto patativa', '2020-10-13 11:20:28'),
(114, 3, 16479.0, 10.00, 50.00, 'MENDES ROCHA ', '2020-10-13 13:19:23'),
(115, 2, 0.0, 4.00, 20.00, 'posto patativa', '2020-10-14 15:16:45'),
(116, 3, 16765.0, 4.00, 20.00, 'POSTO COMPEÇAS', '2020-10-16 09:42:08'),
(117, 2, 0.0, 4.00, 20.00, 'posto patativa', '2020-10-16 14:32:00'),
(118, 3, 16924.0, 3.00, 15.00, 'POSTO COMPEÇAS', '2020-10-19 10:48:16'),
(119, 3, 16888.0, 6.00, 30.00, 'POSTO COMPEÇAS', '2020-10-19 10:49:19');

-- --------------------------------------------------------

--
-- Table structure for table `itens_bills_received`
--

CREATE TABLE `itens_bills_received` (
  `id` int(11) NOT NULL,
  `id_bills_received` int(11) NOT NULL,
  `id_night_trip` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `itens_bills_received`
--

INSERT INTO `itens_bills_received` (`id`, `id_bills_received`, `id_night_trip`) VALUES
(1, 1, 22),
(2, 1, 30),
(3, 1, 45),
(4, 1, 54),
(5, 1, 58),
(6, 1, 83),
(7, 1, 88),
(8, 1, 107),
(9, 1, 116),
(10, 1, 124),
(11, 2, 97),
(12, 2, 110),
(13, 3, 29),
(14, 4, 8),
(15, 4, 25),
(16, 4, 31),
(17, 4, 40),
(18, 4, 51),
(19, 4, 52),
(20, 4, 55),
(21, 4, 59),
(22, 4, 60),
(23, 4, 63),
(24, 4, 86),
(25, 4, 101),
(26, 4, 120),
(27, 4, 121),
(28, 5, 4),
(29, 5, 5),
(30, 5, 6),
(31, 5, 7),
(32, 5, 13),
(33, 5, 19),
(34, 5, 32),
(35, 5, 92),
(36, 5, 94),
(37, 5, 111),
(38, 5, 123),
(39, 5, 125),
(40, 6, 23),
(41, 6, 26),
(42, 6, 53),
(43, 6, 56),
(44, 6, 62),
(45, 6, 66),
(46, 6, 78),
(47, 6, 87),
(48, 6, 89),
(49, 6, 106),
(50, 7, 1),
(51, 7, 2),
(52, 7, 3),
(53, 7, 24),
(54, 7, 38),
(55, 7, 39),
(56, 7, 57),
(57, 7, 64),
(58, 7, 85),
(59, 7, 90),
(60, 7, 135),
(61, 8, 17),
(62, 8, 18),
(63, 8, 27),
(64, 8, 46),
(65, 8, 47),
(66, 8, 48),
(67, 8, 49),
(68, 8, 50),
(69, 8, 68),
(70, 8, 77),
(71, 8, 80),
(72, 8, 84),
(73, 8, 95),
(74, 8, 96),
(75, 8, 103),
(76, 8, 112),
(77, 8, 114),
(78, 8, 115),
(79, 8, 118),
(80, 8, 127),
(81, 8, 129),
(82, 9, 148),
(83, 9, 149),
(84, 10, 139),
(85, 10, 140),
(86, 10, 142),
(87, 10, 152),
(88, 10, 153),
(89, 10, 154),
(90, 10, 155),
(91, 10, 156),
(92, 10, 163),
(93, 10, 164),
(94, 10, 165),
(95, 10, 171),
(96, 10, 178),
(97, 10, 180),
(98, 10, 182),
(99, 10, 187),
(100, 10, 193),
(101, 10, 195),
(102, 10, 196),
(103, 10, 204),
(104, 10, 207),
(105, 10, 208),
(106, 12, 61),
(107, 12, 65),
(108, 12, 81),
(109, 12, 98),
(110, 12, 122),
(111, 12, 170),
(112, 12, 184),
(113, 12, 189),
(114, 13, 34),
(115, 13, 71),
(116, 13, 79),
(117, 13, 119),
(118, 13, 130),
(119, 13, 134),
(120, 13, 157),
(121, 13, 173),
(122, 13, 201),
(123, 13, 211),
(124, 13, 215),
(125, 14, 100),
(126, 14, 104),
(127, 14, 161),
(128, 14, 174),
(129, 14, 185),
(130, 15, 12),
(131, 15, 15),
(132, 15, 28),
(133, 15, 41),
(134, 15, 42),
(135, 15, 44),
(136, 15, 67),
(137, 15, 69),
(138, 15, 70),
(139, 15, 74),
(140, 15, 75),
(141, 15, 76),
(142, 15, 102),
(143, 15, 113),
(144, 15, 128),
(145, 15, 188),
(146, 15, 278),
(147, 16, 133),
(148, 16, 146),
(149, 16, 160),
(150, 16, 175),
(151, 16, 186),
(152, 16, 194),
(153, 16, 199),
(154, 16, 213),
(155, 16, 230),
(156, 16, 246),
(157, 16, 262),
(158, 16, 273),
(159, 16, 274),
(160, 16, 305),
(161, 17, 33),
(162, 17, 72),
(163, 17, 73),
(164, 17, 141),
(165, 17, 167),
(166, 17, 222),
(167, 17, 252),
(168, 18, 216),
(169, 18, 217),
(170, 18, 218),
(171, 18, 223),
(172, 18, 224),
(173, 18, 235),
(174, 18, 237),
(175, 18, 239),
(176, 18, 250),
(177, 18, 251),
(178, 18, 253),
(179, 18, 254),
(180, 18, 269),
(181, 18, 288),
(182, 18, 289),
(183, 18, 290),
(184, 18, 293),
(185, 18, 296),
(186, 18, 297),
(187, 18, 299),
(188, 18, 303),
(189, 18, 304),
(190, 19, 132),
(191, 19, 168),
(192, 19, 169),
(193, 19, 181),
(194, 19, 197),
(195, 19, 198),
(196, 19, 203),
(197, 19, 209),
(198, 19, 225),
(199, 19, 227),
(200, 19, 242),
(201, 19, 257),
(202, 19, 279),
(203, 19, 291),
(204, 19, 314),
(205, 20, 11),
(206, 20, 14),
(207, 20, 333),
(208, 21, 131),
(209, 21, 143),
(210, 21, 158),
(211, 21, 166),
(212, 21, 179),
(213, 21, 202),
(214, 21, 205),
(215, 21, 214),
(216, 21, 228),
(217, 21, 285),
(218, 21, 286),
(219, 21, 292),
(220, 21, 328),
(221, 21, 345),
(222, 21, 361),
(223, 22, 192),
(224, 22, 212),
(225, 22, 231),
(226, 22, 276),
(227, 22, 301),
(228, 23, 307),
(229, 23, 312),
(230, 23, 313),
(231, 23, 322),
(232, 23, 324),
(233, 23, 326),
(234, 23, 334),
(235, 23, 336),
(236, 23, 337),
(237, 23, 346),
(238, 23, 347),
(239, 23, 358),
(240, 23, 359),
(241, 23, 369),
(242, 23, 370),
(243, 23, 371),
(244, 23, 373),
(245, 23, 376),
(246, 23, 377),
(247, 23, 384),
(248, 23, 388),
(249, 23, 389),
(250, 23, 391),
(251, 24, 16),
(252, 24, 20),
(253, 24, 35),
(254, 24, 36),
(255, 24, 37),
(256, 24, 82),
(257, 24, 91),
(258, 24, 93),
(259, 24, 99),
(260, 24, 105),
(261, 24, 108),
(262, 24, 109),
(263, 24, 117),
(264, 24, 144),
(265, 24, 145),
(266, 24, 159),
(267, 24, 172),
(268, 24, 200),
(269, 24, 226),
(270, 25, 177),
(271, 25, 183),
(272, 25, 190),
(273, 25, 244),
(274, 26, 147),
(275, 26, 245),
(276, 26, 261),
(277, 26, 275),
(278, 26, 277),
(279, 26, 316),
(280, 26, 332),
(281, 27, 455),
(282, 29, 126),
(283, 29, 136),
(284, 29, 137),
(285, 29, 138),
(286, 29, 150),
(287, 29, 151),
(288, 29, 162),
(289, 29, 176),
(290, 29, 232),
(291, 29, 236),
(292, 29, 247),
(293, 29, 249),
(294, 29, 266),
(295, 29, 306),
(296, 29, 318),
(297, 29, 323),
(298, 29, 325),
(299, 29, 339),
(300, 29, 357),
(301, 29, 385),
(302, 29, 386),
(303, 29, 387),
(304, 29, 400),
(305, 29, 432),
(306, 29, 433),
(307, 29, 438),
(308, 29, 452),
(309, 29, 453),
(310, 29, 454),
(311, 30, 240),
(312, 30, 241),
(313, 30, 255),
(314, 30, 256),
(315, 30, 270),
(316, 30, 283),
(317, 30, 284),
(318, 30, 298),
(319, 30, 300),
(320, 30, 329),
(321, 30, 342),
(322, 30, 348),
(323, 30, 349),
(324, 30, 350),
(325, 30, 378),
(326, 30, 381),
(327, 30, 395),
(328, 30, 420),
(329, 30, 427),
(330, 30, 451),
(331, 32, 472),
(332, 33, 412),
(333, 34, 320),
(334, 34, 335),
(335, 34, 341),
(336, 34, 401),
(337, 34, 413),
(338, 34, 423),
(339, 34, 468),
(340, 35, 402),
(341, 35, 405),
(342, 35, 406),
(343, 35, 411),
(344, 35, 416),
(345, 35, 417),
(346, 35, 422),
(347, 35, 424),
(348, 35, 426),
(349, 35, 443),
(350, 35, 444),
(351, 35, 445),
(352, 35, 446),
(353, 35, 447),
(354, 35, 456),
(355, 35, 457),
(356, 35, 459),
(357, 35, 469),
(358, 35, 471),
(359, 35, 475),
(360, 35, 479),
(361, 36, 483),
(362, 37, 43),
(363, 37, 287),
(364, 37, 294),
(365, 37, 295),
(366, 37, 321),
(367, 37, 338),
(368, 37, 362),
(369, 37, 363),
(370, 37, 372),
(371, 37, 410),
(372, 37, 414),
(373, 37, 440),
(374, 37, 441),
(375, 37, 467),
(376, 37, 492),
(377, 38, 524),
(378, 39, 546),
(379, 40, 480),
(380, 40, 490),
(381, 40, 493),
(382, 40, 495),
(383, 40, 497),
(384, 40, 500),
(385, 40, 503),
(386, 40, 504),
(387, 40, 506),
(388, 40, 510),
(389, 40, 511),
(390, 40, 515),
(391, 40, 516),
(392, 40, 521),
(393, 40, 522),
(394, 40, 539),
(395, 40, 545),
(396, 40, 549),
(397, 40, 550),
(398, 40, 557),
(399, 40, 558),
(400, 41, 233),
(401, 41, 234),
(402, 41, 248),
(403, 41, 264),
(404, 41, 267),
(405, 42, 206),
(406, 42, 238),
(407, 42, 308),
(408, 42, 309),
(409, 42, 340),
(410, 42, 343),
(411, 42, 481),
(412, 42, 541),
(413, 42, 556),
(414, 43, 460),
(415, 43, 464),
(416, 43, 477),
(417, 43, 488),
(418, 43, 501),
(419, 43, 513),
(420, 43, 514),
(421, 43, 535),
(422, 43, 554),
(423, 44, 243),
(424, 44, 260),
(425, 44, 271),
(426, 44, 272),
(427, 44, 353),
(428, 44, 394),
(429, 44, 407),
(430, 44, 418),
(431, 44, 430),
(432, 44, 476),
(433, 44, 496),
(434, 44, 548),
(435, 44, 563),
(436, 45, 482),
(437, 45, 484),
(438, 45, 485),
(439, 45, 494),
(440, 45, 533),
(441, 45, 540),
(442, 45, 543),
(443, 45, 560),
(444, 45, 561),
(445, 45, 562),
(446, 46, 319),
(447, 46, 355),
(448, 46, 383),
(449, 46, 435),
(450, 46, 508),
(451, 46, 512),
(452, 46, 542),
(453, 46, 551),
(454, 47, 191),
(455, 47, 210),
(456, 47, 229),
(457, 47, 258),
(458, 47, 259),
(459, 47, 263),
(460, 47, 265),
(461, 47, 268),
(462, 47, 280),
(463, 47, 281),
(464, 47, 282),
(465, 47, 315),
(466, 47, 330),
(467, 47, 351),
(468, 47, 352),
(469, 47, 356),
(470, 47, 360),
(471, 47, 365),
(472, 47, 366),
(473, 47, 367),
(474, 47, 368),
(475, 47, 379),
(476, 47, 382),
(477, 47, 393),
(478, 47, 408),
(479, 47, 419),
(480, 47, 421),
(481, 47, 428),
(482, 47, 429),
(483, 47, 434),
(484, 47, 449),
(485, 48, 302),
(486, 48, 317),
(487, 48, 331),
(488, 48, 354),
(489, 48, 374),
(490, 48, 380),
(491, 48, 396),
(492, 48, 397),
(493, 48, 398),
(494, 48, 399),
(495, 48, 409),
(496, 48, 431),
(497, 48, 442),
(498, 48, 458),
(499, 48, 470),
(500, 48, 489),
(501, 48, 498),
(502, 48, 526),
(503, 48, 527),
(504, 48, 528),
(505, 49, 531),
(506, 49, 532),
(507, 49, 576),
(508, 49, 603),
(509, 49, 604),
(510, 49, 605),
(511, 50, 611),
(512, 51, 607),
(513, 52, 618),
(514, 52, 619),
(515, 52, 620),
(516, 52, 621),
(517, 52, 622),
(518, 52, 623),
(519, 52, 624),
(520, 53, 310),
(521, 53, 344),
(522, 53, 404),
(523, 53, 425),
(524, 53, 462),
(525, 53, 491),
(526, 53, 507),
(527, 53, 567),
(528, 53, 609),
(529, 54, 559),
(530, 54, 564),
(531, 54, 565),
(532, 54, 574),
(533, 54, 577),
(534, 54, 585),
(535, 54, 597),
(536, 54, 606),
(537, 54, 617),
(538, 54, 626),
(539, 54, 627),
(540, 54, 628),
(541, 54, 629),
(542, 54, 630),
(543, 54, 631),
(544, 54, 632),
(545, 54, 633),
(546, 55, 219),
(547, 55, 220),
(548, 55, 221),
(549, 55, 311),
(550, 55, 436),
(551, 56, 590),
(552, 56, 591),
(553, 56, 596),
(554, 56, 612),
(555, 56, 615),
(556, 56, 625),
(557, 56, 635),
(558, 56, 642),
(559, 56, 645),
(560, 56, 656),
(561, 56, 660),
(562, 56, 673),
(563, 57, 608),
(564, 57, 658),
(565, 57, 674),
(566, 57, 675),
(567, 57, 676),
(568, 57, 677),
(569, 57, 678),
(570, 57, 679),
(571, 57, 680),
(572, 57, 681),
(573, 57, 682),
(574, 57, 683),
(575, 57, 684),
(576, 57, 685),
(577, 58, 644),
(578, 58, 646),
(579, 58, 647),
(580, 58, 648),
(581, 58, 649),
(582, 58, 657),
(583, 58, 667),
(584, 58, 668),
(585, 58, 669),
(586, 58, 670),
(587, 58, 671),
(588, 59, 530),
(589, 59, 566),
(590, 59, 661),
(591, 59, 662),
(592, 59, 663),
(593, 60, 10),
(594, 61, 569),
(595, 61, 581),
(596, 62, 505),
(597, 62, 520),
(598, 62, 523),
(599, 62, 593),
(600, 62, 634),
(601, 63, 327),
(602, 63, 364),
(603, 63, 392),
(604, 63, 448),
(605, 63, 461),
(606, 63, 478),
(607, 63, 487),
(608, 63, 538),
(609, 63, 586),
(610, 63, 600),
(611, 64, 465),
(612, 64, 466),
(613, 64, 473),
(614, 64, 474),
(615, 64, 529),
(616, 64, 544),
(617, 64, 572),
(618, 64, 579),
(619, 64, 595),
(620, 64, 599),
(621, 64, 616),
(622, 64, 637),
(623, 64, 654),
(624, 64, 655),
(625, 64, 672),
(626, 64, 690),
(627, 65, 578),
(628, 65, 650),
(629, 66, 659),
(630, 67, 536),
(631, 67, 537),
(632, 67, 552),
(633, 67, 553),
(634, 67, 568),
(635, 67, 575),
(636, 67, 582),
(637, 67, 583),
(638, 67, 584),
(639, 67, 592),
(640, 67, 594),
(641, 67, 613),
(642, 67, 636),
(643, 67, 643),
(644, 67, 651),
(645, 67, 652),
(646, 67, 664),
(647, 67, 665),
(648, 67, 666),
(649, 67, 686),
(650, 67, 687),
(651, 67, 693),
(652, 67, 694),
(653, 67, 698),
(654, 68, 688),
(655, 68, 689),
(656, 68, 692),
(657, 68, 696),
(658, 68, 697),
(659, 69, 589),
(660, 69, 601),
(661, 69, 639),
(662, 70, 571),
(663, 70, 573),
(664, 70, 641),
(665, 70, 653),
(666, 71, 691),
(667, 71, 695),
(668, 71, 699);

-- --------------------------------------------------------

--
-- Table structure for table `juridical_person`
--

CREATE TABLE `juridical_person` (
  `id` int(11) NOT NULL,
  `id_client` int(11) NOT NULL,
  `cnpj` varchar(18) NOT NULL,
  `legal_name` varchar(200) NOT NULL,
  `trade_name` varchar(200) NOT NULL,
  `answerable` varchar(200) NOT NULL,
  `phone` varchar(200) NOT NULL,
  `phone_fixed` varchar(200) NOT NULL,
  `email` varchar(200) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `juridical_person`
--

INSERT INTO `juridical_person` (`id`, `id_client`, `cnpj`, `legal_name`, `trade_name`, `answerable`, `phone`, `phone_fixed`, `email`) VALUES
(1, 1, '', 'DANIEL FERREIRA COSTA', 'QUINTAL PORTUGUÊS', 'DANIEL', '(38) 99124-9971', '', ''),
(2, 2, '', 'FLAVIA DIAS', 'MASTER DELIVERY', 'FLAVIA', '(38) 99138-5070', '(38) 9150-6421', ''),
(3, 3, '', 'MARIA NIVIA PEREIRA RODRIGUES', 'PIZZA MAIS', 'MARIA NIVIA', '(38) 98828-3570', '', ''),
(4, 7, '', 'SABRINA OLIVEIRA SILVA', 'ROYAL LANCHES', 'SABRINA E ITALO', '(38) 99195-0054', '', ''),
(5, 9, '', 'PEDRO PAULO COSTA', 'VIANA BAR', 'PEDRO', '(38) 99125-8168', '', ''),
(6, 11, '13.226.933/0001-43', 'Rede Albatroz', 'ALBATROZ', 'ASBEL JUNIOR', '(38) 99133-3499', '(38) 3845-2385', ''),
(7, 14, '', 'LUIZ FERREIRA DE ARAUJO', 'ESKINÃO DO PEIXE', 'LUIZ', '(38) 99183-5352', '', ''),
(8, 17, '', 'BW', 'BW ARTESANAIS ', 'Nailde', '(38) 99136-0775', '', ''),
(9, 18, '', 'Ponto do Açaí ', 'Ponto do Açaí Taio', 'Nielly Guimarães Esteves ', '(38) 99197-7965', '', ''),
(10, 19, '', 'SWEET ICE', 'SWEET ICE SORVETERIA ', 'LUIZ OTÁVIO MINI', '(38) 99136-2970', '(38) 9184-7916', ''),
(11, 24, '', 'ERMONE DIAS CORRÊA JUNIOR', 'D\'GUST RESTAURANTE', 'ERMONE DIAS CORRÊA JUNIOR', '(38) 99845-2636', '(38) 9244-9495', ''),
(12, 26, '', 'DELICIAS DA PATY', 'DELICIAS DA PATY', 'PATRICIA SOUZA COSTA', '(38) 99151-5511', '', ''),
(13, 29, '', 'Clube Jujuba moda infantil LTDA', 'CLUBE JUJUBA', 'LAUDIMARA', '(38) 98415-8342', '', ''),
(14, 31, '', 'IRMÃOS ANDRADA LTDA', 'DECK LOUNGE', 'BLENDA ANDRADA OLIVEIRA', '(38) 99192-9269', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `legal_person`
--

CREATE TABLE `legal_person` (
  `id` int(11) NOT NULL,
  `id_client` int(11) NOT NULL,
  `cpf` varchar(100) NOT NULL,
  `name` varchar(100) NOT NULL,
  `surname` varchar(100) NOT NULL,
  `phone` varchar(100) NOT NULL,
  `phone_fixed` varchar(200) NOT NULL,
  `email` varchar(200) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `legal_person`
--

INSERT INTO `legal_person` (`id`, `id_client`, `cpf`, `name`, `surname`, `phone`, `phone_fixed`, `email`) VALUES
(1, 4, '', 'CLIENTE AVULSO', '', '', '', ''),
(2, 5, '271.297.398-43', 'VALDIVINO RODRIGUES SANTOS ', 'VENTOS DO CONTORNO ', '(38) 99221-3093', '', ''),
(3, 6, '077.533.236-44', 'KLEYTON CARLOS ROGRIGUES RIBEIRO (CAMELO)', 'A CASA DO SUCHI', '(38) 99128-6136', '(38) 9998-9089', ''),
(4, 8, '004.918.546-25', 'CELIO FONSECA', 'CREPERIA PREMIUM', '(38) 99270-3531', '', ''),
(5, 10, '', 'CLAUDIO DE OLIVEIRA ', ' BARBOSA', '(38) 99168-4040', '(38) 3845-1095', ''),
(6, 12, '052.001.176-74', 'JOELINA MARIA DE MATOS ', 'RESTAURANTE DA JÓ', '(38) 99191-6566', '', ''),
(7, 13, '', 'Terezinha ', 'Alves Fernandes', '(38) 99188-2465', '', ''),
(8, 15, '', '', '', '', '', ''),
(9, 16, '', 'CARLOS HENRIQUE ', ' BRANT MAGELA (sushi)', '(38) 99154-1771', '', ''),
(10, 20, '138.155.816-05', 'VICTOR FERNANDO MOURA DE FREITAS', 'D e V SALGADOS', '(38) 99247-7884', '', ''),
(11, 21, '107.591.476-04', 'SWEET DONUTS', '(MARIANA OLIVEIRA DE BRITO)', '(38) 99163-6265', '(38) 8842-2321', ''),
(12, 22, '', 'ALAENIO', 'ALVES DA SILVA', '(38) 99910-2707', '', ''),
(13, 23, '068.258.136-41', 'SAMANTHA', 'PANCINI', '(38) 99228-8461', '', ''),
(14, 25, '', 'PATATIVA SUPER PEÇAS', '', '(38) 99128-5168', '', ''),
(15, 27, '', 'BIG LANCHE', 'Nei', '(38) 99121-3029', '', ''),
(16, 28, '', 'EMPORIO VERDE', 'MATEUS PEREIRA SALES', '(38) 99228-7826', '', ''),
(17, 30, '591.964.836-87', 'MR SHAKE', 'JOEL GONÇALVES DA SILVA', '(38) 98825-0000', '(38) 9154-3626', '');

-- --------------------------------------------------------

--
-- Table structure for table `maintenance_control`
--

CREATE TABLE `maintenance_control` (
  `id` int(11) NOT NULL,
  `id_motorcycle` int(11) NOT NULL,
  `odometer` float(10,1) NOT NULL,
  `amount` float(10,2) NOT NULL,
  `description` text NOT NULL,
  `register` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `maintenance_control`
--

INSERT INTO `maintenance_control` (`id`, `id_motorcycle`, `odometer`, `amount`, `description`, `register`) VALUES
(1, 9, 0.0, 350.00, 'MARTELINHO DE OURO - SOLDAS', '2020-08-28 14:45:54'),
(2, 9, 0.0, 300.00, 'HIGIENIZAÇÃO', '2020-08-28 14:46:22'),
(3, 9, 0.0, 120.00, 'alinhamento, balanceamento e cambagem  ', '2020-08-29 08:59:16'),
(4, 9, 0.0, 740.00, 'MAO DE OBRA E PEÇAS - DURÃO SUSPENSÃO', '2020-09-01 16:24:24'),
(5, 1, 15822.0, 0.00, 'TROCA DE OLEO', '2020-09-04 16:57:06'),
(6, 3, 14461.0, 0.00, 'TROCA DE ÓLEO DIA 09/09/2020', '2020-09-09 18:26:19'),
(7, 5, 61557.0, 0.00, 'troca de oleo', '2020-09-12 13:31:55'),
(8, 4, 16619.0, 0.00, 'TROCA DE OLEO 18/09', '2020-09-18 13:20:14'),
(9, 3, 16627.0, 0.00, 'TROCA DE OLEO ', '2020-10-13 13:53:29'),
(10, 9, 0.0, 25.00, 'LAVAGEM', '2020-10-13 14:28:17');

-- --------------------------------------------------------

--
-- Table structure for table `motorcycles`
--

CREATE TABLE `motorcycles` (
  `id` int(11) NOT NULL,
  `brand` varchar(200) NOT NULL,
  `model` varchar(200) NOT NULL,
  `color` varchar(200) NOT NULL,
  `license_plate` varchar(200) NOT NULL,
  `image` varchar(100) NOT NULL,
  `year_motorcycle` varchar(30) NOT NULL,
  `code_motorcycle` varchar(100) NOT NULL,
  `register` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `motorcycles`
--

INSERT INTO `motorcycles` (`id`, `brand`, `model`, `color`, `license_plate`, `image`, `year_motorcycle`, `code_motorcycle`, `register`) VALUES
(1, 'Yamaha', 'YBR125 Factor', 'Vermelha', 'HCP-8506', 'perfil.png', '2008/2009', '04', '2020-08-10 13:34:03'),
(2, 'Honda', 'CG 150 Titan Mix KS', 'Preta', 'HFA-7806', 'perfil.png', '2009/2009', '07', '2020-08-10 13:42:46'),
(3, 'Honda', 'CG 125 FAN KS', 'Preta', 'HME-2094', 'perfil.png', '2010/2010', '03', '2020-08-10 13:48:49'),
(4, 'Yamaha', 'YBR125 Factor K1', 'Preta', 'PWP-2C27', 'perfil.png', '2015/2016', '05', '2020-08-10 13:50:16'),
(5, 'Yamaha', 'YBR125 Factor E', 'Vermelha', 'OPO-6H49', 'perfil.png', '2013/2014', '01', '2020-08-10 14:20:33'),
(6, 'Yamaha', 'YBR125 Factor K', 'Vermelha', 'ASK-9F51', 'perfil.png', '2009/2010', '02', '2020-08-10 14:21:33'),
(7, 'Yamaha', 'YBR125 Factor K', 'Vermelha', 'PVU-9I57', 'perfil.png', '2014/2014', '06', '2020-08-10 14:22:34'),
(8, 'HONDA', 'POP 100', 'PRETA', 'OIK-2324', 'perfil.png', '2012/2012', '08', '2020-08-21 20:10:45'),
(9, 'FIAT', 'FIAT UNO', 'CINZA', 'OQA-4964', 'perfil.png', '2013', '09', '2020-08-28 14:44:55');

-- --------------------------------------------------------

--
-- Table structure for table `night_trips`
--

CREATE TABLE `night_trips` (
  `id` int(11) NOT NULL,
  `id_biker` int(11) NOT NULL,
  `id_motorcycle` int(11) NOT NULL,
  `id_client` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `departure_odometer` float(10,2) NOT NULL,
  `arrival_odometer` float(10,2) NOT NULL,
  `price` float(10,2) NOT NULL,
  `biker_commission` float(10,2) NOT NULL,
  `user_commission` float(10,2) NOT NULL,
  `payment_status` enum('0','1') NOT NULL COMMENT '0 = Nao pago, 1 = pago',
  `date` date NOT NULL,
  `day_receipt` date DEFAULT NULL,
  `user` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `night_trips`
--

INSERT INTO `night_trips` (`id`, `id_biker`, `id_motorcycle`, `id_client`, `quantity`, `departure_odometer`, `arrival_odometer`, `price`, `biker_commission`, `user_commission`, `payment_status`, `date`, `day_receipt`, `user`) VALUES
(1, 1, 1, 5, 17, 60727.00, 60798.00, 3.00, 0.00, 0.00, '1', '2020-08-11', '2020-08-27', 0),
(2, 1, 1, 5, 18, 60798.00, 60867.00, 3.00, 0.00, 0.00, '1', '2020-08-12', '2020-08-27', 0),
(3, 1, 1, 5, 13, 60867.00, 60915.00, 3.00, 0.00, 0.00, '1', '2020-08-13', '2020-08-27', 0),
(4, 4, 2, 11, 2, 63900.00, 63906.00, 3.00, 0.00, 0.00, '1', '2020-08-12', '2020-08-27', 0),
(5, 3, 7, 11, 1, 37084.00, 37088.00, 3.00, 0.00, 0.00, '1', '2020-08-13', '2020-08-27', 0),
(6, 2, 2, 11, 1, 63906.00, 63911.00, 3.00, 0.00, 0.00, '1', '2020-08-13', '2020-08-27', 0),
(7, 3, 7, 11, 1, 37159.00, 37163.00, 3.00, 0.00, 0.00, '1', '2020-08-15', '2020-08-27', 0),
(8, 2, 2, 1, 14, 63966.00, 63996.00, 2.50, 0.00, 0.00, '1', '2020-08-16', '2020-08-27', 0),
(10, 2, 3, 17, 26, 13255.00, 13317.00, 2.50, 0.00, 0.00, '1', '2020-08-17', '2020-10-15', 0),
(11, 4, 5, 12, 1, 60365.00, 60369.00, 3.00, 0.00, 0.00, '1', '2020-08-19', '2020-09-11', 0),
(12, 2, 3, 19, 19, 13348.00, 13399.00, 3.00, 0.00, 0.00, '1', '2020-08-19', '2020-09-08', 0),
(13, 2, 3, 11, 2, 13456.00, 13469.00, 3.00, 0.00, 0.00, '1', '2020-08-20', '2020-08-27', 0),
(14, 3, 7, 12, 1, 37314.00, 37318.00, 3.00, 0.00, 0.00, '1', '2020-08-20', '2020-09-11', 0),
(15, 2, 5, 19, 16, 60374.00, 60421.00, 3.00, 0.00, 0.00, '1', '2020-08-19', '2020-09-08', 0),
(16, 3, 7, 9, 2, 37254.00, 37265.00, 2.50, 0.00, 0.00, '1', '2020-08-19', '2020-09-18', 0),
(17, 3, 3, 18, 7, 37318.00, 37348.00, 3.00, 0.00, 0.00, '1', '2020-08-20', '2020-08-27', 0),
(18, 5, 3, 18, 1, 13476.00, 13478.00, 3.00, 0.00, 0.00, '1', '2020-08-20', '2020-08-27', 0),
(19, 2, 3, 11, 1, 13478.00, 13481.00, 3.00, 0.00, 0.00, '1', '2020-08-20', '2020-08-27', 0),
(20, 3, 7, 9, 6, 37213.00, 37235.00, 2.50, 0.00, 0.00, '1', '2020-08-17', '2020-09-18', 0),
(21, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, '0', '2020-08-17', '0000-00-00', 0),
(22, 4, 5, 3, 1, 60357.00, 60363.00, 3.00, 0.00, 0.00, '1', '2020-08-17', '2020-08-27', 0),
(23, 4, 1, 8, 2, 61115.00, 61123.00, 2.50, 0.00, 0.00, '1', '2020-08-17', '2020-08-27', 0),
(24, 1, 1, 5, 18, 61129.00, 61183.00, 3.00, 0.00, 0.00, '1', '2020-08-18', '2020-08-27', 0),
(25, 5, 4, 1, 14, 14863.00, 14895.00, 2.50, 0.00, 0.00, '1', '2020-08-18', '2020-08-27', 0),
(26, 2, 3, 8, 1, 13340.00, 13345.00, 2.50, 0.00, 0.00, '1', '0000-00-00', '2020-08-27', 0),
(27, 3, 7, 18, 1, 37285.00, 37287.00, 3.00, 0.00, 0.00, '1', '2020-08-19', '2020-08-27', 0),
(28, 4, 3, 19, 6, 13399.00, 13469.00, 3.00, 0.00, 0.00, '1', '2020-08-19', '2020-09-08', 0),
(29, 4, 1, 2, 6, 60698.00, 60725.00, 3.00, 0.00, 0.00, '1', '2020-08-10', '2020-08-27', 0),
(30, 4, 3, 3, 1, 13141.00, 13147.00, 3.00, 0.00, 0.00, '1', '2020-08-13', '2020-08-27', 0),
(31, 5, 4, 1, 15, 14597.00, 14632.00, 2.50, 0.00, 0.00, '1', '2020-08-10', '2020-08-27', 0),
(32, 4, 3, 11, 1, 13495.00, 13499.00, 3.00, 0.00, 0.00, '1', '2020-08-21', '2020-08-27', 0),
(33, 4, 3, 16, 4, 13422.00, 13434.00, 2.50, 0.00, 0.00, '1', '2020-08-19', '2020-09-09', 0),
(34, 2, 5, 20, 3, 60430.00, 60453.00, 3.00, 0.00, 0.00, '1', '2020-08-20', '2020-09-04', 0),
(35, 3, 7, 9, 6, 37287.00, 37314.00, 2.50, 0.00, 0.00, '1', '2020-08-19', '2020-09-18', 0),
(36, 4, 1, 9, 2, 61253.00, 61265.00, 2.50, 0.00, 0.00, '1', '2020-08-20', '2020-09-18', 0),
(37, 6, 7, 9, 2, 37360.00, 37378.00, 2.50, 0.00, 0.00, '1', '2020-08-20', '2020-09-18', 0),
(38, 1, 1, 5, 21, 61187.00, 61253.00, 3.00, 0.00, 0.00, '1', '2020-08-19', '2020-08-27', 0),
(39, 1, 1, 5, 16, 61265.00, 61316.00, 3.00, 0.00, 0.00, '1', '2020-08-20', '2020-08-27', 0),
(40, 5, 4, 1, 26, 14895.00, 14977.00, 2.50, 0.00, 0.00, '1', '2020-08-20', '2020-08-27', 0),
(41, 4, 3, 19, 11, 13439.00, 13455.00, 3.00, 0.00, 0.00, '1', '2020-08-19', '2020-09-08', 0),
(42, 4, 3, 19, 2, 13469.00, 13476.00, 3.00, 0.00, 0.00, '1', '2020-08-20', '2020-09-08', 0),
(43, 3, 7, 19, 1, 37348.00, 37354.00, 3.00, 0.00, 0.00, '1', '2020-08-20', '2020-09-25', 0),
(44, 2, 5, 19, 3, 60434.00, 60453.00, 3.00, 0.00, 0.00, '1', '2020-08-20', '2020-09-08', 0),
(45, 3, 7, 3, 1, 37298.00, 37304.00, 3.00, 0.00, 0.00, '1', '2020-08-19', '2020-08-27', 0),
(46, 2, 5, 18, 4, 60369.00, 60421.00, 3.00, 0.00, 0.00, '1', '2020-08-19', '2020-08-27', 0),
(47, 4, 3, 18, 2, 13434.00, 13455.00, 3.00, 0.00, 0.00, '1', '2020-08-19', '2020-08-27', 0),
(48, 2, 5, 18, 2, 60421.00, 60453.00, 3.00, 0.00, 0.00, '1', '2020-08-20', '2020-08-27', 0),
(49, 2, 5, 18, 1, 60454.00, 60459.00, 3.00, 0.00, 0.00, '1', '2020-08-21', '2020-08-27', 0),
(50, 4, 3, 18, 5, 13484.00, 13495.00, 3.00, 0.00, 0.00, '1', '2020-08-20', '2020-08-27', 0),
(51, 5, 4, 1, 26, 14641.00, 14692.00, 2.50, 0.00, 0.00, '1', '2020-08-11', '2020-08-27', 0),
(52, 5, 4, 1, 21, 14692.00, 14727.00, 2.50, 0.00, 0.00, '1', '2020-08-12', '2020-08-27', 0),
(53, 3, 7, 8, 5, 37058.00, 37077.00, 2.50, 0.00, 0.00, '1', '2020-08-12', '2020-08-27', 0),
(54, 4, 3, 3, 1, 13128.00, 13138.00, 3.00, 0.00, 0.00, '1', '2020-08-12', '2020-08-27', 0),
(55, 5, 7, 1, 16, 37088.00, 37127.00, 2.50, 0.00, 0.00, '1', '2020-08-13', '2020-08-27', 0),
(56, 3, 2, 8, 4, 63911.00, 63935.00, 2.50, 0.00, 0.00, '1', '2020-08-13', '2020-08-27', 0),
(57, 1, 1, 5, 25, 60915.00, 60985.00, 3.00, 0.00, 0.00, '1', '2020-08-14', '2020-08-27', 0),
(58, 4, 3, 3, 2, 13153.00, 13163.00, 3.00, 0.00, 0.00, '1', '2020-08-14', '2020-08-27', 0),
(59, 5, 4, 1, 13, 14728.00, 14769.00, 2.50, 0.00, 0.00, '1', '2020-08-14', '2020-08-27', 0),
(60, 2, 2, 1, 12, 63936.00, 63966.00, 2.50, 0.00, 0.00, '1', '2020-08-14', '2020-08-27', 0),
(61, 3, 7, 14, 11, 37127.00, 37159.00, 3.00, 0.00, 0.00, '1', '2020-08-14', '2020-09-04', 0),
(62, 4, 3, 8, 1, 13163.00, 13167.00, 2.50, 0.00, 0.00, '1', '2020-08-14', '2020-08-27', 0),
(63, 5, 4, 1, 26, 14769.00, 14829.00, 2.50, 0.00, 0.00, '1', '2020-08-15', '2020-08-27', 0),
(64, 1, 1, 5, 23, 60985.00, 61054.00, 3.00, 0.00, 0.00, '1', '2020-08-15', '2020-08-27', 0),
(65, 3, 7, 14, 18, 37164.00, 37207.00, 3.00, 0.00, 0.00, '1', '2020-08-15', '2020-09-04', 0),
(66, 4, 3, 8, 3, 13172.00, 13181.00, 2.50, 0.00, 0.00, '1', '2020-08-15', '2020-08-27', 0),
(67, 2, 5, 19, 18, 60459.00, 60522.00, 3.00, 0.00, 0.00, '1', '2020-08-21', '2020-09-08', 0),
(68, 4, 3, 18, 8, 13499.00, 13518.00, 3.00, 0.00, 0.00, '1', '2020-08-21', '2020-08-27', 0),
(69, 3, 4, 19, 2, 14997.00, 14984.00, 3.00, 0.00, 0.00, '1', '2020-08-21', '2020-09-08', 0),
(70, 3, 7, 19, 3, 37378.00, 37393.00, 3.00, 0.00, 0.00, '1', '2020-08-21', '2020-09-08', 0),
(71, 6, 7, 20, 1, 37364.00, 37376.00, 3.00, 0.00, 0.00, '1', '2020-08-20', '2020-09-04', 0),
(72, 6, 8, 16, 10, 11607.00, 11623.00, 2.50, 0.00, 0.00, '1', '2020-08-21', '2020-09-09', 0),
(73, 2, 5, 16, 9, 60522.00, 60550.00, 2.50, 0.00, 0.00, '1', '2020-08-21', '2020-09-09', 0),
(74, 4, 3, 19, 3, 13528.00, 13575.00, 3.00, 0.00, 0.00, '1', '2020-08-21', '2020-09-08', 0),
(75, 2, 5, 19, 4, 60550.00, 60573.00, 3.00, 0.00, 0.00, '1', '2020-08-21', '2020-09-08', 0),
(76, 6, 8, 19, 2, 11631.00, 11637.00, 3.00, 0.00, 0.00, '1', '2020-08-21', '2020-09-08', 0),
(77, 2, 5, 18, 4, 60550.00, 60573.00, 3.00, 0.00, 0.00, '1', '2020-08-21', '2020-08-27', 0),
(78, 4, 3, 8, 3, 13532.00, 13575.00, 2.50, 0.00, 0.00, '1', '2020-08-21', '2020-08-27', 0),
(79, 4, 3, 20, 11, 13525.00, 13575.00, 3.00, 0.00, 0.00, '1', '2020-08-21', '2020-09-04', 0),
(80, 3, 7, 18, 1, 37412.00, 37439.00, 3.00, 0.00, 0.00, '1', '2020-08-21', '2020-08-27', 0),
(81, 3, 7, 14, 13, 37393.00, 37439.00, 3.00, 0.00, 0.00, '1', '2020-08-21', '2020-09-04', 0),
(82, 6, 8, 9, 2, 11623.00, 11637.00, 2.50, 0.00, 0.00, '1', '2020-08-21', '2020-09-18', 0),
(83, 1, 1, 3, 1, 61335.00, 61340.00, 3.00, 0.00, 0.00, '1', '2020-08-21', '2020-08-27', 0),
(84, 1, 1, 18, 1, 61327.00, 61330.00, 3.00, 0.00, 0.00, '1', '2020-08-21', '2020-08-27', 0),
(85, 1, 1, 5, 21, 61316.00, 61398.00, 3.00, 0.00, 0.00, '1', '2020-08-21', '2020-08-27', 0),
(86, 5, 4, 1, 15, 0.00, 0.00, 2.50, 0.00, 0.00, '1', '2020-08-21', '2020-08-27', 0),
(87, 4, 3, 8, 2, 13187.00, 13190.00, 2.50, 0.00, 0.00, '1', '2020-08-15', '2020-08-27', 0),
(88, 4, 3, 3, 1, 13181.00, 13187.00, 3.00, 0.00, 0.00, '1', '2020-08-15', '2020-08-27', 0),
(89, 4, 3, 8, 5, 13190.00, 13222.00, 2.50, 0.00, 0.00, '1', '2020-08-16', '2020-08-27', 0),
(90, 1, 1, 5, 18, 61055.00, 61113.00, 3.00, 0.00, 0.00, '1', '2020-08-16', '2020-08-27', 0),
(91, 4, 3, 9, 2, 13225.00, 13232.00, 2.50, 0.00, 0.00, '1', '2020-08-17', '2020-09-18', 0),
(92, 0, 0, 11, 3, 37446.00, 37456.00, 3.00, 0.00, 0.00, '1', '2020-08-21', '2020-08-27', 0),
(93, 3, 7, 9, 2, 37439.00, 37446.00, 2.50, 0.00, 0.00, '1', '2020-08-21', '2020-09-18', 0),
(94, 3, 7, 11, 3, 37446.00, 37456.00, 3.00, 0.00, 0.00, '1', '2020-08-22', '2020-08-27', 0),
(95, 1, 1, 18, 13, 61398.00, 61440.00, 3.00, 0.00, 0.00, '1', '2020-08-22', '2020-08-27', 0),
(96, 3, 7, 18, 8, 37456.00, 37489.00, 3.00, 0.00, 0.00, '1', '2020-08-22', '2020-08-27', 0),
(97, 4, 3, 10, 3, 13585.00, 13599.00, 3.00, 0.00, 0.00, '1', '2020-08-22', '2020-08-27', 0),
(98, 6, 8, 14, 19, 11637.00, 11679.00, 3.00, 0.00, 0.00, '1', '2020-08-22', '2020-09-04', 0),
(99, 4, 3, 9, 4, 13575.00, 13604.00, 2.50, 0.00, 0.00, '1', '2020-08-22', '2020-09-18', 0),
(100, 1, 1, 5, 21, 61442.00, 61515.00, 3.00, 0.00, 0.00, '1', '2020-08-22', '2020-09-04', 0),
(101, 5, 4, 1, 28, 15025.00, 15075.00, 2.50, 0.00, 0.00, '1', '2020-08-22', '2020-08-27', 0),
(102, 5, 5, 19, 17, 60580.00, 60597.00, 3.00, 0.00, 0.00, '1', '2020-08-23', '2020-09-08', 0),
(103, 1, 1, 18, 15, 61515.00, 61558.00, 3.00, 0.00, 0.00, '1', '2020-08-23', '2020-08-27', 0),
(104, 1, 1, 5, 19, 61558.00, 61630.00, 3.00, 0.00, 0.00, '1', '2020-08-23', '2020-09-04', 0),
(105, 4, 3, 9, 11, 13604.00, 13651.00, 2.50, 0.00, 0.00, '1', '2020-08-23', '2020-09-18', 0),
(106, 4, 3, 8, 2, 13651.00, 13658.00, 2.50, 0.00, 0.00, '1', '2020-08-23', '2020-08-27', 0),
(107, 4, 3, 3, 1, 13658.00, 13661.00, 3.00, 0.00, 0.00, '1', '2020-04-23', '2020-08-27', 0),
(108, 3, 7, 9, 6, 37235.00, 37241.00, 2.50, 0.00, 0.00, '1', '2020-08-18', '2020-09-18', 0),
(109, 3, 7, 9, 2, 37241.00, 37254.00, 2.50, 0.00, 0.00, '1', '2020-08-18', '2020-09-18', 0),
(110, 2, 8, 10, 3, 11695.00, 11722.00, 3.00, 0.00, 0.00, '1', '2020-08-23', '2020-08-27', 0),
(111, 3, 7, 11, 1, 37522.00, 37526.00, 3.00, 0.00, 0.00, '1', '2020-08-24', '2020-08-27', 0),
(112, 2, 8, 18, 13, 11679.00, 11722.00, 3.00, 0.00, 0.00, '1', '2020-08-23', '2020-08-27', 0),
(113, 6, 4, 19, 19, 15148.00, 15218.00, 3.00, 0.00, 0.00, '1', '2020-08-23', '2020-09-08', 0),
(114, 2, 8, 18, 4, 11725.00, 11738.00, 3.00, 0.00, 0.00, '1', '2020-08-24', '2020-08-27', 0),
(115, 4, 3, 18, 3, 13664.00, 13668.00, 3.00, 0.00, 0.00, '1', '2020-08-24', '2020-08-27', 0),
(116, 6, 7, 3, 1, 13668.00, 13702.00, 3.00, 0.00, 0.00, '1', '2020-08-24', '2020-08-27', 0),
(117, 3, 7, 9, 5, 37526.00, 37556.00, 2.50, 0.00, 0.00, '1', '2020-08-24', '2020-09-18', 0),
(118, 2, 5, 18, 2, 60600.00, 60608.00, 3.00, 0.00, 0.00, '1', '2020-08-24', '2020-08-27', 0),
(119, 6, 3, 20, 10, 13677.00, 13702.00, 3.00, 0.00, 0.00, '1', '2020-08-24', '2020-09-04', 0),
(120, 5, 4, 1, 18, 15225.00, 15264.00, 2.50, 0.00, 0.00, '1', '2020-08-24', '2020-08-27', 0),
(121, 2, 4, 1, 14, 15075.00, 15148.00, 2.50, 0.00, 0.00, '1', '2020-08-23', '2020-08-27', 0),
(122, 3, 7, 14, 12, 37491.00, 37519.00, 3.00, 0.00, 0.00, '1', '2020-08-23', '2020-09-04', 0),
(123, 3, 7, 11, 1, 37556.00, 37559.00, 3.00, 0.00, 0.00, '1', '2020-08-25', '2020-08-27', 0),
(124, 6, 7, 3, 2, 37354.00, 37376.00, 3.00, 0.00, 0.00, '1', '2020-08-20', '2020-08-27', 0),
(125, 3, 7, 11, 1, 37560.00, 37563.00, 3.00, 0.00, 0.00, '1', '2020-08-25', '2020-08-27', 0),
(126, 4, 3, 11, 3, 13702.00, 13717.00, 3.00, 0.00, 0.00, '1', '2020-08-25', '2020-09-21', 0),
(127, 2, 5, 18, 10, 60608.00, 60630.00, 3.00, 0.00, 0.00, '1', '2020-08-25', '2020-08-27', 0),
(128, 2, 5, 19, 4, 60630.00, 60637.00, 3.00, 0.00, 0.00, '1', '2020-08-25', '2020-09-08', 0),
(129, 2, 5, 18, 4, 60642.00, 60662.00, 3.00, 0.00, 0.00, '1', '2020-08-25', '2020-08-27', 0),
(130, 6, 7, 20, 3, 37565.00, 37588.00, 3.00, 0.00, 0.00, '1', '2020-08-25', '2020-09-04', 0),
(131, 4, 3, 3, 4, 13724.00, 13755.00, 3.00, 0.00, 0.00, '1', '2020-08-25', '2020-09-14', 0),
(132, 4, 3, 8, 3, 13724.00, 13755.00, 2.50, 0.00, 0.00, '1', '2020-08-25', '2020-09-10', 0),
(133, 5, 4, 1, 11, 15264.00, 15293.00, 2.50, 0.00, 0.00, '1', '2020-08-25', '2020-09-09', 0),
(134, 4, 3, 20, 1, 13755.00, 13758.00, 3.00, 0.00, 0.00, '1', '2020-08-25', '2020-09-04', 0),
(135, 1, 1, 5, 10, 61630.00, 61685.00, 3.00, 0.00, 0.00, '1', '2020-08-25', '2020-08-27', 0),
(136, 2, 5, 11, 1, 60662.00, 60664.00, 3.00, 0.00, 0.00, '1', '2020-08-26', '2020-09-21', 0),
(137, 3, 7, 11, 2, 37588.00, 37597.00, 3.00, 0.00, 0.00, '1', '2020-08-26', '2020-09-21', 0),
(138, 2, 5, 11, 1, 60664.00, 60670.00, 3.00, 0.00, 0.00, '1', '2020-08-26', '2020-09-21', 0),
(139, 4, 3, 18, 6, 13758.00, 13774.00, 3.00, 0.00, 0.00, '1', '2020-08-26', '2020-09-02', 0),
(140, 3, 7, 18, 5, 37598.00, 37618.00, 3.00, 0.00, 0.00, '1', '2020-08-26', '2020-09-02', 0),
(141, 2, 5, 16, 5, 60670.00, 60690.00, 2.50, 0.00, 0.00, '1', '2020-08-26', '2020-09-09', 0),
(142, 3, 7, 18, 7, 37618.00, 37640.00, 3.00, 0.00, 0.00, '1', '2020-08-26', '2020-09-02', 0),
(143, 4, 3, 3, 1, 13774.00, 13779.00, 3.00, 0.00, 0.00, '1', '2020-08-26', '2020-09-14', 0),
(144, 6, 7, 9, 5, 37569.00, 37588.00, 2.50, 0.00, 0.00, '1', '2020-08-25', '2020-09-18', 0),
(145, 6, 8, 9, 1, 11748.00, 11759.00, 2.50, 0.00, 0.00, '1', '2020-08-26', '2020-09-18', 0),
(146, 5, 4, 1, 17, 15293.00, 15320.00, 2.50, 0.00, 0.00, '1', '2020-08-26', '2020-09-09', 0),
(147, 1, 1, 5, 12, 61685.00, 61735.00, 3.00, 0.00, 0.00, '1', '2020-08-26', '2020-09-18', 0),
(148, 5, 4, 1, 16, 14895.00, 14929.00, 2.50, 0.00, 0.00, '1', '2020-08-19', '2020-08-27', 5),
(149, 5, 4, 1, 19, 14831.00, 14863.00, 2.50, 0.00, 0.00, '1', '2020-08-17', '2020-08-27', 5),
(150, 3, 7, 11, 1, 13640.00, 13644.00, 3.00, 0.00, 0.00, '1', '2020-08-26', '2020-09-21', 5),
(151, 4, 5, 11, 1, 60690.00, 60694.00, 3.00, 0.00, 0.00, '1', '2020-08-27', '2020-09-21', 5),
(152, 4, 3, 18, 7, 13782.00, 13805.00, 3.00, 0.00, 0.00, '1', '2020-08-27', '2020-09-02', 5),
(153, 4, 3, 18, 5, 13805.00, 13818.00, 3.00, 0.00, 0.00, '1', '2020-08-27', '2020-09-02', 5),
(154, 3, 7, 18, 1, 37646.00, 37648.00, 3.00, 0.00, 0.00, '1', '2020-08-27', '2020-09-02', 5),
(155, 2, 5, 18, 7, 60694.00, 60733.00, 3.00, 0.00, 0.00, '1', '2020-08-27', '2020-09-02', 3),
(156, 2, 5, 18, 2, 60733.00, 60738.00, 3.00, 0.00, 0.00, '1', '2020-08-27', '2020-09-02', 3),
(157, 4, 3, 20, 6, 13818.00, 13836.00, 3.00, 0.00, 0.00, '1', '2020-08-27', '2020-09-04', 3),
(158, 3, 7, 3, 1, 37656.00, 37668.00, 3.00, 0.00, 0.00, '1', '2020-08-27', '2020-09-14', 3),
(159, 3, 7, 9, 5, 37648.00, 37668.00, 2.50, 0.00, 0.00, '1', '2020-08-27', '2020-09-18', 3),
(160, 5, 4, 1, 22, 15320.00, 15358.00, 2.50, 0.00, 0.00, '1', '2020-08-27', '2020-09-09', 3),
(161, 1, 1, 5, 17, 61737.00, 61797.00, 3.00, 0.00, 0.00, '1', '2020-08-27', '2020-09-04', 3),
(162, 4, 1, 11, 1, 61797.00, 61800.00, 3.00, 0.00, 0.00, '1', '2020-08-28', '2020-09-21', 5),
(163, 3, 7, 18, 3, 37668.00, 37676.00, 3.00, 0.00, 0.00, '1', '2020-08-28', '2020-09-02', 3),
(164, 2, 2, 18, 3, 64040.00, 64050.00, 3.00, 0.00, 0.00, '1', '2020-08-28', '2020-09-02', 3),
(165, 4, 3, 18, 1, 13836.00, 13841.00, 3.00, 0.00, 0.00, '1', '2020-08-28', '2020-09-02', 3),
(166, 3, 7, 3, 2, 37676.00, 37685.00, 3.00, 0.00, 0.00, '1', '2020-08-28', '2020-09-14', 3),
(167, 2, 5, 16, 14, 60738.00, 60790.00, 2.50, 0.00, 0.00, '1', '2020-08-28', '2020-09-09', 3),
(168, 3, 7, 8, 1, 37685.00, 37710.00, 2.50, 0.00, 0.00, '1', '2020-08-28', '2020-09-10', 3),
(169, 4, 3, 8, 1, 13841.00, 13890.00, 2.50, 0.00, 0.00, '1', '2020-08-28', '2020-09-10', 3),
(170, 6, 2, 14, 18, 64050.00, 64099.00, 3.00, 0.00, 0.00, '1', '2020-08-28', '2020-09-04', 3),
(171, 4, 3, 18, 9, 13841.00, 13890.00, 3.00, 0.00, 0.00, '1', '2020-08-28', '2020-09-02', 3),
(172, 3, 7, 9, 6, 37689.00, 37710.00, 2.50, 0.00, 0.00, '1', '2020-08-28', '2020-09-18', 3),
(173, 4, 3, 20, 6, 13841.00, 13890.00, 3.00, 0.00, 0.00, '1', '2020-08-28', '2020-09-04', 3),
(174, 1, 1, 5, 22, 61799.00, 61864.00, 3.00, 0.00, 0.00, '1', '2020-08-28', '2020-09-04', 3),
(175, 5, 4, 1, 32, 15359.00, 15413.00, 2.50, 0.00, 0.00, '1', '2020-08-28', '2020-09-09', 3),
(176, 2, 5, 11, 1, 60804.00, 60810.00, 3.00, 0.00, 0.00, '1', '2020-08-29', '2020-09-21', 5),
(177, 2, 5, 9, 5, 60813.00, 60834.00, 2.50, 0.00, 0.00, '1', '2020-08-28', '2020-09-18', 5),
(178, 3, 7, 18, 12, 37700.00, 37770.00, 3.00, 0.00, 0.00, '1', '2020-08-29', '2020-09-02', 3),
(179, 3, 7, 3, 2, 37782.00, 37801.00, 3.00, 0.00, 0.00, '1', '2020-08-29', '2020-09-14', 3),
(180, 3, 7, 18, 3, 37770.00, 37801.00, 3.00, 0.00, 0.00, '1', '2020-08-29', '2020-09-02', 3),
(181, 4, 3, 8, 1, 13915.00, 13947.00, 2.50, 0.00, 0.00, '1', '2020-08-29', '2020-09-10', 3),
(182, 4, 3, 18, 4, 13910.00, 13947.00, 3.00, 0.00, 0.00, '1', '2020-08-29', '2020-09-02', 3),
(183, 4, 3, 9, 4, 13919.00, 13947.00, 2.50, 0.00, 0.00, '1', '2020-08-29', '2020-09-18', 3),
(184, 6, 2, 14, 26, 64119.00, 64184.00, 3.00, 0.00, 0.00, '1', '2020-08-29', '2020-09-04', 3),
(185, 1, 1, 5, 22, 61884.00, 61963.00, 3.00, 0.00, 0.00, '1', '2020-08-29', '2020-09-04', 3),
(186, 5, 4, 1, 28, 15436.00, 15485.00, 2.50, 0.00, 0.00, '1', '2020-08-29', '2020-09-09', 3),
(187, 1, 1, 18, 22, 61963.00, 62006.00, 3.00, 0.00, 0.00, '1', '2020-08-30', '2020-09-02', 5),
(188, 4, 3, 19, 21, 13947.00, 14006.00, 3.00, 0.00, 0.00, '1', '2020-08-30', '2020-09-08', 5),
(189, 6, 2, 14, 19, 64184.00, 64243.00, 3.00, 0.00, 0.00, '1', '2000-08-30', '2020-09-04', 5),
(190, 3, 7, 9, 10, 37801.00, 37842.00, 2.50, 0.00, 0.00, '1', '2020-08-30', '2020-09-18', 5),
(191, 3, 7, 9, 3, 37801.00, 37842.00, 2.50, 0.00, 0.00, '1', '2020-08-30', '2020-10-03', 5),
(192, 1, 4, 5, 22, 15485.00, 15573.00, 3.00, 0.00, 0.00, '1', '2020-08-30', '2020-09-15', 5),
(193, 2, 5, 18, 38, 60860.00, 60957.00, 3.00, 0.00, 0.00, '1', '2020-08-30', '2020-09-02', 5),
(194, 2, 5, 1, 16, 60834.00, 60860.00, 2.50, 0.00, 0.00, '1', '2020-08-30', '2020-09-09', 5),
(195, 4, 3, 18, 18, 14006.00, 14046.00, 3.00, 0.00, 0.00, '1', '2020-08-31', '2020-09-02', 5),
(196, 3, 7, 18, 12, 37847.00, 37883.00, 3.00, 0.00, 0.00, '1', '2020-08-31', '2020-09-02', 3),
(197, 6, 2, 8, 1, 64243.00, 64271.00, 2.50, 0.00, 0.00, '1', '2020-08-31', '2020-09-10', 3),
(198, 2, 5, 8, 1, 61010.00, 61014.00, 2.50, 0.00, 0.00, '1', '2020-08-31', '2020-09-10', 3),
(199, 5, 4, 1, 18, 15573.00, 15623.00, 2.50, 0.00, 0.00, '1', '2020-08-31', '2020-09-09', 3),
(200, 6, 2, 9, 6, 64251.00, 64271.00, 2.50, 0.00, 0.00, '1', '2020-08-31', '2020-09-18', 3),
(201, 2, 5, 20, 4, 60990.00, 61014.00, 3.00, 0.00, 0.00, '1', '2020-08-31', '2020-09-04', 3),
(202, 2, 5, 3, 1, 61014.00, 61020.00, 3.00, 0.00, 0.00, '1', '2020-08-31', '2020-09-14', 3),
(203, 3, 7, 8, 1, 37883.00, 37888.00, 2.50, 0.00, 0.00, '1', '2020-09-01', '2020-09-10', 5),
(204, 2, 5, 18, 4, 61020.00, 61034.00, 3.00, 0.00, 0.00, '1', '2020-09-01', '2020-09-02', 5),
(205, 2, 5, 3, 3, 61034.00, 61092.00, 3.00, 0.00, 0.00, '1', '2020-09-01', '2020-09-14', 5),
(206, 4, 2, 21, 4, 64271.00, 64283.00, 3.00, 0.00, 0.00, '1', '2020-09-01', '2020-10-01', 5),
(207, 4, 2, 18, 3, 64283.00, 64287.00, 3.00, 0.00, 0.00, '1', '2020-09-01', '2020-09-02', 3),
(208, 2, 5, 18, 3, 61047.00, 61055.00, 3.00, 0.00, 0.00, '1', '2020-09-01', '2020-09-02', 3),
(209, 4, 3, 8, 1, 14063.00, 14069.00, 2.50, 0.00, 0.00, '1', '2020-09-01', '2020-09-10', 3),
(210, 4, 3, 9, 6, 14046.00, 14069.00, 2.50, 0.00, 0.00, '1', '2020-09-01', '2020-10-03', 3),
(211, 4, 3, 20, 1, 14065.00, 14069.00, 3.00, 0.00, 0.00, '1', '2020-09-01', '2020-09-04', 3),
(212, 1, 1, 5, 16, 62006.00, 62065.00, 3.00, 0.00, 0.00, '1', '2020-09-01', '2020-09-15', 3),
(213, 5, 4, 1, 18, 15623.00, 15657.00, 2.50, 0.00, 0.00, '1', '2020-09-01', '2020-09-09', 3),
(214, 6, 2, 3, 1, 64297.00, 64311.00, 3.00, 0.00, 0.00, '1', '2020-09-01', '2020-09-14', 3),
(215, 6, 2, 20, 2, 64291.00, 64311.00, 3.00, 0.00, 0.00, '1', '2020-09-01', '2020-09-04', 3),
(216, 3, 7, 18, 4, 37888.00, 37902.00, 3.00, 0.00, 0.00, '1', '2020-09-02', '2020-09-09', 5),
(217, 2, 5, 18, 1, 61055.00, 61060.00, 3.00, 0.00, 0.00, '1', '2020-09-02', '2020-09-09', 5),
(218, 2, 5, 18, 3, 61062.00, 61074.00, 3.00, 0.00, 0.00, '1', '2020-09-02', '2020-09-09', 3),
(219, 3, 7, 23, 3, 37914.00, 37932.00, 2.50, 0.00, 0.00, '1', '2020-09-02', '2020-10-13', 3),
(220, 6, 2, 23, 7, 64311.00, 64330.00, 2.50, 0.00, 0.00, '1', '2020-09-02', '2020-10-13', 3),
(221, 4, 3, 23, 8, 14080.00, 14097.00, 2.50, 0.00, 0.00, '1', '2020-09-02', '2020-10-13', 3),
(222, 2, 5, 16, 12, 61074.00, 61099.00, 2.50, 0.00, 0.00, '1', '2020-09-02', '2020-09-09', 3),
(223, 3, 7, 18, 5, 37905.00, 37940.00, 3.00, 0.00, 0.00, '1', '2020-09-02', '2020-09-09', 3),
(224, 4, 3, 18, 1, 14080.00, 14097.00, 3.00, 0.00, 0.00, '1', '2020-09-02', '2020-09-09', 3),
(225, 3, 7, 8, 2, 37914.00, 37940.00, 2.50, 0.00, 0.00, '1', '2020-09-02', '2020-09-10', 3),
(226, 3, 7, 9, 2, 37914.00, 37940.00, 2.50, 0.00, 0.00, '1', '2020-09-02', '2020-09-18', 3),
(227, 4, 3, 8, 1, 14097.00, 14107.00, 2.50, 0.00, 0.00, '1', '2020-09-02', '2020-09-10', 3),
(228, 4, 3, 3, 2, 14097.00, 14107.00, 3.00, 0.00, 0.00, '1', '2020-09-02', '2020-09-14', 3),
(229, 6, 2, 9, 2, 64333.00, 64338.00, 2.50, 0.00, 0.00, '1', '2020-09-02', '2020-10-03', 3),
(230, 5, 4, 1, 20, 15660.00, 15696.00, 2.50, 0.00, 0.00, '1', '2020-09-02', '2020-09-09', 3),
(231, 1, 1, 5, 25, 62065.00, 62137.00, 3.00, 0.00, 0.00, '1', '2020-09-02', '2020-09-15', 3),
(232, 2, 5, 11, 1, 61105.00, 61107.00, 3.00, 0.00, 0.00, '1', '2020-09-03', '2020-09-21', 5),
(233, 2, 5, 24, 2, 61107.00, 61112.00, 2.50, 0.00, 0.00, '1', '2020-09-03', '2020-10-01', 5),
(234, 3, 5, 24, 2, 61112.00, 61117.00, 2.50, 0.00, 0.00, '1', '2020-09-03', '2020-10-01', 5),
(235, 3, 5, 18, 5, 61117.00, 61113.00, 3.00, 0.00, 0.00, '1', '2020-09-03', '2020-09-09', 5),
(236, 3, 5, 11, 1, 61136.00, 61139.00, 3.00, 0.00, 0.00, '1', '2020-09-03', '2020-09-21', 5),
(237, 4, 3, 18, 1, 14122.00, 14124.00, 3.00, 0.00, 0.00, '1', '2020-09-03', '2020-09-09', 3),
(238, 4, 3, 21, 6, 14107.00, 14124.00, 3.00, 0.00, 0.00, '1', '2020-09-03', '2020-10-01', 3),
(239, 2, 5, 18, 4, 61139.00, 61160.00, 3.00, 0.00, 0.00, '1', '2020-09-03', '2020-09-09', 3),
(240, 2, 5, 20, 1, 61146.00, 61160.00, 3.00, 0.00, 0.00, '1', '2020-09-03', '2020-09-21', 3),
(241, 3, 7, 20, 6, 37940.00, 37963.00, 3.00, 0.00, 0.00, '1', '2020-09-03', '2020-09-21', 3),
(242, 4, 3, 8, 1, 14124.00, 14131.00, 2.50, 0.00, 0.00, '1', '2020-09-03', '2020-09-10', 3),
(243, 6, 2, 14, 9, 64338.00, 64370.00, 3.00, 0.00, 0.00, '1', '2020-09-03', '2020-10-01', 3),
(244, 4, 3, 9, 1, 14128.00, 14131.00, 2.50, 0.00, 0.00, '1', '2020-09-03', '2020-09-18', 3),
(245, 1, 1, 5, 16, 62137.00, 62200.00, 3.00, 0.00, 0.00, '1', '2020-09-03', '2020-09-18', 3),
(246, 5, 4, 1, 15, 15699.00, 15731.00, 2.50, 0.00, 0.00, '1', '2020-09-03', '2020-09-09', 3),
(247, 3, 7, 11, 1, 37963.00, 37968.00, 3.00, 0.00, 0.00, '1', '2020-08-04', '2020-09-21', 5),
(248, 3, 7, 24, 5, 37968.00, 37993.00, 2.50, 0.00, 0.00, '1', '2020-09-04', '2020-10-01', 5),
(249, 2, 5, 11, 1, 61160.00, 61163.00, 3.00, 0.00, 0.00, '1', '2020-09-04', '2020-09-21', 5),
(250, 4, 3, 18, 3, 14132.00, 14142.00, 3.00, 0.00, 0.00, '1', '2020-09-04', '2020-09-09', 5),
(251, 2, 5, 18, 4, 61166.00, 61176.00, 3.00, 0.00, 0.00, '1', '2020-09-04', '2020-09-09', 5),
(252, 2, 5, 16, 15, 61176.00, 61213.00, 2.50, 0.00, 0.00, '1', '2020-09-04', '2020-09-09', 3),
(253, 3, 7, 18, 8, 37993.00, 38038.00, 3.00, 0.00, 0.00, '1', '2020-09-04', '2020-09-09', 3),
(254, 4, 3, 18, 2, 14178.00, 14187.00, 3.00, 0.00, 0.00, '1', '2020-09-04', '2020-09-09', 3),
(255, 4, 3, 20, 6, 14161.00, 14190.00, 3.00, 0.00, 0.00, '1', '2020-09-04', '2020-09-21', 3),
(256, 2, 5, 20, 1, 61213.00, 61218.00, 3.00, 0.00, 0.00, '1', '2020-09-04', '2020-09-21', 3),
(257, 4, 3, 8, 1, 14187.00, 14190.00, 2.50, 0.00, 0.00, '1', '2020-09-04', '2020-09-10', 3),
(258, 4, 3, 9, 1, 14158.00, 14190.00, 2.50, 0.00, 0.00, '1', '2020-09-04', '2020-10-03', 3),
(259, 3, 7, 9, 3, 38028.00, 38038.00, 2.50, 0.00, 0.00, '1', '2020-09-04', '2020-10-03', 3),
(260, 6, 2, 14, 26, 64370.00, 64424.00, 3.00, 0.00, 0.00, '1', '2020-09-04', '2020-10-01', 3),
(261, 1, 1, 5, 26, 62200.00, 62274.00, 3.00, 0.00, 0.00, '1', '2020-09-04', '2020-09-18', 3),
(262, 5, 4, 1, 22, 15822.00, 15873.00, 2.50, 0.00, 0.00, '1', '2020-09-04', '2020-09-09', 3),
(263, 4, 3, 9, 1, 14190.00, 14192.00, 2.50, 0.00, 0.00, '1', '2020-09-05', '2020-10-03', 5),
(264, 2, 5, 24, 3, 61226.00, 61236.00, 2.50, 0.00, 0.00, '1', '2020-09-05', '2020-10-01', 5),
(265, 4, 3, 9, 1, 14200.00, 14203.00, 2.50, 0.00, 0.00, '1', '2020-09-05', '2020-10-03', 5),
(266, 2, 5, 11, 2, 61240.00, 61247.00, 3.00, 0.00, 0.00, '1', '2020-09-05', '2020-09-21', 5),
(267, 4, 3, 24, 1, 14208.00, 14210.00, 2.50, 0.00, 0.00, '1', '2020-09-05', '2020-10-01', 5),
(268, 4, 3, 9, 1, 14200.00, 14203.00, 2.50, 0.00, 0.00, '1', '2020-09-05', '2020-10-03', 5),
(269, 3, 7, 18, 10, 38038.00, 38066.00, 3.00, 0.00, 0.00, '1', '2020-09-05', '2020-09-09', 3),
(270, 1, 1, 20, 2, 62277.00, 62285.00, 3.00, 0.00, 0.00, '1', '2020-09-05', '2020-09-21', 3),
(271, 6, 2, 14, 28, 64423.00, 64471.00, 3.00, 0.00, 0.00, '1', '2020-09-05', '2020-10-01', 3),
(272, 6, 2, 14, 27, 64471.00, 64520.00, 3.00, 0.00, 0.00, '1', '2020-09-06', '2020-10-01', 3),
(273, 5, 4, 1, 30, 15873.00, 15938.00, 2.50, 0.00, 0.00, '1', '2020-09-05', '2020-09-09', 3),
(274, 5, 4, 1, 32, 15938.00, 15997.00, 2.50, 0.00, 0.00, '1', '2020-09-07', '2020-09-09', 3),
(275, 1, 1, 5, 20, 62285.00, 62326.00, 3.00, 0.00, 0.00, '1', '2020-09-05', '2020-09-18', 3),
(276, 1, 8, 5, 2, 11795.00, 11800.00, 3.00, 0.00, 0.00, '1', '2020-09-05', '2020-09-15', 3),
(277, 1, 8, 5, 14, 11841.00, 11900.00, 3.00, 0.00, 0.00, '1', '2020-09-06', '2020-09-18', 3),
(278, 3, 7, 19, 20, 38152.00, 38227.00, 3.00, 0.00, 0.00, '1', '2020-09-07', '2020-09-08', 3),
(279, 3, 7, 8, 3, 38075.00, 38090.00, 2.50, 0.00, 0.00, '1', '2020-09-05', '2020-09-10', 3),
(280, 3, 7, 9, 4, 38069.00, 38090.00, 2.50, 0.00, 0.00, '1', '2020-09-05', '2020-10-03', 3),
(281, 3, 7, 9, 9, 38090.00, 37152.00, 2.50, 0.00, 0.00, '1', '2020-09-06', '2020-10-03', 3),
(282, 3, 7, 9, 1, 38152.00, 38227.00, 2.50, 0.00, 0.00, '1', '2020-09-07', '2020-10-03', 3),
(283, 1, 2, 20, 7, 11900.00, 11921.00, 3.00, 0.00, 0.00, '1', '2020-09-07', '2020-09-21', 3),
(284, 3, 7, 20, 6, 38152.00, 38227.00, 3.00, 0.00, 0.00, '1', '2020-09-07', '2020-09-21', 3),
(285, 1, 8, 3, 2, 11851.00, 11861.00, 3.00, 0.00, 0.00, '1', '2020-09-06', '2020-09-14', 3),
(286, 1, 8, 3, 1, 11900.00, 11921.00, 3.00, 0.00, 0.00, '1', '2020-09-07', '2020-09-14', 3),
(287, 1, 8, 19, 14, 11804.00, 11841.00, 3.00, 0.00, 0.00, '1', '2020-09-06', '2020-09-25', 5),
(288, 6, 2, 18, 8, 64520.00, 64569.00, 3.00, 0.00, 0.00, '1', '2020-09-06', '2020-09-09', 5),
(289, 3, 7, 18, 8, 38090.00, 38152.00, 3.00, 0.00, 0.00, '1', '2020-09-06', '2020-09-09', 5),
(290, 5, 2, 18, 8, 64569.00, 64620.00, 3.00, 0.00, 0.00, '1', '2020-09-07', '2020-09-09', 3),
(291, 4, 3, 8, 2, 14233.00, 14240.00, 2.50, 0.00, 0.00, '1', '2020-09-07', '2020-09-10', 3),
(292, 4, 4, 3, 2, 14223.00, 14233.00, 3.00, 0.00, 0.00, '1', '2020-09-05', '2020-09-14', 3),
(293, 2, 5, 18, 25, 61271.00, 61303.00, 3.00, 0.00, 0.00, '1', '2020-09-06', '2020-09-09', 3),
(294, 4, 3, 19, 18, 14240.00, 14303.00, 3.00, 0.00, 0.00, '1', '2020-09-06', '2020-09-25', 5),
(295, 4, 3, 19, 27, 14303.00, 14400.00, 3.00, 0.00, 0.00, '1', '2020-09-07', '2020-09-25', 5),
(296, 4, 3, 18, 9, 14400.00, 14426.00, 3.00, 0.00, 0.00, '1', '2020-09-08', '2020-09-09', 5),
(297, 4, 2, 18, 3, 64620.00, 64629.00, 3.00, 0.00, 0.00, '1', '2020-09-08', '2020-09-09', 3),
(298, 6, 3, 20, 1, 14426.00, 14430.00, 3.00, 0.00, 0.00, '1', '2020-09-08', '2020-09-21', 3),
(299, 2, 5, 18, 3, 61410.00, 61434.00, 3.00, 0.00, 0.00, '1', '2020-09-08', '2020-09-09', 3),
(300, 6, 2, 20, 4, 64629.00, 64638.00, 3.00, 0.00, 0.00, '1', '2020-09-08', '2020-09-21', 3),
(301, 1, 1, 5, 18, 62356.00, 62435.00, 3.00, 0.00, 0.00, '1', '2020-09-08', '2020-09-15', 3),
(302, 5, 4, 1, 26, 16129.00, 16172.00, 2.50, 0.00, 0.00, '1', '2020-09-08', '2020-10-03', 3),
(303, 2, 5, 18, 35, 61303.00, 61335.00, 3.00, 0.00, 0.00, '1', '2020-09-07', '2020-09-09', 5),
(304, 4, 3, 18, 9, 14210.00, 14223.00, 3.00, 0.00, 0.00, '1', '2020-09-05', '2020-09-09', 5),
(305, 2, 5, 1, 11, 61252.00, 61271.00, 2.50, 0.00, 0.00, '1', '2020-09-06', '2020-09-09', 5),
(306, 3, 7, 11, 1, 38243.00, 38247.00, 3.00, 0.00, 0.00, '1', '2020-09-09', '2020-09-21', 3),
(307, 4, 3, 18, 12, 14434.00, 14461.00, 3.00, 0.00, 0.00, '1', '2020-09-09', '2020-09-16', 3),
(308, 3, 3, 21, 1, 14430.00, 14434.00, 3.00, 0.00, 0.00, '1', '2020-09-09', '2020-10-01', 3),
(309, 3, 7, 21, 1, 38247.00, 38251.00, 3.00, 0.00, 0.00, '1', '2020-09-09', '2020-10-01', 3),
(310, 2, 5, 16, 8, 61436.00, 61460.00, 2.50, 0.00, 0.00, '1', '2020-09-09', '2020-10-13', 3),
(311, 4, 3, 23, 15, 14461.00, 14482.00, 2.50, 0.00, 0.00, '1', '2020-09-09', '2020-10-13', 3),
(312, 3, 7, 18, 8, 38253.00, 38286.00, 3.00, 0.00, 0.00, '1', '2020-09-09', '2020-09-16', 3),
(313, 6, 2, 18, 1, 64638.00, 64644.00, 3.00, 0.00, 0.00, '1', '2020-09-09', '2020-09-16', 3),
(314, 3, 7, 8, 1, 38265.00, 38286.00, 2.50, 0.00, 0.00, '1', '2020-09-09', '2020-09-10', 3),
(315, 6, 2, 9, 2, 64644.00, 64654.00, 2.50, 0.00, 0.00, '1', '2020-09-09', '2020-10-03', 3),
(316, 1, 1, 5, 12, 62435.00, 62481.00, 3.00, 0.00, 0.00, '1', '2020-09-09', '2020-09-18', 3),
(317, 5, 4, 1, 14, 16276.00, 16308.00, 2.50, 0.00, 0.00, '1', '2020-09-09', '2020-10-03', 3),
(318, 4, 3, 11, 2, 14482.00, 14487.00, 3.00, 0.00, 0.00, '1', '2020-10-10', '2020-09-21', 5),
(319, 3, 7, 24, 1, 38234.00, 38238.00, 2.50, 0.00, 0.00, '1', '2020-09-08', '2020-10-02', 5),
(320, 3, 7, 26, 1, 38286.00, 38292.00, 3.00, 0.00, 0.00, '1', '2020-09-10', '2020-09-23', 5),
(321, 3, 7, 19, 6, 38297.00, 38312.00, 3.00, 0.00, 0.00, '1', '2020-09-10', '2020-09-25', 5),
(322, 3, 7, 18, 2, 38292.00, 38297.00, 3.00, 0.00, 0.00, '1', '2020-09-10', '2020-09-16', 5),
(323, 2, 5, 11, 2, 61460.00, 61475.00, 3.00, 0.00, 0.00, '1', '2020-09-10', '2020-09-21', 5),
(324, 2, 8, 18, 7, 11928.00, 11958.00, 3.00, 0.00, 0.00, '1', '2020-09-10', '2020-09-16', 5),
(325, 2, 5, 11, 1, 61475.00, 61479.00, 3.00, 0.00, 0.00, '1', '2020-09-10', '2020-09-21', 5),
(326, 2, 5, 18, 4, 61479.00, 61493.00, 3.00, 0.00, 0.00, '1', '2020-09-10', '2020-09-16', 3),
(327, 3, 7, 8, 2, 38312.00, 38340.00, 2.50, 0.00, 0.00, '1', '2020-09-10', '2020-10-15', 3),
(328, 4, 3, 3, 1, 14496.00, 14505.00, 3.00, 0.00, 0.00, '1', '2020-09-10', '2020-09-14', 3),
(329, 4, 3, 20, 5, 14487.00, 14505.00, 3.00, 0.00, 0.00, '1', '2020-09-10', '2020-09-21', 3),
(330, 3, 7, 9, 5, 38312.00, 38340.00, 2.50, 0.00, 0.00, '1', '2020-09-10', '2020-10-03', 3),
(331, 5, 4, 1, 16, 16311.00, 16349.00, 2.50, 0.00, 0.00, '1', '2020-09-10', '2020-10-03', 3),
(332, 1, 1, 5, 16, 62481.00, 62536.00, 3.00, 0.00, 0.00, '1', '2020-09-10', '2020-09-18', 3),
(333, 3, 7, 12, 2, 38340.00, 38344.00, 3.00, 0.00, 0.00, '1', '2020-09-11', '2020-09-11', 5),
(334, 2, 5, 18, 10, 61493.00, 61514.00, 2.80, 0.00, 0.00, '1', '2020-09-11', '2020-09-16', 5),
(335, 4, 3, 26, 2, 14541.00, 14551.00, 3.00, 0.00, 0.00, '1', '2020-09-11', '2020-09-23', 5),
(336, 2, 5, 18, 1, 61514.00, 61516.00, 2.80, 0.00, 0.00, '1', '2020-09-11', '2020-09-16', 5),
(337, 4, 3, 18, 1, 14551.00, 14553.00, 2.80, 0.00, 0.00, '1', '2020-09-11', '2020-09-16', 5),
(338, 4, 3, 19, 3, 14532.00, 14541.00, 3.00, 0.00, 0.00, '1', '2020-09-11', '2020-09-25', 5),
(339, 4, 3, 11, 1, 14527.00, 14532.00, 3.00, 0.00, 0.00, '1', '2020-09-11', '2020-09-21', 5),
(340, 4, 3, 21, 5, 14505.00, 14527.00, 3.00, 0.00, 0.00, '1', '2020-09-11', '2020-10-01', 5),
(341, 4, 3, 26, 2, 14534.00, 14541.00, 3.00, 0.00, 0.00, '1', '2020-09-11', '2020-09-23', 5),
(342, 2, 5, 20, 2, 61516.00, 61522.00, 3.00, 0.00, 0.00, '1', '2020-09-11', '2020-09-21', 3),
(343, 3, 7, 21, 1, 38352.00, 38357.00, 3.00, 0.00, 0.00, '1', '2020-09-11', '2020-10-01', 3),
(344, 2, 5, 16, 15, 61522.00, 61547.00, 2.50, 0.00, 0.00, '1', '2020-09-11', '2020-10-13', 3),
(345, 3, 7, 3, 1, 38365.00, 38383.00, 3.00, 0.00, 0.00, '1', '2020-09-11', '2020-09-14', 3),
(346, 3, 7, 18, 5, 38347.00, 38383.00, 2.80, 0.00, 0.00, '1', '2020-09-11', '2020-09-16', 3),
(347, 4, 3, 18, 4, 14553.00, 14580.00, 2.80, 0.00, 0.00, '1', '2020-09-11', '2020-09-16', 3),
(348, 3, 7, 20, 2, 38357.00, 38383.00, 3.00, 0.00, 0.00, '1', '2020-09-11', '2020-09-21', 3),
(349, 1, 8, 20, 2, 11959.00, 11966.00, 3.00, 0.00, 0.00, '1', '2020-09-11', '2020-09-21', 3),
(350, 4, 3, 20, 4, 14569.00, 14580.00, 3.00, 0.00, 0.00, '1', '2020-09-11', '2020-09-21', 3),
(351, 4, 3, 9, 2, 14553.00, 14580.00, 2.50, 0.00, 0.00, '1', '2020-09-11', '2020-10-03', 3),
(352, 3, 7, 9, 3, 38365.00, 38383.00, 2.50, 0.00, 0.00, '1', '2020-09-11', '2020-10-03', 3),
(353, 6, 2, 14, 12, 64654.00, 64701.00, 3.00, 0.00, 0.00, '1', '2020-09-11', '2020-10-01', 3),
(354, 5, 4, 1, 25, 16358.00, 16414.00, 2.50, 0.00, 0.00, '1', '2020-09-11', '2020-10-03', 5),
(355, 2, 2, 24, 2, 64717.00, 64720.00, 2.50, 0.00, 0.00, '1', '2020-09-12', '2020-10-02', 5),
(356, 2, 2, 9, 1, 64705.00, 64717.00, 2.50, 0.00, 0.00, '1', '2020-09-12', '2020-10-03', 5),
(357, 2, 5, 11, 1, 61557.00, 61562.00, 3.00, 0.00, 0.00, '1', '2020-09-12', '2020-09-21', 5),
(358, 4, 3, 18, 28, 14580.00, 14650.00, 2.80, 0.00, 0.00, '1', '2020-11-12', '2020-09-16', 5),
(359, 4, 3, 18, 29, 14650.00, 14725.00, 2.80, 0.00, 0.00, '1', '2020-09-13', '2020-09-16', 5),
(360, 4, 3, 9, 8, 14725.00, 14750.00, 2.50, 0.00, 0.00, '1', '2020-11-13', '2020-10-03', 5),
(361, 4, 3, 3, 3, 14750.00, 14767.00, 3.00, 0.00, 0.00, '1', '2020-11-13', '2020-09-14', 5),
(362, 1, 1, 19, 16, 62544.00, 62600.00, 3.00, 0.00, 0.00, '1', '2020-09-12', '2020-09-25', 5),
(363, 1, 1, 19, 18, 62600.00, 62667.00, 3.00, 0.00, 0.00, '1', '2020-09-13', '2020-09-25', 5),
(364, 3, 7, 8, 1, 38442.00, 38446.00, 2.50, 0.00, 0.00, '1', '2020-09-12', '2020-10-15', 5),
(365, 3, 7, 9, 1, 38387.00, 38390.00, 2.50, 0.00, 0.00, '1', '2020-09-12', '2020-10-03', 5),
(366, 3, 7, 9, 7, 38439.00, 38467.00, 2.50, 0.00, 0.00, '1', '2020-09-12', '2020-10-03', 5),
(367, 0, 0, 9, 0, 0.00, 0.00, 2.50, 0.00, 0.00, '1', '0000-00-00', '2020-10-03', 5),
(368, 0, 0, 9, 0, 0.00, 0.00, 2.50, 0.00, 0.00, '1', '0000-00-00', '2020-10-03', 5),
(369, 3, 7, 18, 17, 38384.00, 38439.00, 2.80, 0.00, 0.00, '1', '2020-09-12', '2020-09-16', 5),
(370, 3, 7, 18, 27, 38467.00, 38549.00, 2.80, 0.00, 0.00, '1', '2020-09-13', '2020-09-16', 5),
(371, 2, 5, 18, 16, 61604.00, 61654.00, 2.80, 0.00, 0.00, '1', '2020-09-13', '2020-09-16', 5),
(372, 3, 7, 19, 3, 38556.00, 38562.00, 3.00, 0.00, 0.00, '1', '2020-09-14', '2020-09-25', 5),
(373, 2, 5, 18, 8, 61664.00, 61698.00, 2.80, 0.00, 0.00, '1', '2020-09-14', '2020-09-16', 5),
(374, 5, 4, 1, 32, 16415.00, 16467.00, 2.50, 0.00, 0.00, '1', '2020-09-12', '2020-10-03', 5),
(375, 3, 7, 12, 1, 38581.00, 38586.00, 3.00, 0.00, 0.00, '0', '2020-09-14', NULL, 5),
(376, 3, 7, 18, 1, 38586.00, 38608.00, 2.80, 0.00, 0.00, '1', '2020-09-14', '2020-09-16', 3),
(377, 2, 5, 18, 7, 61701.00, 61728.00, 2.80, 0.00, 0.00, '1', '2020-09-14', '2020-09-16', 3),
(378, 3, 7, 20, 6, 38589.00, 38608.00, 3.00, 0.00, 0.00, '1', '2020-09-14', '2020-09-21', 3),
(379, 2, 5, 9, 2, 61701.00, 61735.00, 2.50, 0.00, 0.00, '1', '2020-09-14', '2020-10-03', 3),
(380, 5, 4, 1, 12, 16488.00, 16518.00, 2.50, 0.00, 0.00, '1', '2020-09-14', '2020-10-03', 3),
(381, 3, 7, 20, 1, 38608.00, 38611.00, 3.00, 0.00, 0.00, '1', '2020-09-14', '2020-09-21', 3),
(382, 3, 7, 9, 1, 38581.00, 38611.00, 2.50, 0.00, 0.00, '1', '2020-09-14', '2020-10-03', 3),
(383, 3, 7, 24, 2, 38615.00, 38622.00, 2.50, 0.00, 0.00, '1', '2020-09-15', '2020-10-02', 3),
(384, 2, 5, 18, 4, 61735.00, 61746.00, 2.80, 0.00, 0.00, '1', '2020-09-15', '2020-09-16', 3),
(385, 2, 5, 11, 2, 61750.00, 61578.00, 3.00, 0.00, 0.00, '1', '2020-09-15', '2020-09-21', 3),
(386, 4, 3, 11, 1, 14793.00, 14798.00, 3.00, 0.00, 0.00, '1', '2020-09-15', '2020-09-21', 3),
(387, 4, 3, 11, 1, 14808.00, 14812.00, 3.00, 0.00, 0.00, '1', '2020-09-15', '2020-09-21', 3),
(388, 4, 3, 18, 3, 14786.00, 14793.00, 2.80, 0.00, 0.00, '1', '2020-09-15', '2020-09-16', 3),
(389, 4, 3, 18, 1, 14812.00, 14817.00, 2.80, 0.00, 0.00, '1', '2020-09-15', '2020-09-16', 3),
(390, 2, 5, 12, 1, 61766.00, 61770.00, 3.00, 0.00, 0.00, '0', '2020-09-15', NULL, 3),
(391, 4, 3, 18, 8, 14832.00, 14854.00, 2.80, 0.00, 0.00, '1', '2020-09-15', '2020-09-16', 3),
(392, 4, 3, 8, 1, 14825.00, 14854.00, 2.50, 0.00, 0.00, '1', '2020-09-15', '2020-10-15', 3),
(393, 4, 3, 9, 3, 14843.00, 14854.00, 2.50, 0.00, 0.00, '1', '2020-09-15', '2020-10-03', 3),
(394, 6, 2, 14, 10, 64831.00, 64859.00, 3.00, 0.00, 0.00, '1', '2020-09-15', '2020-10-01', 3),
(395, 2, 5, 20, 10, 61766.00, 61798.00, 3.00, 0.00, 0.00, '1', '2020-09-15', '2020-09-21', 3),
(396, 5, 4, 1, 21, 16522.00, 16558.00, 2.50, 0.00, 0.00, '1', '2020-09-15', '2020-10-03', 3),
(397, 6, 2, 1, 17, 64720.00, 64779.00, 2.50, 0.00, 0.00, '1', '2020-09-12', '2020-10-03', 5),
(398, 6, 2, 1, 13, 64779.00, 64831.00, 2.50, 0.00, 0.00, '1', '2020-09-13', '2020-10-03', 5),
(399, 2, 5, 1, 14, 61564.00, 61606.00, 2.50, 0.00, 0.00, '1', '2020-09-13', '2020-10-03', 5),
(400, 2, 5, 11, 1, 61802.00, 61806.00, 3.00, 0.00, 0.00, '1', '2020-09-16', '2020-09-21', 5),
(401, 3, 7, 26, 1, 38632.00, 38634.00, 3.00, 0.00, 0.00, '1', '2020-09-16', '2020-09-23', 5),
(402, 4, 3, 18, 18, 14856.00, 14892.00, 2.80, 0.00, 0.00, '1', '2020-09-16', '2020-09-23', 5),
(403, 4, 3, 12, 1, 14892.00, 14895.00, 3.00, 0.00, 0.00, '0', '2020-09-16', NULL, 3),
(404, 2, 5, 16, 9, 61806.00, 61828.00, 2.50, 0.00, 0.00, '1', '2020-09-16', '2020-10-13', 3),
(405, 3, 7, 18, 10, 38645.00, 38679.00, 2.80, 0.00, 0.00, '1', '2020-09-16', '2020-09-23', 3),
(406, 4, 3, 18, 1, 14892.00, 14917.00, 2.80, 0.00, 0.00, '1', '2020-09-16', '2020-09-23', 3),
(407, 6, 2, 14, 7, 64859.00, 64885.00, 3.00, 0.00, 0.00, '1', '2020-09-16', '2020-10-01', 3),
(408, 4, 3, 9, 8, 14895.00, 14923.00, 2.50, 0.00, 0.00, '1', '2020-09-16', '2020-10-03', 3),
(409, 5, 4, 1, 9, 16564.00, 16590.00, 2.50, 0.00, 0.00, '1', '2020-09-16', '2020-10-03', 3),
(410, 3, 7, 19, 8, 38692.00, 38171.00, 3.00, 0.00, 0.00, '1', '2020-09-17', '2020-09-25', 5),
(411, 3, 7, 18, 4, 38679.00, 38692.00, 2.80, 0.00, 0.00, '1', '2020-09-17', '2020-09-23', 5),
(412, 7, 6, 18, 2, 31802.00, 31812.00, 2.80, 0.00, 0.00, '1', '2020-09-17', '2020-09-23', 5),
(413, 2, 5, 26, 1, 61834.00, 61839.00, 3.00, 0.00, 0.00, '1', '2020-09-17', '2020-09-23', 5),
(414, 2, 5, 19, 5, 61850.00, 61863.00, 3.00, 0.00, 0.00, '1', '2020-09-17', '2020-09-25', 5),
(415, 2, 5, 12, 1, 61863.00, 61866.00, 3.00, 0.00, 0.00, '0', '2020-09-17', NULL, 3),
(416, 7, 1, 18, 10, 62668.00, 62693.00, 2.80, 0.00, 0.00, '1', '2020-09-17', '2020-09-23', 3),
(417, 2, 5, 18, 11, 61866.00, 61897.00, 2.80, 0.00, 0.00, '1', '2020-09-17', '2020-09-23', 3),
(418, 6, 2, 14, 5, 64887.00, 64908.00, 3.00, 0.00, 0.00, '1', '2020-09-17', '2020-10-01', 3),
(419, 7, 1, 9, 6, 62693.00, 62713.00, 2.50, 0.00, 0.00, '1', '2020-09-17', '2020-10-03', 3),
(420, 4, 3, 20, 1, 14932.00, 14937.00, 3.00, 0.00, 0.00, '1', '2020-09-17', '2020-09-21', 3),
(421, 5, 4, 9, 12, 16590.00, 16617.00, 2.50, 0.00, 0.00, '1', '2020-09-17', '2020-10-03', 5),
(422, 7, 1, 18, 17, 62713.00, 62751.00, 2.80, 0.00, 0.00, '1', '2020-09-18', '2020-09-23', 5),
(423, 2, 5, 26, 1, 61899.00, 61905.00, 3.00, 0.00, 0.00, '1', '2020-09-18', '2020-09-23', 5),
(424, 4, 3, 18, 5, 14939.00, 14953.00, 2.80, 0.00, 0.00, '1', '2020-09-18', '2020-09-23', 5),
(425, 2, 5, 16, 16, 61906.00, 61940.00, 2.50, 0.00, 0.00, '1', '2020-09-18', '2020-10-13', 3),
(426, 3, 7, 18, 10, 38726.00, 38756.00, 2.80, 0.00, 0.00, '1', '2020-09-18', '2020-09-23', 3),
(427, 7, 1, 20, 4, 62751.00, 62767.00, 3.00, 0.00, 0.00, '1', '2020-09-18', '2020-09-21', 3),
(428, 4, 3, 9, 4, 14953.00, 14975.00, 2.50, 0.00, 0.00, '1', '2020-09-18', '2020-10-03', 3),
(429, 6, 2, 9, 1, 64950.00, 64955.00, 2.50, 0.00, 0.00, '1', '2020-09-18', '2020-10-03', 3),
(430, 6, 2, 14, 13, 64908.00, 64955.00, 3.00, 0.00, 0.00, '1', '2020-09-18', '2020-10-01', 3),
(431, 5, 4, 1, 22, 16899.00, 16943.00, 2.50, 0.00, 0.00, '1', '2020-09-18', '2020-10-03', 3),
(432, 4, 7, 11, 1, 14923.00, 14931.00, 3.00, 0.00, 0.00, '1', '2020-09-17', '2020-09-21', 5),
(433, 2, 5, 11, 1, 61958.00, 61963.00, 3.00, 0.00, 0.00, '1', '2020-08-19', '2020-09-21', 5),
(434, 2, 5, 9, 3, 61963.00, 61968.00, 2.50, 0.00, 0.00, '1', '2020-09-19', '2020-10-03', 5),
(435, 2, 5, 24, 2, 61947.00, 61958.00, 2.50, 0.00, 0.00, '1', '2020-09-19', '2020-10-02', 5),
(436, 3, 7, 23, 13, 38753.00, 38770.00, 2.50, 0.00, 0.00, '1', '2020-09-19', '2020-10-13', 5),
(437, 4, 3, 9, 1, 14975.00, 14979.00, 2.50, 0.00, 0.00, '0', '2020-09-11', NULL, 5),
(438, 2, 5, 11, 1, 61973.00, 61977.00, 3.00, 0.00, 0.00, '1', '2020-09-19', '2020-09-21', 5),
(439, 2, 5, 9, 1, 61997.00, 61980.00, 2.50, 0.00, 0.00, '0', '2020-09-19', NULL, 5),
(440, 7, 1, 19, 23, 62767.00, 62831.00, 3.00, 0.00, 0.00, '1', '2020-09-19', '2020-09-25', 5),
(441, 7, 1, 19, 18, 62831.00, 62891.00, 3.00, 0.00, 0.00, '1', '2020-09-20', '2020-09-25', 5),
(442, 8, 5, 1, 12, 61991.00, 62017.00, 2.50, 0.00, 0.00, '1', '2020-09-20', '2020-10-03', 5),
(443, 8, 5, 18, 29, 62023.00, 62094.00, 2.80, 0.00, 0.00, '1', '2020-09-20', '2020-09-23', 5),
(444, 3, 7, 18, 31, 38795.00, 38892.00, 2.80, 0.00, 0.00, '1', '2020-09-20', '2020-09-23', 5),
(445, 4, 3, 18, 24, 14979.00, 15027.00, 2.80, 0.00, 0.00, '1', '2020-09-19', '2020-09-23', 5),
(446, 4, 3, 18, 27, 15027.00, 15081.00, 2.80, 0.00, 0.00, '1', '2020-09-20', '2020-09-23', 5),
(447, 3, 3, 18, 14, 38753.00, 38795.00, 2.80, 0.00, 0.00, '1', '2020-09-19', '2020-09-23', 5),
(448, 4, 3, 8, 1, 15135.00, 15138.00, 2.50, 0.00, 0.00, '1', '2020-09-20', '2020-10-15', 5),
(449, 4, 3, 9, 17, 15081.00, 15115.00, 2.50, 0.00, 0.00, '1', '2020-09-19', '2020-10-03', 5),
(450, 4, 3, 9, 10, 15115.00, 15150.00, 2.50, 0.00, 0.00, '0', '2020-09-20', NULL, 5),
(451, 4, 3, 20, 4, 15138.00, 151150.00, 3.00, 0.00, 0.00, '1', '2020-09-19', '2020-09-21', 5),
(452, 0, 7, 11, 2, 0.00, 0.00, 3.00, 0.00, 0.00, '1', '2020-08-27', '2020-09-21', 5),
(453, 2, 7, 11, 1, 0.00, 0.00, 3.00, 0.00, 0.00, '1', '2020-08-27', '2020-09-21', 5),
(454, 3, 5, 11, 1, 0.00, 0.00, 3.00, 0.00, 0.00, '1', '2020-08-27', '2020-09-21', 5),
(455, 8, 8, 27, 23, 11989.00, 12033.00, 3.00, 0.00, 0.00, '1', '2020-09-19', '2020-09-21', 5),
(456, 3, 7, 18, 5, 38892.00, 38905.00, 2.80, 0.00, 0.00, '1', '2020-09-21', '2020-09-23', 5),
(457, 2, 5, 18, 9, 62104.00, 62125.00, 2.80, 0.00, 0.00, '1', '2020-09-21', '2020-09-23', 5),
(458, 5, 4, 1, 15, 17005.00, 17050.00, 2.50, 0.00, 0.00, '1', '2020-09-21', '2020-10-03', 5),
(459, 2, 5, 18, 5, 62125.00, 62142.00, 2.80, 0.00, 0.00, '1', '2020-09-21', '2020-09-23', 5),
(460, 7, 1, 20, 4, 62906.00, 62926.00, 3.00, 0.00, 0.00, '1', '2020-09-21', '2020-10-01', 5),
(461, 7, 1, 8, 1, 62926.00, 62930.00, 2.50, 0.00, 0.00, '1', '2020-09-21', '2020-10-15', 5),
(462, 3, 7, 16, 1, 38924.00, 38928.00, 2.50, 0.00, 0.00, '1', '2020-09-21', '2020-10-13', 5),
(463, 3, 7, 9, 3, 38909.00, 38924.00, 2.50, 0.00, 0.00, '0', '2020-09-21', NULL, 5),
(464, 7, 1, 20, 1, 62915.00, 62919.00, 3.00, 0.00, 0.00, '1', '2020-09-21', '2020-10-01', 5),
(465, 4, 3, 11, 1, 15752.00, 15758.00, 3.00, 0.00, 0.00, '1', '2020-09-22', '2020-10-15', 5),
(466, 7, 1, 11, 1, 62949.00, 62952.00, 3.00, 0.60, 0.00, '1', '2020-09-22', '2020-10-15', 5),
(467, 7, 1, 19, 3, 62945.00, 62949.00, 3.00, 1.80, 0.00, '1', '2020-09-22', '2020-09-25', 5),
(468, 7, 1, 26, 3, 62936.00, 62945.00, 3.00, 1.80, 0.00, '1', '2020-09-22', '2020-09-23', 5),
(469, 7, 1, 18, 5, 62931.00, 62936.00, 2.80, 2.80, 0.00, '1', '2020-09-22', '2020-09-23', 5),
(470, 5, 4, 1, 12, 17050.00, 17075.00, 2.50, 6.00, 0.00, '1', '2020-09-22', '2020-10-03', 5),
(471, 2, 5, 18, 7, 62199.00, 62220.00, 2.80, 3.92, 0.00, '1', '2020-09-22', '2020-09-23', 5),
(472, 3, 7, 28, 2, 38930.00, 38937.00, 3.00, 1.20, 0.00, '1', '2020-09-23', '2020-09-23', 5),
(473, 3, 7, 11, 2, 38943.00, 38951.00, 3.00, 1.20, 0.00, '1', '2020-09-23', '2020-10-15', 5),
(474, 3, 7, 11, 1, 38951.00, 38955.00, 3.00, 0.60, 0.00, '1', '2020-09-23', '2020-10-15', 5),
(475, 7, 1, 18, 11, 62936.00, 62978.00, 2.80, 6.16, 0.00, '1', '2020-09-22', '2020-09-23', 5),
(476, 7, 1, 14, 11, 62978.00, 63020.00, 3.00, 6.60, 0.00, '1', '2020-09-22', '2020-10-01', 5),
(477, 3, 3, 20, 6, 15132.00, 15154.00, 3.00, 3.60, 0.00, '1', '2020-09-22', '2020-10-01', 5),
(478, 4, 3, 8, 1, 15128.00, 15132.00, 2.50, 0.50, 0.00, '1', '2020-09-22', '2020-10-15', 5),
(479, 4, 3, 18, 1, 15124.00, 15128.00, 2.80, 0.56, 0.00, '1', '2020-09-22', '2020-09-23', 5),
(480, 7, 1, 18, 13, 63021.00, 63055.00, 2.80, 7.28, 0.00, '1', '2020-09-23', '2020-09-30', 5),
(481, 4, 3, 21, 4, 15198.00, 15206.00, 3.00, 4.00, 0.00, '1', '2020-09-29', '2020-10-01', 5),
(482, 4, 3, 26, 3, 15206.00, 15215.00, 3.00, 1.80, 0.00, '1', '2020-09-23', '2020-10-01', 5),
(483, 4, 3, 28, 1, 15255.00, 15258.00, 3.00, 0.60, 0.00, '1', '2020-09-24', '2020-09-24', 5),
(484, 4, 3, 26, 1, 15154.00, 15159.00, 3.00, 0.60, 0.00, '1', '2020-09-23', '2020-10-01', 5),
(485, 3, 7, 26, 1, 38989.00, 38993.00, 3.00, 0.60, 0.00, '1', '2020-09-24', '2020-10-01', 5),
(486, 4, 3, 9, 3, 15234.00, 15243.00, 2.50, 1.50, 0.00, '0', '2020-09-23', NULL, 5),
(487, 4, 3, 8, 1, 15227.00, 15231.00, 2.50, 0.50, 0.00, '1', '2020-09-23', '2020-10-15', 5),
(488, 4, 3, 20, 1, 15231.00, 15234.00, 3.00, 0.60, 0.00, '1', '2020-09-23', '2020-10-01', 5),
(489, 7, 1, 1, 5, 63051.00, 63066.00, 2.50, 2.50, 0.00, '1', '2020-09-23', '2020-10-03', 5),
(490, 3, 7, 18, 6, 38981.00, 38989.00, 2.80, 3.36, 0.00, '1', '2020-09-23', '2020-09-30', 5),
(491, 2, 5, 16, 8, 62276.00, 62303.00, 2.50, 4.00, 0.00, '1', '2020-09-23', '2020-10-13', 5),
(492, 3, 7, 19, 3, 39016.00, 39209.00, 3.00, 1.80, 0.00, '1', '2020-09-24', '2020-09-25', 5),
(493, 3, 7, 18, 3, 38993.00, 38999.00, 2.80, 1.68, 0.00, '1', '2020-09-24', '2020-09-30', 5),
(494, 3, 7, 26, 3, 39001.00, 39011.00, 3.00, 1.80, 0.00, '1', '2020-09-24', '2020-10-01', 5),
(495, 3, 7, 18, 2, 39030.00, 39034.00, 2.80, 1.12, 0.00, '1', '2020-09-24', '2020-09-30', 5),
(496, 7, 1, 14, 7, 63066.00, 63082.00, 3.00, 4.20, 0.00, '1', '2020-09-24', '2020-10-01', 5),
(497, 3, 7, 18, 3, 39038.00, 39048.00, 2.80, 1.68, 0.00, '1', '2020-09-24', '2020-09-30', 5),
(498, 10, 4, 1, 13, 17111.00, 17150.00, 2.50, 6.50, 0.00, '1', '2020-09-24', '2020-10-03', 5),
(499, 3, 7, 12, 3, 15284.00, 15293.00, 3.00, 1.80, 0.00, '0', '2020-09-24', NULL, 5),
(500, 4, 7, 18, 1, 15258.00, 15260.00, 2.80, 0.56, 0.00, '1', '2020-09-24', '2020-09-30', 5),
(501, 4, 7, 20, 4, 15272.00, 15260.00, 3.00, 2.40, 0.00, '1', '2020-09-24', '2020-10-01', 5),
(502, 4, 7, 9, 4, 15272.00, 15284.00, 2.50, 2.00, 0.00, '0', '2020-09-24', NULL, 5),
(503, 4, 3, 18, 4, 15293.00, 15301.00, 2.80, 2.24, 0.00, '1', '2020-09-25', '2020-09-30', 5),
(504, 4, 7, 18, 9, 15319.00, 15333.00, 2.80, 5.04, 0.00, '1', '2020-09-25', '2020-09-30', 5),
(505, 4, 7, 19, 6, 15301.00, 15319.00, 3.00, 3.60, 0.00, '1', '2020-09-25', '2020-10-15', 5),
(506, 3, 7, 18, 7, 39050.00, 39071.00, 2.80, 3.92, 0.00, '1', '2020-09-25', '2020-09-30', 5),
(507, 2, 5, 16, 12, 62418.00, 62442.00, 2.50, 6.00, 0.00, '1', '2020-09-25', '2020-10-13', 5),
(508, 3, 8, 24, 3, 12041.00, 12048.00, 2.50, 3.00, 0.00, '1', '2020-09-26', '2020-10-02', 5),
(509, 3, 8, 9, 1, 12048.00, 12052.00, 2.50, 0.50, 0.00, '0', '2020-09-26', NULL, 5),
(510, 3, 7, 18, 11, 39092.00, 39128.00, 2.80, 6.16, 0.00, '1', '2020-09-26', '2020-09-30', 5),
(511, 3, 7, 18, 21, 39128.00, 39193.00, 2.80, 11.76, 0.00, '1', '2020-09-27', '2020-09-30', 5),
(512, 3, 7, 24, 2, 39087.00, 39092.00, 2.50, 2.00, 0.00, '1', '2020-09-26', '2020-10-02', 5),
(513, 4, 3, 20, 7, 15319.00, 15340.00, 3.00, 4.20, 0.00, '1', '2020-09-25', '2020-10-01', 5),
(514, 4, 3, 20, 12, 15441.00, 15471.00, 3.00, 7.20, 0.00, '1', '2020-09-26', '2020-10-01', 5),
(515, 4, 3, 18, 3, 15319.00, 15328.00, 2.80, 1.68, 0.00, '1', '2020-09-25', '2020-09-30', 5),
(516, 4, 3, 18, 13, 15472.00, 15481.00, 2.80, 7.28, 0.00, '1', '2020-09-26', '2020-09-30', 5),
(517, 4, 3, 9, 9, 15319.00, 15341.00, 2.50, 4.50, 0.00, '0', '2020-09-25', NULL, 5),
(518, 4, 3, 9, 7, 15471.00, 151484.00, 2.50, 3.50, 0.00, '0', '2020-09-26', NULL, 5),
(519, 4, 3, 9, 4, 15338.00, 15551.00, 2.50, 2.00, 0.00, '0', '2020-09-27', NULL, 5),
(520, 4, 3, 19, 18, 15484.00, 15538.00, 3.00, 10.80, 0.00, '1', '2020-09-27', '2020-10-15', 5),
(521, 8, 5, 18, 23, 62604.00, 62672.00, 2.80, 25.76, 0.00, '1', '2020-09-27', '2020-09-30', 5),
(522, 7, 1, 18, 22, 63137.00, 63181.00, 2.80, 12.32, 0.00, '1', '2020-09-28', '2020-09-30', 5),
(523, 8, 5, 19, 26, 62448.00, 62511.00, 3.00, 31.20, 0.00, '1', '2020-09-26', '2020-10-15', 5),
(524, 8, 5, 27, 27, 62516.00, 62574.00, 3.00, 32.40, 0.00, '1', '2020-09-27', '2020-09-28', 5),
(525, 3, 8, 12, 1, 12064.00, 12066.00, 3.00, 0.60, 0.00, '0', '2020-09-28', NULL, 5),
(526, 10, 4, 1, 21, 17150.00, 17202.00, 2.50, 0.00, 0.00, '1', '2020-09-25', '2020-10-03', 5),
(527, 10, 4, 1, 15, 17202.00, 17247.00, 2.50, 7.50, 0.00, '1', '2020-09-26', '2020-10-03', 5),
(528, 8, 5, 1, 10, 62577.00, 62601.00, 2.50, 10.00, 0.00, '1', '2020-08-27', '2020-10-03', 5),
(529, 11, 8, 11, 1, 12070.00, 12074.00, 3.00, 0.00, 0.00, '1', '2020-09-28', '2020-10-15', 5),
(530, 3, 7, 31, 8, 39056.00, 39080.00, 3.00, 4.80, 0.00, '1', '2020-09-25', '2020-10-15', 5),
(531, 7, 1, 31, 22, 63083.00, 63080.00, 3.00, 13.20, 0.00, '1', '2020-09-26', '2020-10-05', 5),
(532, 7, 1, 31, 22, 63181.00, 63247.00, 3.00, 13.20, 0.00, '1', '2020-09-29', '2020-10-05', 5),
(533, 7, 1, 26, 2, 63262.00, 63269.00, 3.00, 1.20, 0.00, '1', '2020-09-28', '2020-10-01', 5),
(534, 2, 5, 9, 3, 62733.00, 62742.00, 2.50, 1.50, 0.00, '0', '2020-09-28', NULL, 5),
(535, 2, 4, 20, 5, 62742.00, 62764.00, 3.00, 3.00, 0.00, '1', '2020-09-28', '2020-10-01', 5),
(536, 7, 1, 30, 1, 63284.00, 63287.00, 3.00, 1.00, 0.00, '1', '2020-09-28', '2020-10-16', 5),
(537, 11, 6, 30, 1, 31819.00, 31822.00, 3.00, 0.00, 0.00, '1', '2020-09-28', '2020-10-16', 5),
(538, 3, 5, 8, 1, 39206.00, 39210.00, 2.50, 0.50, 0.00, '1', '2020-09-28', '2020-10-15', 5),
(539, 3, 7, 18, 10, 39210.00, 39242.00, 2.80, 5.60, 0.00, '1', '2020-09-28', '2020-09-30', 5),
(540, 11, 6, 26, 1, 31826.00, 31832.00, 3.00, 0.00, 0.00, '1', '2020-08-29', '2020-10-01', 5),
(541, 11, 6, 21, 2, 31832.00, 31837.00, 3.00, 0.00, 0.00, '1', '2020-08-29', '2020-10-01', 5),
(542, 11, 6, 24, 2, 39245.00, 39251.00, 2.50, 0.00, 0.00, '1', '2020-09-29', '2020-10-02', 5),
(543, 11, 6, 26, 2, 31836.00, 31843.00, 3.00, 0.00, 0.00, '1', '2020-09-29', '2020-10-01', 5),
(544, 11, 6, 11, 2, 31843.00, 31849.00, 3.00, 0.00, 0.00, '1', '2020-08-29', '2020-10-15', 5),
(545, 3, 6, 18, 13, 31844.00, 31883.00, 2.80, 7.28, 0.00, '1', '2020-09-29', '2020-09-30', 5);
INSERT INTO `night_trips` (`id`, `id_biker`, `id_motorcycle`, `id_client`, `quantity`, `departure_odometer`, `arrival_odometer`, `price`, `biker_commission`, `user_commission`, `payment_status`, `date`, `day_receipt`, `user`) VALUES
(546, 3, 6, 28, 1, 31897.00, 31899.00, 3.00, 0.60, 0.00, '1', '2020-09-30', '2020-09-30', 5),
(547, 3, 6, 12, 1, 31899.00, 31901.00, 3.00, 0.60, 0.00, '0', '2020-09-30', NULL, 5),
(548, 12, 7, 14, 6, 39272.00, 39290.00, 3.00, 7.20, 0.00, '1', '2020-09-29', '2020-10-01', 5),
(549, 12, 7, 18, 4, 39260.00, 39272.00, 2.80, 4.48, 0.00, '1', '2020-09-29', '2020-09-30', 5),
(550, 4, 3, 18, 22, 61570.00, 61636.00, 2.80, 12.32, 0.00, '1', '2020-09-29', '2020-09-30', 5),
(551, 4, 3, 24, 3, 31553.00, 631562.00, 2.50, 3.00, 0.00, '1', '2020-09-29', '2020-10-02', 5),
(552, 3, 6, 30, 1, 31907.00, 31911.00, 3.00, 1.00, 0.00, '1', '2020-09-30', '2020-10-16', 5),
(553, 4, 3, 30, 2, 61562.00, 61570.00, 3.00, 1.20, 0.00, '1', '2020-09-29', '2020-10-16', 5),
(554, 4, 3, 20, 2, 61636.00, 61642.00, 3.00, 1.20, 0.00, '1', '2020-08-29', '2020-10-01', 5),
(555, 4, 3, 9, 2, 61642.00, 61650.00, 2.50, 1.00, 0.00, '0', '2020-09-29', NULL, 5),
(556, 4, 3, 21, 4, 61572.00, 61584.00, 3.00, 4.00, 0.00, '1', '2020-09-29', '2020-10-01', 5),
(557, 7, 5, 18, 21, 63238.00, 62301.00, 2.80, 11.76, 0.00, '1', '2020-09-28', '2020-09-30', 5),
(558, 4, 3, 18, 8, 15322.00, 15346.00, 2.80, 4.48, 0.00, '1', '2020-08-25', '2020-09-30', 5),
(559, 4, 3, 18, 1, 15667.00, 15670.00, 2.80, 0.56, 0.00, '1', '2020-09-30', '2020-10-13', 5),
(560, 4, 3, 26, 2, 15657.00, 15663.00, 3.00, 1.20, 0.00, '1', '2020-09-30', '2020-10-01', 5),
(561, 4, 3, 26, 1, 15663.00, 15667.00, 3.00, 0.60, 0.00, '1', '2020-09-30', '2020-10-01', 5),
(562, 4, 3, 26, 1, 15675.00, 15680.00, 3.00, 0.60, 0.00, '1', '2020-09-30', '2020-10-01', 5),
(563, 12, 6, 14, 9, 31954.00, 31995.00, 3.00, 10.80, 0.00, '1', '2020-09-30', '2020-10-01', 5),
(564, 12, 6, 18, 18, 31915.00, 31954.00, 2.80, 20.16, 0.00, '1', '2020-09-30', '2020-10-13', 5),
(565, 3, 7, 18, 14, 39269.00, 39341.00, 2.80, 7.84, 0.00, '1', '2020-09-30', '2020-10-13', 5),
(566, 13, 1, 31, 8, 63307.00, 63331.00, 3.00, 9.60, 0.00, '1', '2020-09-30', '2020-10-15', 5),
(567, 2, 5, 16, 8, 62892.00, 62909.00, 2.50, 4.00, 0.00, '1', '2020-09-30', '2020-10-13', 5),
(568, 3, 7, 30, 1, 39349.00, 39352.00, 3.00, 0.60, 0.00, '1', '2020-10-01', '2020-10-16', 5),
(569, 3, 7, 28, 1, 39355.00, 39358.00, 3.00, 0.60, 0.00, '1', '2020-10-01', '2020-10-15', 5),
(570, 3, 7, 12, 1, 39352.00, 39355.00, 3.00, 0.60, 0.00, '0', '2020-10-01', NULL, 5),
(571, 2, 5, 21, 2, 62910.00, 62915.00, 3.00, 1.20, 0.00, '1', '2020-10-01', '2020-10-26', 5),
(572, 11, 2, 11, 1, 65476.00, 65480.00, 3.00, 0.00, 0.00, '1', '2020-10-01', '2020-10-15', 5),
(573, 2, 5, 21, 1, 62925.00, 62927.00, 3.00, 0.60, 0.00, '1', '2020-10-01', '2020-10-26', 5),
(574, 2, 5, 18, 5, 62915.00, 62927.00, 2.80, 2.80, 0.00, '1', '2020-10-01', '2020-10-13', 5),
(575, 11, 2, 30, 1, 65482.00, 65482.00, 3.00, 0.00, 0.00, '1', '2020-10-01', '2020-10-16', 5),
(576, 13, 1, 31, 10, 63333.00, 63370.00, 3.00, 12.00, 0.00, '1', '2020-10-01', '2020-10-05', 5),
(577, 2, 5, 18, 15, 62950.00, 62983.00, 2.80, 8.40, 0.00, '1', '2020-10-01', '2020-10-13', 5),
(578, 2, 5, 24, 1, 62952.00, 62955.00, 2.50, 0.50, 0.00, '1', '2020-10-01', '2020-10-15', 5),
(579, 2, 5, 11, 1, 62996.00, 63000.00, 3.00, 0.60, 0.00, '1', '2020-10-02', '2020-10-15', 5),
(580, 3, 7, 14, 9, 39359.00, 39391.00, 3.00, 5.40, 0.00, '0', '2020-10-01', NULL, 5),
(581, 2, 5, 28, 2, 63000.00, 63006.00, 3.00, 1.20, 0.00, '1', '2020-10-02', '2020-10-15', 5),
(582, 11, 2, 30, 1, 65484.00, 65487.00, 3.00, 0.00, 0.00, '1', '2020-10-01', '2020-10-16', 5),
(583, 4, 7, 30, 4, 15742.00, 15751.00, 3.00, 2.40, 0.00, '1', '2020-10-01', '2020-10-16', 5),
(584, 4, 3, 30, 1, 15667.00, 15670.00, 3.00, 0.60, 0.00, '1', '2020-09-30', '2020-10-16', 5),
(585, 4, 3, 18, 17, 15730.00, 15781.00, 2.80, 9.52, 0.00, '1', '2020-10-01', '2020-10-13', 5),
(586, 4, 5, 8, 1, 15697.00, 15701.00, 2.50, 0.50, 0.00, '1', '2020-09-30', '2020-10-15', 5),
(587, 4, 7, 9, 4, 15701.00, 15713.00, 2.50, 2.00, 0.00, '0', '2020-09-30', NULL, 5),
(588, 4, 3, 9, 2, 15781.00, 15787.00, 2.50, 1.00, 0.00, '0', '2020-10-01', NULL, 5),
(589, 4, 7, 20, 4, 15777.00, 15789.00, 3.00, 2.40, 0.00, '1', '2020-10-01', '2020-10-22', 5),
(590, 3, 7, 26, 1, 39391.00, 39397.00, 3.00, 0.60, 0.00, '1', '2020-10-02', '2020-10-14', 5),
(591, 3, 6, 26, 1, 39407.00, 39410.00, 3.00, 0.60, 0.00, '1', '2020-10-02', '2020-10-14', 5),
(592, 3, 7, 30, 1, 39410.00, 39414.00, 3.00, 0.60, 0.00, '1', '2020-10-02', '2020-10-16', 5),
(593, 3, 7, 19, 14, 39414.00, 39442.00, 3.00, 8.40, 0.00, '1', '2020-10-02', '2020-10-15', 5),
(594, 11, 6, 30, 1, 31998.00, 32001.00, 3.00, 0.00, 0.00, '1', '2020-10-02', '2020-10-16', 5),
(595, 11, 6, 11, 1, 32001.00, 32004.00, 3.00, 0.00, 0.00, '1', '2020-10-02', '2020-10-15', 5),
(596, 3, 7, 26, 1, 39444.00, 39448.00, 3.00, 0.60, 0.00, '1', '2020-10-02', '2020-10-14', 5),
(597, 3, 7, 18, 10, 39448.00, 39483.00, 2.80, 5.60, 0.00, '1', '2020-10-02', '2020-10-13', 5),
(598, 3, 7, 9, 3, 39486.00, 39495.00, 2.50, 1.50, 0.00, '0', '2020-10-03', NULL, 5),
(599, 3, 7, 11, 2, 39502.00, 39510.00, 3.00, 1.20, 0.00, '1', '2020-10-03', '2020-10-15', 5),
(600, 3, 7, 8, 1, 39526.00, 39529.00, 2.50, 0.50, 0.00, '1', '2020-10-03', '2020-10-15', 5),
(601, 3, 7, 20, 4, 39529.00, 39541.00, 3.00, 2.40, 0.00, '1', '2020-09-03', '2020-10-22', 5),
(602, 3, 7, 9, 9, 39541.00, 39568.00, 2.50, 4.50, 0.00, '0', '2020-10-03', NULL, 5),
(603, 13, 1, 31, 2, 63377.00, 63383.00, 3.00, 2.40, 0.00, '1', '2020-10-02', '2020-10-05', 5),
(604, 13, 1, 31, 19, 63449.00, 63506.00, 3.00, 22.80, 0.00, '1', '2020-10-03', '2020-10-05', 5),
(605, 13, 1, 31, 24, 63563.00, 63635.00, 3.00, 28.80, 0.00, '1', '2020-10-04', '2020-10-05', 5),
(606, 8, 8, 18, 38, 12150.00, 12230.00, 2.80, 42.56, 0.00, '1', '2020-10-04', '2020-10-13', 5),
(607, 8, 8, 27, 18, 12092.00, 12128.00, 3.00, 21.60, 0.00, '1', '2020-10-03', '2020-10-08', 5),
(608, 8, 8, 1, 12, 12129.00, 12147.00, 2.50, 12.00, 0.00, '1', '2020-09-04', '2020-10-14', 5),
(609, 2, 5, 16, 8, 63006.00, 63040.00, 2.50, 4.00, 0.00, '1', '2020-10-02', '2020-10-13', 5),
(610, 2, 5, 12, 2, 63008.00, 63014.00, 3.00, 1.20, 0.00, '0', '2020-10-02', NULL, 5),
(611, 6, 2, 14, 61, 65430.00, 65613.00, 3.00, 73.20, 0.00, '1', '2020-09-25', '2020-10-08', 5),
(612, 2, 4, 26, 1, 63040.00, 63045.00, 3.00, 0.60, 0.00, '1', '2020-10-05', '2020-10-14', 5),
(613, 2, 5, 30, 1, 63047.00, 63052.00, 3.00, 0.60, 0.00, '1', '2020-10-05', '2020-10-16', 5),
(614, 6, 2, 9, 6, 65613.00, 65631.00, 2.50, 6.00, 0.00, '0', '2020-10-04', NULL, 5),
(615, 11, 2, 26, 3, 65634.00, 65643.00, 3.00, 0.00, 0.00, '1', '2020-10-05', '2020-10-14', 5),
(616, 11, 2, 11, 1, 65643.00, 65646.00, 3.00, 0.00, 0.00, '1', '2020-10-05', '2020-10-15', 5),
(617, 2, 4, 18, 6, 63052.00, 63067.00, 2.80, 3.36, 0.00, '1', '2020-10-05', '2020-10-13', 5),
(618, 2, 5, 25, 17, 62148.00, 62199.00, 3.00, 17.00, 0.00, '1', '2020-09-22', '2020-10-08', 5),
(619, 2, 5, 25, 13, 62220.00, 62276.00, 3.00, 13.00, 0.00, '1', '2020-09-23', '2020-10-08', 5),
(620, 2, 5, 25, 12, 62303.00, 62344.00, 3.00, 12.00, 0.00, '1', '2020-09-24', '2020-10-08', 5),
(621, 2, 5, 25, 21, 62344.00, 62418.00, 3.00, 12.60, 0.00, '1', '2020-09-25', '2020-10-08', 5),
(622, 2, 5, 25, 18, 62680.00, 62733.00, 3.00, 10.80, 0.00, '1', '2020-09-28', '2020-10-08', 5),
(623, 2, 5, 25, 16, 62764.00, 62826.00, 3.00, 9.60, 0.00, '1', '2020-09-29', '2020-10-08', 5),
(624, 2, 5, 25, 12, 62826.00, 62892.00, 3.00, 7.20, 0.00, '1', '2020-09-30', '2020-10-08', 5),
(625, 11, 8, 26, 3, 12235.00, 12244.00, 3.00, 0.00, 0.00, '1', '2020-10-07', '2020-10-14', 5),
(626, 4, 3, 18, 15, 15794.00, 15845.00, 2.80, 8.40, 0.00, '1', '2020-10-02', '2020-10-13', 5),
(627, 4, 3, 18, 6, 16050.00, 16058.00, 2.80, 3.36, 0.00, '1', '2020-10-05', '2020-10-13', 5),
(628, 4, 3, 18, 15, 15872.00, 15917.00, 2.80, 8.40, 0.00, '1', '2020-10-03', '2020-10-13', 5),
(629, 4, 1, 18, 25, 15917.00, 15992.00, 2.80, 14.00, 0.00, '1', '2020-10-04', '2020-10-13', 5),
(630, 4, 7, 18, 21, 16064.00, 16138.00, 2.80, 11.76, 0.00, '1', '2020-10-06', '2020-10-13', 5),
(631, 13, 1, 18, 37, 63385.00, 63496.00, 2.80, 41.44, 0.00, '1', '2020-10-03', '2020-10-13', 5),
(632, 13, 1, 18, 31, 63520.00, 63598.00, 2.80, 34.72, 0.00, '1', '2020-10-04', '2020-10-13', 5),
(633, 13, 1, 18, 7, 63640.00, 63663.00, 2.80, 7.84, 0.00, '1', '2020-10-05', '2020-10-13', 5),
(634, 4, 3, 19, 12, 15877.00, 15913.00, 3.00, 7.20, 0.00, '1', '2020-10-03', '2020-10-15', 5),
(635, 4, 3, 26, 2, 16058.00, 16064.00, 3.00, 1.20, 0.00, '1', '2020-10-06', '2020-10-14', 5),
(636, 4, 3, 30, 4, 15880.00, 15892.00, 3.00, 2.40, 0.00, '1', '2020-10-03', '2020-10-16', 5),
(637, 4, 3, 11, 2, 16087.00, 16093.00, 3.00, 1.20, 0.00, '1', '2020-10-06', '2020-10-15', 5),
(638, 4, 3, 20, 6, 15854.00, 15872.00, 3.00, 3.60, 0.00, '0', '2020-10-02', NULL, 5),
(639, 4, 3, 20, 5, 15890.00, 15905.00, 3.00, 3.00, 0.00, '1', '2020-10-03', '2020-10-22', 5),
(640, 4, 3, 9, 2, 15872.00, 15878.00, 2.50, 1.00, 0.00, '0', '2020-10-02', NULL, 5),
(641, 4, 3, 21, 1, 16154.00, 16157.00, 3.00, 0.60, 0.00, '1', '2020-10-07', '2020-10-26', 5),
(642, 4, 3, 26, 1, 16150.00, 16154.00, 3.00, 0.60, 0.00, '1', '2020-10-07', '2020-10-14', 5),
(643, 4, 3, 30, 2, 16157.00, 16163.00, 3.00, 1.20, 0.00, '1', '2020-10-07', '2020-10-16', 5),
(644, 4, 3, 18, 37, 16140.00, 16243.00, 2.80, 20.72, 0.00, '1', '2020-10-07', '2020-10-14', 5),
(645, 11, 6, 26, 1, 32000.00, 32004.00, 3.00, 0.00, 0.00, '1', '2020-10-08', '2020-10-14', 5),
(646, 4, 3, 18, 38, 16243.00, 16354.00, 2.80, 21.28, 0.00, '1', '2020-10-08', '2020-10-14', 5),
(647, 4, 3, 18, 38, 16371.00, 16452.00, 2.80, 21.28, 0.00, '1', '2020-10-09', '2020-10-14', 5),
(648, 4, 3, 18, 27, 16481.00, 16558.00, 2.80, 15.12, 0.00, '1', '2020-10-10', '2020-10-14', 5),
(649, 4, 3, 18, 26, 16558.00, 16619.00, 2.80, 14.56, 0.00, '1', '2020-10-11', '2020-10-14', 5),
(650, 4, 3, 24, 3, 16472.00, 16481.00, 2.50, 1.50, 0.00, '1', '2020-10-10', '2020-10-15', 5),
(651, 4, 3, 30, 2, 16346.00, 16352.00, 3.00, 1.20, 0.00, '1', '2020-10-08', '2020-10-16', 5),
(652, 4, 3, 30, 1, 16414.00, 16418.00, 3.00, 0.60, 0.00, '1', '2020-10-09', '2020-10-16', 5),
(653, 4, 3, 21, 4, 16402.00, 16414.00, 3.00, 2.40, 0.00, '1', '2020-10-09', '2020-10-26', 5),
(654, 4, 3, 11, 1, 16398.00, 16402.00, 3.00, 0.60, 0.00, '1', '2020-10-09', '2020-10-15', 5),
(655, 4, 3, 11, 2, 16469.00, 16472.00, 3.00, 1.20, 0.00, '1', '2020-10-10', '2020-10-15', 5),
(656, 4, 3, 26, 5, 16383.00, 16392.00, 3.00, 3.00, 0.00, '1', '2020-10-09', '2020-10-14', 5),
(657, 8, 5, 18, 17, 63163.00, 63211.00, 2.80, 19.04, 0.00, '1', '2020-10-11', '2020-10-14', 5),
(658, 8, 5, 1, 9, 63141.00, 63158.00, 2.50, 9.00, 0.00, '1', '2020-10-11', '2020-10-14', 5),
(659, 8, 5, 27, 19, 63084.00, 63141.00, 3.00, 22.80, 0.00, '1', '2020-10-10', '2020-10-16', 5),
(660, 11, 0, 26, 1, 0.00, 0.00, 3.00, 0.00, 0.00, '1', '2020-10-13', '2020-10-14', 5),
(661, 13, 1, 31, 19, 63773.00, 63231.00, 3.00, 22.80, 0.00, '1', '2020-10-09', '2020-10-15', 5),
(662, 13, 1, 31, 19, 63868.00, 63925.00, 3.00, 22.80, 0.00, '1', '2020-10-10', '2020-10-15', 5),
(663, 13, 1, 31, 17, 63963.00, 64014.00, 3.00, 20.40, 0.00, '1', '2020-10-11', '2020-10-15', 5),
(664, 11, 7, 30, 2, 39567.00, 39573.00, 3.00, 0.00, 0.00, '1', '2020-10-13', '2020-10-16', 5),
(665, 13, 1, 30, 2, 63919.00, 63925.00, 3.00, 2.40, 0.00, '1', '2020-10-10', '2020-10-16', 5),
(666, 13, 1, 30, 6, 64022.00, 64040.00, 3.00, 7.20, 0.00, '1', '2020-10-12', '2020-10-16', 5),
(667, 13, 1, 18, 6, 63773.00, 63791.00, 2.80, 6.72, 0.00, '1', '2020-10-09', '2020-10-14', 5),
(668, 13, 1, 18, 20, 63833.00, 63893.00, 2.80, 22.40, 0.00, '1', '2020-10-10', '2020-10-14', 5),
(669, 13, 1, 18, 18, 63927.00, 63978.00, 2.80, 20.16, 0.00, '1', '2020-10-11', '2020-10-14', 5),
(670, 13, 1, 18, 49, 64019.00, 64120.00, 2.80, 54.88, 0.00, '1', '2020-10-12', '2020-10-14', 5),
(671, 4, 3, 18, 24, 16627.00, 16699.00, 2.80, 13.44, 0.00, '1', '2020-10-13', '2020-10-14', 5),
(672, 4, 3, 11, 2, 16700.00, 16706.00, 3.00, 1.20, 0.00, '1', '2020-10-14', '2020-10-15', 5),
(673, 4, 3, 26, 1, 16699.00, 16702.00, 3.00, 0.60, 0.00, '1', '2020-10-13', '2020-10-14', 5),
(674, 6, 2, 1, 29, 0.00, 0.00, 2.50, 29.00, 0.00, '1', '2020-10-03', '2020-10-14', 5),
(675, 6, 2, 1, 20, 0.00, 0.00, 2.50, 20.00, 0.00, '1', '2020-10-02', '2020-10-14', 5),
(676, 13, 1, 1, 19, 63665.00, 63713.00, 2.50, 19.00, 0.00, '1', '2020-10-06', '2020-10-14', 5),
(677, 13, 2, 1, 16, 63719.00, 63764.00, 2.50, 16.00, 0.00, '1', '2020-10-07', '2020-10-14', 5),
(678, 6, 2, 1, 10, 0.00, 0.00, 2.50, 10.00, 0.00, '1', '2020-10-08', '2020-10-14', 5),
(679, 6, 2, 1, 20, 0.00, 0.00, 2.50, 20.00, 0.00, '1', '2020-10-09', '2020-10-14', 5),
(680, 6, 2, 1, 19, 0.00, 0.00, 2.50, 19.00, 0.00, '1', '2020-10-10', '2020-10-14', 5),
(681, 10, 4, 1, 12, 0.00, 0.00, 2.50, 6.00, 0.00, '1', '2020-09-28', '2020-10-14', 5),
(682, 10, 4, 1, 12, 0.00, 0.00, 2.50, 6.00, 0.00, '1', '2020-08-29', '2020-10-14', 5),
(683, 10, 4, 1, 12, 0.00, 0.00, 2.50, 6.00, 0.00, '1', '2020-09-29', '2020-10-14', 5),
(684, 10, 4, 1, 19, 0.00, 0.00, 2.50, 9.50, 0.00, '1', '2020-09-30', '2020-10-14', 5),
(685, 10, 5, 1, 17, 0.00, 0.00, 2.50, 8.50, 0.00, '1', '2020-10-01', '2020-10-14', 5),
(686, 11, 0, 30, 1, 0.00, 0.00, 3.00, 0.00, 0.00, '1', '2020-10-14', '2020-10-16', 5),
(687, 11, 0, 30, 1, 0.00, 0.00, 3.00, 0.00, 0.00, '1', '2020-10-14', '2020-10-16', 5),
(688, 11, 0, 26, 1, 0.00, 0.00, 3.00, 0.00, 0.00, '1', '2020-10-14', '2020-10-16', 5),
(689, 11, 6, 26, 1, 0.00, 0.00, 3.00, 0.00, 0.00, '1', '2020-10-14', '2020-10-16', 5),
(690, 11, 0, 11, 1, 0.00, 0.00, 3.00, 0.00, 0.00, '1', '2020-10-15', '2020-10-15', 5),
(691, 4, 3, 18, 18, 16716.00, 1677.00, 2.80, 10.08, 0.00, '1', '2020-10-14', '2020-10-26', 5),
(692, 4, 3, 26, 1, 16709.00, 16712.00, 3.00, 0.60, 0.00, '1', '2020-10-14', '2020-10-16', 5),
(693, 4, 3, 30, 1, 16712.00, 16716.00, 3.00, 0.60, 0.00, '1', '2020-10-14', '2020-10-16', 5),
(694, 4, 3, 30, 3, 0.00, 0.00, 3.00, 1.80, 0.00, '1', '2020-10-15', '2020-10-16', 5),
(695, 4, 3, 18, 28, 16781.00, 16865.00, 2.80, 15.68, 0.00, '1', '2020-10-15', '2020-10-26', 5),
(696, 4, 3, 26, 1, 16778.00, 16781.00, 3.00, 0.60, 0.00, '1', '2020-10-15', '2020-10-16', 5),
(697, 4, 3, 26, 1, 16778.00, 16781.00, 3.00, 0.60, 0.00, '1', '2020-10-15', '2020-10-16', 5),
(698, 4, 3, 30, 3, 16770.00, 16778.00, 3.00, 1.80, 0.00, '1', '2020-10-15', '2020-10-16', 5),
(699, 4, 3, 18, 61, 0.00, 0.00, 2.80, 34.16, 0.00, '1', '2020-10-16', '2020-10-26', 5);

-- --------------------------------------------------------

--
-- Table structure for table `occurrence`
--

CREATE TABLE `occurrence` (
  `id` int(11) NOT NULL,
  `id_motorcycle` int(11) NOT NULL,
  `id_biker` int(11) NOT NULL,
  `occurrence` varchar(200) NOT NULL,
  `status_occurrence` int(11) NOT NULL,
  `register` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `packages`
--

CREATE TABLE `packages` (
  `id` int(11) NOT NULL,
  `id_client` int(11) NOT NULL,
  `id_trip` int(11) NOT NULL,
  `id_city_payment_rules` int(11) DEFAULT NULL,
  `departure_address` int(11) NOT NULL,
  `arrival_address` int(11) NOT NULL,
  `destinatary` varchar(200) DEFAULT NULL,
  `price` float(10,2) DEFAULT NULL,
  `receiver` varchar(200) DEFAULT NULL,
  `people_authorized` text,
  `status` int(11) NOT NULL,
  `payment` varchar(200) DEFAULT NULL,
  `status_it_payment` varchar(30) DEFAULT NULL,
  `observation` text,
  `register` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `packages`
--

INSERT INTO `packages` (`id`, `id_client`, `id_trip`, `id_city_payment_rules`, `departure_address`, `arrival_address`, `destinatary`, `price`, `receiver`, `people_authorized`, `status`, `payment`, `status_it_payment`, `observation`, `register`) VALUES
(1, 22, 33, 8, 111, 112, 'RICARDO', 12.00, 'maos proprias', '', 1, '', 'nao_pago', '', '2020-09-02 10:29:16'),
(2, 3, 34, NULL, 115, 116, NULL, 3.00, NULL, NULL, 1, NULL, NULL, NULL, '2020-09-02 10:40:35'),
(3, 4, 0, 8, 121, 122, '', 12.00, 'pessoas autorizadas', 'DINHA (DE VANIZA)', 1, 'Destinatario', 'nao_pago', 'REMETENTE VEK (GIFT ATELIÊ), VALOR DA ENCOMENDA PARA RECEBER EM SALINAS.', '2020-09-03 18:33:17'),
(4, 4, 36, NULL, 123, 124, NULL, 50.00, NULL, NULL, 1, NULL, NULL, NULL, '2020-09-04 11:50:15'),
(5, 4, 37, NULL, 125, 126, NULL, 3.00, NULL, NULL, 1, NULL, NULL, NULL, '2020-09-04 19:10:26'),
(6, 4, 38, NULL, 127, 128, NULL, 3.00, NULL, NULL, 1, NULL, NULL, NULL, '2020-09-04 19:17:23'),
(7, 0, 40, NULL, 135, 136, NULL, 5.00, NULL, NULL, 1, NULL, NULL, NULL, '2020-09-05 11:45:38'),
(8, 4, 42, NULL, 141, 142, NULL, 50.00, NULL, NULL, 1, NULL, NULL, NULL, '2020-09-08 14:18:06'),
(9, 25, 43, NULL, 143, 144, NULL, 50.00, NULL, NULL, 1, NULL, NULL, NULL, '2020-09-09 13:02:10'),
(10, 4, 46, NULL, 149, 150, NULL, 5.00, NULL, NULL, 1, NULL, NULL, NULL, '2020-09-16 13:21:34'),
(11, 0, 47, NULL, 153, 154, NULL, 5.00, NULL, NULL, 1, NULL, NULL, NULL, '2020-09-16 15:10:34'),
(12, 4, 48, NULL, 155, 156, NULL, 5.00, NULL, NULL, 1, NULL, NULL, NULL, '2020-09-16 15:25:06'),
(13, 0, 49, NULL, 157, 158, NULL, 5.00, NULL, NULL, 1, NULL, NULL, NULL, '2020-09-16 16:53:45'),
(14, 4, 50, NULL, 159, 160, NULL, 5.00, NULL, NULL, 1, NULL, NULL, NULL, '2020-09-17 15:15:47'),
(15, 4, 51, NULL, 161, 162, NULL, 170.00, NULL, NULL, 1, NULL, NULL, NULL, '2020-09-18 13:02:22'),
(16, 4, 52, NULL, 163, 164, NULL, 5.00, NULL, NULL, 1, NULL, NULL, NULL, '2020-09-21 14:15:13'),
(17, 4, 53, NULL, 165, 166, NULL, 5.00, NULL, NULL, 1, NULL, NULL, NULL, '2020-09-21 17:14:19'),
(18, 4, 54, NULL, 169, 170, NULL, 5.00, NULL, NULL, 1, NULL, NULL, NULL, '2020-09-22 09:13:37'),
(19, 4, 55, NULL, 171, 172, NULL, 5.00, NULL, NULL, 1, NULL, NULL, NULL, '2020-09-22 16:00:15'),
(20, 4, 56, NULL, 173, 174, NULL, 5.00, NULL, NULL, 1, NULL, NULL, NULL, '2020-09-23 10:30:26'),
(21, 4, 60, NULL, 183, 184, NULL, 150.00, NULL, NULL, 1, NULL, NULL, NULL, '2020-09-24 18:07:57'),
(22, 4, 61, NULL, 185, 186, NULL, 5.00, NULL, NULL, 1, NULL, NULL, NULL, '2020-09-25 15:20:04'),
(23, 4, 63, NULL, 189, 190, NULL, 5.00, NULL, NULL, 1, NULL, NULL, NULL, '2020-09-28 09:51:20'),
(24, 4, 64, NULL, 191, 192, NULL, 5.00, NULL, NULL, 1, NULL, NULL, NULL, '2020-09-28 10:12:56'),
(25, 4, 65, NULL, 195, 196, NULL, 5.00, NULL, NULL, 1, NULL, NULL, NULL, '2020-09-28 16:42:10'),
(26, 4, 66, NULL, 197, 198, NULL, 5.00, NULL, NULL, 1, NULL, NULL, NULL, '2020-09-30 14:07:28'),
(27, 4, 67, NULL, 199, 200, NULL, 5.00, NULL, NULL, 1, NULL, NULL, NULL, '2020-09-30 14:56:13');

-- --------------------------------------------------------

--
-- Table structure for table `patrimony`
--

CREATE TABLE `patrimony` (
  `id` int(11) NOT NULL,
  `description` varchar(200) NOT NULL,
  `quantity` int(11) NOT NULL,
  `register` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `patrimony`
--

INSERT INTO `patrimony` (`id`, `description`, `quantity`, `register`) VALUES
(1, 'CARRO FIAT UNO', 1, '2020-08-28 14:49:22');

-- --------------------------------------------------------

--
-- Table structure for table `payment_rules`
--

CREATE TABLE `payment_rules` (
  `id` int(11) NOT NULL,
  `description` varchar(500) NOT NULL,
  `type` int(11) DEFAULT NULL,
  `minimum` int(11) DEFAULT NULL,
  `maximum` int(11) DEFAULT NULL,
  `price` float(10,2) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `payment_rules`
--

INSERT INTO `payment_rules` (`id`, `description`, `type`, `minimum`, `maximum`, `price`) VALUES
(1, 'Contrato', 0, 0, 0, 3.00),
(0, 'Comum', 0, 0, 0, 5.00),
(3, '0 a 10', 0, 0, 10, 1.00),
(4, '11 a 20', 0, 11, 20, 0.75),
(5, '21 a 30', 0, 21, 30, 0.60),
(6, '31 a 90', 0, 31, 90, 0.50),
(7, '91 acima', 0, 91, 10000, 0.40),
(8, 'Taiobeiras à Salinas', 1, NULL, NULL, 15.00);

-- --------------------------------------------------------

--
-- Table structure for table `provider`
--

CREATE TABLE `provider` (
  `id` int(11) NOT NULL,
  `type_provider` int(11) NOT NULL,
  `register` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `provider`
--

INSERT INTO `provider` (`id`, `type_provider`, `register`) VALUES
(1, 1, '2020-08-21 09:34:36'),
(2, 2, '2020-08-21 09:47:10'),
(3, 2, '2020-08-21 10:26:05'),
(4, 2, '2020-08-21 10:37:43'),
(5, 1, '2020-08-21 12:26:18'),
(6, 1, '2020-08-21 12:34:35'),
(7, 1, '2020-08-21 12:51:15'),
(8, 2, '2020-08-21 12:56:28'),
(9, 2, '2020-08-21 15:56:00'),
(10, 1, '2020-08-24 17:35:51'),
(11, 1, '2020-08-28 09:40:11'),
(12, 1, '2020-08-28 14:47:41'),
(13, 2, '2020-08-29 09:22:19'),
(14, 1, '2020-09-01 10:53:45'),
(15, 1, '2020-09-01 16:21:45'),
(16, 1, '2020-09-03 17:05:39'),
(17, 2, '2020-09-09 08:23:02'),
(18, 2, '2020-09-23 15:00:42'),
(19, 1, '2020-09-25 16:25:49');

-- --------------------------------------------------------

--
-- Table structure for table `provider_address`
--

CREATE TABLE `provider_address` (
  `id` int(11) NOT NULL,
  `id_provider` int(11) NOT NULL,
  `zip_code` varchar(200) NOT NULL,
  `address` varchar(200) NOT NULL,
  `number` varchar(200) NOT NULL,
  `neighborhood` varchar(200) NOT NULL,
  `city` varchar(200) NOT NULL,
  `state` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `provider_address`
--

INSERT INTO `provider_address` (`id`, `id_provider`, `zip_code`, `address`, `number`, `neighborhood`, `city`, `state`) VALUES
(1, 1, '39550-000', 'Praça doutor José Americano mendes ', '145', 'Centro', 'Taiobeiras', ''),
(2, 2, '39550-000', 'SANTOS DUMONT', '619', 'VILA FORMOSA', 'Taiobeiras', 'MG'),
(3, 3, '30190-131', 'AV. BARBACENA', '1200', 'SANTO AGOSTINHO', 'BELO HORIZONTE', 'MG'),
(4, 4, '39550-000', 'RIO PARDO', '1119', 'Centro', 'Taiobeiras', 'MG'),
(5, 5, '39550-000', 'Tamois', '362', 'Nossa Senhora de Fatima ', 'Taiobeiras', 'mg'),
(6, 6, '39550-000', 'BELO HORIZONTE', '', 'VILA FORMOSA', 'Taiobeiras', 'MG'),
(7, 7, '39550-000', 'AV, CONTORNO ', '', '', 'TAIOBEIRAS', 'MG'),
(8, 8, '31250-115', 'JOSÉ BENEDITO ANTÃO ', '200', 'NOVA ESPERANÇA', 'BELO HORIZONTE', 'MG'),
(9, 9, '39550-000', 'TRAV MARTINHO REGO ', '52', 'CENTRO', 'Taiobeiras', 'MG'),
(10, 10, '39550-000', 'RIO PARDO', '769', 'Centro', 'Taiobeiras', 'MG'),
(11, 11, '39550-000', 'av contorno ', '', '', 'Taiobeiras', 'MG'),
(12, 12, '39550-000', 'SALINAS', '106', 'Centro', 'Taiobeiras', 'MG'),
(13, 13, '39550-000', 'av contorno ', '961', 'Centro', 'Taiobeiras', 'MG'),
(14, 14, '', '', '', '', 'Taiobeiras', 'Mg'),
(15, 15, '39550-000', 'JOVITA REGO', '40', '', 'Taiobeiras', 'Mg'),
(16, 16, '39560-000', 'CLEMENTE MEDRADO', '37', 'CENTRO', 'SALINAS', 'Mg'),
(17, 17, '39550-000', 'SAO ROMAO', '487', 'CENTRO', 'Taiobeiras', 'Mg'),
(18, 18, '39550-000', 'AV SAO JOAO ', '62', 'CENTRO', 'TAIOBEIRAS', 'MG'),
(19, 19, '', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `provider_bank_data`
--

CREATE TABLE `provider_bank_data` (
  `id` int(11) NOT NULL,
  `id_provider` int(11) NOT NULL,
  `cpf` varchar(200) NOT NULL,
  `agency` varchar(200) NOT NULL,
  `account_number` varchar(200) NOT NULL,
  `bank` varchar(200) NOT NULL,
  `account_type` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `provider_juridical`
--

CREATE TABLE `provider_juridical` (
  `id` int(11) NOT NULL,
  `id_provider` int(11) NOT NULL,
  `cnpj` varchar(200) NOT NULL,
  `legal_name` varchar(200) NOT NULL,
  `trade_name` varchar(200) NOT NULL,
  `answerable` varchar(200) NOT NULL,
  `phone` varchar(200) NOT NULL,
  `phone_fixed` varchar(200) NOT NULL,
  `email` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `provider_juridical`
--

INSERT INTO `provider_juridical` (`id`, `id_provider`, `cnpj`, `legal_name`, `trade_name`, `answerable`, `phone`, `phone_fixed`, `email`) VALUES
(1, 2, '11.539.200/0001-33', 'ENTER INFO INFORMATICA E SERVIÇOS LTDA', 'ENTER INFO', 'GIRLÂNIO', '(38) 99193-1603', '(38) 3845-2098', 'contato@enterinfo.com.br'),
(2, 3, '06.981.180/0001-16', 'CEMIG DISTRIBUIÇÃO S.A.', 'CEMIG', 'CEMIG', '', '', ''),
(3, 4, '08.760.011/0001-81', 'DIGITEC - COPIADORA E INFORMATICA EIRELI', 'DIGITEC', 'DIGITEC', '(38) 98827-1714', '', ''),
(4, 8, '62.395.546/0013-80', 'CAR-CENTRAL DE AUTOPEÇAS', 'CAR- CENTRAL ', 'DENILSON / MARILDA ', '', '(31) 9103-9088', ''),
(5, 9, '', 'CONTABILIDADE CORREIA DE TAIOBEIRAS LTDA', 'CONTABILIDADE CORREIA', 'MARDEM MIRANDA', '', '(38) 3845-1252', ''),
(6, 13, '07.584.792/0001-38', 'alinhalv', 'ALINHALV', 'alinhalv', '(38) 99109-0005', '(38) 9990-0534', ''),
(7, 17, '17.281.106/0001-03', 'COPASA', 'COPASA', '', '', '', ''),
(8, 18, '41.853.771/0001-09', 'GRAFICA', 'GRAFICA IMPRESSOGRAF', '', '(38) 99188-1650', '(38) 3845-1966', '');

-- --------------------------------------------------------

--
-- Table structure for table `provider_legal`
--

CREATE TABLE `provider_legal` (
  `id` int(11) NOT NULL,
  `id_provider` int(11) NOT NULL,
  `cpf` varchar(200) NOT NULL,
  `name` varchar(200) NOT NULL,
  `surname` varchar(200) NOT NULL,
  `phone` varchar(200) NOT NULL,
  `phone_fixed` varchar(200) NOT NULL,
  `email` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `provider_legal`
--

INSERT INTO `provider_legal` (`id`, `id_provider`, `cpf`, `name`, `surname`, `phone`, `phone_fixed`, `email`) VALUES
(1, 1, '', 'cicero/ ANE', 'ALUGUEL', '(38) 99189-1213', '', ''),
(2, 5, '103.795.226-08', 'Anderleny  ( leny ) ', 'Alves Marques', '', '', ''),
(3, 6, '', 'LUCAS ', 'SISTEMA', '(11) 98558-6531', '', ''),
(4, 7, '', 'EVERTON ', 'MECANICO', '', '', ''),
(5, 10, '', 'EVIDENCE', '', '(38) 99192-1631', '(38) 3845-1079', ''),
(6, 11, '', 'martelinho', 'de ouro', '', '', ''),
(7, 12, '', 'DODO', 'AUTO BRILHO', '(38) 99131-3335', '', ''),
(8, 14, '', 'Carla', 'diaz', '', '', ''),
(9, 15, '', 'DURÃO ', '', '(38) 99238-6243', '', ''),
(10, 16, '', 'RESTAURANTE', 'FAMILIAR', '', '(38) 3841-1206', ''),
(11, 19, '', 'LUCAS', 'RIBEIRO', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `recover_password`
--

CREATE TABLE `recover_password` (
  `id` int(11) NOT NULL,
  `email` varchar(100) NOT NULL,
  `token` varchar(100) NOT NULL,
  `data` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `shared_trips`
--

CREATE TABLE `shared_trips` (
  `id` int(11) NOT NULL,
  `id_biker` int(11) NOT NULL,
  `id_motorcycle` int(11) NOT NULL,
  `id_city_payment_rules` int(11) NOT NULL,
  `departure_adress` int(11) NOT NULL,
  `arrival_adress` int(11) NOT NULL,
  `departure_time` datetime NOT NULL,
  `arrival_time` datetime NOT NULL,
  `departure_odometer` varchar(20) NOT NULL,
  `arrival_odometer` varchar(20) NOT NULL,
  `status` int(11) NOT NULL,
  `register` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `status_client`
--

CREATE TABLE `status_client` (
  `id` int(11) NOT NULL,
  `description` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `status_client`
--

INSERT INTO `status_client` (`id`, `description`) VALUES
(1, 'ativo'),
(2, 'bloqueado');

-- --------------------------------------------------------

--
-- Table structure for table `status_packages`
--

CREATE TABLE `status_packages` (
  `id` int(11) NOT NULL,
  `description` varchar(200) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `status_packages`
--

INSERT INTO `status_packages` (`id`, `description`) VALUES
(1, 'Aguardando'),
(2, 'Em andamento'),
(3, 'Entregue');

-- --------------------------------------------------------

--
-- Table structure for table `status_trips`
--

CREATE TABLE `status_trips` (
  `id` int(11) NOT NULL,
  `description` varchar(50) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `status_trips`
--

INSERT INTO `status_trips` (`id`, `description`) VALUES
(1, 'Fila'),
(2, 'Agendamento'),
(3, 'Em andamento'),
(4, 'Encerrada'),
(5, 'Cancelado');

-- --------------------------------------------------------

--
-- Table structure for table `traffic_ticket`
--

CREATE TABLE `traffic_ticket` (
  `id` int(11) NOT NULL,
  `id_motorcycle` int(11) NOT NULL,
  `date_event` date NOT NULL,
  `location` varchar(200) NOT NULL,
  `type_infringement` varchar(200) NOT NULL,
  `amount` float NOT NULL,
  `id_biker` varchar(200) NOT NULL,
  `due_date` date NOT NULL,
  `register` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `trips`
--

CREATE TABLE `trips` (
  `id` int(11) NOT NULL,
  `id_client` int(11) NOT NULL,
  `id_biker` int(11) DEFAULT NULL,
  `id_motorcycle` int(11) DEFAULT NULL,
  `service` varchar(50) NOT NULL COMMENT 'Pessoa, Material, Compartilhado',
  `locale` enum('I','O') NOT NULL COMMENT 'Interno ou Fora da Cidade',
  `id_city` int(11) DEFAULT NULL,
  `km_provided` int(11) DEFAULT NULL,
  `expected_value` float(10,2) NOT NULL,
  `departure_adress` int(11) DEFAULT NULL,
  `arrival_adress` int(11) DEFAULT NULL,
  `schedule_trip` datetime DEFAULT NULL,
  `departure_time` datetime DEFAULT NULL,
  `arrival_time` datetime DEFAULT NULL,
  `departure_odometer` float(10,1) DEFAULT NULL,
  `arrival_odometer` float(10,1) DEFAULT NULL,
  `status` int(11) NOT NULL,
  `register` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `trips`
--

INSERT INTO `trips` (`id`, `id_client`, `id_biker`, `id_motorcycle`, `service`, `locale`, `id_city`, `km_provided`, `expected_value`, `departure_adress`, `arrival_adress`, `schedule_trip`, `departure_time`, `arrival_time`, `departure_odometer`, `arrival_odometer`, `status`, `register`) VALUES
(1, 4, 2, 1, 'Pessoa', 'I', NULL, NULL, 5.00, 3, 4, NULL, '2020-08-10 17:59:57', '2020-08-10 18:00:49', 60690.0, 60698.0, 4, '2020-08-10 17:59:57'),
(2, 4, 2, 4, 'Pessoa', 'I', NULL, NULL, 5.00, 5, 6, NULL, '2020-08-11 12:00:06', '2020-08-11 12:00:37', 14632.0, 14637.0, 4, '2020-08-11 12:00:06'),
(3, 0, 3, 4, 'Pessoa', 'I', NULL, NULL, 5.00, 7, 8, NULL, '2020-08-11 13:06:30', '2020-08-11 13:24:57', 20000.0, 20020.0, 5, '2020-08-11 13:06:30'),
(4, 13, 4, 3, 'Pessoa', 'I', NULL, NULL, 5.00, 9, 10, '2020-08-12 17:20:00', '2020-08-12 17:26:47', '2020-08-12 17:43:13', 13123.0, 13127.0, 4, '2020-08-12 15:28:30'),
(5, 4, 4, 3, 'Pessoa', 'I', NULL, NULL, 5.00, 11, 12, NULL, '2020-08-13 11:42:28', '2020-08-13 11:48:34', 13138.0, 13141.0, 4, '2020-08-13 11:42:28'),
(6, 4, 3, 7, 'Pessoa', 'I', NULL, NULL, 5.00, 13, 14, NULL, '2020-08-13 14:20:44', '2020-08-13 14:36:10', 6889.0, 6913.0, 4, '2020-08-13 14:20:44'),
(7, 0, 3, 7, 'Pessoa', 'I', NULL, NULL, 5.00, 25, 26, NULL, '2020-08-13 15:26:00', '2020-08-13 15:39:16', 6913.0, 6940.0, 4, '2020-08-13 15:15:12'),
(8, 4, 4, 3, 'Material', 'I', NULL, NULL, 5.00, 29, 30, NULL, '2020-08-14 17:56:38', '2020-08-14 18:07:09', 13150.0, 13153.0, 4, '2020-08-14 17:56:38'),
(9, 4, 4, 3, 'Pessoa', 'O', NULL, 0, 0.00, 31, 32, '2020-08-17 14:00:00', '2020-08-17 14:26:02', '2020-08-17 14:54:55', 13232.0, 13255.0, 4, '2020-08-17 11:21:34'),
(10, 4, 4, 3, 'Material', 'I', NULL, NULL, 3.00, 33, 34, NULL, '2020-08-17 12:09:56', '2020-08-17 12:10:22', 13222.0, 13225.0, 4, '2020-08-17 12:09:56'),
(11, 4, 4, 3, 'Pessoa', 'O', NULL, 0, 0.00, 35, 36, '2020-08-18 14:00:00', '2020-08-18 14:09:14', '2020-08-18 14:46:34', 13317.0, 13337.0, 4, '2020-08-18 11:32:50'),
(12, 4, 2, 1, 'Material', 'I', NULL, NULL, 3.00, 37, 38, NULL, '2020-08-18 12:14:43', '2020-08-18 12:15:06', 61123.0, 61126.0, 4, '2020-08-18 12:14:43'),
(13, 0, 4, 3, 'Pessoa', 'I', NULL, NULL, 5.00, 41, 42, NULL, '2020-08-18 15:26:35', '2020-08-18 15:38:33', 13337.0, 13340.0, 4, '2020-08-18 15:26:35'),
(14, 4, 2, 3, 'Material', 'I', NULL, NULL, 3.00, 43, 44, NULL, '2020-08-20 11:33:10', '2020-08-20 11:45:54', 13455.0, 13456.0, 4, '2020-08-20 11:33:10'),
(15, 4, 4, 3, 'Material', 'I', NULL, NULL, 3.00, 45, 46, NULL, '2020-08-21 18:24:13', '2020-08-21 18:25:16', 13518.0, 13522.0, 4, '2020-08-21 18:24:13'),
(16, 4, 2, 5, 'Material', 'I', NULL, NULL, 3.00, 47, 48, NULL, '2020-08-22 10:40:48', '2020-08-22 10:57:13', 60563.0, 60576.0, 4, '2020-08-22 10:40:48'),
(17, 4, 3, 7, 'Material', 'I', NULL, NULL, 3.00, 49, 50, NULL, '2020-08-22 20:24:56', '2020-08-22 20:25:20', 37475.0, 37478.0, 4, '2020-08-22 20:24:56'),
(18, 0, 4, 4, 'Material', 'I', NULL, NULL, 5.00, 53, 54, NULL, '2020-08-24 15:39:29', '2020-08-24 15:53:46', 15218.0, 15235.0, 4, '2020-08-24 15:39:29'),
(19, 0, 4, 3, 'Pessoa', 'I', NULL, NULL, 5.00, 57, 58, NULL, '2020-08-25 16:51:02', '2020-08-25 17:02:54', 13717.0, 13722.0, 4, '2020-08-25 16:51:02'),
(20, 4, 2, 7, 'Material', 'I', NULL, NULL, 5.00, 59, 60, NULL, '2020-08-25 21:40:26', '2020-08-25 21:40:42', 60648.0, 60652.0, 4, '2020-08-25 21:40:26'),
(21, 4, 6, 8, 'Material', 'I', NULL, NULL, 5.00, 61, 62, NULL, '2020-08-26 22:31:34', '2020-08-26 22:32:41', 11750.0, 11755.0, 4, '2020-08-26 22:31:34'),
(22, 0, 3, 3, 'Material', 'I', NULL, NULL, 5.00, 65, 66, NULL, '2020-08-27 08:18:11', '2020-08-27 08:33:56', 13779.0, 13782.0, 4, '2020-08-27 08:18:11'),
(23, 4, 2, 5, 'Pessoa', 'I', NULL, NULL, 5.00, 67, 68, NULL, '2020-08-27 20:37:38', '2020-08-27 20:46:57', 60714.0, 60718.0, 4, '2020-08-27 20:37:38'),
(24, 0, 4, 2, 'Pessoa', 'I', NULL, NULL, 5.00, 71, 72, NULL, '2020-08-28 09:49:35', '2020-08-28 10:33:38', 64018.0, 64024.0, 4, '2020-08-28 09:49:35'),
(25, 0, 4, 2, 'Pessoa', 'I', NULL, NULL, 5.00, 75, 76, NULL, '2020-08-28 10:37:26', '2020-08-28 10:51:24', 64024.0, 64028.0, 4, '2020-08-28 10:37:26'),
(26, 4, 2, 2, 'Pessoa', 'I', NULL, NULL, 5.00, 77, 78, NULL, '2020-08-28 13:18:36', '2020-08-28 13:25:21', 64032.0, 64037.0, 4, '2020-08-28 13:18:36'),
(27, 0, 2, 2, 'Material', 'I', NULL, NULL, 5.00, 79, 80, NULL, '2020-08-28 14:16:35', '2020-08-28 14:27:44', 64037.0, 64040.0, 4, '2020-08-28 14:16:35'),
(28, 18, 2, 5, 'Pessoa', 'I', NULL, NULL, 5.00, 81, 82, NULL, '2020-08-28 22:33:07', '2020-08-28 22:43:47', 60778.0, 60782.0, 4, '2020-08-28 22:33:07'),
(29, 18, 2, 5, 'Pessoa', 'I', NULL, NULL, 5.00, 83, 84, NULL, '2020-08-28 22:36:32', '2020-08-28 22:43:58', 60782.0, 60785.0, 4, '2020-08-28 22:36:32'),
(30, 0, 2, 5, 'Pessoa', 'O', NULL, 16, 15.00, 85, 86, NULL, '2020-08-31 15:27:18', '2020-08-31 15:56:12', 60957.0, 60982.0, 4, '2020-08-31 15:27:18'),
(31, 4, 2, 5, 'Pessoa', 'I', NULL, NULL, 5.00, 87, 88, NULL, '2020-08-31 18:28:14', '2020-08-31 18:28:30', 60982.0, 60990.0, 4, '2020-08-31 18:28:14'),
(32, 4, 6, 2, 'Pessoa', 'I', NULL, NULL, 5.00, 89, 90, NULL, '2020-09-01 23:26:39', '2020-09-02 00:01:42', 64304.0, 64311.0, 4, '2020-09-01 23:26:39'),
(33, 12, 5, 9, 'Compartilhado', 'O', 8, NULL, 0.00, 0, 0, NULL, '2020-09-02 10:30:06', '2020-09-05 12:25:25', 0.0, 0.0, 4, '2020-09-02 10:30:06'),
(34, 3, 4, 3, 'Material', 'I', NULL, NULL, 3.00, 115, 116, NULL, '2020-09-02 10:40:35', NULL, 14074.0, 14079.0, 5, '2020-09-02 10:40:35'),
(35, 4, 2, 5, 'Pessoa', 'I', NULL, NULL, 5.00, 119, 120, NULL, '2020-09-03 08:40:44', '2020-09-04 11:53:33', 61099.0, 61105.0, 4, '2020-09-03 08:40:44'),
(36, 4, 1, 4, 'Material', 'O', NULL, 50, 50.00, 123, 124, NULL, '2020-09-04 11:50:15', '2020-09-05 11:16:21', 15731.0, 15822.0, 4, '2020-09-04 11:50:15'),
(37, 4, 3, 7, 'Material', 'I', NULL, NULL, 3.00, 125, 126, NULL, '2020-09-04 19:10:26', '2020-09-04 19:17:51', 38801.0, 38804.0, 4, '2020-09-04 19:10:26'),
(38, 4, 3, 7, 'Material', 'I', NULL, NULL, 3.00, 127, 128, NULL, '2020-09-04 19:17:23', '2020-09-04 19:18:02', 38804.0, 38807.0, 4, '2020-09-04 19:17:23'),
(39, 0, 2, 5, 'Pessoa', 'I', NULL, NULL, 5.00, 131, 132, NULL, '2020-09-05 11:02:01', '2020-09-05 11:13:55', 61222.0, 61226.0, 4, '2020-09-05 11:02:01'),
(40, 0, 4, 3, 'Material', 'I', NULL, NULL, 5.00, 135, 136, NULL, '2020-09-05 11:45:38', '2020-09-05 12:08:12', 14192.0, 14200.0, 4, '2020-09-05 11:45:38'),
(41, 0, 2, 5, 'Pessoa', 'I', NULL, NULL, 0.00, 139, 140, NULL, '2020-09-05 12:27:49', '2020-09-08 16:13:51', 61236.0, 61240.0, 4, '2020-09-05 12:27:49'),
(42, 4, 2, 4, 'Material', 'O', NULL, 124, 50.00, 141, 142, NULL, '2020-09-08 14:18:06', '2020-09-08 16:13:28', 16000.0, 16126.0, 4, '2020-09-08 14:18:06'),
(43, 25, 2, 4, 'Material', 'O', NULL, 90, 50.00, 143, 144, NULL, '2020-09-09 13:02:10', '2020-09-10 08:38:00', 16174.0, 16276.0, 4, '2020-09-09 13:02:10'),
(44, 4, 1, 9, 'Pessoa', 'O', NULL, 100, 110.00, 145, 146, NULL, '2020-09-11 21:27:00', '2020-09-11 21:28:37', 123284.0, 123386.0, 4, '2020-09-11 21:27:00'),
(45, 0, 2, 0, 'Pessoa', 'I', NULL, NULL, 0.00, 147, 148, NULL, '2020-09-12 10:27:51', '2020-10-01 16:12:17', 61547.0, 61550.0, 4, '2020-09-12 10:27:51'),
(46, 4, 3, 7, 'Material', 'I', NULL, NULL, 5.00, 149, 150, NULL, '2020-09-16 13:21:34', '2020-09-16 13:34:02', 38622.0, 38626.0, 4, '2020-09-16 13:21:34'),
(47, 0, 3, 7, 'Material', 'I', NULL, NULL, 5.00, 153, 154, NULL, '2020-09-16 15:10:34', '2020-09-16 15:14:03', 38626.0, 38629.0, 4, '2020-09-16 15:10:34'),
(48, 4, 3, 7, 'Material', 'I', NULL, NULL, 5.00, 155, 156, NULL, '2020-09-16 15:25:06', '2020-09-16 15:37:02', 38629.0, 38632.0, 4, '2020-09-16 15:25:06'),
(49, 0, 3, 7, 'Material', 'I', NULL, NULL, 5.00, 157, 158, NULL, '2020-09-16 16:53:45', '2020-09-16 17:05:46', 38639.0, 38642.0, 4, '2020-09-16 16:53:45'),
(50, 4, 2, 5, 'Material', 'I', NULL, NULL, 5.00, 159, 160, NULL, '2020-09-17 15:15:47', '2020-09-17 15:31:53', 61842.0, 61846.0, 4, '2020-09-17 15:15:47'),
(51, 4, 9, 4, 'Material', 'O', NULL, 270, 170.00, 161, 162, NULL, '2020-09-18 13:02:22', '2020-09-18 17:57:15', 16620.0, 16899.0, 4, '2020-09-18 13:02:22'),
(52, 4, 2, 5, 'Material', 'I', NULL, NULL, 5.00, 163, 164, NULL, '2020-09-21 14:15:13', '2020-09-21 14:35:19', 62099.0, 62102.0, 4, '2020-09-21 14:15:13'),
(53, 4, 3, 3, 'Material', 'I', NULL, NULL, 5.00, 165, 166, NULL, '2020-09-21 17:14:19', '2020-09-21 17:22:21', 38907.0, 38909.0, 4, '2020-09-21 17:14:19'),
(54, 4, 4, 3, 'Material', 'I', NULL, NULL, 5.00, 169, 170, NULL, '2020-09-22 09:13:37', NULL, 15150.0, NULL, 5, '2020-09-22 09:13:37'),
(55, 4, 0, 6, 'Material', 'I', NULL, NULL, 5.00, 171, 172, NULL, '2020-09-22 16:00:15', '2020-10-01 16:12:38', 31814.0, 31816.0, 4, '2020-09-22 16:00:15'),
(56, 4, 3, 7, 'Material', 'I', NULL, NULL, 5.00, 173, 174, NULL, '2020-09-23 10:30:26', '2020-09-23 15:46:12', 38937.0, 38945.0, 4, '2020-09-23 10:30:26'),
(57, 4, 3, 7, 'Pessoa', 'I', NULL, NULL, 5.00, 175, 176, NULL, '2020-09-23 15:27:55', '2020-09-25 13:49:04', 38955.0, 38960.0, 4, '2020-09-23 15:27:55'),
(58, 4, 4, 3, 'Pessoa', 'I', NULL, NULL, 5.00, 179, 180, NULL, '2020-09-24 11:10:26', '2020-09-24 11:10:45', 15251.0, 15255.0, 4, '2020-09-24 11:10:26'),
(59, 4, 3, 7, 'Pessoa', 'I', NULL, NULL, 5.00, 181, 182, NULL, '2020-09-24 17:20:55', '2020-09-24 17:22:29', 39025.0, 39030.0, 4, '2020-09-24 17:20:55'),
(60, 4, 9, 2, 'Material', 'O', NULL, 200, 150.00, 183, 184, NULL, '2020-09-24 18:07:57', '2020-09-25 13:49:24', 65000.0, 65209.0, 4, '2020-09-24 18:07:57'),
(61, 4, 11, 9, 'Material', 'I', NULL, NULL, 5.00, 185, 186, NULL, '2020-09-25 15:20:04', '2020-09-26 10:22:54', 12038.0, 12041.0, 4, '2020-09-25 15:20:04'),
(62, 4, 3, 7, 'Pessoa', 'I', NULL, NULL, 5.00, 187, 188, NULL, '2020-09-26 10:12:44', '2020-09-26 10:22:35', 39082.0, 39027.0, 4, '2020-09-26 10:12:44'),
(63, 4, 3, 8, 'Material', 'I', NULL, NULL, 5.00, 189, 190, NULL, '2020-09-28 09:51:20', '2020-09-28 10:11:41', 12058.0, 12062.0, 4, '2020-09-28 09:51:20'),
(64, 4, 3, 8, 'Material', 'I', NULL, NULL, 5.00, 191, 192, NULL, '2020-09-28 10:12:56', '2020-09-28 10:17:32', 12062.0, 12064.0, 4, '2020-09-28 10:12:56'),
(65, 4, 11, 8, 'Material', 'I', NULL, NULL, 5.00, 195, 196, NULL, '2020-09-28 16:42:10', '2020-09-28 16:50:29', 12074.0, 12080.0, 4, '2020-09-28 16:42:10'),
(66, 4, 12, 6, 'Material', 'I', NULL, NULL, 5.00, 197, 198, NULL, '2020-09-30 14:07:28', '2020-09-30 14:24:17', 31911.0, 31915.0, 4, '2020-09-30 14:07:28'),
(67, 4, 4, 3, 'Material', 'I', NULL, NULL, 5.00, 199, 200, NULL, '2020-09-30 14:56:13', '2020-09-30 14:56:38', 15655.0, 15657.0, 4, '2020-09-30 14:56:13'),
(68, 4, 2, 5, 'Material', 'I', NULL, NULL, 5.00, 201, 202, NULL, '2020-10-01 15:51:35', '2020-10-01 16:11:48', 62993.0, 62999.0, 4, '2020-10-01 15:51:35'),
(69, 4, 2, 6, 'Material', 'I', NULL, NULL, 5.00, 203, 204, NULL, '2020-10-02 09:35:55', '2020-10-02 09:50:12', 62992.0, 62995.0, 4, '2020-10-02 09:35:55'),
(70, 4, 11, 2, 'Material', 'O', NULL, 100, 75.00, 205, 206, NULL, '2020-10-02 09:52:02', '2020-10-02 09:52:34', 65372.0, 65476.0, 4, '2020-10-02 09:52:02'),
(71, 4, 3, 7, 'Material', 'I', NULL, NULL, 5.00, 207, 208, NULL, '2020-10-02 17:40:45', '2020-10-02 17:52:03', 39442.0, 39444.0, 4, '2020-10-02 17:40:45'),
(72, 4, 3, 7, 'Pessoa', 'I', NULL, NULL, 5.00, 211, 212, NULL, '2020-10-03 11:20:15', '2020-10-03 11:32:05', 39482.0, 39486.0, 4, '2020-10-03 11:20:15'),
(73, 4, 3, 7, 'Material', 'I', NULL, NULL, 5.00, 213, 214, NULL, '2020-10-03 12:47:50', '2020-10-03 13:02:12', 39495.0, 39502.0, 4, '2020-10-03 12:47:50');

-- --------------------------------------------------------

--
-- Table structure for table `type_client`
--

CREATE TABLE `type_client` (
  `id` int(11) NOT NULL,
  `description` varchar(50) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `type_client`
--

INSERT INTO `type_client` (`id`, `description`) VALUES
(1, 'Pessoa física'),
(2, 'Pessoa jurídica');

-- --------------------------------------------------------

--
-- Table structure for table `type_document`
--

CREATE TABLE `type_document` (
  `id` int(11) NOT NULL,
  `description` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `type_document`
--

INSERT INTO `type_document` (`id`, `description`) VALUES
(1, 'CPF FRENTE'),
(2, 'CPF VERSO'),
(3, 'RG FRENTE'),
(4, 'RG VERSO'),
(5, 'CNH FRENTE'),
(6, 'CNH VERSO'),
(7, 'COMPROVANTE DE RESIDENCIA');

-- --------------------------------------------------------

--
-- Table structure for table `type_register`
--

CREATE TABLE `type_register` (
  `id` int(11) NOT NULL,
  `profile` int(11) NOT NULL,
  `description` varchar(50) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `type_register`
--

INSERT INTO `type_register` (`id`, `profile`, `description`) VALUES
(1, 1, 'Administrador'),
(2, 2, 'Vendedor');

-- --------------------------------------------------------

--
-- Table structure for table `where_from`
--

CREATE TABLE `where_from` (
  `id` int(11) NOT NULL,
  `description` varchar(100) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `where_from`
--

INSERT INTO `where_from` (`id`, `description`) VALUES
(1, 'Facebook'),
(2, 'Instagram'),
(3, 'Indicação'),
(4, 'Panfleto'),
(5, 'Muro');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `address`
--
ALTER TABLE `address`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bikers`
--
ALTER TABLE `bikers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bills_pay`
--
ALTER TABLE `bills_pay`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bills_payments`
--
ALTER TABLE `bills_payments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bills_received`
--
ALTER TABLE `bills_received`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `blood_type`
--
ALTER TABLE `blood_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `clients`
--
ALTER TABLE `clients`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contracts`
--
ALTER TABLE `contracts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `document_biker`
--
ALTER TABLE `document_biker`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gasoline_control`
--
ALTER TABLE `gasoline_control`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `itens_bills_received`
--
ALTER TABLE `itens_bills_received`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `juridical_person`
--
ALTER TABLE `juridical_person`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `legal_person`
--
ALTER TABLE `legal_person`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `maintenance_control`
--
ALTER TABLE `maintenance_control`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `motorcycles`
--
ALTER TABLE `motorcycles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `night_trips`
--
ALTER TABLE `night_trips`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `occurrence`
--
ALTER TABLE `occurrence`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `packages`
--
ALTER TABLE `packages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `patrimony`
--
ALTER TABLE `patrimony`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `payment_rules`
--
ALTER TABLE `payment_rules`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `provider`
--
ALTER TABLE `provider`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `provider_address`
--
ALTER TABLE `provider_address`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `provider_bank_data`
--
ALTER TABLE `provider_bank_data`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `provider_juridical`
--
ALTER TABLE `provider_juridical`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `provider_legal`
--
ALTER TABLE `provider_legal`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `recover_password`
--
ALTER TABLE `recover_password`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shared_trips`
--
ALTER TABLE `shared_trips`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `status_client`
--
ALTER TABLE `status_client`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `status_packages`
--
ALTER TABLE `status_packages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `status_trips`
--
ALTER TABLE `status_trips`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `traffic_ticket`
--
ALTER TABLE `traffic_ticket`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `trips`
--
ALTER TABLE `trips`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `type_client`
--
ALTER TABLE `type_client`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `type_document`
--
ALTER TABLE `type_document`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `type_register`
--
ALTER TABLE `type_register`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `where_from`
--
ALTER TABLE `where_from`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `address`
--
ALTER TABLE `address`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=215;

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `bikers`
--
ALTER TABLE `bikers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `bills_pay`
--
ALTER TABLE `bills_pay`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `bills_payments`
--
ALTER TABLE `bills_payments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `bills_received`
--
ALTER TABLE `bills_received`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=72;

--
-- AUTO_INCREMENT for table `blood_type`
--
ALTER TABLE `blood_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `clients`
--
ALTER TABLE `clients`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT for table `contracts`
--
ALTER TABLE `contracts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `document_biker`
--
ALTER TABLE `document_biker`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `gasoline_control`
--
ALTER TABLE `gasoline_control`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=120;

--
-- AUTO_INCREMENT for table `itens_bills_received`
--
ALTER TABLE `itens_bills_received`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=669;

--
-- AUTO_INCREMENT for table `juridical_person`
--
ALTER TABLE `juridical_person`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `legal_person`
--
ALTER TABLE `legal_person`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `maintenance_control`
--
ALTER TABLE `maintenance_control`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `motorcycles`
--
ALTER TABLE `motorcycles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `night_trips`
--
ALTER TABLE `night_trips`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=700;

--
-- AUTO_INCREMENT for table `occurrence`
--
ALTER TABLE `occurrence`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `packages`
--
ALTER TABLE `packages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `patrimony`
--
ALTER TABLE `patrimony`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `payment_rules`
--
ALTER TABLE `payment_rules`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `provider`
--
ALTER TABLE `provider`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `provider_address`
--
ALTER TABLE `provider_address`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `provider_bank_data`
--
ALTER TABLE `provider_bank_data`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `provider_juridical`
--
ALTER TABLE `provider_juridical`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `provider_legal`
--
ALTER TABLE `provider_legal`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `recover_password`
--
ALTER TABLE `recover_password`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `shared_trips`
--
ALTER TABLE `shared_trips`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `status_client`
--
ALTER TABLE `status_client`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `status_packages`
--
ALTER TABLE `status_packages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `traffic_ticket`
--
ALTER TABLE `traffic_ticket`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `trips`
--
ALTER TABLE `trips`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=74;

--
-- AUTO_INCREMENT for table `type_client`
--
ALTER TABLE `type_client`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `type_document`
--
ALTER TABLE `type_document`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `type_register`
--
ALTER TABLE `type_register`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `where_from`
--
ALTER TABLE `where_from`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
