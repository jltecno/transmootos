<?php

class Shared_trips_model extends CI_Model {

  public $table = 'shared_trips';

  public function __construct(){
      parent::__construct();
  }

  public function insert($posts, $id_departure, $id_arrival){

    date_default_timezone_set('America/Sao_Paulo');
    $departure_time = date('Y-m-d H:i:s');
    $data = array(
      'id_biker' => (isset($posts['biker'])?$posts['biker']:null),
      'id_motorcycle' => (isset($posts['motorcycle'])?$posts['motorcycle']:null),
      'id_city_payment_rules' => $posts['id_city'],
      'departure_adress' => $id_departure,
      'arrival_adress' => $id_arrival,
      'departure_time' => $departure_time,
      // 'arrival_time' => (isset($departure_time)?$departure_time:null),
      'departure_odometer' => $posts['departure_odometer'],
      // 'arrival_odometer' => (isset($odometer)?$odometer:null),
      'status' => 1,
    );
    $this->db->insert('shared_trips', $data);
    return $this->db->insert_id();
  }

  public function insert_itens_shared_trips($posts, $trip){
    $package = $posts['package'];
    foreach ($package as $key => $value) {
      $data = array(
        'id_shared_trips' => $trip,
        'id_package' => $value,
        'status' => 1,
      );
      $this->db->insert('itens_shared_trips', $data);
    }
    return $this->db->insert_id();
  }

  public function fetch_shared_progress(){
    $this->db->select('t.*,b.name, b.surname, m.license_plate, m.code_motorcycle');
    $this->db->from('shared_trips as t');
    $this->db->join('motorcycles as m', 't.id_motorcycle = m.id');
    $this->db->join('bikers as b', 't.id_biker = b.id');
    $this->db->where('t.status', 1);
    $query = $this->db->get();
    return $query->result();
  }

  public function stop_shared_trip($posts){
    date_default_timezone_set('America/Sao_Paulo');
    $arrival_date = date('Y-m-d H:i:s');

    $this->db->set('arrival_odometer', $posts['arrival_odometer']);
    $this->db->set('arrival_time', $arrival_date);
    $this->db->set('status', 2);
    $this->db->where('id', $posts['id_trip']);
    $this->db->update('shared_trips');
  }

  public function update_itens_shared_trips($posts){
    foreach ($posts['package'] as $key => $value) {
      $this->db->set('status', 2);
      $this->db->where('id_shared_trips', $posts['id_trip']);
      $this->db->where('id_package', $value);
      $this->db->update('itens_shared_trips');
    }
  }

  public function update_shared_trips($posts){
    $this->db->set('id_biker', $posts['biker']);
    $this->db->set('id_motorcycle', $posts['motorcycle_shared']);
    $this->db->set('departure_odometer', $posts['departure_odometer']);
    $this->db->set('arrival_odometer', $posts['arrival_odometer']);
    $this->db->where('id', $posts['id_shared_trip']);
    $this->db->update('shared_trips');
  }

  public function fetch_itens_shared_trips($posts){
    $this->db->select('t.*');
    $this->db->from('itens_shared_trips as t');
    $this->db->where('t.id_shared_trips', $posts['id_trip']);
    $this->db->where('t.status', 1);
    // $this->db->or_where('t.status', 1);
    $query = $this->db->get();
    return $query->result();
  }

  public function fetch_shared_closed(){
    $this->db->select('t.id, t.departure_odometer, t.arrival_odometer, t.departure_time, t.arrival_time');
    $this->db->from('shared_trips as t');
    $this->db->where('t.status', 2);
    $query = $this->db->get();
    return $query->result();
  }

  public function fetch_shared_detail_closed($posts){
    $this->db->select('t.id, t.departure_odometer, t.arrival_odometer, t.departure_time, t.arrival_time,
      b.id as id_biker, b.name as biker_name, b.surname as biker_surname,
      m.id as id_motorcycle, m.license_plate, m.code_motorcycle,
    ');
    $this->db->from('shared_trips as t');
    $this->db->join('bikers as b', 't.id_biker = b.id');
    $this->db->join('motorcycles as m', 't.id_motorcycle = m.id');
    $this->db->where('t.id', $posts['id']);
    $query = $this->db->get();
    return $query->row();
  }

  public function fetch_shared_trip_modal($posts){
    $this->db->select('
      p.id, p.register,
      c.contract, c.type_client,
      l.name, l.surname, l.phone as legal_phone,
      j.trade_name, j.phone as juridical_phone,
    ');
    $this->db->from('itens_shared_trips as i');
    $this->db->join('packages as p', 'i.id_package = p.id');
    $this->db->join('clients as c', 'p.id_client = c.id', 'left');
    $this->db->join('legal_person as l', 'p.id_client = l.id_client', 'left');
    $this->db->join('juridical_person as j', 'p.id_client = j.id_client', 'left');
    $this->db->where('i.id_shared_trips', $posts['id_trip']);
    $this->db->where('i.status', 2);
    // $this->db->where('p.id_package', $posts['id_package']);
    $query = $this->db->get();
    return $query->result();
  }

}
