<?php

class traffic_ticket_model extends CI_Model {

  public $table = 'traffic_ticket';

  public function __construct(){
      parent::__construct();
  }

  public function insert($posts){
    $data = array(
      'id_motorcycle' => $_SESSION['id_motorcycle'],
      'date_event' => $posts['date_event'],
      'location' => $posts['location'],
      'type_infringement' => $posts['type_infringement'],
      'amount' => $posts['amount'],
      'id_biker' => $posts['biker'],
      'due_date' => $posts['due_date'],
    );
    $this->db->insert('traffic_ticket', $data);
    return $this->db->insert_id();
  }

  public function fetch_traffic_ticket($id){
    $this->db->select('t.id, t.date_event, t.type_infringement, t.amount, t.due_date, b.name, b.surname');
    $this->db->from('traffic_ticket as t');
    $this->db->join('bikers as b', 't.id_biker = b.id');
    $this->db->where('t.id_motorcycle', $id);
    $query = $this->db->get();
    return $query->result();
  }

  public function count_traffic_ticket(){
    $this->db->select('count(id) as count_traffic_ticket');
    $this->db->from('traffic_ticket');
    $query = $this->db->get();
    return $query->row();
  }

}
