<?php

class Maintenance_control_model extends CI_Model {

  public $table = 'maintenance_control';

  public function __construct(){
      parent::__construct();
  }

  public function insert($posts){
    $data = array(
      'id_motorcycle' => $_SESSION['id_motorcycle'],
      'odometer' => $posts['odometer'],
      'amount' => $posts['amount'],
      'description' => $posts['description'],
    );
    $this->db->insert('maintenance_control', $data);
    return $this->db->insert_id();
  }

  public function fetch_maintenance_control($id){
    $this->db->select('*');
    $this->db->from('maintenance_control');
    $this->db->where('id_motorcycle', $id);
    $query = $this->db->get();
    return $query->result();
  }

}
