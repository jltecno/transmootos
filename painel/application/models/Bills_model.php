<?php

class Bills_model extends CI_Model {

  public $table = 'bills_pay';

  public function __construct(){
      parent::__construct();
  }

  public function insert($posts){
    $data = array(
      'id_provider' => $posts['provider'],
      'name_bill' => $posts['name_bill'],
      'bill_value' => $posts['bill_value'],
      'type_bill' => $posts['type_bill'],
      'due_date' => $posts['due_date'],
      'paid_out' => '0',
    );
    $this->db->insert('bills_pay', $data);
    return $this->db->insert_id();
  }

  public function insert_payment($posts){
    $data = array(
      'id_bill' => $posts['id_bill']
    );
    $this->db->insert('bills_payments', $data);
    return $this->db->insert_id();
  }

  public function update_bill($posts){
    $this->db->set('paid_out', '1');
    $this->db->where('id', $posts['id_bill']);
    $this->db->update('bills_pay');
  }

  public function fetch_total_price(){
    $this->db->select_sum("bill_value");
    $this->db->from('bills_pay');
    $this->db->where('paid_out', '0');
    $query =$this->db->get();
    return $query->row();
  }

  public function fetch_bill_month(){
    $this->db->select_sum("bill_value");
    $this->db->from('bills_pay');
    $this->db->where('MONTH(due_date)', date('m'));
    $this->db->where('paid_out', '0');
    $query =$this->db->get();
    return $query->row();
  }

  public function fetch_bill_week(){
    $week = date('W');

    $this->db->select_sum("bill_value");
    $this->db->from('bills_pay');
    $this->db->where('WEEK(due_date)', $week - 1);
    $this->db->where('paid_out', '0');
    $query =$this->db->get();
    return $query->row();
  }

  public function fetch_bill_day(){
    $this->db->select_sum("bill_value");
    $this->db->from('bills_pay');
    $this->db->where('DATE(due_date)', date('Y-m-d'));
    $this->db->where('paid_out', '0');
    $query =$this->db->get();
    return $query->row();
  }

  public function fetch_fixed_accounts(){
    $this->db->select('b.id, b.name_bill, b.bill_value, b.due_date,
      j.trade_name,
      l.name, l.surname,
    ');
    $this->db->from('bills_pay as b');
    $this->db->join('provider as p', 'b.id_provider = p.id', 'left');
    $this->db->join('provider_juridical as j', 'j.id_provider = p.id', 'left');
    $this->db->join('provider_legal as l', 'l.id_provider = p.id', 'left');
    $this->db->where('type_bill', 'fixo');
    $query = $this->db->get();
    return $query->result();
  }

  public function fetch_variable_accounts(){
    $this->db->select('b.id, b.name_bill, b.bill_value, b.due_date,
      j.trade_name,
      l.name, l.surname,
    ');
    $this->db->from('bills_pay as b');
    $this->db->join('provider as p', 'b.id_provider = p.id', 'left');
    $this->db->join('provider_juridical as j', 'j.id_provider = p.id', 'left');
    $this->db->join('provider_legal as l', 'l.id_provider = p.id', 'left');
    $this->db->where('b.paid_out', '0');
    $this->db->where('b.type_bill', 'variavel');
    $query = $this->db->get();
    return $query->result();
  }

  public function fetch_bills_payments(){
    $this->db->select('b.id, b.name_bill, b.bill_value, b.due_date,
      j.trade_name,
      l.name, l.surname, bp.date as date_payment
    ');
    $this->db->from('bills_pay as b');
    $this->db->join('bills_payments as bp', 'b.id = bp.id_bill');
    $this->db->join('provider as p', 'b.id_provider = p.id', 'left');
    $this->db->join('provider_juridical as j', 'j.id_provider = p.id', 'left');
    $this->db->join('provider_legal as l', 'l.id_provider = p.id', 'left');
    $this->db->where('b.paid_out', '1');
    $query = $this->db->get();
    return $query->result();
  }

}
