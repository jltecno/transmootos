<?php

class Trips_model extends CI_Model {

  public $table = 'trips';

  public function __construct(){
      parent::__construct();
  }

  public function insert($posts, $id_departure, $id_arrival){
    $leave = $posts['leave'];
    // $odometer = null;
    date_default_timezone_set('America/Sao_Paulo');
    if ($leave == 'G') {
      $departure_time = date('Y-m-d H:i:s');
      $odometer = $posts['odometer'];
      $locale = $posts['locale'];
      $service = $posts['service_type'];
      $status = 3;
    } else if($leave == 'Q'){
      $departure_date = null;
      $locale = $posts['locale'];
      $service = $posts['service_type'];
      $status = 1;
    } else if($leave == 'S'){
      $schedule_trip = $posts['date_scheduling'].' '.$posts['hour_scheduling'].':00';
      $locale = $posts['locale'];
      $service = $posts['service_type'];
      $status = 2;
    } else if($leave == 'P'){
      $departure_time = date('Y-m-d H:i:s');
      $status = 3;
      $service = 'Compartilhado';
      $locale = 'O';
      $id_arrival = 0;
      $id_departure = 0;
      $odometer = $posts['departure_odometer'];
    }

    $data = array(
      'id_client' => (isset($posts['id_client'])) ? $posts['id_client'] : 0 ,
      'id_biker' => (isset($posts['biker'])?$posts['biker']:null),
      'id_motorcycle' => (isset($posts['motorcycle'])?$posts['motorcycle']:null),
      'service' => $service,
      'locale	' => $locale,
      'id_city	' => (isset($posts['id_city'])) ? $posts['id_city'] : null,
      'km_provided' => ($posts['locale'] == 'O') ? $posts['distance'] : null,
      'expected_value' => (isset($posts['price'])) ? $posts['price'] : 0,
      'departure_adress' => $id_departure,
      'arrival_adress' => $id_arrival,
      'departure_time' => (isset($departure_time)?$departure_time:null),
      'schedule_trip' => (isset($schedule_trip)?$schedule_trip:null),
      'departure_odometer' => (isset($odometer)?$odometer:null),
      'status' => $status,
    );
    $this->db->insert('trips', $data);
    return $this->db->insert_id();
  }

  public function cancel_trip($posts){
    $this->db->set('status', 5);
    $this->db->where('id', $posts['id_trip']);
    $this->db->update('trips');
  }

  public function update_trips_closed($posts){
    $this->db->set('id_biker', $posts['biker']);
    $this->db->set('id_motorcycle', $posts['motorcycle']);
    $this->db->set('departure_odometer', $posts['departure_odometer']);
    $this->db->set('arrival_odometer', $posts['arrival_odometer']);
    $this->db->where('id', $posts['id_trip']);
    $this->db->update('trips');
  }

  public function count_trips(){
    $this->db->select('
      (select count(id) as fila from trips where status = 1) as count_queue,
      (select count(id) as fila from trips where status = 2) as count_schedule,
      (select count(id) as fila from trips where status = 3) as count_in_progress,
      (select count(id) as fila from trips where status = 4) as count_closed,
    ');
    $this->db->from('trips');
    // $this->db->where('status', 1);
    $query = $this->db->get();
    return $query->row();
  }

  public function fetch_detail_trip($posts){
    $this->db->select('
      t.id, t.departure_adress, t.arrival_adress, t.register, t.locale, t.service, t.km_provided, t.expected_value, t.departure_time, t.departure_odometer, t.arrival_odometer,
      c.contract, c.type_client,
      l.name, l.surname, l.phone as legal_phone,
      j.trade_name, j.phone as juridical_phone,
      b.id as id_biker, b.name as biker_name, b.surname as biker_surname,
      m.id as id_motorcycle, m.license_plate, m.code_motorcycle, p.id as id_package'
    );
    $this->db->from('trips as t');
    $this->db->join('clients as c', 't.id_client = c.id', 'left');
    $this->db->join('legal_person as l', 't.id_client = l.id_client', 'left');
    $this->db->join('juridical_person as j', 't.id_client = j.id_client', 'left');
    $this->db->join('bikers as b', 't.id_biker = b.id', 'left');
    $this->db->join('motorcycles as m', 't.id_motorcycle = m.id', 'left');
    $this->db->join('packages as p', 't.id = p.id_trip', 'left');
    $this->db->where('t.id', $posts['id_trip']);
    $query = $this->db->get();
    return $query->row_array();
  }

  public function fetch_detail_trip_packages($posts){
    $this->db->select('
      t.id, t.register, t.departure_time, t.departure_odometer, t.arrival_odometer,
      b.name as biker_name, b.surname as biker_surname,
      m.license_plate, m.code_motorcycle'
    );
    $this->db->from('shared_trips as t');
    $this->db->join('bikers as b', 't.id_biker = b.id', 'left');
    $this->db->join('motorcycles as m', 't.id_motorcycle = m.id', 'left');
    $this->db->where('t.id', $posts['id_trip']);
    $query = $this->db->get();
    return $query->row_array();
  }

  public function fetch_in_progress(){
    $this->db->select('t.*,b.name, b.surname, m.license_plate, m.code_motorcycle');
    $this->db->from('trips as t');
    $this->db->join('motorcycles as m', 't.id_motorcycle = m.id', 'left');
    $this->db->join('bikers as b', 't.id_biker = b.id', 'left');
    $this->db->where('t.status', 3);
    $query = $this->db->get();
    return $query->result();
  }

  public function fetch_waiting(){
    $this->db->select('t.*');
    $this->db->from('trips as t');
    $this->db->where('t.status', 1);
    $query = $this->db->get();
    return $query->result();
  }

  public function fetch_closed(){
    $this->db->select('t.id, t.locale, t.departure_odometer, t.arrival_odometer, t.departure_time, t.arrival_time, t.service');
    $this->db->from('trips as t');
    $this->db->where('t.status', 4);
    $query = $this->db->get();
    return $query->result();
  }

  public function fetch_schedules(){
    $this->db->select('t.id, t.locale, t.service, t.km_provided, t.schedule_trip');
    $this->db->from('trips as t');
    $this->db->where('t.status', 2);
    $query = $this->db->get();
    return $query->result();
  }

  public function start_trip($posts){
    $departure_date = date('Y-m-d H:i:s');

    $this->db->set('id_biker', $posts['biker']);
    $this->db->set('id_motorcycle', $posts['motorcycle']);
    $this->db->set('departure_odometer', $posts['departure_odometer']);
    $this->db->set('departure_time', $departure_date);
    $this->db->set('status', 3);
    $this->db->where('id', $posts['id_trip']);
    $this->db->update('trips');
  }

  public function stop_trip($posts){
    $arrival_date = date('Y-m-d H:i:s');

    $this->db->set('arrival_odometer', $posts['arrival_odometer']);
    $this->db->set('arrival_time', $arrival_date);
    $this->db->set('status', 4);
    $this->db->where('id', $posts['id_trip']);
    $this->db->update('trips');
  }

  public function fetch_trips_client($id){
    $this->db->select('t.id, t.departure_time, t.arrival_time, t.id_client, t.service, t.locale, t.departure_adress, t.arrival_adress, t.departure_time, t.arrival_time, t.departure_odometer, t.arrival_odometer, b.name, b.surname, m.license_plate');
    $this->db->from('trips as t');
    $this->db->join('bikers as b', 't.id_biker = b.id');
    $this->db->join('motorcycles as m', 't.id_motorcycle = m.id');
    $this->db->where('t.id_client', $id);
    $this->db->where('t.status', 4);
    $query = $this->db->get();
    return $query->result();
  }

  public function fetch_trips_biker($id){
    $this->db->select('t.id, t.departure_time, t.arrival_time, t.id_client, t.service, t.locale, t.departure_adress, t.arrival_adress, t.departure_time, t.arrival_time, t.departure_odometer, t.arrival_odometer, l.name, l.surname, j.trade_name, m.license_plate');
    $this->db->from('trips as t');
    $this->db->join('legal_person as l', 't.id_client = l.id_client', 'left');
    $this->db->join('juridical_person as j', 't.id_client = j.id_client', 'left');
    $this->db->join('motorcycles as m', 't.id_motorcycle = m.id');
    $this->db->where('t.id_biker', $id);
    $this->db->where('t.status', 4);
    $query = $this->db->get();
    return $query->result();
  }

  public function fetch_trips_motorcycles($id){
    $this->db->select('t.departure_time, t.arrival_time, t.id_client, t.service, b.name, b.surname');
    $this->db->from('trips as t');
    $this->db->join('bikers as b', 't.id_biker = b.id');
    $this->db->where('t.id_motorcycle', $id);
    $query = $this->db->get();
    return $query->result();
  }

  public function fetch_trips_day(){
    $this->db->select('t.service, t.expected_value, m.license_plate');
    $this->db->from('trips as t');
    $this->db->join('motorcycles as m', 't.id_motorcycle = m.id');
    $this->db->where('date(t.departure_time)', date('Y-m-d'));
    $query = $this->db->get();
    return $query->result();
  }

  function fetch_trips_per_month(){
    $query = $this->db->query("SELECT count(id) as count_trips, sum(departure_odometer) as departure_odometer, sum(arrival_odometer) as arrival_odometer, extract(month from register) as month, extract(year from register) as year FROM trips where status = 4 group by extract(month from register),  extract(year from register)");
    return $query->result();
  }

  public function fetch_data_package($posts){
    $this->db->select('
    t.departure_odometer, t.departure_time,
    b.name as biker_name, b.surname as biker_surname,
    m.license_plate,
    ');
    $this->db->from('trips as t');
    $this->db->join('bikers as b', 't.id_biker = b.id');
    $this->db->join('motorcycles as m', 't.id_motorcycle = m.id');
    $this->db->where('t.id', $posts['id_trip']);
    $query = $this->db->get();
    return $query->row_array();
  }

  public function fetch_cancel_trips(){
    $this->db->select('t.*,
     c.contract, c.type_client, c.status as status_client,
     l.name, l.surname, l.phone as legal_phone,
     j.trade_name, j.phone as juridical_phone,
     ');
    $this->db->from('trips as t');
    $this->db->join('clients as c', 't.id_client = c.id', 'left');
    $this->db->join('legal_person as l', 't.id_client = l.id_client', 'left');
    $this->db->join('juridical_person as j', 't.id_client = j.id_client', 'left');
    $this->db->where('t.status', 5);
    $query = $this->db->get();
    return $query->result();
  }


}
