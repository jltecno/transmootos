<?php

class Packages_model extends CI_Model {

  public $table = 'packages';

  public function __construct(){
      parent::__construct();
  }

  public function insert_package($posts, $id_departure, $id_arrival, $id_trip){
    $data = array(
      'id_client' => $posts['id_client'],
      'id_trip' => $id_trip,
      'id_city_payment_rules' => (isset($posts['package_city'])?$posts['package_city']:null),
      'price' => (isset($posts['price'])?$posts['price']:null),
      'departure_address' => $id_departure,
      'arrival_address' => $id_arrival,
      'destinatary' => (isset($posts['destinatary'])?$posts['destinatary']:null),
      'receiver' => (isset($posts['receiver'])?$posts['receiver']:null),
      'people_authorized' => (isset($posts['people_authorized'])?$posts['people_authorized']:null),
      'observation' => (isset($posts['observation'])?$posts['observation']:null),
      'payment' => (isset($posts['payment'])?$posts['payment']:null),
      'status_it_payment' => (isset($posts['payment'])?($posts['payment'] == 'Remetente') ? 'pago' : 'nao_pago':null),
      'status' => 1,
    );
    $this->db->insert('packages', $data);
    return $this->db->insert_id();
  }

  public function start_trip_packages($posts, $trip){
    $package = $posts['package'];
    foreach ($package as $key => $value) {
      $this->db->set('id_trip', $trip);
      $this->db->set('status', 2);
      // $this->db->set('id_client', $posts['package']);
      $this->db->where('id', $value);
      $this->db->update('packages');
    }
  }

  public function fetch_waiting(){
    $this->db->select('
      p.id, pr.description as city, p.register,
      l.name, l.surname, l.phone as legal_phone,
      j.trade_name, j.phone as juridical_phone,
    ');
    $this->db->from('packages as p');
    $this->db->join('legal_person as l', 'p.id_client = l.id_client', 'left');
    $this->db->join('juridical_person as j', 'p.id_client = j.id_client', 'left');
    $this->db->join('payment_rules as pr', 'p.id_city_payment_rules = pr.id');
    $this->db->where('p.status', 1);
    $query = $this->db->get();
    return $query->result();
  }

  public function fetch_city_waiting($id){
    $this->db->select('
      p.id, pr.description as city,
      l.name, l.surname, l.phone as legal_phone, l.id as id_client,
      j.trade_name, j.phone as juridical_phone,
    ');
    $this->db->from('packages as p');
    $this->db->join('legal_person as l', 'p.id_client = l.id_client', 'left');
    $this->db->join('juridical_person as j', 'p.id_client = j.id_client', 'left');
    $this->db->join('payment_rules as pr', 'p.id_city_payment_rules = pr.id');
    $this->db->where('p.status', 1);
    $this->db->where('p.id_city_payment_rules', $id);
    $query = $this->db->get();
    return $query->result();
  }

  public function fetch_package($posts){
    $this->db->select('
      p.id, p.id_client, p.departure_address, p.arrival_address, p.id_trip, p.register, p.destinatary,
      c.contract, c.type_client,
      l.name, l.surname, l.phone as legal_phone,
      j.trade_name, j.phone as juridical_phone,
    ');
    $this->db->from('packages as p');
    $this->db->join('clients as c', 'p.id_client = c.id');
    $this->db->join('legal_person as l', 'p.id_client = l.id_client', 'left');
    $this->db->join('juridical_person as j', 'p.id_client = j.id_client', 'left');
    $this->db->where('p.id', $posts['id_trip']);
    // $this->db->where('p.id_package', $posts['id_package']);
    $query = $this->db->get();
    return $query->row();
  }

  public function fetch_packages_trip($posts){
    $this->db->select('
      p.id, p.id_client, p.departure_address, p.arrival_address, p.id_trip, p.register,
      p.destinatary, p.receiver, p.payment, p.status_it_payment, p.people_authorized, p.observation,
      c.contract, c.type_client,
      l.name, l.surname, l.phone as legal_phone,
      j.trade_name, j.phone as juridical_phone,
    ');
    $this->db->from('packages as p');
    $this->db->join('clients as c', 'p.id_client = c.id');
    $this->db->join('legal_person as l', 'p.id_client = l.id_client', 'left');
    $this->db->join('juridical_person as j', 'p.id_client = j.id_client', 'left');
    $this->db->where('p.id_trip', $posts['id_trip']);
    // $this->db->where('p.id_package', $posts['id_package']);
    $query = $this->db->get();
    return $query->result();
  }

  public function fetch_packages_trip_modal($posts){
    $this->db->select('
      p.id, p.register,
      c.contract, c.type_client,
      l.name, l.surname, l.phone as legal_phone,
      j.trade_name, j.phone as juridical_phone,
    ');
    $this->db->from('itens_shared_trips as i');
    $this->db->join('packages as p', 'i.id_package = p.id');
    $this->db->join('clients as c', 'p.id_client = c.id', 'left');
    $this->db->join('legal_person as l', 'p.id_client = l.id_client', 'left');
    $this->db->join('juridical_person as j', 'p.id_client = j.id_client', 'left');
    $this->db->where('i.id_shared_trips', $posts['id_trip']);
    // $this->db->where('p.id_package', $posts['id_package']);
    $query = $this->db->get();
    return $query->result();
  }

}
