<?php

class Patrimony_model extends CI_Model {

  public $table = 'motorcycles';

  public function __construct(){
      parent::__construct();
  }

  public function insert($posts){
    $data = array(
      'description' => $posts['description'],
      'quantity' => $posts['quantity'],
    );
    $this->db->insert('patrimony', $data);
    return $this->db->insert_id();
  }

  public function fetch_patrimony(){
    $this->db->select('description, quantity');
    $this->db->from('patrimony');
    $query = $this->db->get();
    return $query->result();
  }

}
