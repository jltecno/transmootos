<?php

class Payment_rules_model extends CI_Model {

  public $table = 'payment_rules';

  public function __construct(){
      parent::__construct();
  }

  public function update_payment_rule($posts){
    $this->db->set('description', $posts['description']);
    $this->db->set('minimum', $posts['minimum']);
    $this->db->set('maximum', $posts['maximum']);
    $this->db->set('price', $posts['price']);
    $this->db->where('id', $posts['id_payment_rules']);
    $this->db->update('payment_rules');
  }

  public function fetch_payment_rules(){
    $this->db->select('*');
    $this->db->from('payment_rules');
    $query = $this->db->get();
    return $query->result();
  }

  public function fetch_payment_rule($id){
    $this->db->select('*');
    $this->db->from('payment_rules');
    $this->db->where('id', $id);
    $query = $this->db->get();
    return $query->row();
  }

  public function fetch_price_inside($id){
    $this->db->select('price');
    $this->db->from('payment_rules');
    $this->db->where('id', $id);
    $query = $this->db->get();
    return $query->row();
  }

  public function fetch_price_outide($distance){
    $this->db->select('price');
    $this->db->from('payment_rules');
    $this->db->where('minimum <=', $distance);
    $this->db->where('maximum >=', $distance);
    $query = $this->db->get();
    return $query->row();
  }

  public function fetch_cities_payment(){
    $this->db->select('id, description');
    $this->db->from('payment_rules');
    $this->db->where('type', 1);
    $query = $this->db->get();
    return $query->result();
  }

  public function fetch_price_city($id){
    $this->db->select('price');
    $this->db->from('payment_rules');
    $this->db->where('id', $id);
    $query = $this->db->get();
    return $query->row();
  }

  public function fetch_price_contract($client){
    $this->db->select('price');
    $this->db->from('payment_rules');
    $this->db->where('id', 0);
    $query = $this->db->get();
    return $query->row();
  }

}
