<?php

class Reports_clients_model extends CI_Model {

  public $table = 'night_trips';

  public function __construct(){
      parent::__construct();
  }

  public function fetch_night_trips_client(){
    $this->db->select('sum(n.quantity) as quantity_night_trips, j.trade_name, l.name');
    $this->db->from('night_trips as n');
    $this->db->join('clients as c', 'n.id_client = c.id');
    $this->db->join('juridical_person as j', 'c.id = j.id_client', 'left');
    $this->db->join('legal_person as l', 'c.id = l.id_client', 'left');
    $this->db->group_by(array('j.trade_name', 'l.name'));
    $query = $this->db->get();
    return $query->result();
  }

  public function fetch_night_trips_client_date($posts){
    $this->db->select('sum(n.quantity) as quantity_night_trips, j.trade_name, l.name, l.surname');
    $this->db->from('night_trips as n');
    $this->db->join('clients as c', 'n.id_client = c.id');
    $this->db->join('juridical_person as j', 'c.id = j.id_client', 'left');
    $this->db->join('legal_person as l', 'c.id = l.id_client', 'left');
    $this->db->group_by(array('j.trade_name', 'l.name'));
    if ($posts['first_date'] != 0 && $posts['last_date'] != 0) {
      $this->db->where('DATE(n.date) >=', $posts['first_date']);
      $this->db->where('DATE(n.date) <=', $posts['last_date']);
    }
    $query = $this->db->get();
    return $query->result();
  }


}
