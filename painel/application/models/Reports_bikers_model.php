<?php

class Reports_bikers_model extends CI_Model {

  public $table = 'night_trips';

  public function __construct(){
      parent::__construct();
  }

  public function fetch_night_trips_km(){
    $this->db->select('sum(n.quantity) as quantity_night_trips, (sum(arrival_odometer) - sum(departure_odometer)) as km, sum(n.biker_commission) as commission, b.name, b.surname');
    $this->db->from('night_trips as n');
    $this->db->join('bikers as b', 'n.id_biker = b.id');
    $this->db->group_by(array('b.name'));
    $query = $this->db->get();
    return $query->result();
  }

  public function fetch_night_trips_km_date($posts){
    $this->db->select('sum(n.quantity) as quantity_night_trips, (sum(arrival_odometer) - sum(departure_odometer)) as km, sum(n.biker_commission) as commission,, b.name, b.surname');
    $this->db->from('night_trips as n');
    $this->db->join('bikers as b', 'n.id_biker = b.id');
    $this->db->group_by(array('b.name'));
    if ($posts['first_date'] != 0 && $posts['last_date'] != 0) {
      $this->db->where('DATE(n.date) >=', $posts['first_date']);
      $this->db->where('DATE(n.date) <=', $posts['last_date']);
    }
    $query = $this->db->get();
    return $query->result();
  }

  public function fetch_trips_separate(){
    $this->db->select('count(t.id) as quantity, (sum(t.arrival_odometer) - sum(t.departure_odometer)) as km, b.name, b.surname');
    $this->db->from('trips as t');
    $this->db->join('bikers as b', 't.id_biker = b.id');
    $this->db->group_by(array('b.name'));
    $this->db->where('t.status', 4);
    $query = $this->db->get();
    return $query->result();
  }

  public function fetch_trips_separate_date($posts){
    $this->db->select('count(t.id) as quantity, (sum(t.arrival_odometer) - sum(t.departure_odometer)) as km, b.name, b.surname');
    $this->db->from('trips as t');
    $this->db->join('bikers as b', 't.id_biker = b.id');
    $this->db->group_by(array('b.name'));
    $this->db->where('t.status', 4);
    if ($posts['first_date'] != 0 && $posts['last_date'] != 0) {
      $this->db->where('DATE(t.register) >=', $posts['first_date']);
      $this->db->where('DATE(t.register) <=', $posts['last_date']);
    }
    $query = $this->db->get();
    return $query->result();
  }

  public function fetch_trips_shared(){
    $this->db->select('count(t.id) as quantity, (sum(t.arrival_odometer) - sum(t.departure_odometer)) as km, b.name, b.surname');
    $this->db->from('shared_trips as t');
    $this->db->join('bikers as b', 't.id_biker = b.id');
    $this->db->group_by(array('b.name'));
    $this->db->where('t.status', 2);
    $query = $this->db->get();
    return $query->result();
  }

  public function fetch_trips_shared_date($posts){
    $this->db->select('count(t.id) as quantity, (sum(t.arrival_odometer) - sum(t.departure_odometer)) as km, b.name, b.surname');
    $this->db->from('shared_trips as t');
    $this->db->join('bikers as b', 't.id_biker = b.id');
    $this->db->group_by(array('b.name'));
    $this->db->where('t.status', 2);
    if ($posts['first_date'] != 0 && $posts['last_date'] != 0) {
      $this->db->where('DATE(t.departure_time) >=', $posts['first_date']);
      $this->db->where('DATE(t.departure_time) <=', $posts['last_date']);
    }
    $query = $this->db->get();
    return $query->result();
  }

}
