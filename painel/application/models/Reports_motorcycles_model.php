<?php

class Reports_motorcycles_model extends CI_Model {

  public $table = 'night_trips';

  public function __construct(){
      parent::__construct();
  }

  public function fetch_night_trips_motorcycles(){
    $this->db->select('sum(n.quantity) as quantity_night_trips, m.code_motorcycle, m.license_plate');
    $this->db->from('night_trips as n');
    $this->db->join('motorcycles as m', 'n.id_motorcycle = m.id');
    $this->db->group_by(array('m.code_motorcycle'));
    $query = $this->db->get();
    return $query->result();
  }

  public function fetch_night_trips_km(){
    $this->db->select('sum(n.quantity) as quantity_night_trips, (sum(arrival_odometer) - sum(departure_odometer)) as km, m.code_motorcycle, m.license_plate');
    $this->db->from('night_trips as n');
    $this->db->join('motorcycles as m', 'n.id_motorcycle = m.id');
    $this->db->group_by(array('m.code_motorcycle'));
    $query = $this->db->get();
    return $query->result();
  }

  public function fetch_night_trips_km_date($posts){
    $this->db->select('sum(n.quantity) as quantity_night_trips, (sum(arrival_odometer) - sum(departure_odometer)) as km, m.code_motorcycle, m.license_plate');
    $this->db->from('night_trips as n');
    $this->db->join('motorcycles as m', 'n.id_motorcycle = m.id');
    $this->db->group_by(array('m.code_motorcycle'));
    if ($posts['first_date'] != 0 && $posts['last_date'] != 0) {
      $this->db->where('DATE(n.date) >=', $posts['first_date']);
      $this->db->where('DATE(n.date) <=', $posts['last_date']);
    }
    $query = $this->db->get();
    return $query->result();
  }

  public function fetch_trips_shared(){
    $this->db->select('count(t.id) as quantity, (sum(t.arrival_odometer) - sum(t.departure_odometer)) as km, m.code_motorcycle, m.license_plate');
    $this->db->from('shared_trips as t');
    $this->db->join('motorcycles as m', 't.id_motorcycle = m.id');
    $this->db->group_by(array('m.code_motorcycle'));
    $this->db->where('t.status', 2);
    $query = $this->db->get();
    return $query->result();
  }

  public function fetch_trips_shared_date($posts){
    $this->db->select('count(t.id) as quantity, (sum(t.arrival_odometer) - sum(t.departure_odometer)) as km, m.code_motorcycle, m.license_plate');
    $this->db->from('shared_trips as t');
    $this->db->join('motorcycles as m', 't.id_motorcycle = m.id');
    $this->db->group_by(array('m.code_motorcycle'));
    $this->db->where('t.status', 2);
    if ($posts['first_date'] != 0 && $posts['last_date'] != 0) {
      $this->db->where('DATE(t.departure_time) >=', $posts['first_date']);
      $this->db->where('DATE(t.departure_time) <=', $posts['last_date']);
    }
    $query = $this->db->get();
    return $query->result();
  }

}
