<?php

class Motorcycles_model extends CI_Model {

  public $table = 'motorcycles';

  public function __construct(){
      parent::__construct();
  }

  public function insert($posts, $image){
    $data = array(
      'brand' => $posts['brand'],
      'model' => $posts['model'],
      'color' => $posts['color'],
      'license_plate' => $posts['license_plate'],
      'code_motorcycle' => $posts['code_motorcycle'],
      'year_motorcycle' => $posts['year_motorcycle'],
      'image' => $image,
    );
    $this->db->insert('motorcycles', $data);
    return $this->db->insert_id();
  }

  public function insert_occurrence($posts){
    $data = array(
      'id_motorcycle' => $_SESSION['id_motorcycle'],
      'id_biker' => $posts['id_biker'],
      'occurrence' => $posts['occurrence'],
      'status_occurrence' => 'Em espera',
    );
    $this->db->insert('occurrence', $data);
    return $this->db->insert_id();
  }

  public function update_image($id, $image){
    $this->db->set('image', $image);
    $this->db->where('id', $id);
    $this->db->update('motorcycles');
  }

  public function modify_motorcycle($posts){
    $this->db->set('brand', $posts['brand']);
    $this->db->set('model', $posts['model']);
    $this->db->set('color', $posts['color']);
    $this->db->set('license_plate', $posts['license_plate']);
    $this->db->set('code_motorcycle', $posts['code_motorcycle']);
    $this->db->set('year_motorcycle', $posts['year_motorcycle']);
    $this->db->where('id', $posts['id_motorcycle']);
    $this->db->update('motorcycles');
  }

  public function fetch_motorcycles(){
    $this->db->select('*');
    $this->db->from('motorcycles');
    $query = $this->db->get();
    return $query->result();
  }

  public function fetch_motorcycle($id){
    $this->db->select('*');
    $this->db->from('motorcycles');
    $this->db->where('id', $id);
    $query = $this->db->get();
    return $query->row();
  }

  public function fetch_ocurrences(){
    $this->db->select('o.id, o.id_motorcycle, o.id_biker, o.occurrence, o.status_occurrence, o.register, b.name, b.surname');
    $this->db->from('occurrence as o');
    $this->db->join('bikers as b', 'o.id_biker = b.id');
    $this->db->where('o.id_motorcycle', $_SESSION['id_motorcycle']);
    $query = $this->db->get();
    return $query->result();
  }

  public function update_status($posts){
    $this->db->set('status_occurrence', $posts['status_occurrence']);
    $this->db->where('id', $posts['id_ocurrence']);
    $this->db->update('occurrence');
  }

}
