<?php

class Bikers_model extends CI_Model {

  public $table = 'bikers';

  public function __construct(){
      parent::__construct();
  }

  public function insert($posts, $image){
    $data = array(
      'name' => $posts['name'],
      'surname' => $posts['surname'],
      'date_birthday' => $posts['date_birthday'],
      'phone' => $posts['phone'],
      'cpf' => $posts['cpf'],
      'license' => $posts['license'],
      'license_expiration' => $posts['license_expiration'],
      'blood_type' => $posts['type_blood'],
      'commission' => $posts['commission'],
      'image' => $image,
    );
    $this->db->insert('bikers', $data);
    return $this->db->insert_id();
  }

  public function insert_document($id, $image, $posts){
    $data = array(
      'id_biker' => $id,
      'document' => $image,
      'type_document' => $posts['type_document'],
    );

    $this->db->insert('document_biker', $data);
    return $this->db->insert_id();
  }

  public function modify_biker($posts){
    $this->db->set('name', $posts['name']);
    $this->db->set('surname', $posts['surname']);
    $this->db->set('date_birthday', $posts['date_birthday']);
    $this->db->set('phone', $posts['phone']);
    $this->db->set('cpf', $posts['cpf']);
    $this->db->set('license', $posts['license']);
    $this->db->set('license_expiration', $posts['license_expiration']);
    $this->db->set('blood_type', $posts['type_blood']);
    $this->db->set('commission', $posts['commission']);
    $this->db->where('id', $posts['id_biker']);
    $this->db->update('bikers');
  }

  public function update_image($posts, $image){
    $this->db->set('image', $image);
    $this->db->where('id', $posts['id']);
    $this->db->update('bikers');
  }

  public function fetch_bikers(){
    $this->db->select('*');
    $this->db->from('bikers');
    $query = $this->db->get();
    return $query->result();
  }

  public function fetch_biker($id){
    $this->db->select('b.*, t.description');
    $this->db->from('bikers as b');
    $this->db->join('blood_type as t', 'b.blood_type = t.id', 'left');
    $this->db->where('b.id', $id);
    $query = $this->db->get();
    return $query->row();
  }

  public function fetch_license_expiration(){
    $this->db->select('name, surname, license_expiration');
    $this->db->from('bikers');
    $query = $this->db->get();
    return $query->result();
  }

  public function fetch_documents($id){
    $this->db->select('d.id_biker, d.document, t.description');
    $this->db->from('document_biker as d');
    $this->db->join('type_document as t', 'd.type_document = t.id');
    $this->db->where('d.id_biker', $id);
    $query = $this->db->get();
    return $query->result();
  }

  public function fetch_type_documents(){
    $this->db->select('id, description');
    $this->db->from('type_document');
    $query = $this->db->get();
    return $query->result();
  }

  public function fetch_type_blood(){
    $this->db->select('id, description');
    $this->db->from('blood_type');
    $query = $this->db->get();
    return $query->result();
  }

  public function fetch_biker_commission($id){
    $this->db->select('commission');
    $this->db->from('bikers');
    $this->db->where('id', $id);
    $query = $this->db->get();
    return $query->row();
  }

}
