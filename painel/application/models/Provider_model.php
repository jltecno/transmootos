<?php

class Provider_model extends CI_Model {

  public $table = 'provider';

  public function __construct(){
      parent::__construct();
  }

  public function insert($posts){
    $data = array(
      'type_provider' => $posts['type_provider'],
    );
    $this->db->insert('provider', $data);
    return $this->db->insert_id();
  }

  public function insert_provider_legal($posts, $id_provider){
    $data = [
      'id_provider' => $id_provider,
      'id_provider' => $id_provider,
      'cpf' => $posts['cpf'],
      'name' => $posts['name'],
      'surname' => $posts['surname'],
      'phone' => $posts['phone'],
      'phone_fixed' => $posts['phone_fixed'],
      'email' => $posts['email'],
    ];
    $this->db->insert('provider_legal', $data);
    return $this->db->insert_id();
  }

  public function insert_provider_juridical($posts, $id_provider){
    $data = [
      'id_provider' => $id_provider,
      'cnpj' => $posts['cnpj'],
      'legal_name' => $posts['legal_name'],
      'trade_name' => $posts['trade_name'],
      'answerable' => $posts['answerable'],
      'phone' => $posts['phone'],
      'phone_fixed' => $posts['phone_fixed'],
      'email' => $posts['email'],
    ];
    $this->db->insert('provider_juridical', $data);
    return $this->db->insert_id();
  }

  public function insert_address($posts, $id_provider){
    $data = [
      'id_provider' => $id_provider,
      'zip_code	' => $posts['cep'],
      'address	' => $posts['address'],
      'number	' => $posts['number'],
      'neighborhood	' => $posts['neighborhood'],
      'city' => $posts['city'],
      'state' => $posts['uf'],
    ];
    $this->db->insert('provider_address', $data);
    return $this->db->insert_id();
  }

  public function insert_bank_data($posts){
    $data = [
      'id_provider' => $posts['id_provider'],
      'cpf' => $posts['cpf'],
      'agency' => $posts['agency'],
      'account_number' => $posts['account_number'],
      'bank' => $posts['bank'],
      'account_type' => $posts['type_account'],
    ];
    $this->db->insert('provider_bank_data', $data);
    return $this->db->insert_id();
  }

  public function update_legal_provider($posts, $id){
    $this->db->set('cpf', $posts['cpf']);
    $this->db->set('name', $posts['name']);
    $this->db->set('surname', $posts['surname']);
    $this->db->set('phone', $posts['phone']);
    $this->db->set('phone_fixed', $posts['phone_fixed']);
    $this->db->set('email', $posts['email']);
    $this->db->where('id_provider', $id);
    $this->db->update('provider_legal');
  }

  public function update_juridical_provider($posts, $id){
    $this->db->set('cnpj', $posts['cpf']);
    $this->db->set('legal_name', $posts['name']);
    $this->db->set('trade_name', $posts['surname']);
    $this->db->set('answerable', $posts['phone']);
    $this->db->set('phone', $posts['phone']);
    $this->db->set('phone_fixed', $posts['phone_fixed']);
    $this->db->set('email', $posts['email']);
    $this->db->where('id_provider', $id);
    $this->db->update('provider_juridical');
  }

  public function update_provider_address($posts, $id){
    $this->db->set('zip_code', $posts['cep']);
    $this->db->set('address', $posts['address']);
    $this->db->set('number', $posts['number']);
    $this->db->set('neighborhood', $posts['neighborhood']);
    $this->db->set('city', $posts['city']);
    $this->db->set('state', $posts['uf']);
    $this->db->where('id', $posts['id_address']);
    $this->db->update('provider_address');
  }

  public function update_bank_data($posts){
    $this->db->set('cpf', $posts['cpf_edit']);
    $this->db->set('agency', $posts['agency_edit']);
    $this->db->set('account_number', $posts['account_number_edit']);
    $this->db->set('bank', $posts['bank_edit']);
    $this->db->set('account_type', $posts['account_type_edit']);
    $this->db->where('id', $posts['id_bank']);
    $this->db->update('provider_bank_data');
  }

  public function fetch_providers(){
    $this->db->select('p.id,
      j.trade_name, j.email as email_juridical,
      l.name, l.surname, l.email as email_legal,
    ');
    $this->db->from('provider as p');
    $this->db->join('provider_juridical as j', 'j.id_provider = p.id', 'left');
    $this->db->join('provider_legal as l', 'l.id_provider = p.id', 'left');
    $query = $this->db->get();
    return $query->result();
  }

  public function fetch_provider($id){
    $this->db->select('p.id, p.type_provider,
      j.trade_name, j.email as email_juridical, j.cnpj, j.legal_name, j.trade_name, j.answerable, j.phone as phone_juritical, j.phone_fixed as phone_fixed_juridical, j.email,
      l.name, l.surname, l.cpf, l.name, l.surname, l.phone as phone_legal, l.phone_fixed as phone_fixed_legal, l.email as email_legal,
      a.id as id_address, a.zip_code, a.address, a.number, a.neighborhood, a.city, a.state,
    ');
    $this->db->from('provider as p');
    $this->db->join('provider_juridical as j', 'j.id_provider = p.id', 'left');
    $this->db->join('provider_legal as l', 'l.id_provider = p.id', 'left');
    $this->db->join('provider_address as a', 'a.id_provider = p.id', 'left');
    $this->db->where('p.id', $id);
    $query = $this->db->get();
    return $query->row();
  }

  public function fetch_banks_data($id)
  {
    $this->db->select('id, cpf, agency, account_number, bank, account_type');
    $this->db->from('provider_bank_data');
    $this->db->where('id_provider', $id);
    $query = $this->db->get();
    return $query->result();
  }

}
