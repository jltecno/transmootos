<?php

class Gasoline_control_model extends CI_Model {

  public $table = 'gasoline_control';

  public function __construct(){
      parent::__construct();
  }

  public function insert($posts){
    $data = array(
      'id_motorcycle' => $_SESSION['id_motorcycle'],
      'odometer' => $posts['odometer'],
      'quantity' => $posts['quantity'],
      'amount' => $posts['amount'],
      'gas_station' => $posts['gas_station'],
    );
    $this->db->insert('gasoline_control', $data);
    return $this->db->insert_id();
  }

  public function fetch_gasoline_control($id){
    $this->db->select('*');
    $this->db->from('gasoline_control');
    $this->db->where('id_motorcycle', $id);
    $query = $this->db->get();
    return $query->result();
  }

  public function fetch_gasoline_day(){
    $this->db->select('g.gas_station, g.amount, m.license_plate');
    $this->db->from('gasoline_control as g');
    $this->db->join('motorcycles as m', 'g.id_motorcycle = m.id');
    $this->db->where('date(g.register)', date('Y-m-d'));
    $query = $this->db->get();
    return $query->result();
  }

}
