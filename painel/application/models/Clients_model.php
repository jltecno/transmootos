<?php

class Clients_model extends CI_Model {

  public $table = 'clients';

  public function __construct(){
      parent::__construct();
  }

  public function insert($posts, $image){
    $data = array(
      'type_client' => $posts['type_client'],
      'where_from' => $posts['where_from'],
      'observation' => $posts['observation'],
      'status' => 1,
      'image' => $image,
    );
    $this->db->insert('clients', $data);
    return $this->db->insert_id();
  }

  public function insert_legal_person($posts, $id_client){
    $data = array(
      'id_client' => $id_client,
      'cpf' => $posts['cpf'],
      'name' => $posts['name'],
      'surname' => $posts['surname'],
      'phone' => $posts['phone'],
      'phone_fixed' => $posts['phone_fixed'],
      'email' => $posts['email'],
    );
    $this->db->insert('legal_person', $data);
    return $this->db->insert_id();
  }

  public function insert_juridical_person($posts, $id_client){
    $data = array(
      'id_client' => $id_client,
      'legal_name' => $posts['legal_name'],
      'trade_name' => $posts['trade_name'],
      'answerable' => $posts['answerable'],
      'phone' => (isset($posts['phone'])) ? $posts['phone'] : '' ,
      'phone_fixed' => $posts['phone_fixed'],
      'email' => $posts['email'],
    );
    $this->db->insert('juridical_person', $data);
    return $this->db->insert_id();
  }

  public function update_juridical_person($posts){
    $this->db->set('cnpj', $posts['cnpj']);
    $this->db->set('legal_name', $posts['legal_name']);
    $this->db->set('trade_name', $posts['trade_name']);
    $this->db->set('answerable', $posts['answerable']);
    $this->db->set('phone', $posts['phone']);
    $this->db->set('phone_fixed', $posts['phone_fixed']);
    $this->db->set('email', $posts['email']);
    $this->db->where('id', $posts['id_juridical']);
    $this->db->update('juridical_person');
  }

  public function update_legal_person($posts){
    $this->db->set('cpf', $posts['cpf']);
    $this->db->set('name', $posts['name']);
    $this->db->set('surname', $posts['surname']);
    $this->db->set('phone', $posts['phone']);
    $this->db->set('phone_fixed', $posts['phone_fixed']);
    $this->db->set('email', $posts['email']);
    $this->db->where('id', $posts['id_legal']);
    $this->db->update('legal_person');
  }

  public function update_client($posts){
    $this->db->set('where_from', $posts['where_from']);
    $this->db->set('contract', $posts['contract']);
    $this->db->set('price_trip', (isset($posts['price_trip'])?$posts['price_trip']:0.00));
    $this->db->set('observation', $posts['observation']);
    $this->db->set('status', $posts['status']);
    $this->db->where('id', $posts['id_client']);
    $this->db->update('clients');
  }

  public function update_image_client($id, $image){
    $this->db->set('image', $image);
    $this->db->where('id', $id);
    $this->db->update('clients');
  }

  public function fetch_clients(){
    $this->db->select('c.id, c.contract, c.status, l.name, l.surname, j.trade_name');
    $this->db->from('clients as c');
    $this->db->join('legal_person as l', 'c.id = l.id_client', 'left');
    $this->db->join('juridical_person as j', 'c.id = j.id_client', 'left');
    $query = $this->db->get();
    return $query->result();
  }

  public function count_clients(){
    $this->db->select('count(id) as count_clients');
    $this->db->from('clients as c');
    $query = $this->db->get();
    return $query->row();
  }

  public function fetch_where_from(){
    $this->db->select('*');
    $this->db->from('where_from');
    $query = $this->db->get();
    return $query->result();
  }

  public function fetch_client($id){
    $this->db->select('c.id, c.where_from, c.type_client, c.contract, c.price_trip, c.observation, c.image, c.status');
    $this->db->from('clients as c');
    $this->db->where('c.id', $id);
    $query = $this->db->get();
    return $query->row();
  }

  public function fetch_client_legal($id){
    $this->db->select('c.id, c.where_from, c.type_client, l.id as id_legal, l.cpf, l.name, l.surname, l.phone, l.email, l.phone_fixed');
    $this->db->from('clients as c');
    $this->db->join('legal_person as l', 'c.id = l.id_client', 'left');
    $this->db->where('l.id_client', $id);
    $query = $this->db->get();
    return $query->row();
  }

  public function fetch_client_juridical($id){
    $this->db->select('c.id, c.where_from, c.type_client, j.cnpj, j.id as id_juridical, j.trade_name as name, j.phone, j.email, j.answerable, j.legal_name, j.phone_fixed');
    $this->db->from('clients as c');
    $this->db->join('juridical_person as j', 'c.id = j.id_client', 'left');
    $this->db->where('j.id_client', $id);
    $query = $this->db->get();
    return $query->row();
  }

  public function clients_per_month(){
    $query = $this->db->query("SELECT count(id) as count_clients, extract(month from register) as month, extract(year from register) as year FROM clients group by extract(month from register),  extract(year from register)");
    return $query->result();
  }

  public function fetch_status(){
    $this->db->select('id, description');
    $this->db->from('status_client');
    $query = $this->db->get();
    return $query->result();
  }

}
