<?php

class Financial_model extends CI_Model {

  public $table = 'gasoline_control';

  public function __construct(){
      parent::__construct();
  }

  public function fetch_invoice_trips($data){
    $this->db->select_sum('t.expected_value');
    $this->db->from('trips as t');
    $this->db->where('date(t.departure_time)', $data);
    $query =$this->db->get();
    return $query->row();
  }

  public function fetch_expenses_day($data){
    $this->db->select_sum('g.amount');
    $this->db->from('gasoline_control as g');
    $this->db->where('date(g.register)', $data);
    $query =$this->db->get();
    return $query->row();
  }

  public function fetch_count_trips($data){
    $this->db->select("count(id) as quantity_trips");
    $this->db->from('trips');
    $this->db->where('date(departure_time)', $data);
    $query =$this->db->get();
    return $query->row_array();
  }

}
