<?php
class users_model extends CI_Model {

  public $table = 'admin';

  public function __construct(){
      parent::__construct();
  }

  public function fetch_users(){
    $this->db->select('u.id, u.name, u.email, u.img_profile, t.description');
    $this->db->from('admin as u');
    $this->db->join('type_register as t', 'u.profile = t.profile');
    $query = $this->db->get();
    return $query->result();
  }

  public function fetch_user($id){
    $this->db->select('id, name, email, note, img_profile, profile, commission, status');
    $this->db->from('admin');
    $this->db->where('id', $id);
    $query = $this->db->get();
    return $query->row();
  }

  public function update_user($rows){
    $this->db->set('name', $rows['name']);
    $this->db->set('email', $rows['email']);
    $this->db->set('profile', $rows['profile']);
    $this->db->set('note', $rows['note']);
    $this->db->set('status', $rows['status']);
    $this->db->set('commission', $rows['commission']);
		$this->db->where('id', $rows['id_user']);
		$this->db->update('admin');
  }

  public function update_image($id, $image){
    $this->db->set('img_profile', $image);
    $this->db->where('id', $id);
		$this->db->update('admin');
  }

  public function fetch_user_password($posts){
    $return = $this->db->select('*')->where(array('id' => $_SESSION['user']['id']))->get('admin');

    if ($return->num_rows()) {
      $user = $return->row_array();

      $validation = password_verify($posts['current_password'], $user['password']);

      if ($validation){
        return $user;
      } else {
        return array();
      }
    }
  }

  public function update_user_password($posts){
    $password = password_hash($posts['new_password'], PASSWORD_BCRYPT);
    $this->db->set('password', $password);
    $this->db->where('id', $_SESSION['user']['id']);
		$this->db->update('admin');
  }

  public function fetch_user_commission(){
    $this->db->select('commission');
    $this->db->from('admin');
    $this->db->where('id', $_SESSION['user']['id']);
    $query = $this->db->get();
    return $query->row();
  }

}
