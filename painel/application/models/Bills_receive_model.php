<?php

class Bills_receive_model extends CI_Model {

  public function __construct(){
      parent::__construct();
  }

  public function insert_bills_received($posts, $amount){
    $data = array(
      'id_client' => $posts['client'],
      'amount' => $amount,
      'user' => $_SESSION['user']['id']
    );
    $this->db->insert('bills_received', $data);
    return $this->db->insert_id();
  }

  public function insert_itens_bills_received($id_payment, $id_trip){
    $data = array(
      'id_bills_received' => $id_payment,
      'id_night_trip' => $id_trip,
    );
    $this->db->insert('itens_bills_received', $data);
  }

  public function update_bill($posts){
    $this->db->set('paid_out', '1');
    $this->db->where('id', $posts['id_bill']);
    $this->db->update('bills_pay');
  }

  public function update_pay_night_trips($posts){
    $day_receipt = date('Y-m-d');
    $night_trip = $posts['night_trip'];
    foreach ($night_trip as $key => $value) {
      $this->db->set('payment_status', '1');
      $this->db->set('day_receipt', $day_receipt);
      $this->db->where('id', $value);
      $this->db->update('night_trips');
    }
  }

  public function fetch_amount($value){
    // $night_trip = $posts['night_trip'];
      $this->db->select('sum(price * quantity) as total');
      $this->db->from('night_trips');
      $this->db->where('id', $value);
      $query = $this->db->get();
      return $query->row();
  }

  public function fetch_amount_receive(){
    $this->db->select("(price * quantity) as total");
    $this->db->from('night_trips');
    $this->db->where('payment_status', '0');
    $query =$this->db->get();
    return $query->result();
  }

  public function fetch_bill_receives(){
    $this->db->select("j.trade_name, l.name, l.surname, sum(n.quantity) as quantity, sum(n.price) as price, n.id, c.id as id_client");
    $this->db->from('night_trips as n');
    $this->db->join('clients as c', 'n.id_client = c.id', 'left');
    $this->db->join('juridical_person as j', 'n.id_client = j.id_client', 'left');
    $this->db->join('legal_person as l', 'n.id_client = l.id_client', 'left');
    $this->db->where('n.payment_status', '0');
    $this->db->group_by('n.id_client');
    $query =$this->db->get();
    return $query->result();
  }

  public function fetch_trips_receives($posts){
    $this->db->select('j.trade_name, l.name, l.surname, t.id, t.quantity, t.departure_odometer, t.arrival_odometer, t.date, t.price, t.payment_status, t.date,');
    $this->db->from('night_trips as t');
    $this->db->join('clients as c', 't.id_client = c.id', 'left');
    $this->db->join('juridical_person as j', 't.id_client = j.id_client', 'left');
    $this->db->join('legal_person as l', 't.id_client = l.id_client', 'left');
    $this->db->where('t.payment_status', '0');
    $this->db->where('t.id_client', $posts['id_client']);
    $query = $this->db->get();
    return $query->result_array();
  }

  public function fetch_bills_receives_per_client($posts){
    $this->db->select('b.id, b.amount, b.date_receipt,
    l.name, l.surname,
    j.trade_name,
    ');
    $this->db->from('bills_received as b');
    // $this->db->join('night_trips as t', );
    $this->db->join('clients as c', 'b.id_client = c.id', 'left');
    $this->db->join('juridical_person as j', 'b.id_client = j.id_client', 'left');
    $this->db->join('legal_person as l', 'b.id_client = l.id_client', 'left');
    // $this->db->where('t.payment_status', '1');
    $query = $this->db->get();
    return $query->result();
  }

  public function fetch_receiver_this_client($id){
    $this->db->select('b.id, b.amount, b.date_receipt,');
    $this->db->from('bills_received as b');
    // $this->db->join('night_trips as t', );
    $this->db->join('clients as c', 'b.id_client = c.id', 'left');
    $this->db->join('juridical_person as j', 'b.id_client = j.id_client', 'left');
    $this->db->join('legal_person as l', 'b.id_client = l.id_client', 'left');
    $this->db->where('c.id', $id);
    $query = $this->db->get();
    return $query->result();
  }

  public function fetch_paid_bill($posts){
    $this->db->select('b.id, b.amount, b.date_receipt,
    l.name, l.surname,
    j.trade_name,
    n.quantity, n.price, n.day_receipt,
    ');
    $this->db->from('bills_received as b');
    $this->db->join('itens_bills_received as it', 'it.id_bills_received = b.id', 'left');
    $this->db->join('night_trips as n', 'it.id_night_trip = n.id');
    $this->db->join('clients as c', 'b.id_client = c.id', 'left');
    $this->db->join('juridical_person as j', 'b.id_client = j.id_client', 'left');
    $this->db->join('legal_person as l', 'b.id_client = l.id_client', 'left');
    $this->db->where('b.id', $posts['id']);
    $query = $this->db->get();
    return $query->result();
  }

}
