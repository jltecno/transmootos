<?php

class Contract_model extends CI_Model {

  public $table = 'contracts';

  public function __construct(){
      parent::__construct();
  }

  public function insert_contract($contract, $id){
    $data = array(
      'id_client' => $id,
      'contract' => $contract,
    );
    $this->db->insert('contracts', $data);
    return $this->db->insert_id();
  }

  public function fetch_contract($id){
    $this->db->select('contract');
    $this->db->from('contracts');
    $this->db->where('id_client', $id);
    $query = $this->db->get();
    return $query->result();
  }

}
