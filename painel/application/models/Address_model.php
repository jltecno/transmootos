<?php

class Address_model extends CI_Model {

  public $table = 'address';

  public function __construct(){
      parent::__construct();
  }

  public function insert_departure_adress($posts){
    $data = array(
      'zip_code' => $posts['cep_origin'],
      'address' => $posts['address_origin'],
      'number' => $posts['number'],
      'neighborhood' => $posts['neighborhood_origin'],
      'city' => $posts['city_origin'],
      'state' => $posts['uf_origin'],
    );

    $this->db->insert('address', $data);
    return $this->db->insert_id();
  }

  public function insert_arrival_adress($posts){
    $data = array(
      'zip_code' => $posts['cep_destination'],
      'address' => $posts['address_destination'],
      'number' => $posts['number_destination'],
      'neighborhood' => $posts['neighborhood_destination'],
      'city' => $posts['neighborhood_destination'],
      'state' => $posts['uf_destination'],
    );

    $this->db->insert('address', $data);
    return $this->db->insert_id();
  }

  public function fetch_departure_adress($id){
    $this->db->select('address as departure_address, number as departure_number, complement as departure_complement, neighborhood as departure_neighborhood, city as departure_city, state as departure_state');
    $this->db->from('address');
    $this->db->where('id', $id);
    $query = $this->db->get();
    return $query->row_array();
  }

  public function fetch_arrival_adress($id){
    $this->db->select('address as arrival_address, number as arrival_number, complement as arrival_complement, neighborhood as arrival_neighborhood, city as arrival_city, state as arrival_state');
    $this->db->from('address');
    $this->db->where('id', $id);
    $query = $this->db->get();
    return $query->row_array();
  }

}
