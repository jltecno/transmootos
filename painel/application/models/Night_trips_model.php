<?php

class Night_trips_model extends CI_Model {

  public $table = 'night_trips';

  public function __construct(){
      parent::__construct();
  }

  public function insert($posts, $id, $price, $biker_commission, $user_commission){
    $data = array(
      'id_biker' => $posts['id_biker'],
      'id_motorcycle' => $posts['id_motorcycle'],
      'id_client' => $id,
      'departure_odometer' => $posts['departure_odometer'],
      'arrival_odometer' => $posts['arrival_odometer'],
      'quantity' => $posts['amount'],
      'price' => $price,
      'biker_commission' => $biker_commission->commission * (($price * $posts['amount'])/100),
      'user_commission' => $user_commission->commission * (($price * $posts['amount'])/100),
      'date' => $posts['date_trips_packages'],
      'payment_status' => '0',
      'user' => $_SESSION['user']['id']
    );
    $this->db->insert('night_trips', $data);
    return $this->db->insert_id();
  }

  public function update_night_trips($posts){
    $this->db->set('id_biker', $posts['biker_night_trip']);
    $this->db->set('id_motorcycle', $posts['motorcycle_night_trip']);
    $this->db->set('arrival_odometer', $posts['arrival_odometer_night_trip']);
    $this->db->set('departure_odometer', $posts['departure_odometer_night_trip']);
    $this->db->set('price', $posts['amount_night_trip']);
    $this->db->where('id', $posts['id_night_trip']);
    $this->db->update('night_trips');
  }

  public function fetch_night_trips($id){
    $this->db->select('t.id, t.id_biker, t.id_motorcycle, t.id_client, t.quantity, t.departure_odometer, t.arrival_odometer, t.date, t.price, t.payment_status,
      b.name as biker_name, b.surname as biker_surname,
      m.license_plate, m.code_motorcycle,
    ');
    $this->db->from('night_trips as t');
    $this->db->join('bikers as b', 't.id_biker = b.id');
    $this->db->join('motorcycles as m', 't.id_motorcycle = m.id');
    $this->db->where('t.id_client', $id);
    $query = $this->db->get();
    return $query->result();
  }

  public function fetch_night_trip($posts){
    $this->db->select('t.id, t.id_biker, t.id_motorcycle, t.id_client, t.quantity, t.departure_odometer, t.arrival_odometer, t.date, t.price, t.payment_status,
      b.id as id_biker, b.name as biker_name, b.surname as biker_surname,
      m.id as id_motorcycle, m.license_plate, m.code_motorcycle,
    ');
    $this->db->from('night_trips as t');
    $this->db->join('bikers as b', 't.id_biker = b.id');
    $this->db->join('motorcycles as m', 't.id_motorcycle = m.id');
    $this->db->where('t.id', $posts['id']);
    $query = $this->db->get();
    return $query->row();
  }

  public function fetch_night_trips_biker($id){
    $this->db->select('t.id, t.id_biker, t.id_motorcycle, t.id_client, t.quantity, t.departure_odometer, t.arrival_odometer, t.date, t.price, t.biker_commission, t.payment_status,
      c.contract, c.type_client,
      l.name, l.surname, l.phone as legal_phone,
      j.trade_name, j.phone as juridical_phone,
      b.name as biker_name, b.surname as biker_surname,
      m.license_plate, m.code_motorcycle,
    ');
    $this->db->from('night_trips as t');
    $this->db->join('clients as c', 't.id_client = c.id', 'left');
    $this->db->join('legal_person as l', 't.id_client = l.id_client', 'left');
    $this->db->join('juridical_person as j', 't.id_client = j.id_client', 'left');
    $this->db->join('bikers as b', 't.id_biker = b.id');
    $this->db->join('motorcycles as m', 't.id_motorcycle = m.id');
    $this->db->where('t.id_biker', $id);
    $query = $this->db->get();
    return $query->result();
  }

  public function fetch_night_trips_date($posts){
    $this->db->select('t.id, t.id_biker, t.id_motorcycle, t.id_client, t.quantity, t.departure_odometer, t.arrival_odometer, t.date, t.price, t.biker_commission, t.payment_status,
      c.contract, c.type_client,
      l.name, l.surname, l.phone as legal_phone,
      j.trade_name, j.phone as juridical_phone,
      b.name as biker_name, b.surname as biker_surname,
      m.license_plate, m.code_motorcycle,
    ');
    $this->db->from('night_trips as t');
    $this->db->join('clients as c', 't.id_client = c.id', 'left');
    $this->db->join('legal_person as l', 't.id_client = l.id_client', 'left');
    $this->db->join('juridical_person as j', 't.id_client = j.id_client', 'left');
    $this->db->join('bikers as b', 't.id_biker = b.id');
    $this->db->join('motorcycles as m', 't.id_motorcycle = m.id');
    $this->db->where('t.id_biker', $posts['id']);
    if ($posts['first_date'] != 0 && $posts['last_date'] != 0) {
      $this->db->where('DATE(t.date) >=', $posts['first_date']);
      $this->db->where('DATE(t.date) <=', $posts['last_date']);
    }
    $query = $this->db->get();
    return $query->result();
  }

  public function fetch_night_date_client($posts){
    $this->db->select('t.id, t.id_biker, t.id_motorcycle, t.id_client, t.quantity, t.departure_odometer, t.arrival_odometer, t.date, t.price, t.payment_status,
      b.name as biker_name, b.surname as biker_surname,
      m.license_plate, m.code_motorcycle,
    ');
    $this->db->from('night_trips as t');
    $this->db->join('bikers as b', 't.id_biker = b.id');
    $this->db->join('motorcycles as m', 't.id_motorcycle = m.id');
    $this->db->where('t.id_client', $posts['id']);
    if ($posts['first_date'] != 0 && $posts['last_date'] != 0) {
      $this->db->where('DATE(t.date) >=', $posts['first_date']);
      $this->db->where('DATE(t.date) <=', $posts['last_date']);
    }
    $query = $this->db->get();
    return $query->result();
  }

}
