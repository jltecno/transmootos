<?php $this->load->view('elements/header');?>
  <div class="wrapper ">
    <?php $this->load->view('elements/sidebar_lateral');?>
    <?php $this->load->view('elements/sidebar');?>
    <div class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="card">
            <div class="card-header">
              <h2 class="card-title" style="display: inline;">Clientes</h2>
              <a href="<?php echo base_url()?>clients/register" class="btn btn-primary float-md-right float-sm-right float-xs-right">Cadastrar</a>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table id="table_clients" class="table table-striped">
                  <thead class=" text-primary">
                    <th>Clientes</th>
                    <th>Última viagem</th>
                    <th>Detalhe</th>
                  </thead>
                  <tbody>
                    <?php foreach ($clients as $row): ?>
                      <tr>
                        <td><?php echo $row->trade_name; ?> <?php echo $row->name; ?> <?php echo $row->surname; ?></td>
                        <td></td>
                        <td>
                          <a href="<?php echo base_url()?>clients/detail/<?php echo $row->id?>">
                            <i class="nc-icon nc-zoom-split"></i>
                          </a>
                          <?php if ($row->status == 2): ?>
                            <span class="ml-1">
                              <i class="fas fa-lock" style="color: red;"></i>
                            </span>
                          <?php endif; ?>
                        </td>
                      </tr>
                    <?php endforeach; ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
<?php $this->load->view('elements/footer');?>
