<?php $this->load->view('elements/header');?>
<div class="wrapper ">
  <?php $this->load->view('elements/sidebar_lateral');?>
  <?php $this->load->view('elements/sidebar');?>
  <div class="content">
    <?php if ($client->status == 2): ?>
      <div class="row">
        <div class="col-md-12 text-center">
          <h6 class="alert alert-danger">Este cliente está bloqueado!</h6>
        </div>
      </div>
    <?php endif; ?>
    <div class="row">
      <div class="col-md-5">
        <div class="card">
          <div class="card-header text-center">
            <form action="<?php echo base_url(); ?>clients/update_image_client/<?php echo $client->id; ?>" method="post" enctype="multipart/form-data">
            <div class="row">
                <div class="col-md-12">
                <label class="newimgum">
                    <img id="imgum" class="icone_perfil" src="<?php echo base_url(); ?>assets/build/img/profile_clients/<?php echo $client->image; ?>">
                    <input id="picimgum" class='pis' name="img_client" type="file" style="display:none;">
                  </label>
                </div>
                <div class="col-md-12">
                  <button class="btn btn-sm btn-primary" style="display:none;" id="save" type="submit" name="update_image">salvar</button>
                </div>
              </form>
              <div class="col-md-12">
                <h4 class="card-title text-center">
                  <?php if($client->type_client == 2){echo $juridical_client->name;} if($client->type_client == 1){echo $legal_client->name;} ?>
                </h4>
              </div>
            </div>
            <div class="card-body text-center">
              <?php if ($client->type_client == 2): ?>
              <!-- <span>Rua Belo Horizonte, 327, Centro, Taiobeiras - MG</span> -->
              <?php endif; ?>
            </div>
          </div>
        </div>
        <div class="card">
          <div class="card-header">
            <h4 class="card-title text-center">Contratos</h4>
          </div>
          <div class="card-body">
            <button type="button" class="btn btn-primary" name="button" data-toggle="modal" data-target="#modalAddContrato">Adicionar contrato</button>
            <table id="tabela_cliente_contratos" class="table table-striped table-bordered" style="width:100%;">
              <thead>

              </thead>
              <tbody>
                <?php foreach ($contract as $row): ?>
                  <tr>
                    <td> <a href="<?php echo base_url(); ?>assets/contracts/<?php echo $row->contract; ?>" download>Contrato</a> </td>
                  </tr>
                <?php endforeach; ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
      <div class="col-md-7">
        <div class="card card-user">
          <div class="card-body">
            <form action="<?php echo base_url()?>clients/update_client/<?php echo $client->id; ?>"
              id="form_alterar_cliente" method="post">
              <?php if ($client->type_client == 1): ?>
              <section id="form_legal_person">
                <div class="row">
                  <div class="col-md-4 pr-md-1">
                    <div class="form-group">
                      <label for="cpf">Cpf</label>
                      <input type="text" name="cpf" id="cpf" class="form-control" placeholder="Cpf"
                        value="<?php echo $legal_client->cpf; ?>">
                      <?php echo form_error('cpf'); ?>
                    </div>
                  </div>
                  <div class="col-md-4 pl-md-1 pr-md-1">
                    <div class="form-group">
                      <label for="name">Nome</label>
                      <input type="text" name="name" id="name" class="form-control" placeholder="Nome"
                        value="<?php echo $legal_client->name; ?>">
                      <?php echo form_error('name'); ?>
                    </div>
                  </div>
                  <div class="col-md-4 pr-1">
                    <div class="form-group">
                      <label for="surname">Sobrenome</label>
                      <input type="text" name="surname" id="surname" class="form-control" placeholder="Sobrenome"
                        value="<?php echo $legal_client->surname; ?>">
                      <?php echo form_error('surname'); ?>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-6 pr-md-1">
                    <div class="form-group">
                      <label for="telefone">Telefone</label>
                      <input type="text" name="phone" id="telefone" class="form-control" placeholder="Telefone"
                        value="<?php echo $legal_client->phone; ?>">
                      <?php echo form_error('telefone'); ?>
                    </div>
                  </div>
                  <div class="col-md-6 pl-md-1">
                    <div class="form-group">
                      <label for="email">E-mail</label>
                      <input type="text" name="email" id="email" class="form-control" placeholder="E-mail"
                        value="<?php echo $legal_client->email; ?>">
                      <?php echo form_error('email'); ?>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-12">
                    <div class="form-group">
                      <label for="phone_fixed">Telefone fixo</label>
                      <input type="text" name="phone_fixed" id="phone_fixed" class="form-control" placeholder="Telefone fixo"
                        value="<?php echo $legal_client->phone_fixed; ?>">
                    </div>
                  </div>
                </div>
              </section>
              <input type="hidden" name="id_legal" value="<?php echo $legal_client->id_legal; ?>">
              <input type="hidden" name="id_client" value="<?php echo $legal_client->id; ?>" id="id_client">
              <?php endif; ?>
              <?php if ($client->type_client == 2): ?>
              <section id="form_juridical_person">
                <div class="row">
                  <div class="col-md-6 pr-1">
                    <div class="form-group">
                      <label for="cnpj">CNPJ</label>
                      <input type="text" name="cnpj" id="cnpj" class="form-control" placeholder="CNPJ"
                        value="<?php echo $juridical_client->cnpj; ?>">
                      <?php echo form_error('cnpj'); ?>
                    </div>
                  </div>
                  <div class="col-md-6 pl-1">
                    <div class="form-group">
                      <label for="empresa">Empresa</label>
                      <input type="text" name="trade_name" id="empresa" class="form-control" placeholder="Empresa"
                        value="<?php echo $juridical_client->name; ?>">
                      <?php echo form_error('trade_name'); ?>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-6 pr-1">
                    <div class="form-group">
                      <label for="responsavel">Responsável</label>
                      <input type="text" name="answerable" id="responsavel" class="form-control"
                        placeholder="Responsável" value="<?php echo $juridical_client->answerable; ?>">
                      <?php echo form_error('answerable'); ?>
                    </div>
                  </div>
                  <div class="col-md-6 pl-1">
                    <div class="form-group">
                      <label for="legal_name">Nome legal</label>
                      <input type="text" name="legal_name" id="legal_name" class="form-control" placeholder="Nome legal"
                        value="<?php echo $juridical_client->legal_name; ?>">
                      <?php echo form_error('legal_name'); ?>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-6 pr-1">
                    <div class="form-group">
                      <label for="telefone">Telefone</label>
                      <input type="text" name="phone" id="telefone" class="form-control" placeholder="Telefone"
                        value="<?php echo $juridical_client->phone; ?>">
                      <?php echo form_error('telefone'); ?>
                    </div>
                  </div>
                  <div class="col-md-6 pl-1">
                    <div class="form-group">
                      <label for="email">E-mail</label>
                      <input type="text" name="email" id="email" class="form-control" placeholder="E-mail"
                        value="<?php echo $juridical_client->email; ?>">
                      <?php echo form_error('email'); ?>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-12">
                    <div class="form-group">
                      <label for="phone_fixed">Telefone fixo</label>
                      <input type="text" name="phone_fixed" id="phone_fixed" class="form-control" placeholder="Telefone fixo"
                        value="<?php echo $juridical_client->phone_fixed; ?>">
                    </div>
                  </div>
                </div>
              </section>
              <input type="hidden" name="id_juridical" value="<?php echo $juridical_client->id_juridical; ?>">
              <input type="hidden" name="id_client" value="<?php echo $juridical_client->id; ?>" id="id_client">
              <?php endif; ?>
              <div class="row">
                <div class="col-md-3">
                  <div class="form-group">
                    <label for="">Contrato ativo?</label><br>
                    <input type="radio" name="contract" class="contract" id="contract_on" value="1"
                      <?php if($client->contract == 1){echo 'checked';} ?>><label for="contract_on">Sim</label>
                    <input type="radio" name="contract" class="contract" id="contract_off" value="0"
                      <?php if($client->contract == 0){echo 'checked';} ?>><label for="contract_off">Não</label>
                  </div>
                </div>
                <div class="col-md-3">
                  <?php //if($client->contract == 1): ?>
                    <div class="form-group div_price_trip" <?php echo ($client->contract == 0)? 'style="display: none;"' : '' ?>>
                      <label for="">Valor por viagem</label>
                      <input type="text" name="price_trip" id="price_trip" class="form-control valor" value="<?php echo $client->price_trip; ?>" />
                    </div>
                  <?php //endif; ?>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label for="observation">Observação</label>
                    <textarea name="observation" value="<?php echo $client->observation; ?>" class="form-control" id="observation" placeholder="Observação"><?php echo $client->observation; ?></textarea>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-6 ">
                  <div class="form-group">
                    <label for="where_from">De onde veio</label>
                    <select name="where_from" class="form-control" id="where_from">
                      <option value="">Selecione uma opçao..</option>
                      <?php foreach ($where_from as $row): ?>
                      <?php if ($client->where_from == $row->id): ?>
                      <option value="<?php echo $row->id ?>" selected><?php echo $row->description; ?></option>
                      <?php else: ?>
                      <option value="<?php echo $row->id; ?>"><?php echo $row->description; ?></option>
                      <?php endif; ?>
                      <?php endforeach; ?>
                    </select>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label for="status">Status</label>
                    <select class="form-control" name="status" id="status">
                      <option value=""></option>
                      <?php foreach ($status as $row): ?>
                        <?php if ($client->status == $row->id): ?>
                          <option value="<?php echo $client->status; ?>" selected><?php echo $row->description; ?></option>
                          <?php else: ?>
                            <option value="<?php echo $row->id; ?>"><?php echo $row->description; ?></option>
                        <?php endif; ?>
                      <?php endforeach; ?>
                    </select>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="update ml-auto mr-auto">
                  <button
                    <?php if($client->type_client == 1){ echo "id='update_legal_person' name='update_legal_person'";}?>
                    <?php if($client->type_client == 2){ echo "id='update_juridical_person' name='update_juridical_person'";}?>
                    class="btn btn-primary btn-round">
                    Alterar
                  </button>
                </div>
              </div>
            </form>
          </div>
        </div>
        <!-- <div class="card">
          <div class="card-header">
            <h4 class="card-title text-center">Contatos</h4>
            <button type="button" class="btn btn-primary" name="button" data-toggle="modal"
              data-target="#modalAddContato">Adicionar contato</button>
          </div>
          <div class="card-body">
            <table class="table table-striped table-bordered w=100" id="tabela_cliente_contatos" style="width:100%;">
              <thead class="text-primary">
                <tr>
                  <th>oi</th>
                  < <?php if (isset($juridical_client->answerable)): ?>
                      <th>Responsavel</th>
                    <?php endif; ?>
                    <th>Tel</th>
                    <th>E-mail</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>thau</td>
                   <?php if (isset($juridical_client->answerable)): ?>
                      <td><?php echo $juridical_client->answerable ?></td>
                    <?php endif; ?>
                    <td><?php echo $juridical_client->phone; ?></td>
                    <td><?php echo $juridical_client->email; ?></td>
                </tr>
              </tbody>
            </table>
          </div>
        </div> -->
      </div>
    </div>
    <div class="row">
      <div class="col-md-5">
        <div class="card">
          <div class="card-header">
            <h4 class="card-title text-center">Viagens realizadas</h4>
          </div>
          <div class="card-body">
            <div class="row">
              <div class="col-md">
                <table class="table table-striped table-bordered" style="width:100%" id="table_trips_made">
                  <thead class="text-primary">
                    <tr>
                      <th>Local</th>
                      <th>Carga</th>
                      <th>Piloto</th>
                      <th>Data</th>
                      <th>Detalhe</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php foreach ($trips as $row): ?>
                      <?php $departure_time = date_create($row->departure_time); ?>
                      <tr>
                        <td><?php echo ($row->locale == 'I') ? 'Interno' : 'Fora da cidade' ;?></td>
                        <td><?php echo ($row->service == 'P') ? 'Pessoa' : 'Material' ; ?></td>
                        <td><?php echo $row->name; ?> <?php echo $row->surname; ?></td>
                        <td><?php echo ($row->departure_time != null) ? date_format($departure_time, 'd/m/Y H:i') : 'em andamento' ; ?></td>
                        <td><button data-id="modalDashEncerradas" class="detail_trip btn_detalhe ml-2 " data-trip="<?php echo $row->id; ?>"
                            type="button" id="trip_taken" data-departure="<?php echo $row->departure_adress; ?>"><i class="fa fa-1x fa-search "></i>
                          </button></td>
                      </tr>
                    <?php endforeach; ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
        <div class="card">
          <div class="card-header">
            <h4 class="card-title text-center">Recebimentos de viagens</h4>
            <!-- <button type="button" name="button" class="btn btn-sm btn-primary"  data-toggle="modal" data-target="#modalNightTrips">Novo</button> -->
          </div>
          <div class="card-body">
            <div class="row">
              <div class="col-md">
                <table class="table table-striped table-bordered" style="width:100%" id="trips_receipts">
                  <thead class="text-primary">
                    <tr>
                      <th>Valor</th>
                      <th>Recebido</th>
                      <th></th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php foreach ($receiver_client as $row): ?>
                      <?php $date_receipt = date_create($row->date_receipt); ?>
                      <tr>
                        <td>R$<?php echo $row->amount; ?></td>
                        <td><?php echo date_format($date_receipt, 'd/m/Y'); ?></td>
                        <td>
                          <a href="" class="detail_bills"  data-id="<?php echo $row->id; ?>" data-toggle="modal" data-target="#detail_bills_receive">
                            <i class="fa fa-1x fa-search "></i>
                          </a>
                        </td>
                      </tr>
                    <?php endforeach; ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-md-7">
        <div class="card">
          <div class="card-header">
            <h4 class="card-title text-center">Viagens Noturnas</h4>
            <button type="button" name="button" class="btn btn-sm btn-primary"  data-toggle="modal" data-target="#modalNightTrips">Novo</button>
          </div>
          <div class="card-body">
            <div class="row mb-2">
              <div class="col-md-4">
                <label for="first_date"><h6>Data de início:</h6></label>
                <input type="text" class="data form-control" placeholder="00/00/0000" name="" id="first_date" value="" class="form-control">
              </div>
              <div class="col-md-4">
                <label for="last_date"><h6>Data de fim:</h6></label>
                <input type="text" class="data form-control" placeholder="00/00/0000" name="" id="last_date" value="" class="form-control">
              </div>
              <div class="col-md-4 mt-3">
                <button type="button" name="button" id="search_date_client_night_trip" class="btn btn-primary">
                  <i class="fa fa-1x fa-search"></i>
                </button>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <div class="detail_night_trips_clients">
                  <table class="table table-striped table-bordered" style="width:100%" id="table_night_trips">
                    <thead class="text-primary">
                      <tr>
                        <th scope="col">Data</th>
                        <th scope="col">Piloto</th>
                        <th scope="col">Moto</th>
                        <th scope="col">Status</th>
                        <th scope="col"></th>
                      </tr>
                    </thead>
                    <tbody id="detail_night_trips_clients">
                      <?php foreach ($night_trips as $row): ?>
                        <?php $date = date_create($row->date); ?>
                        <?php $price = $row->price * $row->quantity; ?>
                        <tr>
                          <td><?php echo date_format($date, 'd/m/Y'); ?></td>
                          <td><?php echo $row->biker_name; ?> <?php echo $row->biker_surname; ?></td>
                          <td><?php echo $row->code_motorcycle.' | '.$row->license_plate; ?></td>
                          <td><?php echo ($row->payment_status == 0) ? 'Não pago' : 'Pago' ; ?></td>
                          <td>
                            <a href="" class="detail_night_trips" data-id="<?php echo $row->id; ?>" data-toggle="modal" data-target="#modal_detail_night_trips">
                              <i class="fa fa-1x fa-search "></i>
                            </a>
                          </td>
                        </tr>
                      <?php endforeach; ?>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <?php $this->load->view('elements/footer');?>

  <!-- Modal -->
  <div class="modal fade" id="modalAddContrato" tabindex="-1" role="dialog" aria-labelledby="modalAddContrato" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Adicionar contrato</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <form action="<?php echo base_url(); ?>clients/register_contract/<?php echo $client->id; ?>" method="post" enctype="multipart/form-data">
            <div class="col-md-12">
              <label for="contract">Adicionar Contrato:</label>
              <input type="file" name="contract" value="" class="btn btn-primary" id="contract">
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
          <button type="submit" name="register_contract" class="btn btn-primary">Enviar</button>
        </div>
      </form>
      </div>
    </div>
  </div>

  <!-- Modal Viagem noturna -->
  <div class="modal fade " id="modalNightTrips" tabindex="-1" role="dialog" aria-labelledby="modalNightTrips" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Adicionar viagem noturna</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form action="<?php echo base_url(); ?>clients/register_night_trips/<?php echo $client->id; ?>" method="post">
        <div class="modal-body">
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label for="biker">Piloto</label>
                  <select class="form-control" name="id_biker" id="biker">
                    <option value=""></option>
                    <?php foreach ($bikers as $row): ?>
                      <option value="<?php echo $row->id; ?>"><?php echo $row->name; ?> <?php echo $row->surname; ?></option>
                    <?php endforeach; ?>
                  </select>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="motorcycle">Moto</label>
                  <select class="form-control" name="id_motorcycle" id="motorcycle">
                    <option value=""></option>
                    <?php foreach ($motorcycles as $row): ?>
                      <option value="<?php echo $row->id; ?>"><?php echo $row->code_motorcycle; ?> | <?php echo $row->license_plate; ?></option>
                    <?php endforeach; ?>
                  </select>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label for="departure_odometer">Odometro inicial</label>
                  <input type="text" name="departure_odometer" id="departure_odometer" value="" class="form-control odometer">
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="arrival_odometer">Odometro final</label>
                  <input type="text" name="arrival_odometer" id="arrival_odometer" value="" class="form-control odometer">
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label for="amount">Quantidade</label>
                  <input type="number" name="amount" id="amount" value="" class="form-control">
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="date_night_trip">Data</label>
                  <input type="date" name="date_trips_packages" id="date_night_trip" value="" class="form-control">
                </div>
              </div>
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
          <button type="submit" name="register_night_trips" class="btn btn-primary">Cadastrar</button>
        </div>
      </form>
      </div>
    </div>
  </div>

  <!-- Modal detail bills receive -->
  <div class="modal fade" id="detail_bills_receive" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Detalhes do pagamento</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <form action="<?php echo base_url(); ?>bills_receive/update_pay_night_trips" method="post">
            <div id="detail_client" class="row"></div>
            <table class="table table-striped">
              <thead>
                <tr>
                  <th>Viagens</th>
                  <th>Unitario</th>
                  <th>Valor</th>
                  <th>Data</th>
                </tr>
              </thead>
              <tbody class="detail_bills_receive">

              </tbody>
            </table>
            <div id="amount_detail" class="row text-right"></div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
            <input type="hidden" name="client" value="" id="client"/>
            <!-- <button type="submit" name="pay_bills_receive" class="btn btn-primary">Pagar</button> -->
          </div>
        </form>
      </div>
    </div>
  </div>

  <!-- Modal detail night trips -->
  <div class="modal fade" id="modal_detail_night_trips" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Detalhes da viagem</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form action="<?php echo base_url(); ?>clients/update_night_trips/<?php echo $client->id; ?>" method="post">
        <div class="modal-body">
            <div id="detail_client" class="row"></div>
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label for="date_night_trip_detail">Data</label>
                  <input type="date" name="date_night_trip_detail" id="date_night_trip_detail" value="" class="form-control" readonly>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="quantity_night_trip">Quantidade</label>
                  <input type="text" name="quantity_night_trip" id="quantity_night_trip" value="" class="form-control" readonly>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label for="biker_night_trip">Piloto</label>
                  <select class="form-control" name="biker_night_trip" id="biker_night_trip">
                    <option value=""></option>
                    <?php foreach ($bikers as $row): ?>
                      <option value="<?php echo $row->id; ?>"><?php echo $row->name; ?> <?php echo $row->surname; ?></option>
                    <?php endforeach; ?>
                  </select>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="motorcycle_night_trip">Moto</label>
                  <select name="motorcycle_night_trip" id="motorcycle_night_trip" class="form-control">
                    <option value=""></option>
                    <?php foreach ($motorcycles as $row): ?>
                      <option value="<?php echo $row->id; ?>"><?php echo $row->code_motorcycle.' | '.$row->license_plate; ?></option>
                    <?php endforeach; ?>
                  </select>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label for="departure_odometer_night_trip">Odometro inicial</label>
                  <input type="text" name="departure_odometer_night_trip" value="" id="departure_odometer_night_trip" class="form-control odometer">
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="arrival_odometer_night_trip">Odometro final</label>
                  <input type="text" name="arrival_odometer_night_trip" id="arrival_odometer_night_trip" value="" class="form-control odometer">
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label for="amount_night_trip">Valor</label>
                  <input type="text" name="amount_night_trip" id="amount_night_trip" value="" class="form-control valor">
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="status_night_trip">Status</label>
                  <input type="text" name="status_night_trip" id="status_night_trip" value="" class="form-control" readonly>
                </div>
              </div>
            </div>

            <!-- <div class="row" id="table_detail_night_trip">

            </div> -->
            <!-- <div id="amount_detail" class="row text-right"></div> -->
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
            <input type="hidden" name="client" value="" id="client"/>
            <input type="hidden" name="id_night_trip" value="" id="id_night_trip"/>
            <button type="submit" name="update_bills_receive" class="btn btn-primary">Atualizar</button>
            <!-- <button type="submit" name="pay_bills_receive" class="btn btn-primary">Pagar</button> -->
          </div>

        </form>
      </div>
    </div>
  </div>
