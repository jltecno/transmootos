<?php $this->load->view('elements/header');?>
<div class="wrapper ">
  <?php $this->load->view('elements/sidebar_lateral');?>
  <?php $this->load->view('elements/sidebar');?>
  <div class="content">
    <div class="row">
      <div class="col-md-12">
        <div class="card card-user">
          <div class="card-header">
            <h5 class="card-title">Cadastrar cliente</h5>
          </div>
          <div class="card-body">
            <form action="<?php echo base_url(); ?>clients/register_clients" method="post" enctype="multipart/form-data">
              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <input type="radio" name="type_client" id="legal_person" class="type_client" value="1" checked>
                    <label for="legal_person">Pessoa física</label>
                    <input type="radio" name="type_client" id="juridical_person" class="type_client" value="2">
                    <label for="juridical_person">Pessoa jurídica</label>
                  </div>
                </div>
              </div>
              <section id="form_legal_person">
                <div class="row">
                  <div class="col-md-4 pr-1">
                    <div class="form-group">
                      <label for="cpf">Cpf</label>
                      <input type="text" name="cpf" id="cpf" class="form-control" placeholder="Cpf"
                        value="<?php echo set_value('cpf'); ?>">
                      <?php echo form_error('cpf'); ?>
                    </div>
                  </div>
                  <div class="col-md-4 pr-1">
                    <div class="form-group">
                      <label for="name">Nome</label>
                      <input type="text" name="name" id="name" class="form-control" placeholder="Nome"
                        value="<?php echo set_value('name'); ?>">
                      <?php echo form_error('name'); ?>
                    </div>
                  </div>
                  <div class="col-md-4 pr-1">
                    <div class="form-group">
                      <label for="surname">Sobrenome</label>
                      <input type="text" name="surname" id="surname" class="form-control" placeholder="Sobrenome"
                        value="<?php echo set_value('surname'); ?>">
                      <?php echo form_error('surname'); ?>
                    </div>
                  </div>
                </div>
              </section>
              <section id="form_juridical_person" style="display: none;">
                <div class="row">
                  <div class="col-md-6 pr-1">
                    <div class="form-group">
                      <label for="cnpj">CNPJ</label>
                      <input type="text" name="cnpj" id="cnpj" class="form-control" placeholder="CNPJ"
                        value="<?php echo set_value('cnpj'); ?>">
                      <?php echo form_error('cnpj'); ?>
                    </div>
                  </div>
                  <div class="col-md-6 pl-1">
                    <div class="form-group">
                      <label for="legal_name">Razão social</label>
                      <input type="text" name="legal_name" id="legal_name" class="form-control"
                        placeholder="Razão social" value="<?php echo set_value('legal_name'); ?>">
                      <?php echo form_error('legal_name'); ?>
                    </div>
                  </div>

                </div>
                <div class="row">
                  <div class="col-md-6 pr-1">
                    <div class="form-group">
                      <label for="empresa">Nome fantasia</label>
                      <input type="text" name="trade_name" id="empresa" class="form-control" placeholder="Nome fantasia"
                        value="<?php echo set_value('trade_name'); ?>">
                      <?php echo form_error('trade_name'); ?>
                    </div>
                  </div>
                  <div class="col-md-6 pl-1">
                    <div class="form-group">
                      <label for="responsavel">Responsável</label>
                      <input type="text" name="answerable" id="responsavel" class="form-control"
                        placeholder="Responsável" value="<?php echo set_value('answerable'); ?>">
                      <?php echo form_error('answerable'); ?>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-12">
                    <h4>Endereço</h4>
                  </div>
                  <div class="col-md-4 pr-md-1">
                    <div class="form-group">
                      <label for="nome">Cep</label>
                      <input type="text" name="cep" id="cep" class="form-control"
                        value="<?php echo set_value('cep'); ?>">
                      <?php echo form_error('cep'); ?>
                    </div>
                  </div>
                  <div class="col-md-6 pr-md-1 pl-md-1">
                    <div class="form-group">
                      <label for="nome">Rua</label>
                      <input type="text" name="rua" id="rua" class="form-control"
                        value="<?php echo set_value('rua'); ?>">
                      <?php echo form_error('rua'); ?>
                    </div>
                  </div>
                  <div class="col-md pl-md-1">
                    <div class="form-group">
                      <label for="numero">Numero</label>
                      <input type="text" name="numero" id="numero" class="form-control"
                        value="<?php echo set_value('numero'); ?>">
                      <?php echo form_error('numero'); ?>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-5 pr-md-1">
                    <div class="form-group">
                      <label for="bairro">Bairro</label>
                      <input type="text" name="bairro" id="bairro" class="form-control"
                        value="<?php echo set_value('bairro'); ?>">
                      <?php echo form_error('bairro'); ?>
                    </div>
                  </div>
                  <div class="col-md-5 pr-md-1 pl-md-1">
                    <div class="form-group">
                      <label for="cidade">Cidade</label>
                      <input type="text" name="cidade" id="cidade" class="form-control"
                        value="<?php echo set_value('cidade'); ?>">
                      <?php echo form_error('cidade'); ?>
                    </div>
                  </div>
                  <div class="col-md-2 pl-md-1">
                    <div class="form-group">
                      <label for="uf">UF</label>
                      <input type="text" name="uf" id="uf" class="form-control" value="<?php echo set_value('uf'); ?>">
                      <?php echo form_error('uf'); ?>
                    </div>
                  </div>
                  <hr>
                </div>
              </section>
              <section id="common">
                <div class="row">
                  <div class="col-md-6 pr-1">
                    <div class="form-group">
                      <label for="telefone">Telefone</label>
                      <input type="text" name="phone" id="phone" class="form-control" placeholder="Telefone"
                        value="<?php echo set_value('telefone'); ?>">
                      <?php echo form_error('telefone'); ?>
                    </div>
                  </div>
                  <div class="col-md-6 pl-1">
                    <div class="form-group">
                      <label for="phone_fixed">Telefone fixo</label>
                      <input type="text" name="phone_fixed" id="phone_fixed" class="form-control" placeholder="Telefone fixo"
                        value="<?php echo set_value('phone_fixed'); ?>">
                      <?php echo form_error('phone_fixed'); ?>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="where_from">De onde veio</label>
                      <select name="where_from" class="form-control" id="where_from">
                        <option value="">Selecione uma opçao..</option>
                        <option value="1">Facebook</option>
                        <option value="2">Instagram</option>
                        <option value="3">Indicação</option>
                        <option value="4">Panfleto</option>
                        <option value="5">Muro</option>
                      </select>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="email">E-mail</label>
                      <input type="text" name="email" id="email" class="form-control" placeholder="E-mail"
                        value="<?php echo set_value('email'); ?>">
                      <?php echo form_error('email'); ?>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-12">
                    <div class="form-group">
                      <label for="observation">Observação</label>
                      <textarea name="observation" class="form-control" id="observation" placeholder="Observação"></textarea>
                    </div>
                  </div>
                </div>
              </section>
              <div class="row">
                <div class="col-md">
                  <label class="newimgum">
                    <img id="imgum" class="icone_perfil" src="<?php echo base_url(); ?>assets/build/img/icon/perfil.png">
                    <input id="picimgum" class='pis' name="img_client" type="file" style="display:none;">
                  </label>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12">
                  <button type="submit" name="cadastrar_cliente"
                    class="btn btn-primary btn-round float-md-right float-sm-right float-xs-right">Cadastrar</button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
  <?php $this->load->view('elements/footer');?>
