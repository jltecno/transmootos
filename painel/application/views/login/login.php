<?php $this->load->view('elements/header');?>
<div id="bg-login">
  <section class="container-fluid ">
    <div class="row">
      <div class="col-md-6 border-login d-flex ">
        <div class="login__block mx-auto align-self-center ">
          <div class="row my-5">
            <div class="col-md text-center ">
              <img class="login__img--logo mx-auto "
                src="<?php echo base_url();?>assets\build\img\logo\logo_sem_fundo.png" alt="">
            </div>
          </div>
          <?php if ($error = getError()): // Ao mesmo tempo que defino a variável $error, verifico se retorna algo em "getError" ;) ?>
          <div class="alert alert-<?php echo $error['error_type'] ?> alert-dismissible fade show" role="alert">
            <?php echo $error['error_string'] ?>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <?php endif; ?>
          <form method="post" class="" action="<?php echo base_url(); ?>login">
            <div class="login__form shadow-sm rounded">
              <div class="row ">
                <div class="col-md-12">
                  <div class="form-group">
                    <input class="form-control form-control-lg" type="text" name="email" id="email"
                      placeholder="E-mail" />
                    <span id="preencha_email" style="display: none; color: red;">Preencha campo e-mail</span>
                  </div>
                </div>
              </div>

              <div class="row my-2 ">
                <div class="col-md-12">
                  <div class="form-group">
                    <input class="form-control form-control-lg" type="password" name="password" id="password"
                      placeholder="Senha" />
                    <span id="preencha_senha" style="display: none; color: red;">Preencha campo senha</span>
                  </div>
                </div>
              </div>

              <div class="row">
                <div class="col-md-12 text-center ">
                  <a href="#" class="login__form--esqueci-senha " data-toggle="modal"
                    data-target="#esqueci_senha"><i>Esqueci minha senha</i></a>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12 text-center">
                  <input class="btn align-content-center btn-primary login__form--entrar" type="submit" id="entrar" name="entrar" value="Entrar" />
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>








      <div class="col-md-6  m-0 p-0 border-img-bg">
        <div class="img-fluid" id="login__img--bg">
        </div>
      </div>
    </div>
  </section>
</div>

<!-- Modal -->
<div class="modal fade" id="esqueci_senha" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
  aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel" style="display: inline;">Recuperar senha</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form class="" action="<?php echo base_url(); ?>login" method="post">
          <h6 style="display: inline;">Digite seu email</h6>
          <input type="email" name="email" style="width: 60%;"><br>
      </div>
      <div class="modal-footer">
        <button type="submit" name="forgot_password" class="btn btn-primary">Enviar</button>
        </form>
      </div>
    </div>
  </div>
</div>
<?php $this->load->view('elements/footer');?>