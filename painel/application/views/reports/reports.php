<?php $this->load->view('elements/header');?>
<?php $this->load->view('elements/sidebar_lateral');?>
<?php $this->load->view('elements/sidebar');?>
<div class="content">
  <div class="row">
    <div class="col-lg-4 col-md-4 col-sm-6">
      <div class="card card-stats">
        <div class="card-body ">
          <div class="row">
            <div class="col-5 col-md-4">
              <div class="icon-big text-center text-green"><i class="fas fa-check"></i>
              </div>
            </div>
            <div class="col-7 col-md-8">
              <div class="numbers">
                <p class="card-category">Viagens realizadas</p>
                <p class="card-title"><?php echo $count_trips->count_closed; ?><p>
              </div>
            </div>
          </div>
        </div>
        <div class="card-footer ">
        </div>
      </div>
    </div>

    <div class="col-lg-4 col-md-4 col-sm-6">
      <div class="card card-stats">
        <div class="card-body ">
          <div class="row">
            <div class="col-5 col-md-4">
              <div class="icon-big text-center text-green">
              <i class="fas fa-user-alt"></i>
              </div>
            </div>
            <div class="col-7 col-md-8">
              <div class="numbers">
                <p class="card-category ">Clientes</p>

                <p class="card-title"><?php echo $count_clients->count_clients; ?><p>
              </div>
            </div>
          </div>
        </div>
        <div class="card-footer">
        </div>
      </div>
    </div>

    <div class="col-lg-4 col-md-4 col-sm-6">
      <div class="card card-stats">
        <div class="card-body ">
          <div class="row">
            <div class="col-5 col-md-4">
              <div class="icon-big text-center text-green">
              <i class="fas fa-exclamation"></i>
              </div>
            </div>
            <div class="col-7 col-md-8">
              <div class="numbers">
                <p class="card-category ">Multas</p>

                <p class="card-title"><?php echo $count_traffic_ticket->count_traffic_ticket; ?><p>
              </div>
            </div>
          </div>
        </div>
        <div class="card-footer ">
        </div>
      </div>
    </div>
  </div>


  <div class="row">
    <div class="col-md-6">
      <div class="card ">
        <div class="card-header">
          <h3 class="card-title titulo_detalhe_financeiro">Viagens</h3>
        </div>
        <div class="card-body ">
          <div class="row">
            <div class="col-md">
              <table id="all_races_reports" class="table table-striped table-bordered" style="width:100%;border:none;">
                <thead>
                  <tr>
                    <th>Periodo</th>
                    <th>Quantidade</th>
                    <th>Km Rodados</th>
                  </tr>
                </thead>
                <tbody>
                  <?php foreach ($trips_per_month as $row): ?>
                    <?php $distance = (floatval($row->arrival_odometer) - floatval($row->departure_odometer)); ?>
                    <tr>
                      <td><?php echo $row->month; ?>/<?php echo $row->year; ?></td>
                      <td><?php echo $row->count_trips; ?></td>
                      <td><?php echo number_format($distance, 1, '.', '.'); ?></td>
                    </tr>
                  <?php endforeach; ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
        <div class="card-footer"></div>
      </div>
    </div>
    <div class="col-md-6">
      <div class="card ">
        <div class="card-header">
          <h3 class="card-title">Novos clientes</h3>
        </div>
        <div class="card-body ">
          <div class="row">
            <div class="col-md">
              <table id="new_clients_reports" class="table table-striped table-bordered" style="width:100%;border:none;">
                <thead>
                  <tr>
                    <th>Periodo</th>
                    <th>Quantidade</th>
                  </tr>
                </thead>
                <tbody>
                  <?php foreach ($clients_per_month as $row): ?>
                    <tr>
                      <td><?php echo $row->month; ?>/<?php echo $row->year; ?></td>
                      <td><?php echo $row->count_clients; ?></td>
                    </tr>
                  <?php endforeach; ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
        <div class="card-footer "></div>
      </div>
    </div>
  </div>

  <div class="row">
    <div class="col-md-6">
      <div class="card ">
        <div class="card-header">
          <h3 class="card-title titulo_detalhe_financeiro">Validade CNH</h3>
        </div>
        <div class="card-body ">
          <div class="row">
            <div class="col-md">
              <table id="license_expiration_reports" class="table table-striped table-bordered" style="width:100%;border:none;">
                <thead>
                  <tr>
                    <th>Piloto</th>
                    <th>Vencimento</th>
                  </tr>
                </thead>
                <tbody>
                  <?php foreach ($license_expiration as $row): ?>
                    <tr>
                      <td><?php echo $row->name; ?> <?php echo $row->surname; ?></td>
                      <td><?php echo $row->license_expiration; ?></td>
                    </tr>
                  <?php endforeach; ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
        <div class="card-footer"></div>
      </div>
    </div>

  </div>
  <?php $this->load->view('elements/footer');?>
