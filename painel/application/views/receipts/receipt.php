<?php

require_once 'vendor/autoload.php';

date_default_timezone_set('America/Sao_Paulo');
$dia_atual = date('d-m-Y H:i:s');

$register = date_create($trip['register']);
$locale = ($trip['locale'] == 'I') ? 'Interno' : 'Fora da cidade';
$service = ($trip['service'] == 'P') ? 'Pessoa' : 'Material';
$name = (isset($trip['name'])) ? $trip['name'].' '.$trip['surname'] : $trip['trade_name'];
$departure_time = date_create($trip['departure_time']);
$phone = (isset($trip['legal_phone'])) ? $trip['legal_phone'] : $trip['juridical_phone'];
$mpdf = new \Mpdf\Mpdf();
$mpdf->WriteHTML('<h3 style="text-align: center;">Recibo</h3>
<hr>
<div style="border: 1px solid black; height: 50px; padding: 5px 4px;">
Solicitado às:  '.date_format($register, 'd/m/Y H:i:s').'
  |
Localidade: '.$locale.'
  |
Tipo de serviço: '.$service.'
<br><br>
Cliente: '.$name.' |
Telefone: '.$phone.'
</div>
<br>
Endereço de saída: '.$departure_address['departure_address'].', '.$departure_address['departure_number'].', '.$departure_address['departure_complement'].', '.$departure_address['departure_city'].', '.$departure_address['departure_state'].'
  <br><br>
Endereço de chegada: '.$arrival_address['arrival_address'].', '.$arrival_address['arrival_number'].', '.$arrival_address['arrival_complement'].', '.$arrival_address['arrival_neighborhood'].', '.$arrival_address['arrival_city'].', '.$arrival_address['arrival_state'].'
  <br><br>
Dados do piloto: '.$trip['biker_name'].' '.$trip['biker_surname'].'
  |
Placa da moto: '.$trip['license_plate'].'
<br><br>
<table border="1" width="100%">
<tr>
<th>Valor previsto</th>
<th>Horario de partida</th>
</tr>');
  $html = '
  <tr>
    <td style="text-align: center;">R$ '.$trip['expected_value'].'</td>
    <td style="text-align: center;">'.date_format($departure_time, 'd/m/Y H:i:s').'</td>
  </tr>';
$mpdf->WriteHTML($html);

$mpdf->WriteHTML('</table>');
$mpdf->WriteHTML('<br><br><br><div style="float: left; border-top: 1px solid black; width: 200px;">
<p style="text-align: center;">Nome</p>
</div>
<div style="margin-left: 40px; float: left; border-top: 1px solid black; width: 200px;">
<p style="text-align: center;">CPF</p>
</div>
<div style="margin-left: 40px;  float: left; border-top: 1px solid black; width: 200px;">
<p style="text-align: center;">Assinatura</p>
</div>');
$mpdf->WriteHTML('<p style="text-align: center;">'.$dia_atual.'</p>');

ob_clean();
$mpdf->Output();

?>
