<?php $this->load->view('elements/header');?>
<div class="wrapper ">
  <?php $this->load->view('elements/sidebar_lateral');?>
  <?php $this->load->view('elements/sidebar');?>

  <div class="content">
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header">
            <h4 class="card-title" style="display: inline;">Fornecedores</h4>
            <a href="<?php echo base_url()?>provider/register" class="btn btn-primary float-md-right float-sm-right float-xs-right">Cadastrar</a>
          </div>
          <div class="card-body">
            <div class="table-responsive">
              <table id="table_provider" class="table table-striped">
                <thead class=" text-primary">
                  <th>Nome</th>
                  <th>Email</th>
                  <th>Detalhe</th>
                </thead>
                <tbody>
                  <?php foreach ($providers as $row): ?>
                    <tr>
                      <td><?php echo (isset($row->name)) ? $row->name.' '.$row->surname : $row->trade_name; ?></td>
                      <td><?php echo (isset($row->email_juridical)) ? $row->email_juridical : $row->email_legal ; ?></td>
                      <td>
                        <a href="<?php echo base_url()?>provider/detail/<?php echo $row->id?>">
                          <i class="nc-icon nc-zoom-split"></i>
                        </a>
                      </td>
                    </tr>
                  <?php endforeach; ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

<?php $this->load->view('elements/footer');?>
