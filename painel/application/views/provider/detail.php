<?php $this->load->view('elements/header');?>
<div class="wrapper ">
  <?php $this->load->view('elements/sidebar_lateral');?>
  <?php $this->load->view('elements/sidebar');?>
  <div class="content">
    <div class="row">
      <div class="col-md-6">
        <div class="card">
          <div class="card-header text-center">
              <div class="col-md-12">
                <form action="#" method="post" enctype="multipart/form-data">
                <div class="row">
                    <!-- <div class="col-md-12">
                    <label class="newimgum">
                        <img id="imgum" class="icone_perfil" src="<?php echo base_url(); ?>assets/build/img/profile_clients/">
                        <input id="picimgum" class='pis' name="img_client" type="file" style="display:none;">
                      </label>
                    </div>
                    <div class="col-md-12">
                      <button class="btn btn-sm btn-primary" style="display:none;" id="save" type="submit" name="update_image">salvar</button>
                    </div> -->
                  </form>
              </div>
              <h4 class="card-title text-center">
                <?php if($provider->type_provider == 2){echo $provider->trade_name;}else if($provider->type_provider == 1){echo $provider->name.' '.$provider->surname;} ?>
              </h4>
            </div>
            <div class="card-body text-center">
              <?php echo $provider->zip_code.' - '.$provider->address.' - '.$provider->number.' - '.$provider->neighborhood.' - '.$provider->city.' - '.$provider->state; ?>
            </div>
          </div>


        </div>
        <div class="card">
          <div class="card-header">
            <h4 class="card-title text-center">Dados bancarios</h4>
          </div>
          <div class="card-body">
            <button type="button" class="btn btn-primary" name="button" data-toggle="modal" data-target="#modalAddDadosBancarios">Adicionar dados</button>
            <table id="table_bank_data" class="table table-striped">
              <thead>
                <tr>
                  <th scope="col">Cpf</th>
                  <th scope="col">Agencia</th>
                  <th scope="col">Numero</th>
                  <th scope="col">Banco</th>
                  <th scope="col">tipo</th>
                  <th scope="col"></th>
                </tr>
              </thead>
              <tbody>
                <?php foreach ($bank_data as $row): ?>
                  <tr>
                    <td><?php echo $row->cpf; ?></td>
                    <td><?php echo $row->agency; ?></td>
                    <td><?php echo $row->account_number; ?></td>
                    <td><?php echo $row->bank; ?></td>
                    <td><?php echo $row->account_type; ?></td>
                    <td>
                      <a data-toggle="modal" data-target="#modalEditarDadosBancarios"
                          data-id="<?php echo $row->id; ?>" data-cpf="<?php echo $row->cpf; ?>" data-agency="<?php echo $row->agency; ?>" data-account_number="<?php echo $row->account_number; ?>"
                          data-bank="<?php echo $row->bank; ?>" data-account_type="<?php echo $row->account_type; ?>" id="edit_bank_data">
                        <i class="nc-icon nc-zoom-split text-primary"></i>
                      </a>
                    </td>
                  </tr>
                <?php endforeach; ?>
              </tbody>
            </table>
        </div>
      </div>

      </div>
      <div class="col-md-6">
        <div class="card card-user">
          <div class="card-body">
            <form action="<?php echo base_url()?>provider/update_provider/<?php echo $provider->id; ?>"
              id="form_alterar_cliente" method="post">
              <?php if ($provider->type_provider == 1): ?>
              <section id="form_legal_person">
                <div class="row">
                  <div class="col-md-4 pr-md-1">
                    <div class="form-group">
                      <label for="cpf">Cpf</label>
                      <input type="text" name="cpf" id="cpf" class="form-control" placeholder="Cpf"
                        value="<?php echo $provider->cpf; ?>">
                      <?php echo form_error('cpf'); ?>
                    </div>
                  </div>
                  <div class="col-md-4 pl-md-1 pr-md-1">
                    <div class="form-group">
                      <label for="name">Nome</label>
                      <input type="text" name="name" id="name" class="form-control" placeholder="Nome"
                        value="<?php echo $provider->name; ?>">
                      <?php echo form_error('name'); ?>
                    </div>
                  </div>
                  <div class="col-md-4 pr-1">
                    <div class="form-group">
                      <label for="surname">Sobrenome</label>
                      <input type="text" name="surname" id="surname" class="form-control" placeholder="Sobrenome"
                        value="<?php echo $provider->surname; ?>">
                      <?php echo form_error('surname'); ?>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-6 pr-md-1">
                    <div class="form-group">
                      <label for="telefone">Telefone</label>
                      <input type="text" name="phone" id="telefone" class="form-control" placeholder="Telefone"
                        value="<?php  echo $provider->phone_legal; ?>">
                      <?php echo form_error('telefone'); ?>
                    </div>
                  </div>
                  <div class="col-md-6 pl-md-1">
                    <div class="form-group">
                      <label for="email">E-mail</label>
                      <input type="text" name="email" id="email" class="form-control" placeholder="E-mail"
                        value="<?php echo $provider->email_legal; ?>">
                      <?php echo form_error('email'); ?>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-12">
                    <div class="form-group">
                      <label for="phone_fixed">Telefone fixo</label>
                      <input type="text" name="phone_fixed" id="phone_fixed" class="form-control" placeholder="Telefone fixo"
                        value="<?php echo $provider->phone_fixed_legal; ?>">
                    </div>
                  </div>
                </div>
              </section>
              <input type="hidden" name="id_legal" value="<?php //echo $legal_client->id_legal; ?>">
              <input type="hidden" name="id_client" value="<?php //echo $legal_client->id; ?>">
              <?php endif; ?>
              <?php if ($provider->type_provider == 2): ?>
              <section id="form_juridical_person">
                <div class="row">
                  <div class="col-md-6 pr-1">
                    <div class="form-group">
                      <label for="cnpj">CNPJ</label>
                      <input type="text" name="cnpj" id="cnpj" class="form-control" placeholder="CNPJ"
                        value="<?php echo $provider->cnpj; ?>">
                      <?php echo form_error('cnpj'); ?>
                    </div>
                  </div>
                  <div class="col-md-6 pl-1">
                    <div class="form-group">
                      <label for="empresa">Empresa</label>
                      <input type="text" name="trade_name" id="empresa" class="form-control" placeholder="Empresa"
                        value="<?php echo $provider->trade_name; ?>">
                      <?php echo form_error('trade_name'); ?>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-6 pr-1">
                    <div class="form-group">
                      <label for="responsavel">Responsável</label>
                      <input type="text" name="answerable" id="responsavel" class="form-control"
                        placeholder="Responsável" value="<?php echo $provider->answerable; ?>">
                      <?php echo form_error('answerable'); ?>
                    </div>
                  </div>
                  <div class="col-md-6 pl-1">
                    <div class="form-group">
                      <label for="legal_name">Nome legal</label>
                      <input type="text" name="legal_name" id="legal_name" class="form-control" placeholder="Nome legal"
                        value="<?php echo $provider->legal_name; ?>">
                      <?php echo form_error('legal_name'); ?>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-6 pr-1">
                    <div class="form-group">
                      <label for="telefone">Telefone</label>
                      <input type="text" name="phone" id="telefone" class="form-control" placeholder="Telefone"
                        value="<?php echo $provider->phone_juritical; ?>">
                      <?php echo form_error('telefone'); ?>
                    </div>
                  </div>
                  <div class="col-md-6 pl-1">
                    <div class="form-group">
                      <label for="email">E-mail</label>
                      <input type="text" name="email" id="email" class="form-control" placeholder="E-mail"
                        value="<?php echo $provider->email; ?>">
                      <?php echo form_error('email'); ?>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-12">
                    <div class="form-group">
                      <label for="phone_fixed">Telefone fixo</label>
                      <input type="text" name="phone_fixed" id="phone_fixed" class="form-control" placeholder="Telefone fixo"
                        value="<?php echo $provider->phone_fixed_juridical; ?>">
                    </div>
                  </div>
                </div>
              </section>
              <input type="hidden" name="id_juridical" value="<?php //echo $juridical_client->id_juridical; ?>">
              <input type="hidden" name="id_client" value="<?php //echo $juridical_client->id; ?>">
              <?php endif; ?>

              <section id="common">
                <div class="row">
                  <div class="col-md-12">
                    <h4>Endereço</h4>
                  </div>
                  <div class="col-md-4 pr-md-1">
                    <div class="form-group">
                      <label for="cep">Cep</label>
                      <input type="text" name="cep" id="cep" class="form-control"
                        value="<?php echo $provider->zip_code; ?>">
                    </div>
                  </div>
                  <div class="col-md-6 pr-md-1 pl-md-1">
                    <div class="form-group">
                      <label for="address">Rua</label>
                      <input type="text" name="address" id="address" class="form-control"
                        value="<?php echo $provider->address; ?>">
                    </div>
                  </div>
                  <div class="col-md pl-md-1">
                    <div class="form-group">
                      <label for="number">Numero</label>
                      <input type="text" name="number" id="number" class="form-control"
                        value="<?php echo $provider->number; ?>">
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-5 pr-md-1">
                    <div class="form-group">
                      <label for="neighborhood">Bairro</label>
                      <input type="text" name="neighborhood" id="neighborhood" class="form-control"
                        value="<?php echo $provider->neighborhood; ?>">
                    </div>
                  </div>
                  <div class="col-md-5 pr-md-1 pl-md-1">
                    <div class="form-group">
                      <label for="city">Cidade</label>
                      <input type="text" name="city" id="city" class="form-control"
                        value="<?php echo $provider->city; ?>">
                    </div>
                  </div>
                  <div class="col-md-2 pl-md-1">
                    <div class="form-group">
                      <label for="uf">UF</label>
                      <input type="text" name="uf" id="uf" class="form-control" value="<?php echo $provider->state; ?>">
                      <input type="hidden" name="id_address" id="id_address" value="<?php echo $provider->id_address; ?>">
                    </div>
                  </div>
                  <hr>
                </div>
              </section>


              <div class="row">
                <div class="update ml-auto mr-auto">
                  <button
                    <?php if($provider->type_provider == 1){ echo "id='update_legal_provider' name='update_legal_provider'";}?>
                    <?php if($provider->type_provider == 2){ echo "id='update_juridical_provider' name='update_juridical_provider'";}?>
                    class="btn btn-primary btn-round">
                    Alterar
                  </button>
                </div>
              </div>
            </form>
          </div>
        </div>

      </div>
    </div>

  </div>
  <?php $this->load->view('elements/footer');?>

  <!-- Modal -->
  <div class="modal fade" id="modalAddDadosBancarios" tabindex="-1" role="dialog" aria-labelledby="modalAddContrato" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Adicionar dados bancarios</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <form action="<?php echo base_url(); ?>provider/register_bank_data/" method="post">
            <div class="row">
              <div class="col-md-4">
                <div class="form-group">
                  <label for="cpf">Cpf:</label>
                  <input type="text" name="cpf" value="" class="form-control" id="cpf">
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <label for="agency">Agencia:</label>
                  <input type="text" name="agency" value="" class="form-control">
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <label for="account_number">Numero de conta:</label>
                  <input type="text" name="account_number" value="" class="form-control">
                </div>
              </div>

            </div>
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label for="bank">Banco:</label>
                  <input type="text" name="bank" value="" class="form-control">
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="type_account">Tipo de conta:</label>
                  <select class="form-control" id="type_account" name="type_account">
                    <option value=""></option>
                    <option value="Corrente">Corrente</option>
                    <option value="Poupança">Poupança</option>
                  </select>
                </div>
              </div>

            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
          <input type="hidden" name="id_provider" value="<?php echo $provider->id; ?>">
          <button type="submit" name="register_bank_data" class="btn btn-primary">Enviar</button>
        </div>
      </form>
      </div>
    </div>
  </div>

  <!-- Modal editar dados bancarios -->
  <div class="modal fade" id="modalEditarDadosBancarios" tabindex="-1" role="dialog" aria-labelledby="modalAddContrato" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Editar dados bancarios</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <form action="<?php echo base_url(); ?>provider/update_bank_data/<?php echo $provider->id; ?>" method="post">
            <div class="row">
              <div class="col-md-4">
                <div class="form-group">
                  <label for="cpf">Cpf:</label>
                  <input type="text" name="cpf_edit" value="" class="form-control" id="cpf_edit">
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <label for="agency">Agencia:</label>
                  <input type="text" name="agency_edit" value="" class="form-control" id="agency_edit">
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <label for="account_number">Numero de conta:</label>
                  <input type="text" name="account_number_edit" value="" class="form-control" id="account_number_edit">
                </div>
              </div>

            </div>
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label for="bank">Banco:</label>
                  <input type="text" name="bank_edit" value="" class="form-control" id="bank_edit">
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="type_account">Tipo de conta:</label>
                  <select class="form-control" id="account_type_edit" name="account_type_edit">
                    <option value=""></option>
                    <option value="Corrente">Corrente</option>
                    <option value="Poupança">Poupança</option>
                  </select>
                </div>
              </div>

            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
          <input type="hidden" name="id_bank" value="" id="id_bank">
          <button type="submit" name="update_bank_data" class="btn btn-primary">Enviar</button>
        </div>
      </form>
      </div>
    </div>
  </div>
