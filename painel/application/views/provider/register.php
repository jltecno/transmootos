<?php $this->load->view('elements/header');?>
<div class="wrapper ">
  <?php $this->load->view('elements/sidebar_lateral');?>
  <?php $this->load->view('elements/sidebar');?>

  <div class="content">
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header">
            <h4 class="card-title" style="display: inline;">Fornecedores</h4>
            <!-- <a href="<?php echo base_url()?>provider/register" class="btn btn-primary float-md-right float-sm-right float-xs-right">Cadastrar</a> -->
          </div>
          <div class="card-body">
            <form action="<?php echo base_url(); ?>provider/register_provider" method="post">
              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <input type="radio" name="type_provider" id="legal_person" class="type_client" value="1" checked>
                    <label for="legal_person">Pessoa física</label>
                    <input type="radio" name="type_provider" id="juridical_person" class="type_client" value="2">
                    <label for="juridical_person">Pessoa jurídica</label>
                  </div>
                </div>
              </div>
              <section id="form_legal_person">
                <div class="row">
                  <div class="col-md-4 pr-1">
                    <div class="form-group">
                      <label for="cpf">Cpf</label>
                      <input type="text" name="cpf" id="cpf" class="form-control" placeholder="Cpf"
                        value="<?php echo set_value('cpf'); ?>">
                      <?php echo form_error('cpf'); ?>
                    </div>
                  </div>
                  <div class="col-md-4 pr-1">
                    <div class="form-group">
                      <label for="name">Nome</label>
                      <input type="text" name="name" id="name" class="form-control" placeholder="Nome"
                        value="<?php echo set_value('name'); ?>">
                      <?php echo form_error('name'); ?>
                    </div>
                  </div>
                  <div class="col-md-4 pr-1">
                    <div class="form-group">
                      <label for="surname">Sobrenome</label>
                      <input type="text" name="surname" id="surname" class="form-control" placeholder="Sobrenome"
                        value="<?php echo set_value('surname'); ?>">
                      <?php echo form_error('surname'); ?>
                    </div>
                  </div>
                </div>
              </section>
              <section id="form_juridical_person" style="display: none;">
                <div class="row">
                  <div class="col-md-6 pr-1">
                    <div class="form-group">
                      <label for="cnpj">CNPJ</label>
                      <input type="text" name="cnpj" id="cnpj" class="form-control" placeholder="CNPJ"
                        value="<?php echo set_value('cnpj'); ?>">
                      <?php echo form_error('cnpj'); ?>
                    </div>
                  </div>
                  <div class="col-md-6 pl-1">
                    <div class="form-group">
                      <label for="legal_name">Razão social</label>
                      <input type="text" name="legal_name" id="legal_name" class="form-control"
                        placeholder="Razão social" value="<?php echo set_value('legal_name'); ?>">
                      <?php echo form_error('legal_name'); ?>
                    </div>
                  </div>

                </div>
                <div class="row">
                  <div class="col-md-6 pr-1">
                    <div class="form-group">
                      <label for="empresa">Nome fantasia</label>
                      <input type="text" name="trade_name" id="empresa" class="form-control" placeholder="Nome fantasia"
                        value="<?php echo set_value('trade_name'); ?>">
                      <?php echo form_error('trade_name'); ?>
                    </div>
                  </div>
                  <div class="col-md-6 pl-1">
                    <div class="form-group">
                      <label for="responsavel">Responsável</label>
                      <input type="text" name="answerable" id="responsavel" class="form-control"
                        placeholder="Responsável" value="<?php echo set_value('answerable'); ?>">
                      <?php echo form_error('answerable'); ?>
                    </div>
                  </div>
                </div>
              </section>
              <section id="common">
                <div class="row">
                  <div class="col-md-4">
                    <div class="form-group">
                      <label for="phone">Telefone</label>
                      <input type="text" name="phone" id="phone" class="form-control" placeholder="phone"
                        value="<?php echo set_value('phone'); ?>">
                      <?php echo form_error('phone'); ?>
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="form-group">
                      <label for="phone_fixed">Telefone fixo</label>
                      <input type="text" name="phone_fixed" id="phone_fixed" class="form-control" placeholder="Telefone fixo"
                        value="<?php echo set_value('phone_fixed'); ?>">
                      <?php echo form_error('phone_fixed'); ?>
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="form-group">
                      <label for="email">email</label>
                      <input type="email" name="email" id="email" value="<?php echo set_value('phone_fixed'); ?>" placeholder="email" class="form-control">
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-12">
                    <h4>Endereço</h4>
                  </div>
                  <div class="col-md-4 pr-md-1">
                    <div class="form-group">
                      <label for="cep">Cep</label>
                      <input type="text" name="cep" id="cep" class="form-control"
                        value="<?php echo set_value('cep'); ?>">
                      <?php echo form_error('cep'); ?>
                    </div>
                  </div>
                  <div class="col-md-6 pr-md-1 pl-md-1">
                    <div class="form-group">
                      <label for="address">Rua</label>
                      <input type="text" name="address" id="address" class="form-control"
                        value="<?php echo set_value('address'); ?>">
                      <?php echo form_error('address'); ?>
                    </div>
                  </div>
                  <div class="col-md pl-md-1">
                    <div class="form-group">
                      <label for="number">Numero</label>
                      <input type="text" name="number" id="number" class="form-control"
                        value="<?php echo set_value('number'); ?>">
                      <?php echo form_error('number'); ?>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-5 pr-md-1">
                    <div class="form-group">
                      <label for="neighborhood">Bairro</label>
                      <input type="text" name="neighborhood" id="neighborhood" class="form-control"
                        value="<?php echo set_value('neighborhood'); ?>">
                      <?php echo form_error('neighborhood'); ?>
                    </div>
                  </div>
                  <div class="col-md-5 pr-md-1 pl-md-1">
                    <div class="form-group">
                      <label for="city">Cidade</label>
                      <input type="text" name="city" id="city" class="form-control"
                        value="<?php echo set_value('city'); ?>">
                      <?php echo form_error('city'); ?>
                    </div>
                  </div>
                  <div class="col-md-2 pl-md-1">
                    <div class="form-group">
                      <label for="uf">UF</label>
                      <input type="text" name="uf" id="uf" class="form-control" value="<?php echo set_value('uf'); ?>">
                      <?php echo form_error('uf'); ?>
                    </div>
                  </div>
                  <hr>
                </div>
              </section>
              <!-- <div class="row">
                <div class="col-md">
                  <label class="newimgum">
                    <img id="imgum" class="icone_perfil" src="<?php echo base_url(); ?>assets/build/img/icon/perfil.png">
                    <input id="picimgum" class='pis' name="img_client" type="file" style="display:none;">
                  </label>
                </div>
              </div> -->
              <div class="row">
                <div class="col-md-12">
                  <button type="submit" name="register_provider"
                    class="btn btn-primary btn-round float-md-right float-sm-right float-xs-right">Cadastrar</button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>

<?php $this->load->view('elements/footer');?>
