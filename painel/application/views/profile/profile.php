<?php $this->load->view('elements/header');?>
  <div class="wrapper ">
    <?php $this->load->view('elements/sidebar_lateral');?>
    <?php $this->load->view('elements/sidebar');?>
    <div class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="card">
            <div class="card-header">
              <h4 class="card-title d-flex justify-content-center">Olá <?php echo $_SESSION['user']['name']; ?></h4>
              <!-- <a href="<?php echo base_url()?>motorcycles/register" class="btn btn-primary float-md-right float-sm-right float-xs-right">Cadastrar</a> -->

            </div>

            <div class="card-body">
              <div class="row">
                <div class="col-md-12 d-flex justify-content-center">
                  <h4>Redefinir Senha</h4>
                </div>
              </div>
              <div class="row d-flex justify-content-center">
                <form action="<?php echo base_url(); ?>profile/update_user_password" method="post">
                  <div class="row">
                    <div class="col-md-12">
                      <div class="form-group">
                        <label for="current password">Senha atual:</label>
                        <input type="password" id="current_password" name="current_password" class="form-control" placeholder="Senha atual" required>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-12">
                      <div class="form-group">
                        <label for="new_password">Nova Senha:</label>
                        <input type="password" id="new_password" name="new_password" class="form-control" placeholder="Nova Senha" required>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-12">
                      <div class="form-group">
                        <label for="repeat_password">Repita a nova senha:</label>
                        <input type="password" id="repeat_password" name="repeat_password" class="form-control" placeholder="Repita a nova senha" required>
                      </div>
                    </div>
                  </div>
                  <div class="row" id="error_new_password" style="display: none;">
                    <div class="col-md-12">
                        <p class="alert alert-danger">As senhas nao se coincidem</p>
                    </div>
                  </div>
                  <?php if ($this->session->flashdata('error') != null): ?>
                    <div class="row">
                      <div class="col-md-12">
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                          <?php echo $this->session->flashdata('error'); ?>
                        </div>
                      </div>
                    </div>
                  <?php endif; ?>
                  <div class="row">
                    <div class="col-md-12">
                      <div class="form-group">
                        <button type="submit" name="update_user_password" class="btn btn-primary btn-lg btn-block" id="update_user_password">Redefinir</button>
                      </div>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
<?php $this->load->view('elements/footer');?>
