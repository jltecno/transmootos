<?php

require_once 'vendor/autoload.php';

date_default_timezone_set('America/Sao_Paulo');
$dia_atual = date('d-m-Y H:i:s');

$mpdf = new \Mpdf\Mpdf();

foreach ($package as $row) {
  $mpdf->WriteHTML('<div style="border: 1px solid black; height: 50px; padding: 5px 4px;">
  <strong>Pacote nº: '.$row['dados']['id_package'].'</strong><br>
  Solicitado às: '.$row['dados']['register'].' <br>
  <strong>Remetente: </strong> <br> '.$row['dados']['name'].' '.$row['dados']['surname'].' '.$row['dados']['trade_name'].' | Telefone: '.$row['dados']['legal_phone'].' '.$row['dados']['juridical_phone'].'<br>
  '.$row['departure_address']['departure_address'].', '.$row['departure_address']['departure_number'].', '.$row['departure_address']['departure_neighborhood'].', '.$row['departure_address']['departure_city'].' - '.$row['departure_address']['departure_state'].' <br>
  <strong>Destinatario: </strong> <br> '.$row['dados']['destinatary'].' <br>
  '.$row['arrival_address']['arrival_address'].', '.$row['arrival_address']['arrival_number'].', '.$row['arrival_address']['arrival_neighborhood'].', '.$row['arrival_address']['arrival_city'].' - '.$row['arrival_address']['arrival_state'].' <br>

  </div>
  <br><br>');
}

ob_clean();
$mpdf->Output();

?>
