<?php

require_once 'vendor/autoload.php';

date_default_timezone_set('America/Sao_Paulo');
$dia_atual = date('d-m-Y H:i:s');

$mpdf = new \Mpdf\Mpdf();
$mpdf->WriteHTML('<h3 style="text-align: center;">Recibo de entregas</h3>
<hr>
<div style=" padding: 5px 4px 0 5px; margin-bottom: 20px;">
Cidade:  | Piloto: '.$trip['biker_name'].' '.$trip['biker_surname'].' | Moto: '.$trip['license_plate'].' | odometro de saida: '.$trip['departure_odometer'].'
</div>');

foreach ($packages as $row) {
  $mpdf->WriteHTML('<div style="border: 1px solid black; height: 50px; padding: 5px 4px;">
  <div style="width: 50%; float: left;">
    Solicitado às: '.$row['dados']['register'].' <br>
  </div>
  <div style="width: 50%; float: right; text-align: right;">
    <strong>Pacote nº: '.$row['dados']['id_package'].'</strong>
  </div>
  <div style="clear: both; width: 50%; float: left;">
    <strong>Remetente: </strong> <br>
    '.$row['dados']['name'].' '.$row['dados']['surname'].' '.$row['dados']['trade_name'].' | Telefone: '.$row['dados']['legal_phone'].' '.$row['dados']['juridical_phone'].' <br>
    '.$row['departure_address']['departure_address'].', '.$row['departure_address']['departure_number'].', '.$row['departure_address']['departure_neighborhood'].', '.$row['departure_address']['departure_city'].' - '.$row['departure_address']['departure_state'].' <br>
  </div>
  <div style="width: 50%; float: right;">
    <strong>Destinatário: </strong> <br>
    '.$row['dados']['destinatary'].' <br>
    '.$row['arrival_address']['arrival_address'].', '.$row['arrival_address']['arrival_number'].', '.$row['arrival_address']['arrival_neighborhood'].', '.$row['arrival_address']['arrival_city'].' - '.$row['arrival_address']['arrival_state'].' <br>
  </div>
  <div style="clear: both; width: 50%; float: left;">
    <strong>Pagamento por:</strong> '.$row['dados']['payment'].' - <strong>'.$row['dados']['status_it_payment'].'</strong>
  </div>
  <div style="width: 50%; float: right;">
    <strong>Entregar:</strong> '.$row['dados']['receiver'].' ( '.$row['dados']['people_authorized'].' )
  </div>
  <div style="clear: both; width: 100%; float: left;">
    <strong>Observação:</strong> '.$row['dados']['observation'].'
  </div>

  <br><br><br>
  <div style="clear: both; float: left; border-top: 1px solid black; width: 200px;">
  <span style="display: block; text-align: center;">Nome</span>
  </div>
  <div style="margin-left: 40px; float: left; border-top: 1px solid black; width: 200px;">
  <span style="display: block; text-align: center;">CPF</span>
  </div>
  <div style="margin-left: 40px;  float: left; border-top: 1px solid black; width: 200px;">
  <span style="display: block; text-align: center;">Assinatura</span>
  </div>
  </div>
  <br><br>');
}



ob_clean();
$mpdf->Output();

?>
