<div class="modal fade" id="modalCreateTrip" tabindex="-1" role="dialog" aria-labelledby="clients"
  aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="modalCadastroMulta">Criar viagem</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <!-- <form action="<?php echo base_url(); ?>packages/register_trip_packages" method="post"> -->
      <form action="<?php echo base_url(); ?>shared_trips/register_shared_trips" method="post">
        <div class="modal-body">
          <div class="row">
            <div class="col-md">
              <div class="form-group">
                <label for="local">Cidade</label>
                <select class="form-control" id="id_city" name="id_city">
                  <option value=""></option>
                  <?php foreach ($cities_package as $row): ?>
                    <option value="<?php echo $row->id; ?>"><?php echo $row->description; ?></option>
                  <?php endforeach; ?>
                </select>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-4 pr-md-1 ">
              <div class="form-group">
                <label for="biker">Piloto </label>
                <select class="form-control" name="biker" id="biker">
                  <option value=""></option>
                  <?php foreach ($bikers as $row): ?>
                    <option value="<?php echo $row->id; ?>"><?php echo $row->name; ?></option>
                  <?php endforeach; ?>
                </select>
              </div>
            </div>
            <div class="col-md">
              <div class="form-group">
                <label for="motorcycle">Moto</label>
                <select class="form-control" name="motorcycle" id="motorcycle">
                  <option value=""></option>
                  <?php foreach ($motorcycles as $row): ?>
                    <option value="<?php echo $row->id; ?>"><?php echo $row->code_motorcycle; ?> | <?php echo $row->license_plate; ?></option>
                  <?php endforeach; ?>
                </select>
              </div>
            </div>
            <div class="col-md pl-md-1">
              <div class="form-group">
                <label for="motorcycle">Odometro inicial</label>
                <input type="text" name="departure_odometer" value="" class="form-control odometer">
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md">
              <div class="form-group">
                <label for="biker">Encomendas</label>
                <div id="div_packages">

                </div>
              </div>
            </div>
          </div>
        </div>

        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
          <input type="hidden" name="leave" value="P">
          <button type="submit" name="register_trip_packages" class="btn btn-primary">Cadastro </button>
        </div>
      </form>
    </div>
  </div>
</div>
