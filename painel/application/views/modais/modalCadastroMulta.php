<div class="modal fade" id="modalCadastroMulta" tabindex="-1" role="dialog" aria-labelledby="clients"
  aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="modalCadastroMulta">Multa</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="<?php echo base_url(); ?>motorcycles/register_traffic_ticket" method="post">
      <div class="modal-body">
          <div class="row">
            <div class="col-md pr-md-1">
              <div class="form-group">
                <label for="data_ocorrida">Data do ocorrido</label>
                <input type="date" name="date_event" id="data_ocorrida" class="form-control"
                  value="<?php echo set_value('date_event'); ?>">
                <?php echo form_error('date_event'); ?>
              </div>
            </div>
            <div class="col-md pl-md-1">
              <div class="form-group">
                <label for="local">Local</label>
                <input type="text" name="location" id="local" class="form-control" value="<?php echo set_value('location'); ?>">
                <?php echo form_error('location'); ?>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md pr-md-1">
              <div class="form-group">
                <label for="tipo_enfracao">Tipo de enfração</label>
                <input type="text" name="type_infringement" id="tipo_enfracao" class="form-control"
                  value="<?php echo set_value('type_infringement'); ?>">
                <?php echo form_error('type_infringement'); ?>
              </div>
            </div>
            <div class="col-md pl-md-1">
              <div class="form-group">
                <label for="valor">Valor</label>
                <input type="text" name="amount" id="valor" class="form-control" value="<?php echo set_value('amount'); ?>">
                <?php echo form_error('amount'); ?>
              </div>
            </div>
          </div>

          <div class="row">
            <div class="col-md-4 pr-md-1 ">
              <div class="form-group">
                <label for="piloto">Piloto</label>
                <select class="form-control" name="biker" id="piloto">
                  <option value=""></option>
                  <?php foreach ($bikers as $row): ?>
                    <option value="<?php echo $row->id; ?>"><?php echo $row->name; ?> <?php echo $row->surname; ?></option>
                  <?php endforeach; ?>
                </select>
              </div>
            </div>
            <div class="col-md pl-md-1">
              <div class="form-group">
                <label for="vencimento">Vencimento</label>
                <input type="date" name="due_date" id="vencimento" class="form-control"
                  value="<?php echo set_value('due_date'); ?>">
                <?php echo form_error('due_date'); ?>
              </div>
            </div>
          </div>
        </div>

        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
          <button type="submit" name="register_traffic_ticket" class="btn btn-primary">Cadastro </button>
        </div>
      </form>
    </div>
  </div>
</div>
