<div class="modal fade" id="modalTripPackages" tabindex="-1" role="dialog" aria-labelledby="clients"
  aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="modalCadastroMulta">Viagem compartilhada em andamento</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="<?php echo base_url(); ?>shared_trips/stop_trip_packages" method="post">
        <div class="modal-body">
          <div class="row">
            <div class="col">
              <button type="button" name="button" id="pog_packages">Chamar</button>
              <input type="hidden" name="id_trip" id="id_trip" value="">
            </div>
          </div>
          <div class="row">
            <div class="col">
              <h4 class="d-inline">Piloto - Moto</h4><a href="" id="open_receipt" class="btn btn-primary float-right" target="_blank">Recibo</a>
            </div>
          </div>
          <div class="row">
              <div class="col-md-4">
                <div class="form-group">
                  <label for="biker">Piloto</label>
                  <input type="text" name="biker" id="biker" class="form-control" value="" readonly>
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <label for="motorcycle">Moto</label>
                  <input type="text" name="motorcycle" id="motorcycle" class="form-control" value="" readonly>
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <label for="horario_partida">Horário de partida</label>
                  <input type="text" name="departure_time" id="departure_time" class="form-control" value="" readonly>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md pr-md-1">
                <div class="form-group">
                  <label for="departure_odometer">Odómetro de saída</label>
                  <input type="text" name="departure_odometer" id="departure_odometer" class="form-control odometer" value="" readonly>
                </div>
              </div>
              <div class="col-md pl-md-1">
                <div class="form-group">
                  <label for="arrival_odometer">Odómetro de chegada</label>
                  <input type="text" name="arrival_odometer" id="arrival_odometer" class="form-control odometer" value="">
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md">
                <div class="form-group">
                  <label for="biker">Encomendas</label>
                  <div class="row packages_trips">

                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
            <input type="hidden" name="leave" value="P">
            <button type="submit" name="stop_trip_packages" class="btn btn-primary">Encerrar viagem</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
