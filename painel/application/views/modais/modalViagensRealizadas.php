<div class="modal fade" id="modalViagensRealizadas" tabindex="-1" role="dialog" aria-labelledby="clients"
  aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="modalDashAndamento">Andamento</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">

        <h5>Serviço</h5>
        <div class="row">
          <div class="col-md pr-md-1">
            <div class="form-group">
              <label for="ponto_inicio">Ponto de ínicio</label>
              <input type="text" name="ponto_inicio" id="ponto_inicio" class="form-control"
                value="<?php echo set_value('ponto_inicio'); ?>" readonly>
              <?php echo form_error('ponto_inicio'); ?>
            </div>
          </div>
          <div class="col-md pl-md-1">
            <div class="form-group">
              <label for="ponto_final">Ponto de final</label>
              <input type="text" name="ponto_final" id="ponto_final" class="form-control"
                value="<?php echo set_value('ponto_final'); ?>" readonly>
              <?php echo form_error('ponto_final'); ?>
            </div>
          </div>
        </div>

        <div class="row">
          <div class="col-md pr-md-1">
            <div class="form-group">
              <label for="horario_partida">Horário de partida</label>
              <input type="time" name="horario_partida" id="horario_partida" class="form-control"
                value="<?php echo set_value('horario_partida'); ?>" readonly>
              <?php echo form_error('horario_partida'); ?>
            </div>
          </div>
          <div class="col-md pl-md-1">
            <div class="form-group">
              <label for="horario_chegada">Horário de chegada</label>
              <input type="time" name="horario_chegada" id="horario_chegada" class="form-control"
                value="<?php echo set_value('horario_chegada'); ?>" readonly>
              <?php echo form_error('horario_chegada'); ?>
            </div>
          </div>
        </div>

        <div class="row">
          <div class="col-md pr-md-1">
            <div class="form-group">
              <label for="carga">A carga</label>
              <input type="text" name="carga" id="carga" class="form-control" value="<?php echo set_value('carga'); ?>"
                readonly>
              <?php echo form_error('carga'); ?>
            </div>
          </div>
          <div class="col-md pr-md-1 pl-md-1">
            <div class="form-group">
              <label for="odometro_saida">Odómetro de saída</label>
              <input type="text" name="odometro_saida" id="odometro_saida" class="form-control odometer" value="<?php echo set_value('license_p
                    late'); ?>" readonly>
              <?php echo form_error('odometro_saida'); ?>
            </div>
          </div>
          <div class="col-md pl-md-1">
            <div class="form-group">
              <label for="odometro_chegada">Odómetro de chegada</label>
              <input type="text;" name="odometro_chegada" id="odometro_chegada" class="form-control odometer"
                value="<?php echo set_value('odometro_chegada'); ?>" readonly>
              <?php echo form_error('odometro_chegada'); ?>
            </div>
          </div>
        </div>

        <h4>Moto - Piloto</h4>
        <div class="row">
          <div class="col-md pr-md-1">
            <div class="form-group">
              <label for="placa">Placa</label>
              <input type="text" name="placa" id="placa" class="form-control" value="<?php echo set_value('placa'); ?>"
                readonly>
              <?php echo form_error('placa'); ?>
            </div>
          </div>
          <div class="col-md pl-md-1">
            <div class="form-group">
              <label for="piloto">Piloto</label>
              <input type="text" name="piloto" id="piloto" class="form-control"
                value="<?php echo set_value('piloto'); ?>" readonly>
              <?php echo form_error('piloto'); ?>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
        <button type="button" class="btn btn-primary" data-dismiss="modal">Encerrar viagem</button>
      </div>
    </div>
  </div>
</div>
