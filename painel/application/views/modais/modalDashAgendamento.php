<div class="modal fade" id="modalDashAgendamento" tabindex="-1" role="dialog" aria-labelledby="clients" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="modalDashAgendamento">Agendamento</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <h5>Cliente</h5>
        <div class="row" id="legal_person" style="display: none;">
          <div class="col-md pr-md-1">
            <div class="form-group">
              <label for="name">Nome</label>
              <input type="text" name="name" id="name" class="form-control" value="" readonly>
            </div>
          </div>
          <div class="col-md pr-md-1 pl-md-1">
            <div class="form-group">
              <label for="surname">Sobrenome</label>
              <input type="text" name="surname" id="surname" class="form-control" value="" readonly>
            </div>
          </div>
          <div class="col-md pl-md-1">
            <div class="form-group">
              <label for="legal_phone">Telefone</label>
              <input type="text" name="legal_phone" id="legal_phone" class="form-control" value="" readonly>
            </div>
          </div>
        </div>
        <div class="row" id="juridical_person" style="display: none;">
          <div class="col-md pr-md-1">
            <div class="form-group">
              <label for="trade_name">Nome</label>
              <input type="text" name="trade_name" id="trade_name" class="form-control" value="" readonly>
            </div>
          </div>
          <div class="col-md pl-md-1">
            <div class="form-group">
              <label for="juridical_phone">Telefone</label>
              <input type="text" name="juridical_phone" id="juridical_phone" class="form-control" value="" readonly>
            </div>
          </div>
        </div>
        <h4>Serviço</h4>
        <div class="row">
          <div class="col-md pr-md-1">
            <div class="form-group">
              <label for="requested_time">Agendado para</label>
              <input type="text" name="requested_time" id="requested_time" class="form-control" value="" readonly>
            </div>
          </div>
          <div class="col-md pl-md-1 pr-md-1">
            <div class="form-group">
              <label for="locale">Localidade</label>
              <input type="text" name="locale" id="locale" class="form-control" value="" readonly>
            </div>
          </div>
          <div class="col-md pl-md-1">
            <div class="form-group">
              <label for="type_service">Tipo de serviço</label>
              <input type="text" name="type_service" id="type_service" class="form-control" value="" readonly>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md pr-md-1">
            <div class="form-group">
              <label for="contract">Contrato</label>
              <input type="text" name="contract" id="contract" class="form-control" value="" readonly>
            </div>
          </div>
          <div class="col-md pr-md-1 pl-md-1">
            <div class="form-group">
              <label for="km_provided">KM previsto</label>
              <input type="text" name="km_provided" id="km_provided" class="form-control" value="" readonly>
            </div>
          </div>
          <div class="col-md pl-md-1">
            <div class="form-group">
              <label for="expected_value">Valor previsto</label>
              <input type="text" name="expected_value" id="expected_value" class="form-control" value="" readonly>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12">
            <div class="form-group">
              <label for="departure_address">Endereço de partida</label>
              <input type="text" name="departure_address" id="departure_address" class="form-control" value="" readonly>
            </div>
          </div>
          <div class="col-md-12">
            <div class="form-group">
              <label for="arrival_address">Endereço de chegada</label>
              <input type="text" name="arrival_address" id="arrival_address" class="form-control" value="" readonly>
            </div>
          </div>
        </div>
        <form class="" action="<?php echo base_url(); ?>trips/start_trip" method="post">
          <h4>Piloto - Moto</h4>
          <div class="row">
            <div class="col-md-4 pr-md-1">
              <div class="form-group">
                <label for="biker">Piloto </label>
                <select class="form-control" name="biker" id="biker">
                  <option value=""></option>
                  <?php foreach ($bikers as $row): ?>
                    <option value="<?php echo $row->id; ?>"><?php echo $row->name; ?></option>
                  <?php endforeach; ?>
                </select>
              </div>
            </div>
            <div class="col-md-4  pr-md-1 pl-md-1">
              <div class="form-group">
                <label for="motorcycle">Moto</label>
                <select class="form-control" name="motorcycle" id="motorcycle">
                  <option value=""></option>
                  <?php foreach ($motorcycles as $row): ?>
                    <option value="<?php echo $row->id; ?>"><?php echo $row->code_motorcycle; ?> | <?php echo $row->license_plate; ?></option>
                  <?php endforeach; ?>
                </select>
              </div>
            </div>
            <div class="col-md-4 pl-md-1">
              <div class="form-group">
                <label for="departure_odometer">Odômetro de saída</label>
                <input type="text" name="departure_odometer" id="departure_odometer" class="form-control odometer" value="">
              </div>
            </div>
          </div>
          <input type="hidden" name="id_trip" id="id_trip" value="">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
          <button type="submit" name="start_trip" class="btn btn-primary float-right">Iniciar viagem</button>
          <button type="submit" name="cancel_trip" class="btn btn-primary float-right">Cancelar viagem</button>
        </form>
      </div>
    </div>
  </div>
</div>
