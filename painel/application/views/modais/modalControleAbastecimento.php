<!-- modal cadastrar visitantes -->
<div class="modal fade" id="modalControleAbastecimento" tabindex="-1" role="dialog"
  aria-labelledby="modalControleAbastecimento" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="modalControleAbastecimento">Nova abastecimento</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="<?php echo base_url(); ?>motorcycles/register_supply" method="post">
      <div class="modal-body">
        <div class="row">
          <div class="col-md pr-md-1">
            <div class="form-group">
              <label for="odometer">Odômetro</label>
              <input type="text" name="odometer" id="odometer" class="form-control odometer"
                value="<?php echo set_value('odometer'); ?>">
              <?php echo form_error('odometer'); ?>
            </div>
          </div>
          <div class="col-md pl-md-1">
            <div class="form-group">
              <label for="quantity">Quantidade (Litros)</label>
              <input type="text" name="quantity" id="quantity" class="form-control"
                value="<?php echo set_value('quantity'); ?>">
              <?php echo form_error('quantity'); ?>
            </div>
          </div>
        </div>

        <div class="row">
          <div class="col-md pr-md-1">
            <div class="form-group">
              <label for="amount">Valor</label>
              <input type="text" name="amount" id="amount" class="valor form-control"
                value="<?php echo set_value('amount'); ?>">
              <?php echo form_error('amount'); ?>
            </div>
          </div>
          <div class="col-md pl-md-1">
            <div class="form-group">
              <label for="gas_station">Lugar de abastecimento</label>
              <input type="text" name="gas_station" id="gas_station" class="form-control"
                value="<?php echo set_value('gas_station'); ?>">
              <?php echo form_error('gas_station'); ?>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" name="button" class="btn btn-primary" data-dismiss="modal" aria-label="Close">Fechar</button>
        <input type="hidden" name="id_motorcycle" value="" id="id_motorcycle">
        <button type="submit" name="register_supply" class="btn btn-primary">Cadastrar</button>
      </div>
    </form>
    </div>
  </div>
