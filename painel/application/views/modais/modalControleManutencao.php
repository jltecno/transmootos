<!-- modal cadastrar visitantes -->
<div class="modal fade" id="modalControleManutencao" tabindex="-1" role="dialog"
  aria-labelledby="modalControleManutencao" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="modalControleManutencao">Nova manutenção</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="<?php echo base_url(); ?>motorcycles/register_maintenance" method="post">

      <div class="modal-body">
        <div class="row">
          <div class="col-md-6 pr-md-1">
            <div class="form-group">
              <label for="odometer">Odômetro</label>
              <input type="text" name="odometer" id="odometer" class="form-control odometer" placeholder="Odômetro"
                value="<?php echo set_value('odometer'); ?>">
              <?php echo form_error('odometer'); ?>
            </div>
          </div>
          <div class="col-md-6 pl-md-1">
            <div class="form-group">
              <label for="amount">Valor</label>
              <input type="text" name="amount" id="amount" class="form-control" placeholder="Valor"
                value="<?php echo set_value('amount'); ?>">
              <?php echo form_error('amount'); ?>
            </div>
          </div>

        </div>
        <div class="row">
          <div class="col-md">
            <div class="form-group">
              <label for="description">Descrição</label>
              <textarea class="form-control" name="description" id="description" rows="3"></textarea>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" name="button" class="btn btn-primary" data-dismiss="modal" aria-label="Close">Fechar</button>
        <input type="hidden" name="id_motorcycle" value="" id="id_motorcycle">
        <button type="submit" name="register_maintenance" class="btn btn-primary">Cadastrar</button>
      </div>
    </form>
    </div>
  </div>
