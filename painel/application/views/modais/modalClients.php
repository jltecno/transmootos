<div class="modal fade" id="modalClients" tabindex="-1" role="dialog" aria-labelledby="clients" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Clientes</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="table-responsive">
          <table id="tabela_escolher_cliente" class="table table-striped">
            <thead class=" text-primary">
              <th>Clientes</th>
              <th>Contrato</th>
              <th>Inserir</th>
            </thead>
            <tbody>
              <?php foreach ($clients as $row): ?>
                <tr>
                  <td><?php echo $row->trade_name; ?> <?php echo $row->name; ?> <?php echo $row->surname; ?></td>
                  <td><?php echo ($row->contract == 0)? 'Sem contrato' : 'Com contrato'; ?></td>
                  <?php if ($row->status == 2): ?>
                    <td>
                      <button type="button" name="button" class="icon_acao">
                        <i class="fas fa-lock" style="color: red;"></i>
                      </button>
                    </td>
                    <?php else: ?>
                      <td>
                        <button class="icon_acao" type="button" name="button" id="choose_cliente"
                        data-id="<?php echo $row->id; ?>"
                        data-trade_name="<?php echo $row->trade_name; ?>"
                        data-name="<?php echo $row->name; ?>"
                        data-surname="<?php echo $row->surname; ?>"
                        data-contract="<?php echo $row->contract; ?>"
                        >
                        <i class="fa fa-1x fa-plus" aria-hidden="true"></i>
                      </button>
                    </td>
                  <?php endif; ?>
                </tr>
              <?php endforeach; ?>
            </tbody>
          </table>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
      </div>
    </div>
  </div>
</div>
