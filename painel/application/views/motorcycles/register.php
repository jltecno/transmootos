<?php $this->load->view('elements/header');?>
<div class="wrapper ">
  <?php $this->load->view('elements/sidebar_lateral');?>
  <?php $this->load->view('elements/sidebar');?>
  <div class="content">
    <div class="row">
      <div class="col-md-12">
        <div class="card card-user">
          <div class="card-header">
            <h5 class="card-title">Cadastrar moto</h5>
          </div>
          <div class="card-body">
            <form action="<?php echo base_url(); ?>motorcycles/register_motorcycle" method="post" enctype="multipart/form-data">
              <div class="row">
                <div class="col-md-4 pr-1">
                  <div class="form-group">
                    <label for="brand">Marca</label>
                    <input type="text" name="brand" id="brand" class="form-control" placeholder="Marca"
                      value="<?php echo set_value('brand'); ?>">
                    <?php echo form_error('brand'); ?>
                  </div>
                </div>
                <div class="col-md-4 pl-1">
                  <div class="form-group">
                    <label for="model">Modelo</label>
                    <input type="text" name="model" id="model" class="form-control" placeholder="Modelo"
                      value="<?php echo set_value('model'); ?>">
                    <?php echo form_error('model'); ?>
                  </div>
                </div>
                <div class="col-md-4 pl-1">
                  <div class="form-group">
                    <label for="code_motorcycle">Codigo da moto</label>
                    <input type="text" name="code_motorcycle" id="code_motorcycle" class="form-control" placeholder="Codigo da moto"
                      value="<?php echo set_value('code_motorcycle'); ?>">
                    <?php echo form_error('code_motorcycle'); ?>
                  </div>
                </div>
              </div>
                <div class="row">
                  <div class="col-md-4 pr-1">
                    <div class="form-group">
                      <label for="color">Color</label>
                      <input type="text" name="color" id="color" class="form-control" placeholder="Cor"
                        value="<?php echo set_value('color'); ?>">
                      <?php echo form_error('color'); ?>
                    </div>
                  </div>
                  <div class="col-md-4 pl-1">
                    <div class="form-group">
                      <label for="license_plate">Placa</label>
                      <input type="text" name="license_plate" id="license_plate" class="form-control" placeholder="Placa"
                        value="<?php echo set_value('license_plate'); ?>">
                      <?php echo form_error('license_plate'); ?>
                    </div>
                  </div>
                  <div class="col-md-4 pl-1">
                    <div class="form-group">
                      <label for="year_motorcycle">Ano</label>
                      <input type="text" name="year_motorcycle" id="year_motorcycle" class="form-control" placeholder="Ano"
                        value="<?php echo set_value('year_motorcycle'); ?>">
                      <?php echo form_error('year_motorcycle'); ?>
                    </div>
                  </div>
                </div>
              <div class="row">
                <div class="col-md">
                  <label class="newimgum">
                    <img id="imgum" class="icone_perfil" src="<?php echo base_url(); ?>assets/build/img/icon/perfil.png">
                    <input id="picimgum" class='pis' name="profile_motorcycle" type="file" style="display:none;">
                  </label>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12">
                  <button type="submit" name="register_motorcycle"
                    class="btn btn-primary btn-round float-md-right float-sm-right float-xs-right">Cadastrar</button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
  <?php $this->load->view('elements/footer');?>
