<?php $this->load->view('elements/header');?>
  <div class="wrapper ">
    <?php $this->load->view('elements/sidebar_lateral');?>
    <?php $this->load->view('elements/sidebar');?>
    <div class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="card">
            <div class="card-header">
              <h2 class="card-title" style="display: inline;">Motos</h2>
              <a href="<?php echo base_url()?>motorcycles/register" class="btn btn-primary float-md-right float-sm-right float-xs-right">Cadastrar</a>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table id="table_motorcycles" class="table table-striped">
                  <thead class=" text-primary">
                    <th>Número</th>
                    <th>Marca</th>
                    <th>Modelo</th>
                    <th>Cor</th>
                    <th>Placa</th>
                    <th>Detalhe</th>
                  </thead>
                  <tbody>
                    <?php foreach ($motorcycles as $row): ?>
                      <tr>
                        <td><?php echo $row->code_motorcycle; ?></td>
                        <td><?php echo $row->brand; ?></td>
                        <td><?php echo $row->model; ?></td>
                        <td><?php echo $row->color; ?></td>
                        <td><?php echo $row->license_plate; ?></td>
                        <td>
                          <a href="<?php echo base_url()?>motorcycles/detail/<?php echo $row->id?>">
                            <i class="nc-icon nc-zoom-split"></i>
                          </a>
                        </td>
                      </tr>
                    <?php endforeach; ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
<?php $this->load->view('elements/footer');?>
