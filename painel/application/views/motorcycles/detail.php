<?php $this->load->view('elements/header');?>
<div class="wrapper ">
  <?php $this->load->view('elements/sidebar_lateral');?>
  <?php $this->load->view('elements/sidebar');?>
  <div class="content">
    <div class="row">
      <div class="col-md-4">
        <div class="card">
          <div class="card-header text-center">
            <form action="<?php echo base_url(); ?>motorcycles/modify_image_motorcycle/<?php echo $motorcycle->id; ?>" method="post" enctype="multipart/form-data">
              <div class="row">
                <div class="col-md-12">
                  <label class="newimgum">
                    <img id="imgum" class="icone_perfil" src="<?php echo base_url(); ?>assets/build/img/profile_motorcycles/<?php echo $motorcycle->image; ?>">
                    <input id="picimgum" class='pis' name="profile_motorcycle" type="file" style="display:none;">
                  </label>
                </div>
                <div class="col-md-12" >
                  <button class="btn btn-sm btn-primary"style="display:none;" id="save" type="submit">salvar</button>
                </div>
                <div class="col-md-12">
                  <h4 class="card-title text-center">Perfil</h4>
                </div>
              </div>
            </form>
          </div>
          <div class="card-body">


          </div>
        </div>
      </div>

      <!-- // * INICIO CARD  -->
      <div class="col-md-8">
        <div class="card card-user">
          <div class="card-header">
            <h4 class="card-title text-center">Dados da moto</h4>
            <hr>
          </div>
          <div class="card-body">
            <form action="<?php echo base_url(); ?>motorcycles/modify_motorcycle/<?php echo $motorcycle->id; ?>"
              id="form_alterar_cliente" method="post">
              <div class="row">
                <div class="col-md-6 pr-md-1">
                  <div class="form-group">
                    <label for="brand">Marca</label>
                    <input type="text" name="brand" id="brand" class="form-control"
                      value="<?php echo $motorcycle->brand; ?>">
                  </div>
                </div>
                <div class="col-md-6 pl-md-1">
                  <div class="form-group">
                    <label for="model">Modelo</label>
                    <input type="text" name="model" id="model" class="form-control"
                      value="<?php echo $motorcycle->model; ?>">
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-6 pr-md-1">
                  <div class="form-group">
                    <label for="color">Cor</label>
                    <input type="text" name="color" id="color" class="form-control"
                      value="<?php echo $motorcycle->color; ?>">
                  </div>
                </div>
                <div class="col-md-6 pl-md-1">
                  <div class="form-group">
                    <label for="license_plate">Placa</label>
                    <input type="text" name="license_plate" id="license_plate" class="form-control"
                      value="<?php echo $motorcycle->license_plate; ?>">
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-6 pr-1">
                  <div class="form-group">
                    <label for="year_motorcycle">Ano</label>
                    <input type="text" name="year_motorcycle" id="year_motorcycle" class="form-control" placeholder="Ano"
                      value="<?php echo $motorcycle->year_motorcycle; ?>">
                  </div>
                </div>
                <div class="col-md-6 pl-1">
                  <div class="form-group">
                    <label for="code_motorcycle">Codigo da moto</label>
                    <input type="text" name="code_motorcycle" id="code_motorcycle" class="form-control" placeholder="Codigo da moto"
                      value="<?php echo $motorcycle->code_motorcycle; ?>">
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label for="observacao">Observação</label>
                    <textarea name="observacao" id="observacao" class="form-control textarea"></textarea>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="update ml-auto mr-auto">
                  <input type="hidden" name="id_motorcycle" value="<?php echo $motorcycle->id; ?>">
                  <button id="modify_motorcycle" name="modify_motorcycle"
                    class="btn btn-primary btn-round">Alterar</button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>



    <div class="row">
      <div class="col-md-6">
        <div class="card">
          <div class="card-header">
            <h4 class="card-title text-center">Controle de Abastecimento</h4>
            <hr>
            <button type="button" class="btn btn-sm  btn-primary about-extrabtn-primary chama_modal"
              data-id="modalControleAbastecimento" data-motorcycle="<?php echo $motorcycle->id; ?>" name="button"
              id="supply_control">Novo</button>
          </div>
          <div class="card-body">
            <table class="table table-striped table-bordered" style="width:100%;border:none;" id="table_gasoline">
              <thead class=" text-primary">
                <tr>
                  <th scope="col">Data</th>
                  <th scope="col">Quantidade (Litros)</th>
                  <th scope="col">Valor</th>
                  <th scope="col">Odômetro</th>
                </tr>
              </thead>
              <tbody>
                <?php foreach ($gasoline as $row): ?>
                <?php
                    $register = date_create($row->register);
                  ?>
                <tr>
                  <td><?php echo date_format($register, 'd/m/Y H:i'); ?></td>
                  <td><?php echo $row->quantity; ?></td>
                  <td><?php echo $row->amount; ?></td>
                  <td><?php echo $row->odometer; ?></td>
                </tr>
                <?php endforeach; ?>

              </tbody>
            </table>
          </div>
        </div>
      </div>
      <div class="col-md-6">
        <div class="card">
          <div class="card-header">
            <h4 class="card-title text-center">Controle de manutenção</h4>
            <hr>
            <button type="button" class="btn btn-sm  btn-primary about-extrabtn-primary chama_modal"
              data-id="modalControleManutencao" name="button">Novo</button>
          </div>
          <div class="card-body">
            <table class="table table-striped table-bordered" style="width:100%;border:none;" id="table_maintenance">
              <thead class="text-primary">
                <tr>
                  <th scope="col">Data</th>
                  <th scope="col">Valor</th>
                  <th scope="col">odómetro</th>
                </tr>
              </thead>
              <tbody>
                <?php foreach ($maintenance as $row): ?>
                <?php
                    $register = date_create($row->register);
                  ?>
                <tr>
                  <td><?php echo date_format($register, 'd/m/Y H:i'); ?></td>
                  <td><?php echo $row->amount; ?></td>
                  <td><?php echo $row->odometer; ?></td>
                </tr>
                <?php endforeach; ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>


    <div class="row">
      <div class="col-md-6">
        <div class="card">
          <div class="card-header">
            <h4 class="card-title text-center">Histórico de viagens</h4>
            <hr>
          </div>
          <div class="card-body">
            <table class="table table-striped table-bordered" style="width:100%;border:none;" id="table_history_trips">
              <thead class=" text-primary">
                <tr>
                  <th scope="col">Saida</th>
                  <th scope="col">Chegada</th>
                  <th scope="col">Piloto</th>
                  <th scope="col">Serviço</th>
                </tr>
              </thead>
              <tbody>
                <?php foreach ($trips as $row): ?>
                <?php
                    $departure_time = date_create($row->departure_time);
                    $arrival_time = date_create($row->arrival_time);
                  ?>
                <tr>
                  <td><?php echo date_format($departure_time, 'd/m/Y H:m'); ?></td>
                  <td>
                    <?php echo ($row->arrival_time != null) ? date_format($arrival_time, 'd/m/Y H:m') : 'em andamento' ;?>
                  </td>
                  <td><?php echo $row->name; ?> <?php echo $row->surname; ?></td>
                  <td><?php echo ($row->service == 'P') ? 'Pessoa' : 'Material' ; ?></td>
                </tr>
                <?php endforeach; ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
      <div class="col-md-6">
        <div class="card">
          <div class="card-header">
            <h4 class="card-title text-center">Multas</h4>
            <hr>
            <button type="button" class="btn btn-sm btn-primary chama_modal" data-id="modalCadastroMulta"
              name="button">Novo</button>
          </div>
          <div class="card-body">
            <table class="table table-striped table-bordered" style="width:100%;border:none;" id="table_ticket">
              <thead class=" text-primary">
                <tr>
                  <th scope="col">Piloto</th>
                  <th scope="col">Data</th>
                  <th scope="col">Motivo</th>
                  <th scope="col">Vencimento</th>
                  <th scope="col">Valor</th>
                </tr>
              </thead>
              <tbody>
                <?php foreach ($ticket as $row): ?>
                <?php
                    $date = date_create($row->date_event);
                    $due_date = date_create($row->due_date);
                  ?>
                <tr>
                  <td><?php echo $row->name; ?> <?php echo $row->surname; ?></td>
                  <td><?php echo date_format($date, 'd/m/Y');; ?></td>
                  <td><?php echo $row->type_infringement; ?></td>
                  <td><?php echo date_format($due_date, 'd/m/Y'); ?></td>
                  <td><?php echo $row->amount; ?></td>
                </tr>
                <?php endforeach; ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>

    </div>
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header">
            <h4 class="card-title text-center">Ocorrencias</h4>
            <hr>
            <button type="button" class="btn btn-sm btn-primary chama_modal" data-toggle="modal" data-target="#modalExemplo"
              name="button">Novo</button>
          </div>
          <div class="card-body">
            <table class="table table-striped table-bordered" style="width:100%;border:none;" id="table_occurrence">
              <thead class=" text-primary">
                <tr>
                  <th scope="col">Data</th>
                  <th scope="col">Ocorrencia</th>
                  <th scope="col">Piloto</th>
                  <th scope="col">Status</th>
                  <th scope="col"></th>
                </tr>
              </thead>
              <tbody>
                <?php foreach ($occurrence as $row): ?>
                  <?php $data = date_create($row->register); ?>
                  <tr>
                    <td><?php echo date_format($data, 'd/m/Y H:i'); ?></td>
                    <td><?php echo $row->occurrence; ?></td>
                    <td><?php echo $row->name; ?> <?php echo $row->surname; ?></td>
                    <td><?php echo $row->status_occurrence; ?></td>
                    <td>
                      <button type="button"  class="btn btn-sm btn-primary chama_modal" name="button" id="button_edit_status" data-toggle="modal" data-target="#edit_status" data-occurrence="<?php echo $row->id; ?>">
                        <i class="fas fa-edit"></i>
                      </button>
                    </td>
                  </tr>
                <?php endforeach; ?>

              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
  <?php $this->load->view('elements/footer');?>

  <!-- Modal -->
<div class="modal fade" id="modalExemplo" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Ocorrencia</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="<?php echo base_url(); ?>motorcycles/register_occurrence" method="post">
          <div class="row">
            <div class="col-md-12">
              <div class="form-group">
                <label for="biker">Piloto</label>
                <select class="form-control" name="id_biker">
                  <option value="">Selecione o piloto..</option>
                  <?php foreach ($bikers as $row): ?>
                    <option value="<?php echo $row->id; ?>"><?php echo $row->name; ?> <?php echo $row->surname; ?></option>
                  <?php endforeach; ?>
                </select>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <div class="form-group">
                <label for="occurrence">Ocorrencia</label>
                <textarea name="occurrence" class="form-control"  rows="8" cols="80" id="occurrence"></textarea>
              </div>
            </div>
          </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
        <button type="submit" class="btn btn-primary" name="register_occurrence">Salvar mudanças</button>
      </div>
      </form>
    </div>
  </div>
</div>

<!-- Modal edit status occurrence -->
<div class="modal fade" id="edit_status" tabindex="-1" aria-labelledby="edit_status" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Editar status da ocorrencia</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="<?php echo base_url(); ?>motorcycles/update_status" method="post">
      <div class="modal-body">
          <div class="row">
            <div class="col-md-12">
              <div class="form-group">
                <label for="occurrence">Status</label>
                <select class="form-control" name="status_occurrence">
                  <option value="">Selecione o status..</option>
                  <option value="Em espera">Em espera</option>
                  <option value="Resolvido">Resolvido</option>
                </select>
              </div>
            </div>
          </div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
        <input type="hidden" name="id_ocurrence" id="id_ocurrence" value="">
        <button type="submit" name="update_status" class="btn btn-primary">Salvar mudanças</button>
      </div>
      </form>
    </div>
  </div>
</div>
