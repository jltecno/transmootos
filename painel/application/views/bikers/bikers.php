<?php $this->load->view('elements/header');?>
  <div class="wrapper ">
    <?php $this->load->view('elements/sidebar_lateral');?>
    <?php $this->load->view('elements/sidebar');?>
    <div class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="card">
            <div class="card-header">
              <h2 class="card-title" style="display: inline;">Pilotos</h2>
              <a href="<?php echo base_url()?>bikers/register" class="btn btn-primary float-md-right float-sm-right float-xs-right">Cadastrar</a>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table id="table_bikers" class="table table-striped">
                  <thead class=" text-primary">
                    <th>CPF</th>
                    <th>CNH</th>
                    <th>NOME</th>
                    <th>Sobrenome</th>
                    <th>Telefone</th>
                    <th>Data de nascimento</th>
                    <th>Detalhe</th>
                  </thead>
                  <tbody>
                    <?php foreach ($bikers as $row): ?>
                      <tr>
                        <td><?php echo $row->cpf; ?></td>
                        <td><?php echo $row->license; ?></td>
                        <td><?php echo $row->name; ?></td>
                        <td><?php echo $row->surname; ?></td>
                        <td><?php echo $row->phone; ?></td>
                        <td><?php echo $row->date_birthday; ?></td>
                        <td>
                          <a href="<?php echo base_url()?>bikers/detail/<?php echo $row->id?>">
                            <i class="nc-icon nc-zoom-split btn_detalhe"></i>
                          </a>
                        </td>
                      </tr>
                    <?php endforeach; ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
<?php $this->load->view('elements/footer');?>
