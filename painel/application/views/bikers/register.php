<?php $this->load->view('elements/header');?>
<div class="wrapper ">
  <?php $this->load->view('elements/sidebar_lateral');?>
  <?php $this->load->view('elements/sidebar');?>
  <div class="content">
    <div class="row">
      <div class="col-md-12">
        <div class="card card-user">
          <div class="card-header">
            <h5 class="card-title">Cadastrar piloto</h5>
          </div>
          <div class="card-body">
            <form action="<?php echo base_url(); ?>bikers/register_bikers" method="post" enctype="multipart/form-data">
              <div class="row">
                <div class="col-md-6 pr-1">
                  <div class="form-group">
                    <label for="name">Nome</label>
                    <input type="text" name="name" id="name" class="form-control" placeholder="Nome"
                      value="<?php echo set_value('name'); ?>">
                    <?php echo form_error('name'); ?>
                  </div>
                </div>
                <div class="col-md-6 pl-1">
                  <div class="form-group">
                    <label for="surname">Sobrenome</label>
                    <input type="text" name="surname" id="surname" class="form-control" placeholder="Sobrenome"
                      value="<?php echo set_value('surname'); ?>">
                    <?php echo form_error('surname'); ?>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-6 pr-1">
                  <div class="form-group">
                    <label for="date_birthday">Data de nacimento</label>
                    <input type="date" name="date_birthday" id="date_birthday" class="form-control"
                      placeholder="Data de nacimento" value="<?php echo set_value('date_birthday'); ?>">
                    <?php echo form_error('date_birthday'); ?>
                  </div>
                </div>
                <div class="col-md-6 pl-1">
                  <div class="form-group">
                    <label for="telefone">Telefone</label>
                    <input type="text" name="phone" id="telefone" class="form-control" placeholder="Telefone"
                      value="<?php echo set_value('phone'); ?>">
                    <?php echo form_error('phone'); ?>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-6 pr-1">
                  <div class="form-group">
                    <label for="cpf">Cpf</label>
                    <input type="text" name="cpf" id="cpf" class="form-control" placeholder="cpf"
                      value="<?php echo set_value('cpf'); ?>">
                    <?php echo form_error('cpf'); ?>
                  </div>
                </div>
                <div class="col-md-6 pl-1">
                  <div class="form-group">
                    <label for="license">CNH</label>
                    <input type="text" name="license" id="license" class="form-control" placeholder="Cnh"
                      value="<?php echo set_value('license'); ?>">
                    <?php echo form_error('license'); ?>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-4">
                  <div class="form-group">
                    <label for="license_expiration">Validade da CNH</label>
                    <input type="date" name="license_expiration" id="license_expiration" class="form-control"
                      placeholder="Validade" value="<?php echo set_value('license_expiration'); ?>">
                    <?php echo form_error('license_expiration'); ?>
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                    <label for="type_blood">Tipo sanguinio</label>
                    <select class="form-control" name="type_blood" id="type_blood">
                     <option selected>Selecione o tipo sanguinio</option>
                     <?php foreach ($type_blood as $row): ?>
                       <option value="<?php echo $row->id; ?>"><?php echo $row->description; ?></option>
                     <?php endforeach; ?>
                   </select>
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                    <label for="license_expiration">Comissão</label>
                    <input type="text" name="commission" id="commission" class="form-control odometer"
                      placeholder="Comissão" value="<?php echo set_value('commission'); ?>">
                    <?php echo form_error('commission'); ?>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md">
                  <label class="newimgum">
                    <img id="imgum" class="icone_perfil" src="<?php echo base_url(); ?>assets/build/img/icon/perfil.png">
                    <input id="picimgum" class='pis' name="profile_bikers" type="file" style="display:none;">
                  </label>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12">
                  <button type="submit" name="register_bikers"
                    class="btn btn-primary btn-round float-md-right float-sm-right float-xs-right">Cadastrar</button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
  <?php $this->load->view('elements/footer');?>
