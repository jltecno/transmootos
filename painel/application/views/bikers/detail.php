<?php $this->load->view('elements/header');?>
<div class="wrapper ">
  <?php $this->load->view('elements/sidebar_lateral');?>
  <?php $this->load->view('elements/sidebar');?>
  <div class="content">

    <div class="row">
      <div class="col-md-5">
        <div class="card">
          <div class="card-header text-center">
            <form action="<?php echo base_url(); ?>bikers/modify_image/<?php //echo $biker->id; ?>" method="post" enctype="multipart/form-data">
            <div class="row">
              <div class="col-md-12">
                <label class="newimgum">
                  <img id="imgum" class="icone_perfil" src="<?php echo base_url(); ?>assets/build/img/profile_bikers/<?php echo $biker->image; ?>">
                  <input id="picimgum" class='pis' name="image_biker" type="file" style="display:none;">
                </label>
              </div>
              <div class="col-md-12">
                <input type="hidden" name="id" value="<?php echo $biker->id; ?>">
                <button class="btn btn-sm btn-primary" style="display:none;" name="modify_image" id="save" type="submit">salvar</button>
              </div>
              </form>
              <div class="col-12">
                <h4 class="card-title text-center"><?php echo $biker->name; ?></h4>
              </div>
            </div>
          </div>
          <div class="card-body">

          </div>
        </div>
        <div class="card">
          <div class="card-header">
            <h4 class="card-title text-center">Viagens realizadas</h4>
          </div>
          <div class="card-body">
            <table class="table table-striped table-bordered" style="width:100%" id="table_trips_biker">
              <thead class="text-primary">
                <tr>
                  <th>Local</th>
                  <th>Carga</th>
                  <th>Cliente</th>
                  <th>Data</th>
                  <th>Detalhe</th>
                </tr>
              </thead>
              <tbody>
                <?php foreach ($trips as $row): ?>
                  <?php $departure_time = date_create($row->departure_time); ?>
                  <tr>
                    <td><?php echo ($row->locale == 'I') ? 'Interno' : 'Fora da cidade' ;?></td>
                    <td><?php echo ($row->service == 'P') ? 'Pessoa' : 'Material' ; ?></td>
                    <td><?php echo $row->name; ?> <?php echo $row->surname; ?> <?php echo $row->trade_name; ?></td>
                    <td><?php echo ($row->departure_time != null) ? date_format($departure_time, 'd/m/Y H:i') : 'em andamento' ; ?></td>
                    <td><button data-id="modalDashEncerradas" class="detail_trip btn_detalhe ml-2 " data-trip="<?php echo $row->id; ?>"
                        type="button" id="trip_taken" data-departure="<?php echo $row->departure_adress; ?>"><i class="fa fa-1x fa-search btn_detalhe "></i>
                      </button></td>
                  </tr>
                <?php endforeach; ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
      <div class="col-md-7">
        <div class="card card-user">
          <div class="card-body">
            <form action="<?php echo base_url()?>bikers/modify_biker/<?php echo $biker->id; ?>" method="post">
              <div class="row">
                <div class="col-md-6 pr-1">
                  <div class="form-group">
                    <label for="name">Nome</label>
                    <input type="text" name="name" id="name" class="form-control" placeholder="Nome"
                      value="<?php echo $biker->name; ?>">
                    <?php echo form_error('name'); ?>
                  </div>
                </div>
                <div class="col-md-6 pl-1">
                  <div class="form-group">
                    <label for="surname">Sobrenome</label>
                    <input type="text" name="surname" id="surname" class="form-control" placeholder="Sobrenome"
                      value="<?php echo $biker->surname; ?>">
                    <?php echo form_error('surname'); ?>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-6 pr-1">
                  <div class="form-group">
                    <label for="date_birthday">Data de nascimento</label>
                    <input type="date" name="date_birthday" id="date_birthday" class="form-control"
                      placeholder="Data de nacimento" value="<?php echo $biker->date_birthday; ?>">
                    <?php echo form_error('date_birthday'); ?>
                  </div>
                </div>
                <div class="col-md-6 pl-1">
                  <div class="form-group">
                    <label for="telefone">Telefone</label>
                    <input type="text" name="phone" id="telefone" class="form-control" placeholder="Telefone"
                      value="<?php echo $biker->phone; ?>">
                    <?php echo form_error('phone'); ?>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-6 pr-1">
                  <div class="form-group">
                    <label for="cpf">Cpf</label>
                    <input type="text" name="cpf" id="cpf" class="form-control" placeholder="cpf"
                      value="<?php echo $biker->cpf; ?>">
                    <?php echo form_error('cpf'); ?>
                  </div>
                </div>
                <div class="col-md-6 pl-1">
                  <div class="form-group">
                    <label for="license">Cnh</label>
                    <input type="text" name="license" id="license" class="form-control" placeholder="Cnh"
                      value="<?php echo $biker->license; ?>">
                    <?php echo form_error('license'); ?>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-6 pr-1">
                  <div class="form-group">
                    <label for="license_expiration">Validade da CNH</label>
                    <input type="date" name="license_expiration" id="license_expiration" class="form-control"
                      placeholder="Validade" value="<?php echo $biker->license_expiration; ?>">
                    <?php echo form_error('license_expiration'); ?>
                  </div>
                </div>
                <div class="col-md-6 pl-1">
                  <div class="form-group">
                    <label for="type_blood">Tipo sanguinio</label>
                    <select class="form-control" name="type_blood" id="type_blood">
                     <option selected>Selecione o tipo sanguinio</option>
                     <?php foreach ($type_blood as $row): ?>
                       <?php if ($row->id == $biker->blood_type): ?>
                         <option value="<?php echo $biker->blood_type; ?>" selected><?php echo $biker->description; ?></option>
                       <?php endif; ?>
                       <?php if ($row->id != $biker->blood_type): ?>
                         <option value="<?php echo $row->id; ?>"><?php echo $row->description; ?></option>
                       <?php endif; ?>
                     <?php endforeach; ?>
                   </select>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label for="license_expiration">Comissão</label>
                    <input type="text" name="commission" id="commission" class="form-control odometer"
                      placeholder="Comissão" value="<?php echo $biker->commission; ?>">
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="update ml-auto mr-auto">
                  <input type="hidden" name="id_biker" value="<?php echo $biker->id; ?>">
                  <button id="modify_biker" name="modify_biker" class="btn btn-primary btn-round">Alterar</button>
                </div>
              </div>
            </form>
          </div>
        </div>
        <div class="col-md-12">
          <div class="card">
            <div class="card-header">
              <h4 class="card-title text-center">Documentos</h4>
            </div>
            <div class="card-body">
              <button type="button" class="btn btn-primary" name="button" data-toggle="modal" data-target="#modalAddContrato">Adicionar documento</button>
              <table id="tabela_cliente_contratos" class="table table-striped table-bordered" style="width:100%;">
                <thead>
                </thead>
                <tbody>
                  <?php foreach ($documents as $row): ?>
                    <tr>
                      <td> <a href="<?php echo base_url(); ?>assets/build/img/document_biker/<?php echo $row->document; ?>" download><?php echo $row->description; ?></a> </td>
                    </tr>
                  <?php endforeach; ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>

    </div>

    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header">
            <h4>Viagens noturnas</h4>
          </div>
          <div class="card-body">
            <label for=""><h5>Data de início: </h5></label>
            <input type="text" class="data" placeholder="00/00/0000" name="" id="first_date" value="">
            <label for=""><h5>Data de fim: </h5></label>
            <input type="text" class="data" placeholder="00/00/0000" name="" id="last_date" value="">
            <input type="hidden" name="id_biker" id="id_biker" value="<?php echo $id; ?>">

            <button type="button" name="button" id="search_date_biker_night_trip"><i class="fa fa-1x fa-search"></i></button>
            <div id="table_biker_night_trip_refresh">
              <table class="table table-striped table-bordered" style="width:100%" id="table_biker_night_trip">
                <thead class="text-primary">
                  <tr>
                    <th>Data</th>
                    <th>Moto</th>
                    <th>Cliente</th>
                    <th>Odometro inicial</th>
                    <th>Odometro final</th>
                    <th>Preço</th>
                    <th>Quantidade</th>
                    <th>Soma</th>
                    <th>Comissão</th>
                    <th>status</th>
                  </tr>
                </thead>
                <tbody id="tbody_night_trips_biker">
                  <?php foreach ($night_trips_biker as $row): ?>
                    <?php $date = date_create($row->date); ?>
                    <tr>
                      <td><?php echo date_format($date, 'd/m/Y'); ?></td>
                      <td><?php echo $row->code_motorcycle.' | '.$row->license_plate; ?></td>
                      <td><?php echo (isset($row->name)) ? $row->name.' '.$row->surname : $row->trade_name ; ?></td>
                      <!-- <td><?php // echo () ? a : b ; ?></td> -->
                      <td><?php echo $row->departure_odometer; ?></td>
                      <td><?php echo $row->arrival_odometer; ?></td>
                      <td><?php echo moeda($row->price); ?></td>
                      <td><?php echo $row->quantity; ?></td>
                      <td><?php echo moeda($row->price * $row->quantity); ?></td>
                      <td><?php echo moeda($row->biker_commission); ?></td>
                      <td><?php echo ($row->payment_status == '0') ? 'Não pago' : 'Pago'; ?></td>
                    </tr>
                  <?php endforeach; ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>

  </div>
  <?php $this->load->view('elements/footer');?>

  <!-- Modal -->
  <div class="modal fade" id="modalAddContrato" tabindex="-1" role="dialog" aria-labelledby="modalAddContrato" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Adicionar documento</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <form action="<?php echo base_url(); ?>bikers/add_document/<?php echo $biker->id; ?>" method="post" enctype="multipart/form-data">
            <div class="row pb-3">
              <div class="col-md-12">
                <label for="contract">Tipo de documento:</label>
                <select class="form-control" name="type_document" required>
                  <?php foreach ($type_documents as $row): ?>
                    <option value="<?php echo $row->id ?>"><?php echo $row->description; ?></option>
                  <?php endforeach; ?>
                </select>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <label for="contract">Adicionar documento:</label>
                <input type="file" name="document" value="" class="btn btn-primary" id="document">
              </div>
            </div>

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
          <button type="submit" name="register_document" class="btn btn-primary">Enviar</button>
        </div>
      </form>
      </div>
    </div>
  </div>
