<?php $this->load->view('elements/header');?>
<div class="wrapper ">
  <?php $this->load->view('elements/sidebar_lateral');?>
  <?php $this->load->view('elements/sidebar');?>
  <div class="content">
    <div class="row">
      <div class="col-lg-3 col-md-6 col-sm-6">
        <div class="card card-stats">
          <div class="card-body ">
            <div class="row">
              <div class="col-5 col-md-4">
                <div class="icon-big text-center icon-warning">
                  <img src="<?php echo base_url()?>assets/build/img/icon/andamento.svg" alt="">
                </div>
              </div>
              <div class="col-7 col-md-8">
                <div class="numbers">
                  <p class="card-category">Viagens em <br> andamento</p>
                  <p class="card-title"><?php echo (!empty($count_trips->count_in_progress)? $count_trips->count_in_progress : 0); ?><p>
                </div>
              </div>
            </div>
          </div>
          <div class="card-footer ">
          </div>
        </div>
      </div>

      <div class="col-lg-3 col-md-6 col-sm-6">
        <div class="card card-stats">
          <div class="card-body ">
            <div class="row">
              <div class="col-5 col-md-4">
                <div class="icon-big text-center icon-warning">
                  <img src="<?php echo base_url()?>assets/build/img/icon/fila.svg" alt="">
                </div>
              </div>
              <div class="col-7 col-md-8">
                <div class="numbers">
                  <p class="card-category">Viagens <br> na fila</p>
                  <p class="card-title"><?php echo (!empty($count_trips->count_queue)? $count_trips->count_queue : 0); ?><p>
                </div>
              </div>
            </div>
          </div>
          <div class="card-footer">
          </div>
        </div>
      </div>

      <div class="col-lg-3 col-md-6 col-sm-6">
        <div class="card card-stats">
          <div class="card-body ">
            <div class="row">
              <div class="col-5 col-md-4">
                <div class="icon-big text-center icon-warning">
                  <img src="<?php echo base_url()?>assets/build/img/icon/agendamentos.svg" alt="">
                </div>
              </div>
              <div class="col-7 col-md-8">
                <div class="numbers">
                  <p class="card-category">Viagens <br> agendadas</p>
                  <p class="card-title"><?php echo (!empty($count_trips->count_schedule)? $count_trips->count_schedule : 0); ?><p>
                </div>
              </div>
            </div>
          </div>
          <div class="card-footer ">
          </div>
        </div>
      </div>

      <div class="col-lg-3 col-md-6 col-sm-6">
        <div class="card card-stats">
          <div class="card-body ">
            <div class="row">
              <div class="col-5 col-md-4">
                <div class="icon-big text-center icon-warning">
                  <img src="<?php echo base_url()?>assets/build/img/icon/encerradas.svg" alt="">
                </div>
              </div>
              <div class="col-7 col-md-8">
                <div class="numbers">
                  <p class="card-category">Viagens <br> encerradas</p>
                  <p class="card-title"><?php echo (!empty($count_trips->count_closed)? $count_trips->count_closed : 0); ?><p>
                </div>
              </div>
            </div>
          </div>
          <div class="card-footer ">
          </div>
        </div>
      </div>
    </div>


    <div class="row">
      <div class="col-md-12">
        <div class="card ">
          <div class="card-header">
            <div id="detalhe_dash_fila"></div>
            <h5 class="card-title mt-3">Fila</h5>
          </div>
          <div class="card-body ">
            <div class="row">
              <div class="col-md-12 table-responsive-md">
                <table id="table_dash_waiting" class="table table-striped table-bordered" style="width:100%;border:none;">
                  <thead>
                    <tr>
                      <th>Viagem</th>
                      <th>Carga</th>
                      <th>Local</th>
                      <th>KM</th>
                      <th>Solicitação</th>
                      <th></th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php foreach ($waiting as $row): ?>
                    <tr>
                      <td><?php echo $row->id; ?></td>
                      <td><?php echo $row->service; ?></td>
                      <td><?php echo ($row->locale == 'I') ? 'Interno' : 'Fora da cidade' ; ?></td>
                      <td><?php echo $row->km_provided; ?></td>
                      <?php $date = date_create($row->register); ?>
                      <td><?php echo date_format($date, 'H:i'); ?></td>
                      <td><button data-id="modalDashFila" data-trip="<?php echo $row->id; ?>" class="detail_trip icon_acao">
                          <i class="fa fa-1x fa-search"></i>
                        </button></td>
                    </tr>
                    <?php endforeach; ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
          <div class="card-footer "></div>
        </div>
        <div id="em_andamento_cash" class="card">
          <div class="card-header">
            <div id="detalhe_dash_andamento"></div>
            <h5 class="card-title mt-3">Em andamento</h5>
          </div>
          <div class="card-body ">
            <div class="row">
              <div class="col-md-12 table-responsive-md">
                <table id="table_dash_progress" class="table table-striped table-bordered"
                  style="width:100%;border:none;">
                  <thead>
                    <tr>
                      <th>Viagem</th>
                      <th>Moto</th>
                      <th>Piloto</th>
                      <th>Carga</th>
                      <th>Partida</th>
                      <th></th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php foreach ($progress as $row): ?>
                      <?php $departure_time = date_create($row->departure_time); ?>
                      <tr>
                        <td><?php echo $row->id; ?></td>
                        <td><?php echo $row->code_motorcycle.' | '.$row->license_plate; ?></td>
                        <td><?php echo $row->name; ?> <?php echo $row->surname; ?></td>
                        <td><?php echo $row->service; ?></td>
                        <td><?php echo date_format($departure_time, 'H:i'); ?></td>
                        <td>
                          <button data-id="<?php echo ($row->service == 'Compartilhado')? 'modalTripPackages' : 'modalDashAndamento' ?>"  data-trip="<?php echo $row->id; ?>" class="<?php echo ($row->service == 'Compartilhado')? 'detail_trip_packages' : 'detail_trip' ?> icon_acao">
                            <i class="fa fa-1x fa-search"></i>
                          </button>
                        </td>
                      </tr>
                    <?php endforeach; ?>
                    <?php foreach ($progress_shared as $row): ?>
                      <?php $departure_time = date_create($row->departure_time); ?>
                      <tr>
                        <td><?php echo $row->id; ?></td>
                        <td><?php echo $row->code_motorcycle.' | '.$row->license_plate; ?></td>
                        <td><?php echo $row->name; ?> <?php echo $row->surname; ?></td>
                        <td>Compartilhado</td>
                        <td><?php echo date_format($departure_time, 'H:i'); ?></td>
                        <td>
                          <button data-id="modalTripPackages"  data-trip="<?php echo $row->id; ?>" class="detail_trip_packages icon_acao">
                            <i class="fa fa-1x fa-search"></i>
                          </button>
                        </td>
                      </tr>
                    <?php endforeach; ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
          <div class="card-footer"></div>
        </div>

        <div class="card ">
          <div class="card-header">
            <div id="detalhe_dash_agendamento"></div>
            <h5 class="card-title mt-3">Agendamentos</h5>
          </div>
          <div class="card-body ">
            <div class="row">
              <div class="col-md-12 table-responsive-md">
                <table id="table_dash_scheduling" class="table table-striped table-bordered"
                  style="width:100%;border:none;">
                  <thead>
                    <tr>
                      <th>Viagem</th>
                      <th>Carga</th>
                      <th>Local</th>
                      <th>KM</th>
                      <th>Partida</th>
                      <th></th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php foreach ($schedules as $row): ?>
                      <tr>
                        <td><?php echo $row->id; ?></td>
                        <td><?php echo $row->service; ?></td>
                        <td><?php echo ($row->locale == 'I') ? 'Interno' : 'Fora da cidade' ; ?></td>
                        <td><?php echo $row->km_provided; ?></td>
                        <td><?php echo $row->schedule_trip; ?></td>
                        <td><button data-id="modalDashAgendamento" data-trip="<?php echo $row->id; ?>" class="detail_trip icon_acao">
                            <i class="fa fa-1x fa-search"></i>
                          </button></td>
                      </tr>
                    <?php endforeach; ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
          <div class="card-footer"></div>
        </div>

        <div class="card ">
          <div class="card-header">
            <!-- <div id="detalhe_dash_agendamento"></div> -->
            <h5 class="card-title mt-3 d-inline">Encomendas | Compartilhado</h5>
            <button type="button" data-id="modalCreateTrip" class="btn btn-primary float-right chama_modal " name="button">Criar viagem</button>
          </div>
          <div class="card-body ">
            <div class="row">
              <div class="col-md-12 table-responsive-md">
                <table id="table_packages_waiting" class="table table-striped table-bordered"
                  style="width:100%;border:none;">
                  <thead>
                    <tr>
                      <th>Pacote</th>
                      <th>Cliente</th>
                      <th>Cidade</th>
                      <th>Solicitação</th>
                      <th></th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php foreach ($packages_waiting as $row): ?>
                      <tr>
                        <td><?php echo $row->id; ?></td>
                        <td><?php echo $row->name; ?> <?php echo $row->surname; ?><?php echo $row->trade_name; ?></td>
                        <td><?php echo $row->city; ?></td>
                        <td><?php echo $row->register; ?></td>
                        <td>
                          <a href="<?php echo base_url(); ?>packages/package/<?php echo $row->id; ?>" target="_blank">
                            <i class="fa fa-1x fa-search"></i>
                          </a>
                        </td>
                      </tr>
                    <?php endforeach; ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
          <div class="card-footer"></div>
        </div>

        <div class="card ">
          <div class="card-header">
            <div id="detalhe_dash_encerradas"></div>
            <h5 class="card-title mt-3">Encerradas</h5>
          </div>
          <div class="card-body ">
            <div class="row">
              <div class="col-md-12 ">
                <table id="table_dash_closed" class="table table-striped table-bordered"
                  style="width:100%;border:none;">
                  <thead>
                    <tr>
                      <th>Viagem</th>
                      <th>Carga</th>
                      <th>Local</th>
                      <th>KM</th>
                      <th>Inicio</th>
                      <th>Termino</th>
                      <th></th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php foreach ($closed as $row): ?>
                      <tr>
                        <td><?php echo $row->id; ?></td>
                        <td><?php echo $row->service; ?></td>
                        <td><?php echo ($row->locale == 'I') ? 'Interno' : 'Fora da cidade' ; ?></td>
                        <?php $distance = floatval($row->arrival_odometer) - floatval($row->departure_odometer); ?>
                        <td><?php echo number_format($distance, 1, '.', '.'); ?></td>
                        <td><?php echo $row->departure_time; ?></td>
                        <td><?php echo $row->arrival_time; ?></td>
                        <td>
                          <button data-id="modalDashEncerradas" data-trip="<?php echo $row->id; ?>" class="detail_trip icon_acao">
                            <i class="fa fa-1x fa-search"></i>
                          </button>
                        </td>
                      </tr>
                    <?php endforeach; ?>
                    <?php foreach ($shared_closed as $row): ?>
                      <tr>
                        <td><?php echo $row->id; ?></td>
                        <td>Compartilhado</td>
                        <td></td>
                        <?php $distance = floatval($row->arrival_odometer) - floatval($row->departure_odometer); ?>
                        <td><?php echo number_format($distance, 1, '.', '.'); ?></td>
                        <td><?php echo $row->departure_time; ?></td>
                        <td><?php echo $row->arrival_time; ?></td>
                        <td>
                          <button data-id="detail_shared_trip" data-trip="<?php echo $row->id; ?>" class="detail_shared_trip icon_acao" data-toggle="modal" data-target="#detail_shared_modal">
                            <i class="fa fa-1x fa-search"></i>
                          </button>
                        </td>
                      </tr>
                    <?php endforeach; ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
          <div class="card-footer "></div>
        </div>
        <div class="card ">
          <div class="card-header">
            <h5 class="card-title mt-3 d-inline">Viagens canceladas</h5>
          </div>
          <div class="card-body ">
            <div class="row">
              <div class="col-md-12 table-responsive-md">
                <table id="table_dash_cancel_trips" class="table table-striped table-bordered"
                  style="width:100%;border:none;">
                  <thead>
                    <tr>
                      <th>Viagem</th>
                      <th>Cliente</th>
                      <th>Carga</th>
                      <th>Local</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php foreach ($cancel_trips as $row): ?>
                      <tr>
                        <td><?php echo $row->id; ?></td>
                        <td><?php echo (isset($row->name)) ? $row->name.' '.$row->surname : $row->trade_name ; ?></td>
                        <td><?php echo $row->service; ?></td>
                        <td><?php echo ($row->locale == 'I') ? 'Interno' : 'Fora da cidade'; ?></td>
                      </tr>
                    <?php endforeach; ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
          <div class="card-footer"></div>
        </div>
      </div>
    </div>
  </div>
  <?php $this->load->view('elements/footer');?>

  <div class="modal fade" id="detail_shared_modal" tabindex="-1" role="dialog" aria-labelledby="detail_shared_modal"
    aria-hidden="true">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <form action="<?php echo base_url(); ?>shared_trips/update_shared_trips" method="post">
        <div class="modal-header">
          <h5 class="modal-title" id="modalCadastroMulta">Encerradas</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>

          <div class="modal-body">
            <div class="row">
              <div class="col">
                <button type="button" name="button" id="chamar_dados_shared">Chamar</button>
                <input type="hidden" name="id_trip" id="id_trip" value="">
              </div>
            </div>
            <div class="row">
              <div class="col">
                <h4 class="d-inline">Piloto - Moto</h4>
              </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                  <div class="form-group">
                    <label for="biker">Piloto</label>
                    <select class="form-control" name="biker" id="biker_shared">
                      <option value=""></option>
                      <?php foreach ($bikers as $row): ?>
                        <option value="<?php echo $row->id; ?>"><?php echo $row->name.' '.$row->surname; ?></option>
                      <?php endforeach; ?>
                    </select>
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                    <label for="motorcycle">Moto</label>
                    <select name="motorcycle_shared" id="motorcycle_shared" class="form-control">
                      <option value=""></option>
                      <?php foreach ($motorcycles as $row): ?>
                        <option value="<?php echo $row->id; ?>"><?php echo $row->code_motorcycle.' | '.$row->license_plate; ?></option>
                      <?php endforeach; ?>
                    </select>
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                    <label for="horario_partida">Horário de partida</label>
                    <input type="text" name="departure_time" id="departure_time_shared" class="form-control" value="" readonly>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md pr-md-1">
                  <div class="form-group">
                    <label for="departure_odometer">Odómetro de saída</label>
                    <input type="text" name="departure_odometer" id="departure_odometer_shared" class="form-control odometer" value="">
                  </div>
                </div>
                <div class="col-md pl-md-1">
                  <div class="form-group">
                    <label for="arrival_odometer">Odómetro de chegada</label>
                    <input type="text" name="arrival_odometer" id="arrival_odometer_shared" class="form-control odometer" value="">
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md">
                  <div class="form-group">
                    <label for="biker">Encomendas</label>
                    <div class="row">
                      <div class="col-md-12 div_shared">

                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary close_detail_shared" data-dismiss="modal">Fechar</button>
              <input type="hidden" name="leave" value="P">
              <input type="hidden" name="id_shared_trip" id="id_shared_trip" value="">
              <button type="submit" name="update_shared_trips" class="btn btn-primary float-right">Atualizar</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
