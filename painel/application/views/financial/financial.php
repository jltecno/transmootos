<?php $this->load->view('elements/header');?>
<?php $this->load->view('elements/sidebar_lateral');?>
<?php $this->load->view('elements/sidebar');?>
<div class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-body">
          <div class="row">
            <div class="col-md-2 text-left" style="text-align: left;">
              <img style="width:50px;height:50px;" src="<?php echo base_url()?>assets/build/img/icon/angle-left-solid.svg"
                id="anterior"></img>
            </div>
            <div class="col-md-8 text-center" style="text-align: center;">
              <h3 id="data_relatorio"><?php echo date('d-m-Y'); ?></h3>
            </div>
            <div class="col-md-2 text-right" style="text-align: right;">
              <img style="width:50px;height:50px;" src="<?php echo base_url()?>assets/build/img/icon/angle-right-solid.svg"
                id="proximo"></img>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>


  <div class="row">
    <div class="col-lg-3 col-md-6 col-sm-6">
      <div class="card card-stats">
        <div class="card-body ">
          <div class="row">
            <div class="col-5 col-md-4">
              <div class="icon-big text-green text-center icon-warning">
              <i class="fas fa-receipt"></i>
              </div>
            </div>
            <div class="col-7 col-md-8">
              <div class="numbers">
                <p class="card-category">Faturamento <br>do dia</p>
                <p class="card-title invoice_day">R$ <?php echo $invoice_day->expected_value; ?><p>
              </div>
            </div>
          </div>
        </div>
        <div class="card-footer ">
        </div>
      </div>
    </div>

    <div class="col-lg-3 col-md-6 col-sm-6">
      <div class="card card-stats">
        <div class="card-body ">
          <div class="row">
            <div class="col-5 col-md-4">
              <div class="icon-big text-green text-center icon-warning">
              <i class="fas fa-check"></i>
              </div>
            </div>
            <div class="col-7 col-md-8">
              <div class="numbers">
                <p class="card-category">Total de <br> viagem</p>
                <p class="card-title count_trips"><?php echo $count_trips["quantity_trips"];  ?><p>
              </div>
            </div>
          </div>
        </div>
        <div class="card-footer">
        </div>
      </div>
    </div>

    <div class="col-lg-3 col-md-6 col-sm-6">
      <div class="card card-stats">
        <div class="card-body ">
          <div class="row">
            <div class="col-5 col-md-4">
              <div class="icon-big text-green text-center icon-warning">
              <i class="fas fa-arrow-down"></i>
              </div>
            </div>
            <div class="col-7 col-md-8">
              <div class="numbers">
                <p class="card-category">Saída de <br> caixa</p>
                <p class="card-title expenses_day">R$ <?php echo $expenses_day->amount; ?><p>
              </div>
            </div>
          </div>
        </div>
        <div class="card-footer ">
        </div>
      </div>
    </div>

    <div class="col-lg-3 col-md-6 col-sm-6">
      <div class="card card-stats">
        <div class="card-body ">
          <div class="row">
            <div class="col-5 col-md-4">
              <div class="icon-big  text-green text-center icon-warning">
              <i class="fas fa-hand-holding-usd"></i>
              </div>
            </div>
            <div class="col-7 col-md-8">
              <div class="numbers">
                <p class="card-category">Valor a <br> receber</p>
                <p class="card-title">R$<p>
              </div>
            </div>
          </div>
        </div>
        <div class="card-footer ">
        </div>
      </div>
    </div>
  </div>


  <div class="row">
    <div class="col-md-6">
      <div class="card ">
        <div class="card-header">
          <h5 class="card-title titulo_detalhe_financeiro">Todas as corridas</h5>
        </div>
        <div class="card-body ">
          <div class="row">
            <div class="col-md">
              <table id="every_race" class="table table-striped table-bordered" style="width:100%;border:none;">
                <thead>
                  <tr>
                    <th>Moto</th>
                    <th>Tipo e serviço</th>
                    <th>Valor</th>
                  </tr>
                </thead>
                <tbody>
                  <?php foreach ($trips_day as $row): ?>
                    <tr>
                      <td><?php echo $row->license_plate; ?></td>
                      <td><?php echo ($row->service == 'P') ? 'Pessoa' : 'Material' ; ?></td>
                      <td>R$ <?php echo $row->expected_value; ?></td>
                    </tr>
                  <?php endforeach; ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
        <div class="card-footer"></div>
      </div>
    </div>



    <div class="col-md-6">
      <div class="card ">
        <div class="card-header">
          <h5 class="card-title">Todos os abastecimentos</h5>
        </div>
        <div class="card-body ">
          <div class="row">
            <div class="col-md">
              <table id="all_gasoline" class="table table-striped table-bordered" style="width:100%;border:none;">
                <thead>
                  <tr>
                    <th>Moto</th>
                    <th>Local do abastecimento</th>
                    <th>Valor</th>
                  </tr>
                </thead>
                <tbody>
                  <?php foreach ($gasoline_day as $row): ?>
                    <tr>
                      <td><?php echo $row->license_plate; ?></td>
                      <td><?php echo $row->gas_station; ?></td>
                      <td>R$ <?php echo $row->amount; ?></td>
                    </tr>
                  <?php endforeach; ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
        <div class="card-footer "></div>
      </div>
    </div>
  </div>
  <?php $this->load->view('elements/footer');?>
