<?php $this->load->view('elements/header');?>
<?php $this->load->view('elements/sidebar_lateral');?>
<?php $this->load->view('elements/sidebar');?>
<div class="content">
  <div class="row">
    <div class="col-lg-3 col-md-6 col-sm-6">
      <div class="card card-stats">
        <div class="card-body ">
          <div class="row">
            <div class="col-5 col-md-4">
              <div class="icon-big text-center text-green">
              <i class="fas fa-money-bill-wave"></i>
              </div>
            </div>
            <div class="col-7 col-md-8">
              <div class="numbers">
                <p class="card-category">Total a receber</p>
                <p class="card-title" style="font-size: 25px;">R$<?php echo number_format($amount,2,",","."); ?><p>
              </div>
            </div>
          </div>
        </div>
        <div class="card-footer ">

        </div>
      </div>
    </div>
    <!-- <div class="col-lg-3 col-md-6 col-sm-6">
      <div class="card card-stats">
        <div class="card-body ">
          <div class="row">
            <div class="col-5 col-md-4">
              <div class="icon-big text-center text-green">
              <i class="fas fa-calendar-alt"></i>
              </div>
            </div>
            <div class="col-7 col-md-8">
              <div class="numbers">
                <p class="card-category">Mês</p>
                <p class="card-title" style="font-size: 25px;">R$<?php echo number_format($bill_month->bill_value,2,",","."); ?>
                  <p>
              </div>
            </div>
          </div>
        </div>
        <div class="card-footer ">

        </div>
      </div>
    </div>
    <div class="col-lg-3 col-md-6 col-sm-6">
      <div class="card card-stats">
        <div class="card-body ">
          <div class="row">
            <div class="col-5 col-md-4">
              <div class="icon-big text-center text-green">
              <i class="fas fa-calendar-minus"></i>
              </div>
            </div>
            <div class="col-7 col-md-8">
              <div class="numbers">
                <p class="card-category">Semana</p>
                <p class="card-title" style="font-size: 25px;">R$<?php echo number_format($bill_week->bill_value,2,",",".");?>
                  <p>
              </div>
            </div>
          </div>
        </div>
        <div class="card-footer ">
        </div>
      </div>
    </div>
    <div class="col-lg-3 col-md-6 col-sm-6">
      <div class="card card-stats">
        <div class="card-body ">
          <div class="row">
            <div class="col-5 col-md-4">
              <div class="icon-big text-center text-green">
              <i class="fas fa-calendar-day"></i>
              </div>
            </div>
            <div class="col-7 col-md-8">
              <div class="numbers">
                <p class="card-category">Dia</p>
                <p class="card-title" style="font-size: 25px;">R$<?php  echo number_format($bill_day->bill_value,2,",","."); ?>
                  <p>
              </div>
            </div>
          </div>
        </div>
        <div class="card-footer ">

        </div>
      </div>
    </div> -->
  </div>
  <div class="row">
    <div class="col-md-6">
      <div class="card ">
        <div class="card-body ">
          <div class="col-md-12">
            <br><br>
            <div class="row">
              <div class="col-md-12">
                <h5 class="text-centered d-inline">Contas a receber</h5>
                <div class="table-responsive">
                  <table id="table_bills_receive" class="table table-striped" cellspacing="0" width="100%">
                    <thead class=" text-primary">
                      <tr>
                	      <th scope="col">Cliente</th>
                				<th scope="col">Valor a receber</th>
                				<th scope="col">Vencimento</th>
                        <th></th>
                	    </tr>
                    </thead>
                    <tbody>
                      <?php foreach ($receives as $row): ?>
                        <tr>
                          <td><?php echo $row->name; ?> <?php echo $row->surname; ?> <?php echo $row->trade_name; ?></td>
                          <td>R$ Manutenção<?php //echo number_format($row->quantity * $row->price,2,",","."); ?></td>
                          <td>
                            <button type="button" name="button" class="btn btn-primary pay" id="payment_receive" data-id="<?php echo $row->id; ?>" data-id_client="<?php echo $row->id_client; ?>" data-toggle="modal" data-target="#modal_payment">Pagar</button>
                          </td>
                          <td></td>
                        </tr>
                      <?php endforeach; ?>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="card-footer ">
          <!-- <hr>
          <div class="stats">
            <i class="fa fa-history"></i> Updated 3 minutes ago
          </div> -->
        </div>
      </div>
    </div>
    <div class="col-md-6">
      <div class="card ">
        <div class="card-body ">
          <div class="col-md-12">
            <br><br>
            <div class="row">
              <div class="col-md-12">
                <h5 class="text-centered d-inline">Contas recebidas</h5>
                <table id="table_accounts_received" class="table table-striped" cellspacing="0" width="100%">
                  <thead class="text-primary">
                    <tr>
                      <th>Cliente</th>
                      <th>Valor</th>
                      <th>Recebido</th>
                      <th></th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php foreach ($bills_receives as $row): ?>
                      <?php $date_receipt = date_create($row->date_receipt); ?>
                      <tr>
                        <td><?php echo (isset($row->trade_name)) ? $row->trade_name : $row->name.' '.$row->surname ; ?></td>
                        <td>R$<?php echo $row->amount; ?></td>
                        <td><?php echo date_format($date_receipt, 'd/m/Y H:i'); ?></td>
                        <td>
                          <a href="" class="detail_bills" data-id="<?php echo $row->id; ?>" data-toggle="modal" data-target="#detail_bills_receive">
                            <i class="fa fa-1x fa-search "></i>
                          </a>
                      </tr>
                    <?php endforeach; ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>


        </div>
      </div>
    </div>
  </div>
</div>
<?php $this->load->view('elements/footer');?>

<!-- Modal -->
<div class="modal fade" id="modal_payment" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Pagamento</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="<?php echo base_url(); ?>bills_receive/update_pay_night_trips" method="post">
          <div id="detail_client" class="row"></div>
          <table class="table table-striped">
            <thead>
              <tr>
                <th>#</th>
                <th>Viagens</th>
                <th>Unitário</th>
                <th>Valor</th>
                <th>Data</th>
              </tr>
            </thead>
            <tbody class="night_trips_receive"></tbody>
          </table>
          <div id="amount" class="row text-right"></div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
          <input type="hidden" name="client" value="" id="client"/>
          <button type="submit" name="pay_bills_receive" class="btn btn-primary">Pagar</button>
        </div>
      </form>
    </div>
  </div>
</div>

<!-- Modal detail bills receive -->
<div class="modal fade" id="detail_bills_receive" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Detalhes do pagamento</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="<?php echo base_url(); ?>bills_receive/update_pay_night_trips" method="post">
          <div id="detail_client" class="row"></div>
          <table class="table table-striped">
            <thead>
              <tr>
                <th>Viagens</th>
                <th>Unitario</th>
                <th>Valor</th>
                <th>Data</th>
              </tr>
            </thead>
            <tbody class="detail_bills_receive">

            </tbody>
          </table>
          <div id="amount_detail" class="row text-right"></div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
          <input type="hidden" name="client" value="" id="client"/>
          <!-- <button type="submit" name="pay_bills_receive" class="btn btn-primary">Pagar</button> -->
        </div>
      </form>
    </div>
  </div>
</div>
