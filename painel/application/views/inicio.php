<?php $this->load->view('elements/header');?>
<div class="wrapper ">
  <?php $this->load->view('elements/sidebar_lateral');?>
  <?php $this->load->view('elements/sidebar');?>
  <div class="content">
    <div class="row">
      <div class="col-md-12">
        <div class="card ">
          <div class="card-header">
            <h4 class="card-title">Entrada de pedidos</h4>

          </div>
          <div class="card-body">
            <form action="<?php echo base_url(); ?>trips/register_trips" method="post">
              <div class="row">
                <div class="col-md-12 d-flex justify-content-start">
                  <div class="" style="border-right: 2px solid #ccc; margin-right: 12px; padding-right: 12px;">
                    <h6>Cliente</h6>

                    Escolher:
                    <button type="button" class="chama_modal icon_acao" name="button" data-id="modalClients" id=""><i
                        class="fa fa-1x fa-book " aria-hidden="true"></i></button>
                    <div id="dados_cliente"></div>
                    <input type="hidden" name="id_client" id="id_client" value="">
                    <input type="hidden" name="contract_client" id="contract_client" value="0">
                  </div>
                  <div class="" style="border-right: 2px solid #ccc; padding-right: 12px; margin-right: 12px;">
                    <h6>Tipo de serviço</h6>
                    <div class="form-group mt-0 form-check-inline">
                      <input class="form-check-input service_type" type="radio" name="service_type" id="people"
                        value="Pessoa">
                      <label class="form-check-label" for="people">Pessoa</label>
                    </div>
                    <div class="form-group mt-0 form-check-inline" id="evento_area">
                      <input class="form-check-input service_type" type="radio" name="service_type" id="material"
                        value="Material">
                      <label class="form-check-label" for="material">Material</label>
                    </div>
                  </div>
                  <div class="" style="border-right: 2px solid #ccc; padding-right: 12px; margin-right: 12px;">
                    <h6>Localidade</h6>
                    <div class="form-group mt-0 form-check-inline">
                      <input class="form-check-input locale " type="radio" name="locale" id="inside" value="I">
                      <label class="form-check-label" for="inside">Interno</label>
                    </div>
                    <div class="form-group mt-0 form-check-inline" id="evento_area">
                      <input class="form-check-input locale" type="radio" name="locale" id="outside" value="O">
                      <label class="form-check-label" for="outside">Fora da cidade</label>
                    </div>
                    <!-- <div class="form-group mt-0 form-check-inline" id="evento_area">
                      <input class="form-check-input locale" type="radio" name="locale" id="order" value="Or">
                      <label class="form-check-label" for="order">Encomenda | compartilhado</label>
                    </div> -->
                  </div>
                  <div class="" id="div_km"
                    style="display: none; border-right: 2px solid #ccc; padding-right: 12px; margin-right: 12px;">
                    <h6>KM Previsto</h6>
                    <div class="form-group mt-0 form-check-inline">
                      <input class="form-check-input" type="text" name="distance" id="distance"
                        style="height: 40px; width: 100px;">
                      <button class="icon_acao" type="button" name="button" style="height: 40px;"><i
                          class="fa fa-1x fa-search" aria-hidden="true"></i></button>
                    </div>
                  </div>
                  <!-- <div class="" id="div_city"
                    style="display: none; border-right: 2px solid #ccc; padding-right: 12px; margin-right: 12px;">
                    <h6>Cidade</h6>
                    <div class="form-group mt-0 form-check-inline">
                      <select class="" name="package_city" id="package_city">
                        <option value=""></option>
                        <?php foreach ($cities_payment as $row): ?>
                          <option value="<?php echo $row->id; ?>"><?php echo $row->description; ?></option>
                        <?php endforeach; ?>
                      </select>
                    </div>
                  </div> -->
                  <div class="">
                    <h6>Valor previsto</h6>
                    <div class="form-group mt-0 form-check-inline">
                      <input class="form-check-input valor" type="text" name="price" id="price" value=""
                        style="height: 40px; width: 100px; border: 0;">
                    </div>
                  </div>
                </div>
              </div>
              <br><br>
              <div class="row">
                <div class="col-md-6">
                  <h6>Origem</h6>
                </div>
                <div class="col-md-6">
                  <h6>Destino</h6>
                </div>
              </div>
              <div class="row">
                <div class="col-md-6" style="border-right: 2px solid #ccc;">
                  <div class="row">
                    <div class="col-md-3 pr-md-1">
                      <div class="form-group">
                        <label for="cep_origin">Cep</label>
                        <input type="text" name="cep_origin" id="cep_origin" class="form-control"
                          value="<?php echo set_value('cep_origin'); ?>" onblur="search_cep_origin(this.value);">
                        <?php echo form_error('cep_origin'); ?>
                      </div>
                    </div>
                    <div class="col-md-7 pr-md-1 pl-md-1">
                      <div class="form-group">
                        <label for="address_origin">Rua</label>
                        <input type="text" name="address_origin" id="address_origin" class="form-control" value="">
                        <?php echo form_error('address_origin'); ?>
                      </div>
                    </div>
                    <div class="col-md pl-md-1">
                      <div class="form-group">
                        <label for="number">Numero</label>
                        <input type="text" name="number" id="number" class="form-control"
                          value="<?php echo set_value('number'); ?>">
                        <?php echo form_error('number'); ?>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-5 pr-md-1">
                      <div class="form-group">
                        <label for="neighborhood_origin">Bairro</label>
                        <input type="text" name="neighborhood_origin" id="neighborhood_origin" class="form-control"
                          value="<?php echo set_value('neighborhood_origin'); ?>">
                        <?php echo form_error('neighborhood_origin'); ?>
                      </div>
                    </div>
                    <div class="col-md-5 pr-md-1 pl-md-1">
                      <div class="form-group">
                        <label for="city_origin">Cidade</label>
                        <input type="text" name="city_origin" id="city_origin" class="form-control"
                          value="<?php echo set_value('city_origin'); ?>">
                        <?php echo form_error('city_origin'); ?>
                      </div>
                    </div>
                    <div class="col-md-2 pl-md-1">
                      <div class="form-group">
                        <label for="uf_origin">UF</label>
                        <input type="text" name="uf_origin" id="uf_origin" class="form-control"
                          value="<?php echo set_value('uf_origin'); ?>">
                        <?php echo form_error('uf_origin'); ?>
                      </div>
                    </div>
                    <hr>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="row">
                    <div class="col-md-3 pr-md-1">
                      <div class="form-group">
                        <label for="cep_destination">Cep</label>
                        <input type="text" name="cep_destination" id="cep_destination" class="form-control"
                          value="<?php echo set_value('cep_destination'); ?>"
                          onblur="search_cep_destination(this.value);">
                        <?php echo form_error('cep_destination'); ?>
                      </div>
                    </div>
                    <div class="col-md-7 pr-md-1 pl-md-1">
                      <div class="form-group">
                        <label for="address_destination">Rua</label>
                        <input type="text" name="address_destination" id="address_destination" class="form-control"
                          value="<?php echo set_value('address_destination'); ?>">
                        <?php echo form_error('address_destination'); ?>
                      </div>
                    </div>
                    <div class="col-md pl-md-1">
                      <div class="form-group">
                        <label for="number_destination">Numero</label>
                        <input type="text" name="number_destination" id="number_destination" class="form-control"
                          value="<?php echo set_value('number_destination'); ?>">
                        <?php echo form_error('number_destination'); ?>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-5 pr-md-1">
                      <div class="form-group">
                        <label for="neighborhood_destination">Bairro</label>
                        <input type="text" name="neighborhood_destination" id="neighborhood_destination"
                          class="form-control" value="<?php echo set_value('neighborhood_destination'); ?>">
                        <?php echo form_error('neighborhood_destination'); ?>
                      </div>
                    </div>
                    <div class="col-md-5 pr-md-1 pl-md-1">
                      <div class="form-group">
                        <label for="city_destination">Cidade</label>
                        <input type="text" name="city_destination" id="city_destination" class="form-control"
                          value="<?php echo set_value('city_destination'); ?>">
                        <?php echo form_error('city_destination'); ?>
                      </div>
                    </div>
                    <div class="col-md-2 pl-md-1">
                      <div class="form-group">
                        <label for="uf_destination">UF</label>
                        <input type="text" name="uf_destination" id="uf_destination" class="form-control"
                          value="<?php echo set_value('uf_destination'); ?>">
                        <?php echo form_error('uf_destination'); ?>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <br><br>
              <div class="row">
                <div class="col-md-6">
                  <h6>Saida</h6>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12">
                  <div class="form-group mt-0 form-check-inline">
                    <input class="form-check-input leave" type="radio" name="leave" id="go_out_now" value="G">
                    <label class="form-check-label" for="go_out_now">Sair agora</label>
                  </div>
                  <div class="form-group mt-0 form-check-inline">
                    <input class="form-check-input leave" type="radio" name="leave" id="queue" value="Q">
                    <label class="form-check-label" for="queue">Entrar na fila</label>
                  </div>
                  <div class="form-group mt-0 form-check-inline">
                    <input class="form-check-input leave" type="radio" name="leave" id="schedule" value="S">
                    <label class="form-check-label" for="schedule">Agendar</label>
                  </div>
                  <!-- <div class="form-group mt-0 form-check-inline">
                    <input class="form-check-input leave" type="radio" name="leave" id="packages" value="P">
                    <label class="form-check-label" for="packages">Encomenda | compartilhado</label>
                  </div> -->
                </div>
              </div>
              <div id="div_go_out_now" style="display: none;">
                <div class="row">
                  <div class="col-md-6">
                    <div class="row">
                      <div class="col-md pr-1">
                        <label for="">Piloto</label>
                        <select class="form-control" name="biker">
                          <option value=""></option>
                          <?php foreach ($bikers as $row): ?>
                          <option value="<?php echo $row->id; ?>"><?php echo $row->name; ?> <?php echo $row->surname; ?>
                          </option>
                          <?php endforeach; ?>
                        </select>
                      </div>
                      <div class="col-md pl-1 pr-1">
                        <label for="">Moto</label>
                        <select class="form-control" name="motorcycle">
                          <option value=""></option>
                          <?php foreach ($motorcycles as $row): ?>
                          <option value="<?php echo $row->id; ?>"><?php echo $row->code_motorcycle; ?> | <?php echo $row->license_plate; ?></option>
                          <?php endforeach; ?>
                        </select>
                      </div>
                      <div class="col-md pl-1">
                        <label for="">Odômetro</label>
                        <input type="text" class="form-control odometer" name="odometer" value="">
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div id="div_schedule" style="display: none;">
                <div class="row">
                  <div class="col-md-4">
                    <div class="row">
                      <div class="col-md pr-1">
                        <label for="date_scheduling">Data</label>
                        <input type="date" name="date_scheduling" class="form-control" value="" id="date_scheduling">
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md pr-1">
                        <label for="hour_scheduling">Hora</label>
                        <input type="time" name="hour_scheduling" class="form-control" value="" id="hour_scheduling">
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12 text-right">
                  <button type="submit" class="btn btn-primary" name="register_trips"
                    id="register_trips">Cadastrar</button>
                </div>
              </div>
            </form>

          </div>
          <div class="card-footer ">






          </div>
        </div>
      </div>
    </div>
  </div>
  <?php $this->load->view('elements/footer');?>
