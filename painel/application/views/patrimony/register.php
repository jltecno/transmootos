<?php $this->load->view('elements/header');?>
<div class="wrapper ">
  <?php $this->load->view('elements/sidebar_lateral');?>
  <?php $this->load->view('elements/sidebar');?>
  <div class="content">
    <div class="row">
      <div class="col-md-12">
        <div class="card card-user">
          <div class="card-header">
            <h5 class="card-title">Cadastrar Patrimônios</h5>
          </div>
          <div class="card-body">
            <form action="<?php echo base_url(); ?>patrimony/register_patrimony" method="post" enctype="multipart/form-data">
                <div class="row">
                  <div class="col-md-6 pr-1">
                    <div class="form-group">
                      <label for="description">Descrição</label>
                      <input type="text" name="description" id="description" class="form-control" placeholder="Descrição"
                        value="<?php echo set_value('description'); ?>">
                      <?php echo form_error('description'); ?>
                    </div>
                  </div>
                  <div class="col-md-6 pl-1">
                    <div class="form-group">
                      <label for="quantity">Quantidades</label>
                      <input type="number" name="quantity" id="quantity" class="form-control" placeholder="Quantidade"
                        value="<?php echo set_value('quantity'); ?>">
                      <?php echo form_error('quantity'); ?>
                    </div>
                  </div>
                </div>
              <div class="row">
                <div class="col-md-12">
                  <button type="submit" name="register_patrimony"
                    class="btn btn-primary btn-round float-md-right float-sm-right float-xs-right">Cadastrar</button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
  <?php $this->load->view('elements/footer');?>
