<?php $this->load->view('elements/header');?>
  <div class="wrapper ">
    <?php $this->load->view('elements/sidebar_lateral');?>
    <?php $this->load->view('elements/sidebar');?>
    <div class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title" style="display: inline;">Patrimônios</h3>
              <a href="<?php echo base_url()?>patrimony/register" class="btn btn-primary float-md-right float-sm-right float-xs-right">Cadastrar</a>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table id="table_patrimony" class="table table-striped">
                  <thead class=" text-primary">
                    <th>Descrição</th>
                    <th>Quantidade</th>
                  </thead>
                  <tbody>
                    <?php foreach ($patrimony as $row): ?>
                      <tr>
                        <td><?php echo $row->description; ?></td>
                        <td><?php echo $row->quantity; ?></td>
                      </tr>
                    <?php endforeach; ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
<?php $this->load->view('elements/footer');?>
