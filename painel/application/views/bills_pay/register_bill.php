<?php $this->load->view('elements/header');?>
<?php $this->load->view('elements/sidebar_lateral');?>
<?php $this->load->view('elements/sidebar');?>


<div class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="card">
				<div class="card-header">
					<h3 class="text-center"> Cadastro de Pagamento </h3>
				</div>
				<div class="card-body ">

					<div class="col-md-4 centered">
						<div style="color:red;">
							<?//=validation_errors(); ?>
						</div>
					</div>
          <form action="<?php echo base_url(); ?>bills_pay/register_bill" method="post">
  					<div class="row">
  						<div class="col-md-6">
  							<div class="form-group">
  								<label class="label-input-style" for="name_bill">Nome: </label>
  								<input type="text" class="form-control" id="name_bill" name="name_bill" value="<?=set_value('name_bill')?>" required />
  							</div>
  						</div>
							<div class="col-md-6">
								<div class="form-group">
									<label for="provider">Fornecedor</label>
									<select class="form-control" name="provider" id="provider">
										<option value=""></option>
										<?php foreach ($providers as $row): ?>
											<option value="<?php echo $row->id; ?>"><?php echo (isset($row->trade_name)) ? $row->trade_name : $row->name.' '.$row->surname ; ?></option>

										<?php endforeach; ?>
									</select>
								</div>
							</div>

  					</div>
  					<div class="row">
							<div class="col-md-4">
  							<div class="form-group">
  								<label class="label-input-style" for="bill_value">Valor da Conta: </label>
  								<input type="text" class="form-control valor" id="bill_value" name="bill_value" value="<?=set_value('bill_value')?>"
  									required />
  							</div>
  						</div>
  						<div class="col-md-4">
  							<div class="form-group">
  								<label class="label-input-style" for="type_bill">Tipo de Conta: </label>
  								<select class="form-control" id="type_bill" name="type_bill" style="height: 35px;" required>
  									<option value=""></option>
  									<option value="Fixo">Fixo</option>
  									<option value="Variável">Variavél</option>
  								</select>
  							</div>
  						</div>
  						<div class="col-md-4">
  							<div class="form-group">
  								<label class="label-input-style" for="due_date">Data do Vencimento: </label>
  								<input type="date" class="form-control" id="due_date" name="due_date"
  									value="<?=set_value('due_date')?>" required />
  							</div>
  						</div>
  					</div>

  					<div class="card-footer">
  						<div class="col-md">
  							<div class="form-group float-right">
                  <a href="<?php echo base_url(); ?>bills_pay" class="btn btn-danger">Cancelar</a>
  								<input type="submit" name="register_bill" class="btn btn-primary" value="Cadastrar" />
  							</div>
  						</div>
  					</div>
          </form>
				</div>
			</div>
		</div>
	</div>
</div>
</div>
<?php $this->load->view('elements/footer');?>
