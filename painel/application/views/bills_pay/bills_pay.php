<?php $this->load->view('elements/header');?>
<?php $this->load->view('elements/sidebar_lateral');?>
<?php $this->load->view('elements/sidebar');?>
<div class="content">
  <div class="row">
    <div class="col-lg-3 col-md-6 col-sm-6">
      <div class="card card-stats">
        <div class="card-body ">
          <div class="row">
            <div class="col-5 col-md-4">
              <div class="icon-big text-center text-green">
              <i class="fas fa-money-bill-wave"></i>
              </div>
            </div>
            <div class="col-7 col-md-8">
              <div class="numbers">
                <p class="card-category">Total a pagar</p>
                <p class="card-title" style="font-size: 25px;">R$<?php echo number_format($total_price->bill_value,2,",","."); ?><p>
              </div>
            </div>
          </div>
        </div>
        <div class="card-footer ">

        </div>
      </div>
    </div>
    <div class="col-lg-3 col-md-6 col-sm-6">
      <div class="card card-stats">
        <div class="card-body ">
          <div class="row">
            <div class="col-5 col-md-4">
              <div class="icon-big text-center text-green">
              <i class="fas fa-calendar-alt"></i>
              </div>
            </div>
            <div class="col-7 col-md-8">
              <div class="numbers">
                <p class="card-category">Mês</p>
                <p class="card-title" style="font-size: 25px;">R$<?php echo number_format($bill_month->bill_value,2,",","."); ?>
                  <p>
              </div>
            </div>
          </div>
        </div>
        <div class="card-footer ">

        </div>
      </div>
    </div>
    <div class="col-lg-3 col-md-6 col-sm-6">
      <div class="card card-stats">
        <div class="card-body ">
          <div class="row">
            <div class="col-5 col-md-4">
              <div class="icon-big text-center text-green">
              <i class="fas fa-calendar-minus"></i>
              </div>
            </div>
            <div class="col-7 col-md-8">
              <div class="numbers">
                <p class="card-category">Semana</p>
                <p class="card-title" style="font-size: 25px;">R$<?php echo number_format($bill_week->bill_value,2,",",".");?>
                  <p>
              </div>
            </div>
          </div>
        </div>
        <div class="card-footer ">
        </div>
      </div>
    </div>
    <div class="col-lg-3 col-md-6 col-sm-6">
      <div class="card card-stats">
        <div class="card-body ">
          <div class="row">
            <div class="col-5 col-md-4">
              <div class="icon-big text-center text-green">
              <i class="fas fa-calendar-day"></i>
              </div>
            </div>
            <div class="col-7 col-md-8">
              <div class="numbers">
                <p class="card-category">Dia</p>
                <p class="card-title" style="font-size: 25px;">R$<?php  echo number_format($bill_day->bill_value,2,",","."); ?>
                  <p>
              </div>
            </div>
          </div>
        </div>
        <div class="card-footer ">

        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-6">
      <div class="card ">
        <div class="card-body ">
          <div class="col-md-12">
            <br><br>
            <div class="row">
              <div class="col-md-12">
                <h5 class="text-centered d-inline">Contas fixas</h5><a href="<?php echo base_url(); ?>bills_pay/register" class="btn btn-primary float-right">Cadastrar Conta</a>
                <table id="table_fixed_accounts" class="table table-striped" cellspacing="0" width="100%">
              		<thead>
              	    <tr>
              	      <th scope="col">Nome</th>
              				<th scope="col">Valor</th>
                      <th scope="col">Vencimento</th>
              				<th scope="col">Fornecedor</th>
                      <th></th>
              	    </tr>
              	  </thead>
              	  <tbody>
                    <?php foreach ($fixed_accounts as $row): ?>
                      <?php $due_date = date_create($row->due_date); ?>
                      <tr>
                        <td><?php echo $row->name_bill; ?></td>
                        <td>R$<?php echo number_format($row->bill_value,2,",","."); ?></td>
                        <td>Dia <?php echo date_format($due_date, 'd'); ?></td>
                        <td><?php echo (isset($row->trade_name)) ? $row->trade_name : $row->name.' '.$row->surname; ?></td>
                        <td>
                          <button type="button" name="button" class="btn btn-primary pay" data-id="<?php echo $row->id; ?>" data-name="<?php echo $row->name_bill; ?>" data-duedate="<?php echo date_format($due_date, 'd'); ?>" data-value="<?php echo $row->bill_value; ?>">Pagar</button>
                        </td>
                      </tr>
                    <?php endforeach; ?>
              	  </tbody>
              	</table>
              </div>
            </div>
          </div>
        </div>
        <div class="card-footer "></div>
      </div>
      <div class="card ">
        <div class="card-body ">
          <!-- <div class="col-md-12"> -->
            <br><br>
            <div class="row">
              <div class="col-md-12">
                <h5 class="text-centered d-inline">Contas pagas</h5>
                <table id="table_accounts_pay" class="table table-striped" cellspacing="0" width="100%">
              		<thead>
              	    <tr>
              	      <th scope="col">Nome</th>
              				<th scope="col">Valor</th>
                      <th scope="col">Vencimento</th>
              				<th scope="col">Fornecedor</th>
                      <th scope="col">Pagamento</th>
              	    </tr>
              	  </thead>
              	  <tbody>
                    <?php foreach ($bills_payments as $row): ?>
                      <?php $due_date = date_create($row->due_date); ?>
                      <?php $date_payment = date_create($row->date_payment); ?>
                      <tr>
                        <td><?php echo $row->name_bill; ?></td>
                        <td>R$<?php echo number_format($row->bill_value,2,",","."); ?></td>
                        <td>Dia <?php echo date_format($due_date, 'd/m'); ?></td>
                        <td><?php echo (isset($row->trade_name)) ? $row->trade_name : $row->name.' '.$row->surname; ?></td>
                        <td><?php echo date_format($date_payment, 'd/m'); ?></td>
                      </tr>
                    <?php endforeach; ?>
              	  </tbody>
              	</table>
              </div>
            </div>
          <!-- </div> -->
        </div>
        <div class="card-footer "></div>
      </div>
    </div>
    <div class="col-md-6">
      <div class="card ">
        <div class="card-body ">
          <div class="col-md-12">
            <br><br>
            <div class="row">
              <div class="col-md-12">
                <h5 class="text-centered d-inline">Contas variáveis</h5><a href="<?php echo base_url(); ?>bills_pay/register" class="btn btn-primary float-right">Cadastrar Conta</a>
                <table id="table_variable_accounts" class="table table-striped"  width="100%">
              		<thead>
              	    <tr>
              	      <th scope="col">Nome</th>
              				<th scope="col">Valor</th>
                      <th scope="col">Vencimento</th>
              				<th scope="col">Fornecedor</th>
                      <th scope="col"></th>
              	    </tr>
              	  </thead>
              	  <tbody>
                    <?php foreach ($variable_accounts as $row): ?>
                      <?php $due_date = date_create($row->due_date); ?>
                      <tr>
                        <td><?php echo $row->name_bill; ?></td>
                        <td>R$<?php echo number_format($row->bill_value,2,",","."); ?></td>
                        <td>Dia <?php echo date_format($due_date, 'd/m'); ?></td>
                        <td><?php echo (isset($row->trade_name)) ? $row->trade_name : $row->name.' '.$row->surname; ?></td>
                        <td>
                          <button type="button" name="button" class="btn btn-primary pay" data-id="<?php echo $row->id; ?>" data-name="<?php echo $row->name_bill; ?>"
                             data-duedate="<?php echo date_format($due_date, 'd/m'); ?>" data-value="<?php echo $row->bill_value; ?>" data-provider="<?php echo (isset($row->trade_name)) ? $row->trade_name : $row->name.' '.$row->surname; ?>">Pagar</button>
                        </td>
                      </tr>
                    <?php endforeach; ?>
              	  </tbody>
              	</table>
              </div>
            </div>
          </div>
        </div>
        <div class="card-footer "></div>
      </div>
    </div>
  </div>
</div>
<?php $this->load->view('elements/footer');?>

<!-- Modal -->
<div class="modal fade" id="modal_bill_pay" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel" style="display: inline;">Pagar conta</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="<?php echo base_url(); ?>bills_pay/register_payment" method="post">
        <div id="boleto">
          <div class="form-group">
            <label class="label-input-style" for="insert_payment">Nome: </label>
            <input type="text" class="col-md-12 form-control" id="name_bill" name="name_bill" value="" readonly>
          </div>
          <div class="form-group">
            <label class="label-input-style" for="due_date">Vencimento: </label>
            <input type="text" class="col-md-12 form-control" id="due_date" name="due_date" value="" readonly>
          </div>
          <div class="form-group">
            <label class="label-input-style" for="provider">Fornecedor: </label>
            <input type="text" class="col-md-12 form-control" id="provider" name="provider" value="" readonly>
          </div>
          <div class="form-group">
            <label class="label-input-style" for="bill_value">Valor: </label>
            <input type="text" class="col-md-12 form-control" id="bill_value" name="bill_value" value="" readonly>
            <input type="hidden" name="id_bill" value="" id="id_bill"/>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <input type="hidden" id="id_conta" name="id_conta" value="">
        <button type="submit" name="payment_bill" class="btn btn-primary">Pagar</button>
        </form>
      </div>
    </div>
  </div>
</div>
