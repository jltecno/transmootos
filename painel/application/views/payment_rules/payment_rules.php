<?php $this->load->view('elements/header');?>
  <div class="wrapper ">
    <?php $this->load->view('elements/sidebar_lateral');?>
    <?php $this->load->view('elements/sidebar');?>
    <div class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="card">
            <div class="card-header">
              <h4 class="card-title" style="display: inline;">Regras de pagamento</h4>
              <!-- <a href="<?php echo base_url()?>motorcycles/register" class="btn btn-primary float-md-right float-sm-right float-xs-right">Cadastrar</a> -->
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table id="table_payment_rules" class="table table-striped">
                  <thead class="text-primary">
                    <tr>
                      <th>Descrição</th>
                      <th>Mínimo</th>
                      <th>Máximo</th>
                      <th>Preço</th>
                      <th></th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php foreach ($payment_rules as $row): ?>
                       <tr>
                         <td><?php echo $row->description; ?></td>
                         <td><?php echo $row->minimum; ?></td>
                         <td><?php echo $row->maximum; ?></td>
                         <td><?php echo $row->price; ?></td>
                         <td>
                           <a href=""
                            data-toggle="modal"
                            data-target="#edit-payment-rules"
                            data-id="<?php echo $row->id; ?>"
                            data-description="<?php echo $row->description; ?>"
                            data-minimum="<?php echo $row->minimum; ?>"
                            data-maximum="<?php echo $row->maximum; ?>"
                            data-price="<?php echo $row->price; ?>"
                            id="edit-rules"
                           >
                             <i class="fa fa-edit"></i>
                           </a>
                         </td>
                      </tr>
                    <?php endforeach; ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
<?php $this->load->view('elements/footer');?>

<div class="modal fade" id="edit-payment-rules" tabindex="-1" role="dialog" aria-labelledby="edit-payment-rules" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Regras de pagamento</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="<?php echo base_url(); ?>payment_rules/update_payment_rule" method="post">
          <div class="row">
            <div class="col-md-6 pr-1">
              <div class="form-group">
                <label for="description">Descrição</label>
                <input type="text" name="description" id="description" class="form-control" placeholder="Descrição" value="<?php echo set_value('description'); ?>">
                <?php echo form_error('description'); ?>
              </div>
            </div>
            <div class="col-md-6 pr-1">
              <div class="form-group">
                <label for="price">Preço</label>
                <input type="text" name="price" id="price" class="form-control" placeholder="Preço" value="<?php echo set_value('price'); ?>">
                <?php echo form_error('price'); ?>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-6 pr-1">
              <div class="form-group">
                <label for="minimum">Mínimo</label>
                <input type="text" name="minimum" id="minimum" class="form-control" placeholder="" value="<?php echo set_value('minimum'); ?>">
                <?php echo form_error('minimum'); ?>
              </div>
            </div>
            <div class="col-md-6 pr-1">
              <div class="form-group">
                <label for="maximum">Máximo</label>
                <input type="text" name="maximum" id="maximum" class="form-control" placeholder="" value="<?php echo set_value('maximum'); ?>">
                <?php echo form_error('maximum'); ?>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <input type="hidden" name="id_payment_rules" value="" id="id_payment_rules">
          <button type="submit" name="modify_payment_rules" class="btn btn-primary">Salvar mudanças</button>
        </div>
      </form>
    </div>
  </div>
</div>
