<?php $paginaCorrente = $_SERVER['REQUEST_URI'];?>
<div class="sidebar" data-color="white" data-active-color="danger">
  <!--
    Tip 1: You can change the color of the sidebar using: data-color="blue | green | orange | red | yellow"
-->
  <div class="logo">
    <a href="" class="simple-text logo">
      <div class="logo-image">
        <img src="<?php echo base_url();?>assets/build/img/logo/logo-branca.png">
      </div>
    </a>
  </div>
  <div class="sidebar-wrapper">
    <ul class="nav">
      <li <?php echo(strpos($paginaCorrente, '/inicio') !== false ) ? 'class="active"' : ''; ?>>
        <a href="<?php echo base_url();?>inicio">
          <i class="fas fa-home"></i>
          <p>Início</p>
        </a>
      </li>
      <li <?php echo(strpos($paginaCorrente, '/shared') !== false ) ? 'class="active"' : ''; ?>>
        <a href="<?php echo base_url();?>shared">
          <i class="fas fa-tachometer-alt"></i>
          <p>Compartilhado</p>
        </a>
      </li>
      <li <?php echo(strpos($paginaCorrente, '/dashboard') !== false ) ? 'class="active"' : ''; ?>>
        <a href="<?php echo base_url();?>dashboard">
          <i class="fas fa-tachometer-alt"></i>
          <p>Dashboard</p>
        </a>
      </li>
      <li <?php echo(strpos($paginaCorrente, '/clients') !== false ) ? 'class="active"' : ''; ?>>
        <a href="<?php echo base_url();?>clients">
          <i class="fas fa-user"></i>
          <p>Clientes</p>
        </a>
      </li>
      <li <?php echo(strpos($paginaCorrente, '/bikers') !== false ) ? 'class="active"' : ''; ?>>
        <a href="<?php echo base_url();?>bikers">
          <!-- <img class="helmet_icon" src="<?php echo base_url();?>assets\img\icon\motorcycle-helmet.svg" alt=""> -->
          <p class="pilot_icon">Pilotos</p>
        </a>
      </li>
      <li <?php echo(strpos($paginaCorrente, '/motorcycles') !== false ) ? 'class="active"' : ''; ?>>
        <a href="<?php echo base_url();?>motorcycles">
          <i class="fas fa-motorcycle"></i>
          <p>Motos</p>
        </a>
      </li>
      <li <?php echo(strpos($paginaCorrente, '/provider') !== false ) ? 'class="active"' : ''; ?>>
        <a href="<?php echo base_url();?>provider">
          <i class="fas fa-file-invoice-dollar"></i>
          <p>Fornecedores</p>
        </a>
      </li>

      <!-- <li <?php echo(strpos($paginaCorrente, '/reports') !== false ) ? 'class="active"' : ''; ?>>
        <a href="<?php echo base_url();?>reports">
          <i class="far fa-chart-bar"></i>
          <p>Relatórios</p>
        </a>
      </li> -->
      <li>
        <a class="" data-toggle="collapse" href="#collapseReports" id="templates" role="button"
          aria-expanded="false" aria-controls="collapseReports">
          <i class="fa fa-building"></i>
          <p>
            Relatórios
            <i class="fa fa-chevron-down float-md-right" id="seta_baixo"></i>
            <i class="fa fa-chevron-up float-md-right" id="seta_cima" style="display: none;"></i>
          </p>
        </a>
      </li>
      <div <?php echo(strpos($paginaCorrente, '/clientes') !== false ) ? 'class="collapse in"' : 'class="collapse"'; ?> id="collapseReports" style="padding-left: 6%;">
        <li <?php //echo(strpos($paginaCorrente, '/reports') !== false ) ? 'class="active"' : ''; ?>>
          <a href="<?php echo base_url();?>reports">
            <i class="fas fa-money-bill-wave"></i>
            <p>Relatórios</p>
          </a>
        </li>
        <li <?php //echo(strpos($paginaCorrente, '/reports_clients') !== false ) ? 'class="active"' : ''; ?>>
          <a href="<?php echo base_url();?>reports_clients">
            <i class="fas fa-money-bill-wave"></i>
            <p>Relatórios clientes</p>
          </a>
        </li>
        <li <?php //echo(strpos($paginaCorrente, '/reports_bikers') !== false ) ? 'class="active"' : ''; ?>>
          <a href="<?php echo base_url();?>reports_bikers">
            <i class="fas fa-money-bill-wave"></i>
            <p>Relatórios pilotos</p>
          </a>
        </li>
        <li <?php //echo(strpos($paginaCorrente, '/reports_motorcycles') !== false ) ? 'class="active"' : ''; ?>>
          <a href="<?php echo base_url();?>reports_motorcycles">
            <i class="fas fa-money-bill-wave"></i>
            <p>Relatórios motos</p>
          </a>
        </li>
      </div>
      <!-- AQUI -->
      <li <?php echo(strpos($paginaCorrente, '/patrimony') !== false ) ? 'class="active"' : ''; ?>>
        <a href="<?php echo base_url();?>patrimony">
          <i class="fas fa-home"></i>
          <p>Patrimônios</p>
        </a>
      </li>

      <li>
        <a class="" data-toggle="collapse" href="#collapseFinancial" id="templates" role="button"
          aria-expanded="false" aria-controls="collapseFinancial">
          <i class="fa fa-building"></i>
          <p>
            Financeiro
            <i class="fa fa-chevron-down float-md-right" id="seta_baixo"></i>
            <i class="fa fa-chevron-up float-md-right" id="seta_cima" style="display: none;"></i>
          </p>
        </a>
      </li>

      <div <?php echo(strpos($paginaCorrente, '/clientes') !== false ) ? 'class="collapse in"' : 'class="collapse"'; ?> id="collapseFinancial" style="padding-left: 6%;">
        <li <?php echo(strpos($paginaCorrente, '/financial') !== false ) ? 'class="active"' : ''; ?>>
          <a href="<?php echo base_url();?>financial">
            <i class="fas fa-file-invoice-dollar"></i>
            <p>Financeiro</p>
          </a>
        </li>
      <?php if ($_SESSION['user']['profile'] == 1): ?>
        <li <?php echo(strpos($paginaCorrente, '/bills_pay') !== false ) ? 'class="active"' : ''; ?>>
          <a href="<?php echo base_url();?>bills_pay">
            <i class="fas fa-money-bill-wave"></i>
            <p>Contas a pagar</p>
          </a>
        </li>
        <li <?php echo(strpos($paginaCorrente, '/bills_receive') !== false ) ? 'class="active"' : ''; ?>>
          <a href="<?php echo base_url();?>bills_receive">
            <i class="fas fa-money-bill-wave"></i>
            <p>Contas a receber</p>
          </a>
        </li>
      </div>
      <li <?php echo(strpos($paginaCorrente, '/payment_rules') !== false ) ? 'class="active"' : ''; ?>>
        <a href="<?php echo base_url();?>payment_rules">
          <i class="fas fa-money-bill-wave"></i>
          <p>Regras de pagamento</p>
        </a>
      </li>
      <li <?php echo(strpos($paginaCorrente, '/profile') !== false ) ? 'class="active"' : ''; ?>>
        <a href="<?php echo base_url();?>profile">
          <i class="fas fa-user-circle"></i>
          <p>Perfil</p>
        </a>
      </li>
      <li <?php echo(strpos($paginaCorrente, '/users') !== false ) ? 'class="active"' : ''; ?>>
        <a href="<?php echo base_url();?>users">
          <i class="fas fa-user-circle"></i>
          <p>Usuarios</p>
        </a>
      </li>
      <?php endif;?>


      <li class="active-pro">
        <a href="<?php echo base_url();?>login/logout">
          <i class="nc-icon nc-button-power"></i>
          <p>Sair</p>
        </a>
      </li>
    </ul>
  </div>
</div>
