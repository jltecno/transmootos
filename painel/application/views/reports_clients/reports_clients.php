<?php $this->load->view('elements/header');?>
<?php $this->load->view('elements/sidebar_lateral');?>
<?php $this->load->view('elements/sidebar'); ?>
<div class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="card ">
        <div class="card-header">
          <h3 class="card-title">Relatórios de clientes</h3>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      <div class="card ">
        <div class="card-header">
          <h3 class="card-title">Viagens noturnas</h3>
        </div>
        <div class="card-body ">
          <div class="row mb-2">
            <div class="col-md-4">
              <label for="first_date"><h6>Data de início:</h6></label>
              <input type="text" class="data form-control" placeholder="00/00/0000" name="" id="first_date" value="" class="form-control">
            </div>
            <div class="col-md-4">
              <label for="last_date"><h6>Data de fim:</h6></label>
              <input type="text" class="data form-control" placeholder="00/00/0000" name="" id="last_date" value="" class="form-control">
            </div>
            <div class="col-md-4 mt-3">
              <button type="button" name="button" id="search_date_client_night_trip" class="btn btn-primary">
                <i class="fa fa-1x fa-search"></i>
              </button>
            </div>
          </div>
          <div class="row">
            <div class="col-md">
              <div class="reports_night_trips_clients">
                <table id="reports_night_clients" class="table table-striped table-bordered" style="width:100%;border:none;">
                  <thead>
                    <tr>
                      <th>Cliente</th>
                      <th>Quantidade</th>
                    </tr>
                  </thead>
                  <tbody id="tbody_reports_night_clients">
                    <?php foreach ($night_trips_client as $row): ?>
                      <tr>
                        <td><?php echo (isset($row->name))? $row->name : $row->trade_name; ?></td>
                        <td><?php echo $row->quantity_night_trips; ?></td>
                      </tr>
                    <?php endforeach; ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
        <div class="card-footer "></div>
      </div>
    </div>
  </div>
</div>
<?php $this->load->view('elements/footer');?>
