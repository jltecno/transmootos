<?php $this->load->view('elements/header');?>
<div class="wrapper ">
  <?php $this->load->view('elements/sidebar_lateral');?>
  <?php $this->load->view('elements/sidebar');?>
  <script>
    // window.onload = function(){
    //   // alert('entrou');
    //   var url = "<?php echo base_url(); ?>packages/packages";
    //   window.open(url, '_blank');
    // }
  </script>
  <div class="content">
    <div class="row">
      <div class="col-md-12">
        <div class="card ">
          <div class="card-header">
            <h4 class="card-title">Entrada de encomenda | compartilhado</h4>

          </div>
          <div class="card-body">
            <form action="<?php echo base_url(); ?>shared/register_shared" method="post">
              <div class="row">
                <div class="col-md-12 d-flex justify-content-start">
                  <div class="" style="border-right: 2px solid #ccc; margin-right: 12px; padding-right: 12px;">
                    <h6>Cliente</h6>

                    Escolher:
                    <button type="button" class="chama_modal icon_acao" name="button" data-id="modalClients" id=""><i
                        class="fa fa-1x fa-book " aria-hidden="true"></i></button>
                    <div id="dados_cliente"></div>
                    <input type="hidden" name="id_client" id="id_client" value="">
                    <input type="hidden" name="contract_client" id="contract_client" value="0">
                  </div>

                  <div class=""
                    style="border-right: 2px solid #ccc; padding-right: 12px; margin-right: 12px;">
                    <h6>Cidade</h6>
                    <div class="form-group mt-0 form-check-inline">
                      <select name="package_city" id="package_city">
                        <option></option>
                        <?php foreach ($cities_payment as $row): ?>
                          <option value="<?php echo $row->id; ?>"><?php echo $row->description; ?></option>
                        <?php endforeach; ?>
                      </select>
                    </div>
                  </div>

                  <div class="" style="border-right: 2px solid #ccc; padding-right: 12px; margin-right: 12px;">
                    <h6>Recebedor</h6>
                    <div class="form-group mt-0 form-check-inline">
                      <input class="form-check-input locale " type="radio" name="receiver" id="own_hands" value="maos proprias">
                      <label class="form-check-label" for="own_hands">Mãos próprias</label>
                    </div>
                    <div class="form-group mt-0 form-check-inline" >
                      <input class="form-check-input locale" type="radio" name="receiver" id="anyone" value="qualquer um">
                      <label class="form-check-label" for="anyone">Qualquer um</label>
                    </div>
                    <div class="form-group mt-0 form-check-inline" >
                      <input class="form-check-input locale" type="radio" name="receiver" id="people_authorized" value="pessoas autorizadas">
                      <label class="form-check-label" for="people_authorized">Pessoas autorizadas</label>
                    </div>
                  </div>

                  <div class="" id="authorized"
                    style="display: none; padding-right: 12px; margin-right: 12px;">
                    <h6>Pessoa autorizada:</h6>
                    <div class="form-group mt-0 form-check-inline">
                      <input type="text" name="people_authorized" class="" value="">
                    </div>
                  </div>

                </div>
              </div>
              <br>
              <div class="row">
                <div class="col-md-12 d-flex justify-content-start">
                  <div class="col-md-4" style="border-right: 2px solid #ccc; padding-right: 12px; margin-right: 12px;">
                    <h6>Destinatario</h6>
                    <div class=" form-group mt-0 form-check-inline">
                      <input class="form-control" type="text" name="destinatary" id="destinatary" value="">
                    </div>
                    <div class="form-group mt-0 form-check-inline">
                      <button type="button" name="store" id="store" class="btn btn-sm btn-primary">Loja</button>
                    </div>
                  </div>

                  <div class="col-md-4" style="border-right: 2px solid #ccc; padding-right: 12px; margin-right: 12px;">
                    <h6>Pagador</h6>
                    <div class="form-group">
                      <select class="form-control" name="payment">
                        <option value="">Selecione o pagador...</option>
                        <option value="Remetente">Remetente</option>
                        <option value="Destinatario">Destinatario</option>
                        <option value="Contrato">Contrato</option>
                      </select>
                    </div>
                  </div>

                  <div class="col-md-4" style="border-right: 2px solid #ccc; padding-right: 12px; margin-right: 12px;">
                    <h6>Valor previsto</h6>
                    <div class="form-group mt-0 form-check-inline">
                      <input class="form-check-input valor" type="text" name="price" id="price" value=""
                        style="height: 40px; width: 100px; border: 0;">
                    </div>
                  </div>

                </div>
              </div>
              <br><br>
              <div class="row">
                <div class="col-md-6">
                  <h6>Origem</h6>
                </div>
                <div class="col-md-6">
                  <h6>Destino</h6>
                </div>
              </div>
              <div class="row">
                <div class="col-md-6" style="border-right: 2px solid #ccc;">
                  <div class="row">
                    <div class="col-md-3 pr-md-1">
                      <div class="form-group">
                        <label for="cep_origin">Cep</label>
                        <input type="text" name="cep_origin" id="cep_origin" class="form-control"
                          value="<?php echo set_value('cep_origin'); ?>" onblur="search_cep_origin(this.value);">
                        <?php echo form_error('cep_origin'); ?>
                      </div>
                    </div>
                    <div class="col-md-7 pr-md-1 pl-md-1">
                      <div class="form-group">
                        <label for="address_origin">Rua</label>
                        <input type="text" name="address_origin" id="address_origin" class="form-control" value="">
                        <?php echo form_error('address_origin'); ?>
                      </div>
                    </div>
                    <div class="col-md pl-md-1">
                      <div class="form-group">
                        <label for="number">Numero</label>
                        <input type="text" name="number" id="number" class="form-control"
                          value="<?php echo set_value('number'); ?>">
                        <?php echo form_error('number'); ?>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-5 pr-md-1">
                      <div class="form-group">
                        <label for="neighborhood_origin">Bairro</label>
                        <input type="text" name="neighborhood_origin" id="neighborhood_origin" class="form-control"
                          value="<?php echo set_value('neighborhood_origin'); ?>">
                        <?php echo form_error('neighborhood_origin'); ?>
                      </div>
                    </div>
                    <div class="col-md-5 pr-md-1 pl-md-1">
                      <div class="form-group">
                        <label for="city_origin">Cidade</label>
                        <input type="text" name="city_origin" id="city_origin" class="form-control"
                          value="<?php echo set_value('city_origin'); ?>">
                        <?php echo form_error('city_origin'); ?>
                      </div>
                    </div>
                    <div class="col-md-2 pl-md-1">
                      <div class="form-group">
                        <label for="uf_origin">UF</label>
                        <input type="text" name="uf_origin" id="uf_origin" class="form-control"
                          value="<?php echo set_value('uf_origin'); ?>">
                        <?php echo form_error('uf_origin'); ?>
                      </div>
                    </div>
                    <hr>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="row">
                    <div class="col-md-3 pr-md-1">
                      <div class="form-group">
                        <label for="cep_destination">Cep</label>
                        <input type="text" name="cep_destination" id="cep_destination" class="form-control"
                          value="<?php echo set_value('cep_destination'); ?>"
                          onblur="search_cep_destination(this.value);">
                        <?php echo form_error('cep_destination'); ?>
                      </div>
                    </div>
                    <div class="col-md-7 pr-md-1 pl-md-1">
                      <div class="form-group">
                        <label for="address_destination">Rua</label>
                        <input type="text" name="address_destination" id="address_destination" class="form-control"
                          value="<?php echo set_value('address_destination'); ?>">
                        <?php echo form_error('address_destination'); ?>
                      </div>
                    </div>
                    <div class="col-md pl-md-1">
                      <div class="form-group">
                        <label for="number_destination">Numero</label>
                        <input type="text" name="number_destination" id="number_destination" class="form-control"
                          value="<?php echo set_value('number_destination'); ?>">
                        <?php echo form_error('number_destination'); ?>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-5 pr-md-1">
                      <div class="form-group">
                        <label for="neighborhood_destination">Bairro</label>
                        <input type="text" name="neighborhood_destination" id="neighborhood_destination"
                          class="form-control" value="<?php echo set_value('neighborhood_destination'); ?>">
                        <?php echo form_error('neighborhood_destination'); ?>
                      </div>
                    </div>
                    <div class="col-md-5 pr-md-1 pl-md-1">
                      <div class="form-group">
                        <label for="city_destination">Cidade</label>
                        <input type="text" name="city_destination" id="city_destination" class="form-control"
                          value="<?php echo set_value('city_destination'); ?>">
                        <?php echo form_error('city_destination'); ?>
                      </div>
                    </div>
                    <div class="col-md-2 pl-md-1">
                      <div class="form-group">
                        <label for="uf_destination">UF</label>
                        <input type="text" name="uf_destination" id="uf_destination" class="form-control"
                          value="<?php echo set_value('uf_destination'); ?>">
                        <?php echo form_error('uf_destination'); ?>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <br><br>
              <div class="row">
                <div class="col-md-12">
                  <h6>Observaçoes</h6>
                  <div class="form-group">
                    <textarea name="observation" id="observation" class="form-control"></textarea>
                  </div>
                </div>
              </div>

              <div class="row">
                <div class="col-md-12 text-right">
                  <button type="submit" class="btn btn-primary" name="register_shared"
                    id="register_trips">Cadastrar</button>
                </div>
              </div>
            </form>

          </div>
          <div class="card-footer ">

          </div>
        </div>
      </div>
    </div>
  </div>
  <?php $this->load->view('elements/footer');?>
