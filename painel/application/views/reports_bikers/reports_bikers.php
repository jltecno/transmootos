<?php $this->load->view('elements/header');?>
<?php $this->load->view('elements/sidebar_lateral');?>
<?php $this->load->view('elements/sidebar'); ?>
<div class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="card ">
        <div class="card-header">
          <h3 class="card-title">Relatórios de pilotos</h3>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      <div class="card ">
        <div class="card-header">
          <h3 class="card-title">Viagens noturnas</h3>
        </div>
        <div class="card-body ">
          <div class="row mb-2">
            <div class="col-md-4">
              <label for="first_date"><h6>Data de início:</h6></label>
              <input type="text" class="data form-control" placeholder="00/00/0000" name="" id="first_date" value="" class="form-control">
            </div>
            <div class="col-md-4">
              <label for="last_date"><h6>Data de fim:</h6></label>
              <input type="text" class="data form-control" placeholder="00/00/0000" name="" id="last_date" value="" class="form-control">
            </div>
            <div class="col-md-4 mt-3">
              <button type="button" name="button" id="search_date_client_night_trip" class="btn btn-primary">
                <i class="fa fa-1x fa-search"></i>
              </button>
            </div>
          </div>
          <div class="row">
            <div class="col-md">
              <div class="reports_night_trips_km">
                <table id="reports_night_km" class="table table-striped table-bordered" style="width:100%;border:none;">
                  <thead class="text-primary">
                    <tr>
                      <th>Piloto</th>
                      <th>Quantidade</th>
                      <th>KM Percorrido</th>
                      <th>Comissão</th>
                    </tr>
                  </thead>
                  <tbody id="tbody_reports_night_km">
                    <?php foreach ($night_trips_km as $row): ?>
                      <tr>
                        <td><?php echo $row->name.' '.$row->surname; ?></td>
                        <td><?php echo $row->quantity_night_trips; ?></td>
                        <td><?php echo $row->km; ?></td>
                        <td><?php echo $row->commission; ?></td>
                      </tr>
                    <?php endforeach; ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
        <div class="card-footer "></div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      <div class="card ">
        <div class="card-header">
          <h3 class="card-title">Viagens avulsas</h3>
        </div>
        <div class="card-body ">
          <div class="row mb-2">
            <div class="col-md-4">
              <label for="first_date_separate"><h6>Data de início:</h6></label>
              <input type="text" class="data form-control" placeholder="00/00/0000" name="" id="first_date_separate" value="" class="form-control">
            </div>
            <div class="col-md-4">
              <label for="last_date_separate"><h6>Data de fim:</h6></label>
              <input type="text" class="data form-control" placeholder="00/00/0000" name="" id="last_date_separate" value="" class="form-control">
            </div>
            <div class="col-md-4 mt-3">
              <button type="button" name="button" id="search_date_client_night_trip" class="btn btn-primary">
                <i class="fa fa-1x fa-search"></i>
              </button>
            </div>
          </div>
          <div class="row">
            <div class="col-md">
              <div class="reports_trips_separate">
                <table id="reports_trips_separate" class="table table-striped table-bordered" style="width:100%;border:none;">
                  <thead class="text-primary">
                    <tr>
                      <th>Piloto</th>
                      <th>Quantidade</th>
                      <th>KM Percorrido</th>
                    </tr>
                  </thead>
                  <tbody id="tbody_reports_trips_separate">
                    <?php foreach ($trips_separate as $row): ?>
                      <tr>
                        <td><?php echo $row->name.' '.$row->surname; ?></td>
                        <td><?php echo $row->quantity; ?></td>
                        <td><?php echo $row->km; ?></td>
                      </tr>
                    <?php endforeach; ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
        <div class="card-footer "></div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      <div class="card ">
        <div class="card-header">
          <h3 class="card-title">Viagens compartilhadas</h3>
        </div>
        <div class="card-body ">
          <div class="row mb-2">
            <div class="col-md-4">
              <label for="first_date_shared"><h6>Data de início:</h6></label>
              <input type="text" class="data form-control" placeholder="00/00/0000" name="" id="first_date_shared" value="" class="form-control">
            </div>
            <div class="col-md-4">
              <label for="last_date_shared"><h6>Data de fim:</h6></label>
              <input type="text" class="data form-control" placeholder="00/00/0000" name="" id="last_date_shared" value="" class="form-control">
            </div>
            <div class="col-md-4 mt-3">
              <button type="button" name="button" id="search_date_biker_shared" class="btn btn-primary">
                <i class="fa fa-1x fa-search"></i>
              </button>
            </div>
          </div>
          <div class="row">
            <div class="col-md">
              <div class="reports_trips_shared">
                <table id="reports_trips_shared" class="table table-striped table-bordered" style="width:100%;border:none;">
                  <thead class="text-primary">
                    <tr>
                      <th>Piloto</th>
                      <th>Quantidade</th>
                      <th>KM Percorrido</th>
                    </tr>
                  </thead>
                  <tbody id="tbody_reports_shared">
                    <?php foreach ($trips_shared as $row): ?>
                      <tr>
                        <td><?php echo $row->name.' '.$row->surname; ?></td>
                        <td><?php echo $row->quantity; ?></td>
                        <td><?php echo $row->km; ?></td>
                      </tr>
                    <?php endforeach; ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
        <div class="card-footer "></div>
      </div>
    </div>
  </div>
</div>
<?php $this->load->view('elements/footer');?>
