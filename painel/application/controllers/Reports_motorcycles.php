<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reports_motorcycles extends MY_Controller {

  public function __construct(){
		parent::__construct();

    $this->load->model('trips_model');
    $this->load->model('clients_model');
    $this->load->model('traffic_ticket_model');
    $this->load->model('bikers_model');
    $this->load->model('reports_clients_model');
    $this->load->model('reports_bikers_model');
    $this->load->model('reports_motorcycles_model');
	}

	public function index()
	{
    $data['night_trips_motocycles'] = $this->reports_motorcycles_model->fetch_night_trips_motorcycles();
    $data['night_trips_km'] = $this->reports_motorcycles_model->fetch_night_trips_km();
    $data['trips_shared'] = $this->reports_motorcycles_model->fetch_trips_shared();

		$this->load->view('reports_motorcycles/reports_motorcycles', $data);
	}

  public function fetch_night_trips_km_date(){
    $posts = $this->security->xss_clean($this->input->post());

    $return = $this->reports_motorcycles_model->fetch_night_trips_km_date($posts);

    echo json_encode($return);
  }

  public function fetch_trips_shared_date(){
    $posts = $this->security->xss_clean($this->input->post());

    $return = $this->reports_motorcycles_model->fetch_trips_shared_date($posts);

    echo json_encode($return);
  }

}
