<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Bikers extends MY_Controller {

  public function __construct(){
		parent::__construct();

    $this->load->model('bikers_model');
    $this->load->model('trips_model');
    $this->load->model('night_trips_model');
	}

	public function index()
	{
    $data['bikers'] = $this->bikers_model->fetch_bikers();

		$this->load->view('bikers/bikers', $data);
	}

  public function register()
	{
    $data['type_blood'] = $this->bikers_model->fetch_type_blood();

		$this->load->view('bikers/register', $data);
	}

  public function register_bikers(){
    if (isset($_POST['register_bikers'])) {
      $posts = $this->security->xss_clean($this->input->post());

      if ($_FILES["profile_bikers"]['name'] == "") {
        $image = 'perfil.png';
      }else{
        $config['upload_path'] = 'assets/build/img/profile_bikers';
        $config['allowed_types'] = 'jpg|JPEG|png|PNG';
        $config['encrypt_name'] = true;

        $this->load->library('upload', $config);

        if ($this->upload->do_upload('profile_bikers')){
          $info_file = $this->upload->data();
          $image = $info_file['file_name'];
        }
        else{
          $image = 'perfil.png';
          echo $this->upload->display_errors();
          echo "Imagem não enviada";
        }
      }


      $id_biker = $this->bikers_model->insert($posts, $image);

      redirect(base_url().'bikers/detail/'.$id_biker);
    }
  }

  public function detail($id)
	{
    $data['biker'] = $this->bikers_model->fetch_biker($id);
    $data['trips'] = $this->trips_model->fetch_trips_biker($id);
    $data['documents'] = $this->bikers_model->fetch_documents($id);
    $data['type_documents'] = $this->bikers_model->fetch_type_documents($id);
    $data['type_blood'] = $this->bikers_model->fetch_type_blood();
    $data['night_trips_biker'] = $this->night_trips_model->fetch_night_trips_biker($id);
    $data['id'] = $id;
    // echo $id;

    // var_dump($data['trips']); die;

		$this->load->view('bikers/detail', $data);
	}

  public function add_document($id){
    if (isset($_POST['register_document'])) {
      $posts = $this->security->xss_clean($this->input->post());

      $config['upload_path'] = 'assets/build/img/document_biker';
      $config['allowed_types'] = 'jpg|JPEG|png|PNG';
      $config['encrypt_name'] = true;

      $this->load->library('upload', $config);

      if ($this->upload->do_upload('document')){
        $info_file = $this->upload->data();
        $image = $info_file['file_name'];
      }
      else{
        echo $this->upload->display_errors();
        echo "Imagem não enviada";
      }

      $this->bikers_model->insert_document($id, $image, $posts);

    }
    redirect(base_url().'bikers/detail/'.$id);
  }

  public function modify_biker($id){
    if (isset($_POST['modify_biker'])) {
      $posts = $this->security->xss_clean($this->input->post());
      $this->bikers_model->modify_biker($posts);
    }
    redirect(base_url().'bikers/detail/'.$id);
  }

  public function modify_image($id){
    if (isset($_POST['modify_image'])) {
      $posts = $this->security->xss_clean($this->input->post());

      $config['upload_path'] = 'assets/build/img/profile_bikers';
      $config['allowed_types'] = 'jpg|JPEG|png|PNG';
      $config['encrypt_name'] = true;

      $this->load->library('upload', $config);

      if ($this->upload->do_upload('image_biker')){
        $info_file = $this->upload->data();
        $image = $info_file['file_name'];
      }
      else{
        echo $this->upload->display_errors();
        echo "Contrato nao enviado";
      }

      $this->bikers_model->update_image($posts, $image);

      redirect(base_url().'bikers/detail/'.$id);
    }
  }

  public function fetch_night_trips_date(){
    $posts = $this->security->xss_clean($this->input->post());

    $return = $this->night_trips_model->fetch_night_trips_date($posts);

    echo json_encode($return);
  }

}
