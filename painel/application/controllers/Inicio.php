<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Inicio extends CI_Controller {

  public function __construct(){
		parent::__construct();

    $this->load->model('motorcycles_model');
    $this->load->model('bikers_model');
    $this->load->model('trips_model');
    $this->load->model('payment_rules_model');
	}

	public function index()
	{
    $data['bikers'] = $this->bikers_model->fetch_bikers();
    $data['motorcycles'] = $this->motorcycles_model->fetch_motorcycles();
    $data['cities_payment'] = $this->payment_rules_model->fetch_cities_payment();

		$this->load->view('inicio', $data);
	}

}
