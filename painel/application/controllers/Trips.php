<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Trips extends CI_Controller {

  public function __construct(){
		parent::__construct();

    $this->load->model('motorcycles_model');
    $this->load->model('bikers_model');
    $this->load->model('trips_model');
    $this->load->model('address_model');
    $this->load->model('packages_model');
	}

	public function index()
	{
    redirect(base_url(). 'inicio');
	}

  public function register_trips(){
    if (isset($_POST['register_trips'])) {
      $posts = $this->security->xss_clean($this->input->post());
      // var_dump($posts);
      // echo $posts['leave'];
      // die;

      if ($posts['leave'] == 'P') {
        $id_departure = $this->address_model->insert_departure_adress($posts);
        $id_arrival = $this->address_model->insert_arrival_adress($posts);
        $this->packages_model->insert_package($posts, $id_departure, $id_arrival);
      }else{
        $id_departure = $this->address_model->insert_departure_adress($posts);
        $id_arrival = $this->address_model->insert_arrival_adress($posts);
        $id_trip = $this->trips_model->insert($posts, $id_departure, $id_arrival);
        // if ($posts['service_type'] == 'Material') {
        //   $this->packages_model->insert_package($posts, $id_departure, $id_arrival, $id_trip);
        // }
      }

    }
    redirect(base_url(). 'inicio');
  }

  public function fetch_detail_trip(){
    if (isset($_POST['id_trip'])) {
      $posts = $this->security->xss_clean($this->input->post());

      $trip = $this->trips_model->fetch_detail_trip($posts);
      $departure_address = $this->address_model->fetch_departure_adress($trip['departure_adress']);
      $arrival_address = $this->address_model->fetch_arrival_adress($trip['arrival_adress']);

      $return = array_merge( $trip, $departure_address, $arrival_address);
      echo json_encode($return);
    }
  }

  public function start_trip(){
    $posts = $this->security->xss_clean($this->input->post());
    if (isset($_POST['start_trip'])) {
      $trip = $this->trips_model->start_trip($posts);
    }
    if (isset($_POST['cancel_trip'])) {
      $this->trips_model->cancel_trip($posts);
    }
    redirect(base_url().'dashboard');
  }

  public function stop_trip(){
    $posts = $this->security->xss_clean($this->input->post());
    if (isset($_POST['stop_trip'])) {
      $trip = $this->trips_model->stop_trip($posts);
    }
    if (isset($_POST['cancel_trip'])) {
      $this->trips_model->cancel_trip($posts);
    }
    redirect(base_url().'dashboard');
  }

  public function packages_waiting(){
    $packages_waiting = $this->packages_model->fetch_city_waiting($this->input->post('id'));
    echo json_encode($packages_waiting);
  }

  public function stop_trip_packages(){
    if (isset($_POST['stop_trip_packages'])) {
      $posts = $this->security->xss_clean($this->input->post());
      $trip = $this->trips_model->stop_trip($posts);

      foreach ($posts['package'] as $key => $value) {
        $this->db->set('status', 3);
        $this->db->where('id', $value);
        $this->db->update('packages');
      }

      $this->db->set('status', 1);
      $this->db->where('id_trip', $posts['id_trip']);
      $this->db->where('status != ', 3);
      $this->db->update('packages');
      // $trip = $this->packages_model->delivered_package($posts);

      redirect(base_url().'dashboard');
    }
  }

  public function update_trips_closed(){
    if (isset($_POST['update_trips_closed'])) {
      $posts = $this->security->xss_clean($this->input->post());

      $this->trips_model->update_trips_closed($posts);
    }
    redirect(base_url().'dashboard');
  }

}
