<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reports_clients extends MY_Controller {

  public function __construct(){
		parent::__construct();

    $this->load->model('trips_model');
    $this->load->model('clients_model');
    $this->load->model('traffic_ticket_model');
    $this->load->model('bikers_model');
    $this->load->model('reports_clients_model');
	}

	public function index()
	{
    $data['count_clients'] = $this->clients_model->count_clients();
    $data['night_trips_client'] = $this->reports_clients_model->fetch_night_trips_client();

		$this->load->view('reports_clients/reports_clients', $data);
	}

  public function fetch_night_trips_client(){
    $posts = $this->security->xss_clean($this->input->post());

    $return = $this->reports_clients_model->fetch_night_trips_client_date($posts);

    echo json_encode($return);
  }

}
