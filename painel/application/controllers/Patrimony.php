<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Patrimony extends MY_Controller {
  public function __construct(){
		parent::__construct();

    $this->load->model('gasoline_control_model');
    $this->load->model('maintenance_control_model');
    $this->load->model('traffic_ticket_model');
    $this->load->model('trips_model');
    $this->load->model('motorcycles_model');
    $this->load->model('patrimony_model');
	}

	public function index()
	{
    $data['patrimony'] = $this->patrimony_model->fetch_patrimony();

		$this->load->view('patrimony/patrimony', $data);
	}

  public function register()
	{
		$this->load->view('patrimony/register');
	}

  public function register_patrimony(){
    if (isset($_POST['register_patrimony'])) {
      $posts = $this->security->xss_clean($this->input->post());

      $this->patrimony_model->insert($posts);

      redirect(base_url().'patrimony/register');
    }
  }


}
