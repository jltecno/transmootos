<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Financial extends MY_Controller {

  public function __construct(){
		parent::__construct();

    $this->load->model('trips_model');
    $this->load->model('gasoline_control_model');
    $this->load->model('financial_model');
	}

	public function index()
	{
    $data['date'] = date('Y-m-d');

    $data['invoice_day'] = $this->financial_model->fetch_invoice_trips($data['date']);
    $data['count_trips'] = $this->financial_model->fetch_count_trips($data['date']);
    $data['expenses_day'] = $this->financial_model->fetch_expenses_day($data['date']);
    $data['trips_day'] = $this->trips_model->fetch_trips_day();
    $data['gasoline_day'] = $this->gasoline_control_model->fetch_gasoline_day();

		$this->load->view('financial/financial', $data);
	}

  public function chama_relatorio(){
		$dados = $this->security->xss_clean($this->input->post('dados'));
    $data = date('Y-m-d');

		if ($dados[1] == "proximo") {
			$data = date('Y-m-d', strtotime('+1 days', strtotime($dados[0])));
		}

		if ($dados[1] == "anterior") {
			$data = date('Y-m-d', strtotime('-1 days', strtotime($dados[0])));
		}

		$invoice_trips = $this->financial_model->fetch_invoice_trips($data);
    $count_trips = $this->financial_model->fetch_count_trips($data);
    $expenses_day = $this->financial_model->fetch_expenses_day($data);

		$dados = [
			'dia' => $data,
			'invoice' => $invoice_trips->expected_value,
      'count_trips' => $count_trips['quantity_trips'],
      'expenses_day' => $expenses_day->amount,

		];
		echo json_encode($dados);
	}
}
