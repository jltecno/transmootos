<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends MY_Controller {
  public $user;

  public function __construct(){
		parent::__construct();

    $this->load->model("login_model", "login");
		$this->load->model("users_model");
		$logado = $this->login->checkUser();
		if (!$logado || $_SESSION['user']['profile'] != 1) {
			redirect('/');
			exit();
		}

		$this->user = $this->login->user();
	}

	public function index()
	{
    $data['users'] = $this->users_model->fetch_users();

		$this->load->view('users/users', $data);
	}

  public function register()
	{
    if (isset($_POST['register_user'])) {
      // echo "entrou"; die;

      // $this->form_validation->set_rules('name', 'name', 'trim|required|min_length[2]');
      // $this->form_validation->set_rules('profile', 'profile', 'trim|required');
      // $this->form_validation->set_rules('email', 'email', 'trim|required|valid_email');
      // $this->form_validation->set_rules('password', 'password', 'trim|required|min_length[8]');
      // $this->form_validation->set_error_delimiters("<p class='text-danger'>", "</p>");
      //
      // $sucess = $this->form_validation->run();
      // echo $sucess;
      // echo "die"; die;

      // if ($sucess) {
        if ($_FILES["profile_user"]['name'] == "") {
          $image = 'perfil.png';
          // echo "die 1"; die;

        }else{
          // echo "die 2"; die;

          $config['upload_path'] = 'assets/build/img/profile_user';
          $config['allowed_types'] = 'jpg|JPEG|png|PNG';
          $config['encrypt_name'] = true;

          $this->load->library('upload', $config);

          if ($this->upload->do_upload('profile_user')){
            $info_file = $this->upload->data();
            $image = $info_file['file_name'];
          }
          else{
            $image = 'perfil.png';
            echo $this->upload->display_errors();
            echo "Imagem não enviada";
          }
        }
        $data = array("name" => $this->input->post('name'), 'profile' => $this->input->post('profile'), 'email' => $this->input->post('email'), 'password' => $this->input->post('password'),
         'img_profile' => $image, 'commission' => $this->input->post('commission'));
  			$this->login->registerUser($data);
        // echo "die"; die;
        redirect(base_url().'users');
      // }
    }
		$this->load->view('users/register');
	}

  public function detail($id){
    $data['user'] = $this->users_model->fetch_user($id);

    $this->load->view('users/detail', $data);
  }

  public function update_user(){
    $rows = array();

    $rows['name'] = $this->security->xss_clean($this->input->post('name'));
    $rows['email'] = $this->security->xss_clean($this->input->post('email'));
    $rows['profile'] = $this->security->xss_clean($this->input->post('profile'));
    $rows['note'] = $this->security->xss_clean($this->input->post('note'));
    $rows['status'] = $this->security->xss_clean($this->input->post('status'));
    $rows['commission'] = $this->security->xss_clean($this->input->post('commission'));
    $rows['id_user'] = $this->security->xss_clean($this->input->post('id_user'));

    $this->users_model->update_user($rows);

    redirect(base_url().'users/detail/'.$this->input->post('id_user'));
  }

  public function update_image($id){
    if (isset($_POST['modify_image'])) {
      $posts = $this->security->xss_clean($this->input->post());
      if ($_FILES["profile_user"]['name'] == "") {
        $image = 'perfil.png';
      }else{
        $config['upload_path'] = 'assets/build/img/profile_user';
        $config['allowed_types'] = 'jpg|JPEG|png|PNG';
        $config['encrypt_name'] = true;

        $this->load->library('upload', $config);

        if ($this->upload->do_upload('profile_user')){
          $info_file = $this->upload->data();
          $image = $info_file['file_name'];
        }
        else{
          $image = 'perfil.png';
          echo $this->upload->display_errors();
          echo "Imagem não enviada";
        }
      }
      $this->users_model->update_image($id, $image);
      redirect(base_url().'users/detail/'.$id);
    } else {
      redirect(base_url().'users/register');
    }
  }

}
