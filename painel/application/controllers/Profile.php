<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profile extends MY_Controller {
  public function __construct(){
		parent::__construct();

    $this->load->model('gasoline_control_model');
    $this->load->model('maintenance_control_model');
    $this->load->model('traffic_ticket_model');
    $this->load->model('trips_model');
    $this->load->model('motorcycles_model');
    $this->load->model('patrimony_model');
    $this->load->model('users_model');
	}

	public function index()
	{

		$this->load->view('profile/profile');
	}

  public function update_user_password(){
    if (isset($_POST['update_user_password'])) {
      $posts = $this->security->xss_clean($this->input->post());
      $return = $this->users_model->fetch_user_password($posts);
      if (!empty($return)) {
        $this->users_model->update_user_password($posts);
      } else if(empty($return)){
        $this->session->set_flashdata('error', 'Senha atual incorreta');
      }
    }
    redirect(base_url().'profile');
  }

}
