<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reports extends MY_Controller {

  public function __construct(){
		parent::__construct();

    $this->load->model('trips_model');
    $this->load->model('clients_model');
    $this->load->model('traffic_ticket_model');
    $this->load->model('bikers_model');
	}

	public function index()
	{
    $data['count_trips'] = $this->trips_model->count_trips();
    $data['count_clients'] = $this->clients_model->count_clients();
    $data['clients_per_month'] = $this->clients_model->clients_per_month();
    $data['count_traffic_ticket'] = $this->traffic_ticket_model->count_traffic_ticket();
    $data['trips_per_month'] = $this->trips_model->fetch_trips_per_month();
    $data['license_expiration'] = $this->bikers_model->fetch_license_expiration();
    // var_dump($data['trips_reports']); die;
		$this->load->view('reports/reports', $data);
	}
}
