<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Bills_pay extends MY_Controller {

  public function __construct(){
		parent::__construct();

    $this->load->model('trips_model');
    $this->load->model('address_model');
    $this->load->model('bills_model');
    $this->load->model('provider_model');
	}

	public function index()
	{
    $data['total_price'] = $this->bills_model->fetch_total_price();
    $data['bill_month'] = $this->bills_model->fetch_bill_month();
    $data['bill_week'] = $this->bills_model->fetch_bill_week();
    $data['bill_day'] = $this->bills_model->fetch_bill_day();
    $data['fixed_accounts'] = $this->bills_model->fetch_fixed_accounts();
    $data['variable_accounts'] = $this->bills_model->fetch_variable_accounts();
    $data['bills_payments'] = $this->bills_model->fetch_bills_payments();

    $this->load->view('bills_pay/bills_pay', $data);
	}

  public function register(){
    $data['providers'] = $this->provider_model->fetch_providers();

    $this->load->view('bills_pay/register_bill', $data);
  }

  public function register_bill(){
    if (isset($_POST['register_bill'])) {
      $posts = $this->security->xss_clean($this->input->post());

      $this->bills_model->insert($posts);
    }
    redirect(base_url().'bills_pay/register');
  }

  public function register_payment(){
    if (isset($_POST['payment_bill'])) {
      $posts = $this->security->xss_clean($this->input->post());

      $this->bills_model->insert_payment($posts);
      $this->bills_model->update_bill($posts);
    }
    redirect(base_url().'bills_pay');
  }
}
