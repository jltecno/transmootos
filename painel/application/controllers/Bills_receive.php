<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Bills_receive extends MY_Controller {

  public function __construct(){
		parent::__construct();

    $this->load->model('trips_model');
    $this->load->model('bills_receive_model');
	}

	public function index()
	{
    $posts = $this->security->xss_clean($this->input->post());
    $amount_receive = $this->bills_receive_model->fetch_amount_receive();

    $data['amount'] = 0;

    foreach ($amount_receive as $row) {
      $data['amount'] = $data['amount'] + $row->total;
    }

    $data['receives'] = $this->bills_receive_model->fetch_bill_receives();

    // var_dump($receives); die;
    // var_dump($data['amount']); die;
    $data['bills_receives'] = $this->bills_receive_model->fetch_bills_receives_per_client($posts);

    $this->load->view('bills_receive/bills_receive', $data);
	}

  public function fetch_trips_receives(){
    $posts = $this->security->xss_clean($this->input->post());

    $data = $this->bills_receive_model->fetch_trips_receives($posts);

    echo json_encode($data);
  }

  public function update_pay_night_trips(){
    if (isset($_POST['pay_bills_receive'])) {
      $posts = $this->security->xss_clean($this->input->post());

      $amount = 0;
      foreach ($posts['night_trip'] as $key => $value) {
        $price = $this->bills_receive_model->fetch_amount($value);
        $amount = $amount + $price->total;
      }

      $id_payment = $this->bills_receive_model->insert_bills_received($posts, $amount);

      foreach ($posts['night_trip'] as $key => $value) {
        $this->bills_receive_model->insert_itens_bills_received($id_payment, $value);
      }
      $this->bills_receive_model->update_pay_night_trips($posts);
    }
    redirect(base_url().'bills_receive');
  }

  public function fetch_paid_bill(){
    $posts = $this->security->xss_clean($this->input->post());

    $return = $this->bills_receive_model->fetch_paid_bill($posts);

    echo json_encode($return);
  }

}
