<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Receipts extends MY_Controller {

  public function __construct(){
		parent::__construct();

    $this->load->model('trips_model');
    $this->load->model('address_model');
	}

	public function index()
	{
		redirect(base_url().'inicio');
	}

  public function receipt($id){
    $posts = ['id_trip' => $id];
    $trip = $this->trips_model->fetch_detail_trip($posts);
    $departure_address = $this->address_model->fetch_departure_adress($trip['departure_adress']);
    $arrival_address = $this->address_model->fetch_arrival_adress($trip['arrival_adress']);

    $data['trip'] = $trip;
    $data['departure_address'] = $departure_address;
    $data['arrival_address'] = $arrival_address;

    $this->load->view('receipts/receipt', $data);
  }
}
