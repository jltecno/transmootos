<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Provider extends MY_Controller {

  public function __construct(){
		parent::__construct();

    $this->load->model('trips_model');
    $this->load->model('bills_receive_model');
    $this->load->model('bills_receive_model');
    $this->load->model('provider_model');
	}

	public function index()
	{
    $data['providers'] = $this->provider_model->fetch_providers();

    $this->load->view('provider/provider', $data);
  }

  public function register(){

    $this->load->view('provider/register');
  }

  public function register_provider(){
    if (isset($_POST['register_provider'])) {
      $posts = $this->security->xss_clean($this->input->post());
      $id_provider = $this->provider_model->insert($posts);

      if ($posts['type_provider'] == 1) {
        $this->provider_model->insert_provider_legal($posts, $id_provider);

      } else if($posts['type_provider'] == 2){
        $this->provider_model->insert_provider_juridical($posts, $id_provider);

      }

      $this->provider_model->insert_address($posts, $id_provider);
    }
    redirect(base_url().'provider/provider');
  }

  public function detail($id){
    $data['provider'] = $this->provider_model->fetch_provider($id);
    $data['bank_data'] = $this->provider_model->fetch_banks_data($id);

    $this->load->view('provider/detail', $data);
  }

  public function update_provider($id){
    if (isset($_POST['update_legal_provider']) || isset($_POST['update_juridical_provider'])) {
      $posts = $this->security->xss_clean($this->input->post());
      if (isset($_POST['update_legal_provider'])) {
        $this->provider_model->update_legal_provider($posts, $id);
      }
      if (isset($_POST['update_juridical_provider'])) {
        $this->provider_model->update_juridical_provider($posts, $id);
      }
      $this->provider_model->update_provider_address($posts, $id);
    }

    redirect(base_url().'provider/detail/'.$id);
  }

  public function register_bank_data()
  {
    if (isset($_POST['register_bank_data'])) {
      $posts = $this->security->xss_clean($this->input->post());

      $this->provider_model->insert_bank_data($posts);

      redirect(base_url().'provider/detail/'.$posts['id_provider']);
    } else {
      redirect(base_url().'provider/register');
    }
  }
  public function update_bank_data($id)
  {
    if (isset($_POST['update_bank_data'])) {
      $posts = $this->security->xss_clean($this->input->post());

      $this->provider_model->update_bank_data($posts);
      redirect(base_url().'provider/detail/'.$id);
    } else {
      redirect(base_url().'provider/register');
    }
  }

}
