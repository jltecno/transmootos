<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Modais extends MY_Controller {

  public function __construct(){
		parent::__construct();
    $this->load->model('clients_model');
    $this->load->model('bikers_model');
    $this->load->model('motorcycles_model');
    $this->load->model('payment_rules_model');

	}

	public function index()
	{
		redirect(base_url());
	}

  public function modalControleManutencao(){
    $this->load->view('modais/modalControleManutencao');
  }

	public function modalControleAbastecimento(){
    $this->load->view('modais/modalControleAbastecimento');
  }

  public function modalClients(){
    $data['clients'] = $this->clients_model->fetch_clients();

    $this->load->view('modais/modalClients', $data);
  }

  public function modalDashAndamento(){
    $this->load->view('modais/modalDashAndamento');
  }

  public function modalDashFila(){
    $data['bikers'] = $this->bikers_model->fetch_bikers();
    $data['motorcycles'] = $this->motorcycles_model->fetch_motorcycles();

    $this->load->view('modais/modalDashFila', $data);
  }

  public function modalDashAgendamento(){
    $data['bikers'] = $this->bikers_model->fetch_bikers();
    $data['motorcycles'] = $this->motorcycles_model->fetch_motorcycles();

    $this->load->view('modais/modalDashAgendamento', $data);
  }

  public function modalDashEncerradas(){
    $data['bikers'] = $this->bikers_model->fetch_bikers();
    $data['motorcycles'] = $this->motorcycles_model->fetch_motorcycles();

    $this->load->view('modais/modalDashEncerradas', $data);
  }

  public function modalCadastroMulta(){
    $data['bikers'] = $this->bikers_model->fetch_bikers();

    $this->load->view('modais/modalCadastroMulta', $data);
  }

  public function modalViagensRealizadas(){
    $this->load->view('modais/modalViagensRealizadas');
	}

  public function modalCreateTrip(){
    $data['cities_package'] = $this->payment_rules_model->fetch_cities_payment();
    $data['bikers'] = $this->bikers_model->fetch_bikers();
    $data['motorcycles'] = $this->motorcycles_model->fetch_motorcycles();

    $this->load->view('modais/modalCreateTrip', $data);
	}

  public function modalTripPackages(){

    $this->load->view('modais/modalTripPackages');
  }


}
