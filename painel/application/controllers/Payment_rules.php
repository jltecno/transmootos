<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Payment_rules extends MY_Controller {
  public function __construct(){
		parent::__construct();

    $this->load->model('gasoline_control_model');
    $this->load->model('maintenance_control_model');
    $this->load->model('motorcycles_model');
    $this->load->model('payment_rules_model');
	}

	public function index()
	{
    $data['payment_rules'] = $this->payment_rules_model->fetch_payment_rules();

    $this->load->view('payment_rules/payment_rules', $data);
	}

  public function update_payment_rule()
  {
    if (isset($_POST['modify_payment_rules'])) {
      $posts = $this->security->xss_clean($this->input->post());
      $this->payment_rules_model->update_payment_rule($posts);
      redirect(base_url().'payment_rules');
    }
  }

  public function fetch_price_inside(){
    $price = $this->payment_rules_model->fetch_price_inside($this->input->post('contract'));
    echo json_encode($price);
  }

  public function fetch_price_outside(){
    $price = $this->payment_rules_model->fetch_price_outide($this->input->post('distance'));
    // var_dump($price);
    $amount = ($price->price * $this->input->post('distance'));
    echo $amount;
  }

  public function fetch_price_city(){
    $price = $this->payment_rules_model->fetch_price_city($this->input->post('id'));
    echo $price->price;
  }

}
