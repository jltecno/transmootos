<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Shared_trips extends CI_Controller {

  public function __construct(){
		parent::__construct();

    $this->load->model('motorcycles_model');
    $this->load->model('bikers_model');
    $this->load->model('trips_model');
    $this->load->model('address_model');
    $this->load->model('packages_model');
    $this->load->model('payment_rules_model');
    $this->load->model('shared_trips_model');
	}

  public function register_shared_trips(){
    if (isset($_POST['register_trip_packages'])) {
      $posts = $this->security->xss_clean($this->input->post());

      $trip = $this->shared_trips_model->insert($posts, $id_departure = 0, $id_arrival = 0);
      $this->shared_trips_model->insert_itens_shared_trips($posts, $trip);
      $this->packages_model->start_trip_packages($posts, $trip);
    }
    redirect(base_url().'dashboard');
  }

  public function stop_trip_packages(){
    if (isset($_POST['stop_trip_packages'])) {
      $posts = $this->security->xss_clean($this->input->post());

      $itens = $this->shared_trips_model->fetch_itens_shared_trips($posts);

      $trip = $this->shared_trips_model->stop_shared_trip($posts);

      foreach ($posts['package'] as $key => $value) {
        $this->db->set('status', 3);
        $this->db->where('id', $value);
        $this->db->update('packages');
      }

        $this->db->set('status', 1);
        $this->db->where('id_trip', $posts['id_trip']);
        $this->db->where('status != ', 3);
        $this->db->update('packages');
      // $trip = $this->packages_model->delivered_package($posts);

      $this->shared_trips_model->update_itens_shared_trips($posts);
    }
    redirect(base_url().'dashboard');
  }

  public function fetch_shared_detail_closed(){
    $posts = $this->security->xss_clean($this->input->post());

    $return = $this->shared_trips_model->fetch_shared_detail_closed($posts);

    echo json_encode($return);
  }

  public function fetch_finished_shared(){
    $posts = $this->security->xss_clean($this->input->post());

    $return = $this->shared_trips_model->fetch_shared_trip_modal($posts);

    echo json_encode($return);
  }

  public function update_shared_trips(){
    if (isset($_POST['update_shared_trips'])) {
      $posts = $this->security->xss_clean($this->input->post());

      $this->shared_trips_model->update_shared_trips($posts);
    }
    redirect(base_url().'dashboard');
  }

}
