<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

  public function __construct(){
		parent::__construct();

    $this->load->model('clients_model');
    $this->load->model('trips_model');
    $this->load->model('packages_model');
    $this->load->model('shared_trips_model');
    $this->load->model('bikers_model');
		$this->load->model('motorcycles_model');
	}

	public function index()
	{
    $data['count_trips'] = $this->trips_model->count_trips();
    // var_dump($data['count_trips']); die;
		$data['clients'] = $this->clients_model->fetch_clients();
    $data['progress'] = $this->trips_model->fetch_in_progress();
    $data['progress_shared'] = $this->shared_trips_model->fetch_shared_progress();
    $data['waiting'] = $this->trips_model->fetch_waiting();
    $data['closed'] = $this->trips_model->fetch_closed();
    $data['shared_closed'] = $this->shared_trips_model->fetch_shared_closed();
    $data['schedules'] = $this->trips_model->fetch_schedules();
    $data['packages_waiting'] = $this->packages_model->fetch_waiting();
    $data['cancel_trips'] = $this->trips_model->fetch_cancel_trips();
    $data['bikers'] = $this->bikers_model->fetch_bikers();
    $data['motorcycles'] = $this->motorcycles_model->fetch_motorcycles();

		$this->load->view('dashboard/dashboard', $data);
	}
}
