<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Motorcycles extends MY_Controller {
  public function __construct(){
		parent::__construct();

    $this->load->model('gasoline_control_model');
    $this->load->model('maintenance_control_model');
    $this->load->model('traffic_ticket_model');
    $this->load->model('trips_model');
    $this->load->model('motorcycles_model');
    $this->load->model('bikers_model');
	}

	public function index()
	{
    $data['motorcycles'] = $this->motorcycles_model->fetch_motorcycles();

		$this->load->view('motorcycles/motorcycles', $data);
	}

  public function register()
	{
		$this->load->view('motorcycles/register');
	}

  public function register_motorcycle()
	{
    if (isset($_POST['register_motorcycle'])) {
      $posts = $this->security->xss_clean($this->input->post());

      if ($_FILES["profile_motorcycle"]['name'] == "") {
        $image = 'perfil.png';
      }else{
        $config['upload_path'] = 'assets/build/img/profile_motorcycles';
        $config['allowed_types'] = 'jpg|JPEG|png|PNG|';
        $config['encrypt_name'] = true;

        $this->load->library('upload', $config);

        if ($this->upload->do_upload('profile_motorcycle')){
          $info_file = $this->upload->data();
          $image = $info_file['file_name'];
        }
        else{
          $image = 'perfil.png';
          echo $this->upload->display_errors();
          echo "Imagem não enviada";
        }
      }

      $id = $this->motorcycles_model->insert($posts, $image);

      redirect(base_url().'motorcycles/detail/'.$id);
    }
	}

  public function detail($id)
	{
    $this->session->set_userdata('id_motorcycle', $id);
    $data['motorcycle'] = $this->motorcycles_model->fetch_motorcycle($id);
    $data['maintenance'] = $this->maintenance_control_model->fetch_maintenance_control($id);
    $data['gasoline'] = $this->gasoline_control_model->fetch_gasoline_control($id);
    $data['ticket'] = $this->traffic_ticket_model->fetch_traffic_ticket($id);
    $data['trips'] = $this->trips_model->fetch_trips_motorcycles($id);
    $data['bikers'] = $this->bikers_model->fetch_bikers();
    $data['occurrence'] = $this->motorcycles_model->fetch_ocurrences();

		$this->load->view('motorcycles/detail', $data);
	}

  public function modify_motorcycle($id){
    if (isset($_POST['modify_motorcycle'])) {
      $posts = $this->security->xss_clean($this->input->post());

      $this->motorcycles_model->modify_motorcycle($posts);

    }
    redirect(base_url().'motorcycles/detail/'.$id);
  }

  public function modify_image_motorcycle($id){

    $config['upload_path'] = 'assets/build/img/profile_motorcycles';
    $config['allowed_types'] = 'jpg|JPEG|png|PNG';
    $config['encrypt_name'] = true;

    $this->load->library('upload', $config);

    if ($this->upload->do_upload('profile_motorcycle')){
      $info_file = $this->upload->data();
      $image = $info_file['file_name'];
    }
    else{
      echo $this->upload->display_errors();
      echo "Imagem não enviada";
    }

    $this->motorcycles_model->update_image($id, $image);

    redirect(base_url().'motorcycles/detail/'.$id);
  }

  public function register_supply()
  {
    if (isset($_POST['register_supply'])) {
      $posts = $this->security->xss_clean($this->input->post());

      $this->gasoline_control_model->insert($posts);

      redirect(base_url().'motorcycles/detail/'.$_SESSION['id_motorcycle']);
    }
  }

  public function register_maintenance()
  {
    if (isset($_POST['register_maintenance'])) {
      $posts = $this->security->xss_clean($this->input->post());

      $this->maintenance_control_model->insert($posts);

      redirect(base_url().'motorcycles/detail/'.$_SESSION['id_motorcycle']);
    }
  }

  public function register_traffic_ticket(){
    if(isset($_POST['register_traffic_ticket'])){
      $posts = $this->security->xss_clean($this->input->post());

      $this->traffic_ticket_model->insert($posts);
    }
    redirect(base_url().'motorcycles/detail/'.$_SESSION['id_motorcycle']);
  }

  public function register_occurrence(){
    if(isset($_POST['register_occurrence'])){
      $posts = $this->security->xss_clean($this->input->post());

      $this->motorcycles_model->insert_occurrence($posts);
    }
    redirect(base_url().'motorcycles/detail/'.$_SESSION['id_motorcycle']);
  }

  public function update_status(){
    if (isset($_POST['update_status'])) {
      $posts = $this->security->xss_clean($this->input->post());

      $this->motorcycles_model->update_status($posts);
    }
    redirect(base_url().'motorcycles/detail/'.$_SESSION['id_motorcycle']);
  }

}
