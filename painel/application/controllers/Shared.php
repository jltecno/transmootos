<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Shared extends CI_Controller {

  public function __construct(){
		parent::__construct();

    $this->load->model('motorcycles_model');
    $this->load->model('bikers_model');
    $this->load->model('trips_model');
    $this->load->model('address_model');
    $this->load->model('packages_model');
    $this->load->model('payment_rules_model');
	}

	public function index()
	{
    $data['cities_payment'] = $this->payment_rules_model->fetch_cities_payment();

    $this->load->view('shared/shared', $data);
	}

  public function register_shared(){
    if (isset($_POST['register_shared'])) {
      $posts = $this->security->xss_clean($this->input->post());

      $id_departure = $this->address_model->insert_departure_adress($posts);
      $id_arrival = $this->address_model->insert_arrival_adress($posts);
      $id = $this->packages_model->insert_package($posts, $id_departure, $id_arrival, $id_trip = 0);

      // echo "<script>
      //   var url = '".base_url()."packages/package/".$id."';
      //   window.open(url, '_blank');
      // </script>";
    }
    redirect(base_url().'shared');
  }

}
