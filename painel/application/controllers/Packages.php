<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Packages extends MY_Controller {
  public function __construct(){
		parent::__construct();

    $this->load->model('gasoline_control_model');
    $this->load->model('maintenance_control_model');
    $this->load->model('traffic_ticket_model');
    $this->load->model('trips_model');
    $this->load->model('motorcycles_model');
    $this->load->model('patrimony_model');
    $this->load->model('packages_model');
    $this->load->model('address_model');
	}

  public function index()
  {
    redirect(base_url().'inicio');
  }

	public function packages($id)
	{
    $posts = ['id_trip' => $id];
    $data['trip'] = $this->trips_model->fetch_detail_trip_packages($posts);

    $data['packages'] = array();

    $trip_packages = $this->packages_model->fetch_packages_trip($posts);
    foreach ($trip_packages as $row) {
      $departure_address = $this->address_model->fetch_departure_adress($row->departure_address);
      $arrival_address = $this->address_model->fetch_arrival_adress($row->arrival_address);

      $arr = array (
        'dados' => array (
          'id_package' => $row->id ,
          'name' => $row->name ,
          'surname' => $row->surname ,
          'legal_phone' => $row->legal_phone ,
          'trade_name' => $row->trade_name,
          'juridical_phone' => $row->juridical_phone,
          'register' => $row->register,
          'destinatary' => $row->destinatary,
          'receiver' => $row->receiver,
          'payment' => $row->payment,
          'status_it_payment' => $row->status_it_payment,
          'people_authorized' => $row->people_authorized,
          'observation' => $row->observation,
        ),
        'departure_address' => $departure_address,
        'arrival_address' => $arrival_address
      );
      array_push($data['packages'], $arr);
    }
    // var_dump($data['trip']); die;
    // var_dump($data['packages']); die;
    // // die;
    // var_dump($departure_address);
    // var_dump($data['trip']);
    // var_dump($trip_packages); die;
		$this->load->view('packages/packages', $data);
	}

  public function package($id)
	{
    $data['package'] = array();

    $posts = ['id_trip' => $id];
    $package = $this->packages_model->fetch_package($posts);
    // var_dump($package);
    $departure_address = $this->address_model->fetch_departure_adress($package->departure_address);
    $arrival_address = $this->address_model->fetch_arrival_adress($package->arrival_address);

    $arr = array (
      'dados' => array (
        'id_package' => $package->id ,
        'name' => $package->name ,
        'surname' => $package->surname ,
        'legal_phone' => $package->legal_phone ,
        'trade_name' => $package->trade_name,
        'juridical_phone' => $package->juridical_phone,
        'register' => $package->register,
        'destinatary' => $package->destinatary,
      ),
      'departure_address' => $departure_address,
      'arrival_address' => $arrival_address
    );
    array_push($data['package'], $arr);
    // var_dump($data['package']); die;

		$this->load->view('packages/package', $data);
	}

  public function register_trip_packages(){
    if (isset($_POST['register_trip_packages'])) {
      $posts = $this->security->xss_clean($this->input->post());

      // var_dump($posts); die;

      $trip = $this->trips_model->insert($posts, $id_departure = 0, $id_arrival = 0);
      $this->packages_model->start_trip_packages($posts, $trip);
    }
    redirect(base_url().'dashboard');
  }

  public function fetch_detail_trip_packages(){
    // if (isset($_POST[''])) {
      $posts = $this->security->xss_clean($this->input->post());

      $trip = $this->trips_model->fetch_detail_trip_packages($posts);


      // $return = array_merge($trip);
      echo json_encode($trip);
    // }
  }

  public function fetch_packages_trip(){
    $posts = $this->security->xss_clean($this->input->post());

    $trip_packages = $this->packages_model->fetch_packages_trip_modal($posts);

    // var_dump($trip_packages); die;

    // $return = array_merge($trip_packages, $departure_address);
    echo json_encode($trip_packages);
  }

}
