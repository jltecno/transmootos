<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Clients extends MY_Controller {

  public function __construct(){
		parent::__construct();

		$this->load->model('clients_model');
    $this->load->model('trips_model');
    $this->load->model('contract_model');
    $this->load->model('bikers_model');
    $this->load->model('motorcycles_model');
    $this->load->model('payment_rules_model');
    $this->load->model('night_trips_model');
    $this->load->model('bills_receive_model');
	}

	public function index()
	{
		$data['clients'] = $this->clients_model->fetch_clients();

		$this->load->view('clients/clients', $data);
	}

  public function register()
	{

		$this->load->view('clients/register');
	}

	public function register_clients(){
		$posts = $this->security->xss_clean($this->input->post());

    if ($_FILES["img_client"]['name'] == "") {
      $image = 'perfil.png';
    }else{
      $config['upload_path'] = 'assets/build/img/profile_clients';
      $config['allowed_types'] = 'jpg|JPEG|png|PNG|';
      $config['encrypt_name'] = true;

      $this->load->library('upload', $config);

      if ($this->upload->do_upload('img_client')){
        $info_file = $this->upload->data();
        $image = $info_file['file_name'];
      }
      else{
        $image = 'perfil.png';
        echo $this->upload->display_errors();
        echo "Imagem não enviada";
      }
    }

		$id_client = $this->clients_model->insert($posts, $image);
		if ($_POST['type_client'] == 1) {
			$this->clients_model->insert_legal_person($posts, $id_client);
		}

		if ($_POST['type_client'] == 2) {
			$this->clients_model->insert_juridical_person($posts, $id_client);
		}

		redirect(base_url().'clients/detail/'.$id_client);
	}

  public function detail($id)
	{
    $data['client'] = $this->clients_model->fetch_client($id);
    $data['night_trips'] = $this->night_trips_model->fetch_night_trips($id);
    $data['trips'] = $this->trips_model->fetch_trips_client($id);
    $data['contract'] = $this->contract_model->fetch_contract($id);
    $data['bikers'] = $this->bikers_model->fetch_bikers();
    $data['motorcycles'] = $this->motorcycles_model->fetch_motorcycles();
    $data['receiver_client'] = $this->bills_receive_model->fetch_receiver_this_client($id);
    $data['status'] = $this->clients_model->fetch_status();

    $data['where_from'] = $this->clients_model->fetch_where_from();
    if ($data['client']->type_client == 1) {
      $data['legal_client']  = $this->clients_model->fetch_client_legal($id);
    } else if ($data['client']->type_client == 2) {
      $data['juridical_client'] = $this->clients_model->fetch_client_juridical($id);
    }

		$this->load->view('clients/detail', $data);
	}

  public function update_client($id){
    $posts = $this->security->xss_clean($this->input->post());
    if (isset($_POST['update_legal_person'])) {
      $this->clients_model->update_legal_person($posts);
    }
    if (isset($_POST['update_juridical_person'])) {
      $this->clients_model->update_juridical_person($posts);
    }
    $this->clients_model->update_client($posts);
    redirect(base_url().'clients/detail/'.$id);
  }

  public function update_image_client($id){
    if (isset($_POST['update_image'])) {
      $posts = $this->security->xss_clean($this->input->post());

      $config['upload_path'] = 'assets/build/img/profile_clients';
      $config['allowed_types'] = 'jpg|JPEG|png|PNG|';
      $config['encrypt_name'] = true;

      $this->load->library('upload', $config);

      if ($this->upload->do_upload('img_client')){
        $info_file = $this->upload->data();
        $image = $info_file['file_name'];
      }
      else{
        // $image = 'perfil.png';
        echo $this->upload->display_errors();
        echo "Imagem não enviada";
        die;
      }

      $this->clients_model->update_image_client($id, $image);
    }
    redirect(base_url().'clients/detail/'.$id);
  }

  public function register_contract($id){
    if (isset($_POST['register_contract'])) {
      $config['upload_path'] = 'assets/contracts';
      $config['allowed_types'] = 'jpg|JPEG|png|PNG|pdf|PDF';
      $config['encrypt_name'] = true;

      $this->load->library('upload', $config);

      if ($this->upload->do_upload('contract')){
        $info_file = $this->upload->data();
        $contract = $info_file['file_name'];
      }
      else{
        echo $this->upload->display_errors();
        echo "Contrato nao enviado";
      }

      $this->contract_model->insert_contract($contract, $id);

      redirect(base_url().'clients/detail/'.$id);
    }
  }

  public function register_night_trips($id){
    if (isset($_POST['register_night_trips'])) {
      $posts = $this->security->xss_clean($this->input->post());

      $client = $this->clients_model->fetch_client($id);
      if ($client->contract == 1) {
        $price = $client->price_trip;
      }else{
        $fetch_price = $this->payment_rules_model->fetch_price_contract($client);
        $price = $fetch_price->price;
      }

      $biker_commission = $this->bikers_model->fetch_biker_commission($posts['id_biker']);
      $user_commission = $this->users_model->fetch_user_commission();

      $this->night_trips_model->insert($posts, $id, $price, $biker_commission, $user_commission);
    }
    redirect(base_url().'clients/detail/'.$id);
  }

  public function fetch_night_trip(){
    $posts = $this->security->xss_clean($this->input->post());

    $return = $this->night_trips_model->fetch_night_trip($posts);
    echo json_encode($return);
  }

  public function fetch_night_trips_date_client(){
    $posts = $this->security->xss_clean($this->input->post());

    $return = $this->night_trips_model->fetch_night_date_client($posts);

    echo json_encode($return);
  }

  public function update_night_trips($id){
    if (isset($_POST['update_bills_receive'])) {
      $posts = $this->security->xss_clean($this->input->post());

      $this->night_trips_model->update_night_trips($posts);
    }
    redirect(base_url().'clients/detail/'.$id);
  }

}
