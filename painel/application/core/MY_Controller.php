<?php

class MY_Controller extends CI_Controller {
  public $user;

  public function __construct() {
    parent::__construct();

    $this->load->model("login_model", "login");
		$this->load->model("users_model");
		$logado = $this->login->checkUser();
		if (!$logado) {
			redirect('/');
			exit();
		}

		$this->user = $this->login->user();
  }
}
