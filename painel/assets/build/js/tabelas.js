// var urlAtual = 'https://transmootos.com.br/painel/';
var urlAtual = 'http://localhost/transmootos/painel/';
function startTable(idTable){
  $(idTable).DataTable({
    "destroy": true,
    'dom': 'Bfrtip',
    "scrollX": true,
    "language": {
      "lengthMenu": "Mostrar _MENU_ registros",
      "zeroRecords": "Nenhum registro encontrado",
      "info": "Página _PAGE_ de _PAGES_",
      "infoEmpty": "Nenhum registro encontrado",
      "infoFiltered": "(Filtrado de _MAX_ registros no total)",
      "sSearch": "Buscar: _INPUT_",
      "paginate": {
        "previous": "Anterior",
        "next": "Próximo",
        "first": "Primeira página",
        "last": "Última página"
      }
    },
    'buttons': [
      'copy', 'csv', 'excel', 'pdf', 'print'
    ]
  });

  $(idTable).keypress(function(e) {
    if(e.which == 13) {
      e.preventDefault();
      console.log('Não vou enviar');
    }
  });

  return 0;
}

($('#table_biker_night_trip').length) ? startTable($('#table_biker_night_trip')): '';
($('#table_night_trips').length) ? startTable($('#table_night_trips')): '';
($('#table_gasoline').length) ? startTable($('#table_gasoline')): '';
($('#table_maintenance').length) ? startTable($('#table_maintenance')): '';
($('#table_history_trips').length) ? startTable($('#table_history_trips')): '';
($('#table_ticket').length) ? startTable($('#table_ticket')): '';
($('#table_occurrence').length) ? startTable($('#table_occurrence')): '';
($('#every_race').length) ? startTable($('#every_race')): '';
($('#table_provider').length) ? startTable($('#table_provider')): '';
($('#table_bank_data').length) ? startTable($('#table_bank_data')): '';
($('#table_patrimony').length) ? startTable($('#table_patrimony')): '';
($('#table_dash_progress').length) ? startTable($('#table_dash_progress')): '';
($('#table_dash_scheduling').length) ? startTable($('#table_dash_scheduling')): '';
($('#table_dash_waiting').length) ? startTable($('#table_dash_waiting')): '';
($('#table_dash_closed').length) ? startTable($('#table_dash_closed')): '';
($('#table_packages_waiting').length) ? startTable($('#table_packages_waiting')): '';
($('#table_dash_cancel_trips').length) ? startTable($('#table_dash_cancel_trips')): '';
($('#table_bikers').length) ? startTable($('#table_bikers')): '';
($('#table_trips_biker').length) ? startTable($('#table_trips_biker')): '';
($('#table_motorcycles').length) ? startTable($('#table_motorcycles')): '';
($('#all_gasoline').length) ? startTable($('#all_gasoline')): '';
($('#table_fixed_accounts').length) ? startTable($('#table_fixed_accounts')): '';
($('#table_accounts_pay').length) ? startTable($('#table_accounts_pay')): '';
($('#table_variable_accounts').length) ? startTable($('#table_variable_accounts')): '';
($('#table_clients').length) ? startTable($('#table_clients')): '';
($('#trips_receipts').length) ? startTable($('#trips_receipts')): '';
($('#table_bills_receive').length) ? startTable($('#table_bills_receive')): '';
($('#table_accounts_received').length) ? startTable($('#table_accounts_received')): '';
($('#table_payment_rules').length) ? startTable($('#table_payment_rules')): '';
($('#table_users').length) ? startTable($('#table_users')): '';
($('#all_races_reports').length) ? startTable($('#all_races_reports')): '';
($('#new_clients_reports').length) ? startTable($('#new_clients_reports')): '';
($('#license_expiration_reports').length) ? startTable($('#license_expiration_reports')): '';
($('#tabelaum').length) ? startTable($('#tabelaum')): '';
($('#reports_night_bikers').length) ? startTable($('#reports_night_bikers')): '';
($('#tabela_clientes').length) ? startTable($('#tabela_clientes')): '';
if ($('#tabela_escolher_cliente').length) {
  startTable($('#tabela_escolher_cliente'));
  $('#tabela_escolher_cliente').DataTable({
    dropdownParent: $('#modalClients')
  });
}


$(document).on('click', '#search_date_biker_night_trip', function(){
  var id = $('#id_biker').val();
  var date_last_formated = $("#last_date").val().split('/');
  var last_date = date_last_formated[2]+'-'+date_last_formated[1]+'-'+date_last_formated[0];

  var date_first_formated = $("#first_date").val().split('/');
  var first_date = date_first_formated[2]+'-'+date_first_formated[1]+'-'+date_first_formated[0];

  if (first_date == undefined && last_date == undefined) {
    first_date = 0;
    last_date = 0;
  }

  $.ajax({
    url: urlAtual+'bikers/fetch_night_trips_date',
    method: 'post',
    dataType: 'json',
    data: {id:id, first_date:first_date, last_date:last_date},
    success: function (data){

      $('#table_biker_night_trip_refresh').html('');

      $('#table_biker_night_trip_refresh').append('\
        <table class="table table-striped table-bordered" style="width:100%" id="table_biker_night_trip">\
          <thead class="text-primary">\
            <tr>\
              <th>Data</th>\
              <th>Moto</th>\
              <th>Cliente</th>\
              <th>Odometro inicial</th>\
              <th>Odometro final</th>\
              <th>Preço</th>\
              <th>Quantidade</th>\
              <th>Soma</th>\
              <th>Comissão</th>\
              <th>status</th>\
            </tr>\
          </thead>\
          <tbody id="tbody_night_trips_biker">\
          </tbody>\
        </table>\
      ');

      $.each(data, function (index, value){
        if (value.name) {
          var name = value.name+' '+value.surname;
        } else {
          var name = value.trade_name;
        }

        if (value.payment_status == '0') {
          var payment_status = 'Não pago';
        } else {
          var payment_status = 'Pago';
        }

        $('#tbody_night_trips_biker').append('\
        <tr>\
        <td>'+value.date+'</td>\
        <td>'+value.code_motorcycle+' | '+value.license_plate+'</td>\
        <td>'+name+'</td>\
        <td>'+value.departure_odometer+'</td>\
        <td>'+value.arrival_odometer+'</td>\
        <td>'+value.price+'</td>\
        <td>'+value.quantity+'</td>\
        <td>R$'+value.price * value.quantity+'</td>\
        <td>'+value.biker_commission+'</td>\
        <td>'+payment_status+'</td>\
        </tr>\
        ');
      });
      var table_biker_night_trip =  startTable($('#table_biker_night_trip'));
    }
  });
});


$(document).on('click', '#search_date_client_night_trip', function(){
  var id = $('#id_client').val();
  var date_last_formated = $("#last_date").val().split('/');
  var last_date = date_last_formated[2]+'-'+date_last_formated[1]+'-'+date_last_formated[0];

  var date_first_formated = $("#first_date").val().split('/');
  var first_date = date_first_formated[2]+'-'+date_first_formated[1]+'-'+date_first_formated[0];

  if (first_date == undefined && last_date == undefined) {
    first_date = 0;
    last_date = 0;
  }

  $.ajax({
    url: urlAtual+'clients/fetch_night_trips_date_client',
    method: 'post',
    dataType: 'json',
    data: {id:id, first_date:first_date, last_date:last_date},
    success: function (data){

      $('.detail_night_trips_clients').html('');

      $('.detail_night_trips_clients').append('\
        <table class="table table-striped table-bordered" style="width:100%" id="table_night_trips">\
          <thead class="text-primary">\
            <tr>\
              <th scope="col">Data</th>\
              <th scope="col">Piloto</th>\
              <th scope="col">Moto</th>\
              <th scope="col">Status</th>\
              <th scope="col"></th>\
            </tr>\
          </thead>\
          <tbody id="night_trips_clients">\
          </tbody>\
        </table>\
      ');

      $.each(data, function (index, value){
        if (value.payment_status == '0') {
          var payment_status = 'Não pago';
        } else {
          var payment_status = 'Pago';
        }

        $('#night_trips_clients').append('\
        <tr>\
          <td>'+value.date+'</td>\
          <td>'+value.biker_name+' | '+value.biker_surname+'</td>\
          <td>'+value.code_motorcycle+' | '+value.license_plate+'</td>\
          <td>'+payment_status+'</td>\
          <td>\
            <a href="" class="detail_night_trips" data-id="'+value.id+'" data-toggle="modal" data-target="#modal_detail_night_trips">\
              <i class="fa fa-1x fa-search "></i>\
            </a>\
          </td>\
        </tr>\
        ');
      });
      var table_night_trips = startTable($('#table_night_trips'));
    }
  });
});

($('#reports_night_clients').length) ? startTable($('#reports_night_clients')): '';

$(document).on('click', '#search_date_client_night_trip', function(){

  var date_last_formated = $("#last_date").val().split('/');
  var last_date = date_last_formated[2]+'-'+date_last_formated[1]+'-'+date_last_formated[0];

  var date_first_formated = $("#first_date").val().split('/');
  var first_date = date_first_formated[2]+'-'+date_first_formated[1]+'-'+date_first_formated[0];

  if (first_date == undefined && last_date == undefined) {
    first_date = 0;
    last_date = 0;
  }

  $.ajax({
    url: urlAtual+'reports_clients/fetch_night_trips_client',
    method: 'post',
    dataType: 'json',
    data: {first_date:first_date, last_date:last_date},
    success: function (data){

      $('.reports_night_trips_clients').html('');

      $('.reports_night_trips_clients').append('\
      <table id="reports_night_clients" class="table table-striped table-bordered" style="width:100%;border:none;">\
          <thead class="text-primary">\
            <tr>\
              <th>Cliente</th>\
              <th>Quantidade</th>\
            </tr>\
          </thead>\
          <tbody id="tbody_reports_night_clients">\
          </tbody>\
        </table>\
      ');

      $.each(data, function (index, value){
        if (value.name) {
          var name = value.name+' '+value.surname;
        } else {
          var name = value.trade_name;
        }

        $('#tbody_reports_night_clients').append('\
          <tr>\
            <td>'+name+'</td>\
            <td>'+value.quantity_night_trips+'</td>\
          </tr>\
        ');
      });
      var reports_night_clients = startTable($('#reports_night_clients'));
    }
  });
});

($('#reports_night_km').length) ? startTable($('#reports_night_km')): '';
$(document).on('click', '#search_date_client_night_trip', function(){

  var date_last_formated = $("#last_date").val().split('/');
  var last_date = date_last_formated[2]+'-'+date_last_formated[1]+'-'+date_last_formated[0];

  var date_first_formated = $("#first_date").val().split('/');
  var first_date = date_first_formated[2]+'-'+date_first_formated[1]+'-'+date_first_formated[0];

  if (first_date == undefined && last_date == undefined) {
    first_date = 0;
    last_date = 0;
  }

  $.ajax({
    url: urlAtual+'reports_bikers/fetch_night_trips_km_date',
    method: 'post',
    dataType: 'json',
    data: {first_date:first_date, last_date:last_date},
    success: function (data){

      $('.reports_night_trips_km').html('');

      $('.reports_night_trips_km').append('\
      <table id="reports_night_km" class="table table-striped table-bordered" style="width:100%;border:none;">\
          <thead class="text-primary">\
            <tr>\
              <th>Piloto</th>\
              <th>Quantidade</th>\
              <th>KM Percorrido</th>\
              <th>Comissão</th>\
            </tr>\
          </thead>\
          <tbody id="tbody_reports_night_km">\
          </tbody>\
        </table>\
      ');

      $.each(data, function (index, value){

        $('#tbody_reports_night_km').append('\
          <tr>\
            <td>'+value.name+' '+value.surname+'</td>\
            <td>'+value.quantity_night_trips+'</td>\
            <td>'+value.km+'</td>\
            <td>'+value.commission+'</td>\
          </tr>\
        ');
      });
      var reports_night_km = startTable($('#reports_night_km'));
    }
  });
});


($('#reports_night_km_motorcycles').length) ? startTable($('#reports_night_km_motorcycles')): '';
$(document).on('click', '#search_date_client_night_trip', function(){

  var date_last_formated = $("#last_date").val().split('/');
  var last_date = date_last_formated[2]+'-'+date_last_formated[1]+'-'+date_last_formated[0];

  var date_first_formated = $("#first_date").val().split('/');
  var first_date = date_first_formated[2]+'-'+date_first_formated[1]+'-'+date_first_formated[0];

  if (first_date == undefined && last_date == undefined) {
    first_date = 0;
    last_date = 0;
  }

  $.ajax({
    url: urlAtual+'reports_motorcycles/fetch_night_trips_km_date',
    method: 'post',
    dataType: 'json',
    data: {first_date:first_date, last_date:last_date},
    success: function (data){

      $('.reports_night_trips_km_motorcycles').html('');

      $('.reports_night_trips_km_motorcycles').append('\
      <table id="reports_night_km_motorcycles" class="table table-striped table-bordered" style="width:100%;border:none;">\
          <thead class="text-primary">\
            <tr>\
              <th>Moto</th>\
              <th>Quantidade</th>\
              <th>KM Percorrido</th>\
            </tr>\
          </thead>\
          <tbody id="tbody_reports_night_km_motorcycles">\
          </tbody>\
        </table>\
      ');

      $.each(data, function (index, value){

        $('#tbody_reports_night_km_motorcycles').append('\
          <tr>\
            <td>'+value.code_motorcycle+' | '+value.license_plate+'</td>\
            <td>'+value.quantity_night_trips+'</td>\
            <td>'+value.km+'</td>\
          </tr>\
        ');
      });
      var reports_night_km = startTable($('#reports_night_km_motorcycles'));
    }
  });
});

($('#reports_trips_separate').length) ? startTable($('#reports_trips_separate')): '';
$(document).on('click', '#search_date_client_night_trip', function(){

  var date_last_formated = $("#last_date").val();
  var date_last_formated = date_last_formated.split("/");
  var last_date = date_last_formated[2]+'-'+date_last_formated[1]+'-'+date_last_formated[0];

  var date_first_formated = $("#first_date").val();
  var date_first_formated = date_first_formated.split("/");
  var first_date = date_first_formated[2]+'-'+date_first_formated[1]+'-'+date_first_formated[0];

  if (first_date == undefined && last_date == undefined) {
    first_date = 0;
    last_date = 0;
  }

  $.ajax({
    url: urlAtual+'reports_bikers/fetch_trips_separate_date',
    method: 'post',
    dataType: 'json',
    data: {first_date:first_date, last_date:last_date},
    success: function (data){

      $('.reports_trips_separate').html('');

      $('.reports_trips_separate').append('\
      <table id="reports_trips_separate" class="table table-striped table-bordered" style="width:100%;border:none;">\
          <thead class="text-primary">\
            <tr>\
              <th>Piloto</th>\
              <th>Quantidade</th>\
              <th>KM Percorrido</th>\
            </tr>\
          </thead>\
          <tbody id="tbody_reports_trips_separate">\
          </tbody>\
        </table>\
      ');

      $.each(data, function (index, value){

        $('#tbody_reports_trips_separate').append('\
          <tr>\
            <td>'+value.name+' '+value.surname+'</td>\
            <td>'+value.quantity+'</td>\
            <td>'+value.km+'</td>\
          </tr>\
        ');
      });
      var reports_trips_separate = startTable($('#reports_trips_separate'));
    }
  });
});

($('#reports_trips_shared').length) ? startTable($('#reports_trips_shared')): '';
$(document).on('click', '#search_date_biker_shared', function(){

  var date_last_formated = $("#last_date_shared").val().split('/');
  var last_date = date_last_formated[2]+'-'+date_last_formated[1]+'-'+date_last_formated[0];

  var date_first_formated = $("#first_date_shared").val().split('/');
  var first_date = date_first_formated[2]+'-'+date_first_formated[1]+'-'+date_first_formated[0];

  if (first_date == undefined && last_date == undefined) {
    first_date = 0;
    last_date = 0;
  }

  $.ajax({
    url: urlAtual+'reports_bikers/fetch_trips_shared_date',
    method: 'post',
    dataType: 'json',
    data: {first_date:first_date, last_date:last_date},
    success: function (data){

      $('.reports_trips_shared').html('');

      $('.reports_trips_shared').append('\
      <table id="reports_trips_shared" class="table table-striped table-bordered" style="width:100%;border:none;">\
          <thead class="text-primary">\
            <tr>\
              <th>Piloto</th>\
              <th>Quantidade</th>\
              <th>KM Percorrido</th>\
            </tr>\
          </thead>\
          <tbody id="tbody_reports_shared">\
          </tbody>\
        </table>\
      ');

      $.each(data, function (index, value){

        $('#tbody_reports_shared').append('\
          <tr>\
            <td>'+value.name+' '+value.surname+'</td>\
            <td>'+value.quantity+'</td>\
            <td>'+value.km+'</td>\
          </tr>\
        ');
      });
      var reports_trips_shared = startTable($('#reports_trips_shared'));
    }
  });
});

($('#reports_trips_shared_motorcycles').length) ? startTable($('#reports_trips_shared_motorcycles')): '';
$(document).on('click', '#search_date_motorcycle_shared', function(){
  var date_last_formated = $("#last_date_shared").val().split('/');
  var last_date = date_last_formated[2]+'-'+date_last_formated[1]+'-'+date_last_formated[0];

  var date_first_formated = $("#first_date_shared").val().split('/');
  var first_date = date_first_formated[2]+'-'+date_first_formated[1]+'-'+date_first_formated[0];

  if (first_date == undefined && last_date == undefined) {
    first_date = 0;
    last_date = 0;
  }

  $.ajax({
    url: urlAtual+'reports_motorcycles/fetch_trips_shared_date',
    method: 'post',
    dataType: 'json',
    data: {first_date:first_date, last_date:last_date},
    success: function (data){

      $('.reports_trips_shared_motorcycles').html('');

      $('.reports_trips_shared_motorcycles').append('\
      <table id="reports_trips_shared_motorcycles" class="table table-striped table-bordered" style="width:100%;border:none;">\
          <thead class="text-primary">\
            <tr>\
              <th>Moto</th>\
              <th>Quantidade</th>\
              <th>KM Percorrido</th>\
            </tr>\
          </thead>\
          <tbody id="tbody_reports_shared_motorcycles">\
          </tbody>\
        </table>\
      ');

      $.each(data, function (index, value){

        $('#tbody_reports_shared_motorcycles').append('\
          <tr>\
            <td>'+value.code_motorcycle+' '+value.license_plate+'</td>\
            <td>'+value.quantity+'</td>\
            <td>'+value.km+'</td>\
          </tr>\
        ');
      });
      var reports_trips_shared_motorcycles = startTable($('#reports_trips_shared_motorcycles'));
    }
  });
});
