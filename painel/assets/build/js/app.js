// var url_atual = "https://transmootos.com.br/painel/";
var url_atual = "http://localhost/transmootos/painel/";

$(document).on("keydown", 'input', function(){

  if (event.keyCode == 13) {
    event.preventDefault();

    var inputs = $(this).parents("form").eq(0).find(":input");
    var idx = inputs.index(this);

    if (idx == inputs.length - 1) {
        inputs[0].select()
    } else {
        inputs[idx + 1].focus();
        inputs[idx + 1].select();
    }
    return false;
  }
});

$(document).on('change', '.type_client', function(){
  var type_client = $(this).val();
  if (type_client == 1) {
    $('#form_legal_person').show();
    $('#form_juridical_person').hide();
  }else if (type_client == 2){
    $('#form_legal_person').hide();
    $('#form_juridical_person').show();
  }
});

$(document).on('click', '#edit-rules', function (){
  $('#id_payment_rules').val($(this).data('id'));
  $('#description').val($(this).data('description'));
  $('#minimum').val($(this).data('minimum'));
  $('#maximum').val($(this).data('maximum'));
  $('#price').val($(this).data('price'));
});

$(document).on('click', '#choose_cliente', function(){
  $('#dados_cliente').html('<p>'+$(this).data('trade_name')+' '+$(this).data('name')+' '+$(this).data('surname')+'</p>');
  $('#id_client').val($(this).data('id'));
  $('#contract_client').val($(this).data('contract'));
  $('#modalClients').modal('hide');
});

$(document).on('click', '.locale', function(){
  var locale = $(this).val();
  var contract = $('#contract_client').val();
  if (locale == "I") {
    $('#div_km').hide();
    $('#div_city').hide();
    var service_type = $('.service_type:checked').val();
    if (service_type != 'M') {
      contract = 0;
    }

    $.ajax({
     url: url_atual + 'payment_rules/fetch_price_inside',
     method: 'POST',
     dataType: 'JSON',
     data: {contract:contract},
     success: function(data){
       $.each(data, function(index, value){
         $('#price').val(value);
       });
    }
   });
  } else if (locale == "O") {
    $('#div_km').show();
    $('#div_city').hide();
  } else if (locale == "Or"){
    $('#div_km').hide();
    $('#div_city').show();
  }
});

$(document).on('change', '#distance', function(){
  var distance = $(this).val();
  $.ajax({
   url: url_atual + 'payment_rules/fetch_price_outside',
   method: 'POST',
   dataType: 'JSON',
   data: {distance:distance},
   success: function(data){
     $('#price').val(data);
   }
 });
});

$(document).on('change', '#package_city', function(){
  var id = $(this).val();
  $.ajax({
   url: url_atual + 'payment_rules/fetch_price_city',
   method: 'POST',
   dataType: 'JSON',
   data: {id:id},
   success: function(data){
     $('#price').val(data.toFixed(2));
   }
 });
});

$(document).on("change", "#id_city", function () {
  var id = $(this).val();

  $.ajax({
   url: url_atual + 'trips/packages_waiting',
   method: 'POST',
   dataType: 'JSON',
   data: {id:id},
   success: function(data){
     $('#div_packages').html('');
     $('#div_packages').append('\
       <div class="row">\
         <div class="col-md-1"></div>\
         <div class="col-md-2">Pacote</div>\
         <div class="col-md-5">Cliente</div>\
         <div class="col-md-4">Cidade</div>\
       </div>\
     ');
     $.each(data, function(index, value){
       if(value.name != null){ var name = value.name}else{var name = ''}
       if(value.surname != null){ var surname = value.surname}else{var surname = ''}
       if(value.trade_name != null){ var trade_name = value.trade_name}else{var trade_name = ''}

       $('#div_packages').append('\
         <div class="row">\
           <div class="col-md-1"><input type="checkbox" name="package[]" value="'+value.id+'"/></div>\
           <div class="col-md-2">'+value.id+'</div>\
           <div class="col-md-5">'+ name +' '+surname+' '+trade_name+'</div>\
           <div class="col-md-4">'+value.city+'</div>\
           <input type="hidden" name="id_client" value="'+value.id_client+'"/>\
         </div>\
       ');
     });
   }
  });
});

$(document).on('click', '.leave', function(){
  var leave = $(this).val();
  if (leave == 'G') {
    $('#div_go_out_now').show();
    $('#div_schedule').hide();
  }else if (leave == 'S') {
    $('#div_go_out_now').hide();
    $('#div_schedule').show();
  }else{
    $('#div_go_out_now').hide();
    $('#div_schedule').hide();
  }
});

// $(document).on('click', '.detail_trip_packages', function(){
//   var tipo_modal = $(this).data('id');
//   var id_trip = $(this).data('trip');
//
//   $.get(url_atual + 'modais/' + tipo_modal,function(response){
//     $('.modais').html(response);
//   });
//
//   setTimeout(function(){
//     $('#'+tipo_modal).modal('show');
//   }, 200);
//
// });

$(document).on('click', '.detail_trip', function(){
  var tipo_modal = $(this).data('id');
  var id_trip = $(this).data('trip');

  $.get(url_atual + 'modais/' + tipo_modal,function(response){
    $('.modais').html(response);
  });

  $.ajax({
   url: url_atual + 'trips/fetch_detail_trip',
   method: 'POST',
   dataType: 'JSON',
   data: {id_trip:id_trip},
   success: function(data){
     setTimeout(function(){
       $.each(data, function (index, value) {

         if (data.type_client == 1) {
           $('#name').val(data.name);
           $('#surname').val(data.surname);
           $('#legal_phone').val(data.legal_phone);
           $('#legal_person').show();
         }else{
           $('#trade_name').val(data.trade_name);
           $('#juridical_phone').val(data.juridical_phone);
           $('#juridical_person').show();
         }

         $('#requested_time').val(data.register);
         if (data.locale == "I") {
           $('#locale').val('Interno');
         }else{
           $('#locale').val('Fora da cidade');
         }
         if (data.service == "P") {
           $('#type_service').val('Pessoa');
         }else{
           $('#type_service').val('Material');
           $("#etiqueta").attr("href", url_atual + "packages/package/" + data.id_package);
           $("#etiqueta").show();
         }
         if (data.service == 1) {
           $('#contract').val('Sim');
         }else{
           $('#contract').val('Não');
         }
         $('#km_provided').val(data.km_provided);
         $('#expected_value').val(data.expected_value);
         $('#id_trip').val(data.id);
         $('#biker').val(data.id_biker);
         $('#motorcycle').val(data.id_motorcycle);
         $('#departure_time').val(data.departure_time);
         $('#arrival_time').val(data.arrival_time);
         $('#departure_odometer').val(data.departure_odometer);
         $('#arrival_odometer').val(data.arrival_odometer);
         var distance = parseFloat(data.arrival_odometer) - parseFloat(data.departure_odometer);
         $('#distance').val(distance.toFixed(1));
         $('#id_trip').val(data.id);

         $('#departure_address').val(data.departure_address + ', ' + data.departure_number + ', ' + data.departure_complement + ', ' + data.departure_neighborhood + ', ' + data.departure_city  + ' - ' + data.departure_state);
         $('#arrival_address').val(data.arrival_address + ', ' + data.arrival_number + ', ' + data.arrival_complement + ', ' + data.arrival_neighborhood + ', ' + data.arrival_city  + ' - ' + data.arrival_state);
         $("#open_receipt").attr("href", url_atual + "receipts/receipt/" + data.id);

       });
     }, 100);

     setTimeout(function(){
       $('#'+tipo_modal).modal('show');
     }, 200);
   }
  });


});

$(document).on('click', '.detail_trip_packages', function() {
  var tipo_modal = $(this).data('id');
  var id_trip = $(this).data('trip');

  $.get(url_atual + 'modais/' + tipo_modal,function(response){
    $('.modais').html(response);
  });


  setTimeout(function(){
    $('#id_trip').val(id_trip);

    $('#'+tipo_modal).modal('show');
  }, 400);
});

$(document).on('click', '#pog_packages', function() {
  var id_trip = $('#id_trip').val();

  $("#open_receipt").attr("href", url_atual + "packages/packages/" + id_trip);

  async function fetch_detail_trip_packages() {
    await $.ajax({
     url: url_atual + 'packages/fetch_detail_trip_packages',
     method: 'POST',
     dataType: 'JSON',
     data: {id_trip:id_trip},
     success: function(data){
       $('.packages_trips').html('');

       $('#biker').val(data.biker_name + ' ' + data.biker_surname);
       $('#motorcycle').val(data.license_plate);
       $('#departure_time').val(data.departure_time);
       $('#departure_odometer').val(data.departure_odometer);
     }
    });

    return 'Hi from JavaScript';
  }

  const start_fetch_detail_trip_packages = async () => {
    let response = await fetch_detail_trip_packages();
  };

  start_fetch_detail_trip_packages();

  async function fetch_packages_trip() {
    await $.ajax({
     url: url_atual + 'packages/fetch_packages_trip',
     method: 'POST',
     dataType: 'JSON',
     data: {id_trip:id_trip},
     success: function(data){
       $('.packages_trips').html('\
         <table border="1" width="100%">\
           <tr style="width: 100%;">\
             <td style="width: 10%"></td>\
             <td style="width: 20%">Pacote</td>\
             <td style="width: 50%">Remetente</td>\
             <td style="width: 20%">Solicitação</td>\
           </tr>\
         </table>\
       ');
       $.each(data, function (index, value) {
         if (value.name != null) {
           var name = value.name +' '+ value.surname;
         }else{
           var name = value.trade_name;
         }
         $('.packages_trips').append('\
           <table border="1" width="100%">\
             <tr style="width: 100%;">\
               <td style="width: 10%"><input type="checkbox" name="package[]" value="'+ value.id + '"/></td>\
               <td style="width: 20%">'+ value.id + '</td>\
               <td style="width: 50%">'+ name +'</td>\
               <td style="width: 20%">'+ value.register + '</td>\
             </tr>\
            </table>\
         ');
       });
       $('.packages_trips').append('</table>');
     }
    });
    return 'Hi from JavaScript 2';
  }

  const start_fetch_packages_trip = async () => {
    let response = await fetch_packages_trip();
  };

  start_fetch_packages_trip();
});


$('.newimgum').bind("click", function () {
  $('#picimgum').click();
});

$(document).on("change", "#picimgum", function () {
  readURLum(this);
});

function readURLum(input) {
  if (input.files && input.files[0]) {
    var reader = new FileReader();
    reader.onload = function (e) {
      $('#imgum').attr('src', e.target.result);
    };
    reader.readAsDataURL(input.files[0]);
     $('#save').show();
  }
}

$(document).on('click', '.pay', function(){
  var id = $(this).data("id");
  var name = $(this).data("name");
  var due_date = $(this).data("duedate");
  var value = $(this).data("value");
  var provider = $(this).data("provider");
  $('#id_bill').val(id);
  $('#name_bill').val(name);
  $('#due_date').val('Dia '+due_date);
  $('#bill_value').val(value);
  $('#provider').val(provider);

  $("#modal_bill_pay").modal("show");
});

function chama_relatorio(data_atual, action){
  var data_atual = data_atual;
  var action = action;
  var dados = [data_atual, action];
  $.ajax({
    url: url_atual + 'financial/chama_relatorio',
    method: 'POST',
    dataType: "json",
    data: {dados:dados},
    success: function (data) {
      $('#data_relatorio').text(''+data.dia+'');
      if (data.invoice != null) {
        $('.invoice_day').text('R$'+data.invoice+'');
      } else {
        $('.invoice_day').text('R$00.00');
      }

      if (data.count_trips != null) {
        $('.count_trips').text(''+data.count_trips+'');
      } else {
        $('.count_trips').text('0');
      }

      if (data.expenses_day != null) {
        $('.expenses_day').text('R$'+data.expenses_day+'');
      } else {
        $('.expenses_day').text('R$00.00');
      }

    }
  });
}

$(document).on('click', '#anterior', function(){
  var data_atual = $('#data_relatorio').text();
  var action = "anterior";

  chama_relatorio(data_atual, action);
});

$(document).on('click', '#proximo', function(){
  var data_atual = $('#data_relatorio').text();
  var action = "proximo";

  chama_relatorio(data_atual, action);
});

$(document).on('click', '#people_authorized', function(){
  $('#authorized').show();
});

$(document).on('click', '#own_hands', function (){
  $('#authorized').hide();
});

$(document).on('click', '#anyone', function (){
  $('#authorized').hide();
});

$(document).on('click', '#button_edit_status', function (){
  var occurrence = $(this).data('occurrence');

  $('#id_ocurrence').val(occurrence);
});

$(document).on('click', '#store', function (){

  $.ajax({
    url: url_atual + 'shared/shared_package',
    method: 'POST',
    dataType: "json",
    data: {dados:dados},
    success: function (data) {

    }
  });
});

$(document).on('click', '.contract', function(){
  if ($(this).val() == 1) {
    $('.div_price_trip').show();
  }else{
    $('.div_price_trip').hide();
  }
});

$(document).on('click', '#payment_receive', function (){
  let id_client = $(this).data('id_client');
  let id = $(this).data('id');
  var amount = 0;
  var sum_trips = 0;
  $('#client').val(id_client);
  $.ajax({
    url: url_atual + 'bills_receive/fetch_trips_receives',
    method: 'POST',
    dataType: "json",
    data: {id_client:id_client, id:id},
    success: function (data) {
      $('.night_trips_receive').html('');
      $.each(data, function (index, value) {
        if(value.trade_name != null){trade_name = value.trade_name}else{trade_name = ''}
        if(value.name != null){name = value.name}else{name = ''}
        if(value.surname != null){surname = value.surname}else{surname = ''}
        $('#detail_client').html('\
          <div class="col-md-12" style="padding-left: 20px;"><h6>'+trade_name+' '+name+' '+surname+'</h6></div>\
        ');
        amount = parseFloat(amount) + (value.price * value.quantity);
        sum_trips = parseInt(sum_trips) + parseInt(value.quantity);
        $('.night_trips_receive').append('\
            <tr>\
              <td><input type="checkbox" name="night_trip[]" value="'+ value.id + '" id="night_trip"/></td>\
              <td>'+ value.quantity + '</td>\
              <td>R$'+ value.price + '</td>\
              <td>R$'+ value.price * value.quantity + '</td>\
              <td>'+ value.date + '</td>\
            </tr>\
        ');
      });
      $('#amount').html('\
        <div class="col-md-12" style="padding-right: 20px;">Valor total a receber: R$'+amount+'</div>\
        <div class="col-md-12" style="padding-right: 20px;">Total de viagens: '+sum_trips+'</div>\
      ');
    }
  });
});

$(document).on('click', '#edit_bank_data', function (){
  var id_bank = $(this).data('id'),
  cpf = $(this).data('cpf'),
  agency = $(this).data('agency'),
  account_number = $(this).data('account_number'),
  bank = $(this).data('bank'),
  account_type = $(this).data('account_type');

  $('#id_bank').val(id_bank);
  $('#cpf_edit').val(cpf);
  $('#agency_edit').val(agency);
  $('#account_number_edit').val(account_number);
  $('#bank_edit').val(bank);
  $('#account_type_edit').val(account_type);
});

$(document).on('click', '.detail_bills', function (){
  var id = $(this).data('id');
  $('.detail_bills_receive').html('');

  $.ajax({
    url: url_atual+'bills_receive/fetch_paid_bill',
    method: 'POST',
    dataType: 'json',
    data: {id:id},
    success: function (data) {
      $.each(data, function (index, value){
        $('.detail_bills_receive').append('<tr>\
          <td>'+value.quantity+'</td>\
          <td>R$'+value.price+'</td>\
          <td>R$'+value.price * value.quantity+'</td>\
          <td>'+value.day_receipt+'</td>\
        </tr>');
        $('#amount_detail').html('<div class="col-md-12">valor total recebido: R$'+value.amount+'</div>');
      });

    }
  });
});

$(document).on('click', '.detail_night_trips', function (){
  var id = $(this).data('id');

  $.ajax({
    url: url_atual+'clients/fetch_night_trip',
    method: 'POST',
    dataType: 'json',
    data: {id:id},
    success: function (data) {
      if (data.payment_status == '1') {
        var payment_status = 'Pago';
      } else {
        var payment_status = 'Não pago';
      }

      $('#date_night_trip_detail').val(data.date);
      $('#quantity_night_trip').val(data.quantity);
      $('#biker_night_trip').val(data.id_biker);
      $('#motorcycle_night_trip').val(data.id_motorcycle);
      $('#departure_odometer_night_trip').val(data.departure_odometer);
      $('#arrival_odometer_night_trip').val(data.arrival_odometer);
      $('#amount_night_trip').val(data.price);
      $('#status_night_trip').val(payment_status);
      $('#id_night_trip').val(data.id);
    }
  });
});

$(document).on('change', '#repeat_password', function (){
  var new_password = $('#new_password').val();
  var repeat_password = $(this).val();

  if (repeat_password != new_password) {
    $('#error_new_password').show();
    $('#update_user_password').prop('type', 'button');
  } else {
    $('#error_new_password').hide();
    $('#update_user_password').prop('type', 'submit');
  }
});

$(document).on('click', '#chamar_dados_shared', function (){
  var id = $(this).val();

  $.ajax({
   url: url_atual + 'trips/packages_waiting',
   method: 'POST',
   dataType: 'JSON',
   data: {id:id},
   success: function(data){
     $('#div_shared').html('');
     $('#div_shared').append('\
       <div class="row">\
         <div class="col-md-1"></div>\
         <div class="col-md-2">Pacote</div>\
         <div class="col-md-5">Cliente</div>\
         <div class="col-md-4">Cidade</div>\
       </div>\
     ');
     $.each(data, function(index, value){
       if(value.name != null){ var name = value.name}else{var name = ''}
       if(value.surname != null){ var surname = value.surname}else{var surname = ''}
       if(value.trade_name != null){ var trade_name = value.trade_name}else{var trade_name = ''}

       $('#div_shared').append('\
         <div class="row">\
           <div class="col-md-1"><input type="checkbox" name="package[]" value="'+value.id+'"/></div>\
           <div class="col-md-2">'+value.id+'</div>\
           <div class="col-md-5">'+ name +' '+surname+' '+trade_name+'</div>\
           <div class="col-md-4">'+value.city+'</div>\
           <input type="hidden" name="id_client" value="'+value.id_client+'"/>\
         </div>\
       ');
     });
   }
  });
});

$(document).on('click', '.detail_shared_trip', function (){
  var id = $(this).data('trip');

  $('#id_trip').val(id)
});

$(document).on('click', '#chamar_dados_shared', function (){
  var id = $('#id_trip').val();
  $.ajax({
   url: url_atual + 'shared_trips/fetch_shared_detail_closed',
   method: 'POST',
   dataType: 'JSON',
   data: {id:id},
   success: function(data){
     $('#biker_shared').val(data.id_biker);
     $('#motorcycle_shared').val(data.id_motorcycle);
     $('#departure_time_shared').val(data.departure_time);
     $('#departure_odometer_shared').val(data.departure_odometer);
     $('#arrival_odometer_shared').val(data.arrival_odometer);
     $('#id_shared_trip').val(data.id);
   }
  });
});

$(document).on('click', '#chamar_dados_shared', function (){
  var id_trip = $('#id_trip').val();
  $.ajax({
   url: url_atual + 'shared_trips/fetch_finished_shared',
   method: 'POST',
   dataType: 'JSON',
   data: {id_trip:id_trip},
   success: function(data){
     $('.div_shared').html('\
       <table border="1">\
         <tr>\
           <td style="width: 20%">Pacote</td>\
           <td style="width: 50%">Remetente</td>\
           <td style="width: 20%">Solicitação</td>\
         </tr>\
       </table>\
     ');
     $.each(data, function (index, value){
       if (value.name != null) {
         var name = value.name +' '+ value.surname;
       }else{
         var name = value.trade_name;
       }
       $('.div_shared').append('\
         <table border="1">\
           <tr style="width: 100%;">\
             <td style="width: 20%">'+ value.id + '</td>\
             <td style="width: 50%">'+ name +'</td>\
             <td style="width: 20%">'+ value.register + '</td>\
           </tr>\
          </table>\
       ');
       $('.div_shared').append('</table>');
     });
   }
  });
});

$(document).on('click', '.detail_shared_trip ', function (){
  $('.div_shared').html('');
  $('#biker_shared').val('');
  $('#motorcycle_shared').val('');
  $('#departure_time_shared').val('');
  $('#departure_odometer_shared').val('');
  $('#arrival_odometer_shared').val('');
});
