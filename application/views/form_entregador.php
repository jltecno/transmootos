<?php $this->load->view('elements/header');?>
<?php $this->load->view('elements/sidebar');?>
<br><br><br><br><br><br>
<header class="header_entregador mt-5">
</header>

<main>
  <section class="">
    <div class="container">
      <div class="row py-5">
        <div class="col-md anime faq-entregador mr-5">
          <div class="row">
            <div class="col-md-12">
              <h2 class="title--green mt-3"> Perguntas frequentes</h2>
              <dl class="js-accordion py-3">
                <dt>Como funciona?</dt>
                <dd>A trans mootos oferece diversos tipos de serviços como: Entregas avulsas, viagens de motoTaxi,
                  contrato dedicado fixo (onde a empresa contrata nosso serviço de moto boy, fixo. dias da semana e
                  horário pré definidos) .</dd>

                <dt>Atende outras cidades?</dt>
                <dd>Sim, atendemos todas a regiões Rurais de Taiobeiras MG e também Salinas e Rio pardo de Minas com Viagens programadas.</dd>

                <dt>Qual o custo por viagem ?</dt>
                <dd>Isso irá depender para qual sua encomenda ou você ira, nosso sistema ira calcular por quilometragem.consulte nossa atendente pelo whats app que te dará a informação de valor conforme sua viagem.</dd>

                <dt>Posso contratar para minha empresa somente quando precisar? </dt>
                <dd>Sim, para isso temos o serviço avulso, que pode ser solicitado sempre que precisar.</dd>

                <dt>Tenho uma Loja/restaurante e tenho uma demanda maior em um período manhã/tarde/noite posso contratar
                  somente neste período?</dt>
                <dd>Sim, temos o serviço de dedicado fixo. em contrato definimos o dias e horários que nosso piloto ficara a disposição de sua empresa.</dd>
              </dl>
            </div>
          </div>
        </div>
        <div class="col-md">
          <div class="row">
            <div class="col-md-12 mx-auto anime">
              <img class="" src="<?php echo base_url()?>assets\build\img\icons.png" alt="">
            </div>
          </div>
          <div class="row">
            <div class="col-md-12 anime text-center text-contact">
              <span class="title--green">Ainda com dúvida ?</span> <br>
              <span class="title--blue">Ligue: </span><span class=""> 0000 - 0000</span>
            </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-6">
          <div class="row my-2 anime faq-require">
            <div class="col-md-12">
              <h3 class="title--blue">Requisitos</h3>
              <dl class="js-accordion">
                <dt>Carros de passeio,utilitário VUC ou moto</dt>
                <dd>Lorem ipsum dolor sit amet consectetur adipisicing elit. Atque consequuntur nobis, in iusto,
                  est
                  dolorum quia quae soluta, similique quis error.</dd>
                <dt>Celular com internet</dt>
                <dd>Lorem ipsum dolor sit amet consectetur adipisicing elit. Suscipit eveniet a sunt, voluptatibus
                  minus
                  laudantium labore alias autem doloribus impedit illum.</dd>
                <dt>Veículo e cnh em situação regular</dt>
                <dd>Lorem ipsum dolor sit amet consectetur adipisicing elit. Suscipit eveniet a sunt, voluptatibus
                  minus
                  laudantium labore alias autem doloribus impedit illum.</dd>
              </dl>
            </div>
          </div>

          <div class="row faq-info-add anime">
            <div class="col-md-12">
              <h3 class="title--blue">Informações adicionais</h3>
              <dl class="js-accordion">
                <dt>A manifestação de interesse é realizada somente atraés desta página</dt>
                <dd>Lorem ipsum dolor sit amet consectetur adipisicing elit. Atque consequuntur nobis, in iusto,
                  est
                  dolorum quia quae soluta, similique quis error.</dd>

                <dt>o ato de manifestar interesse não garante demanda de serviço</dt>
                <dd>Lorem ipsum dolor sit amet consectetur adipisicing elit. Suscipit eveniet a sunt, voluptatibus
                  minus
                  laudantium labore alias autem doloribus impedit illum.</dd>

                <dt>Não fazemos qualquer tipo de cobrança de entregadores</dt>
                <dd>Lorem ipsum dolor sit amet consectetur adipisicing elit. Suscipit eveniet a sunt, voluptatibus
                  minus
                  laudantium labore alias autem doloribus impedit illum.</dd>
              </dl>
            </div>
          </div>
        </div>
        <div class="col-md-5">
          <div class="row anime">
            <div class="col-md-12">
              <div class="text-center">
                <h3 class="title--blue">Manifeste interresse</h3>
                <p>Precisamos de conhecer</p>
              </div>
              <div class="form-group">
                <label for="exampleInputEmail1">Nome</label>
                <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp"
                  placeholder="Seu email">
              </div>
              <div class="form-group">
                <label for="exampleInputEmail1">Celular (incluir o DDD)</label>
                <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp"
                  placeholder="Seu email">
              </div>
              <div class="form-group">
                <label for="exampleInputEmail1">E-mail</label>
                <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp"
                  placeholder="Seu email">
              </div>
              <div class="form-group">
                <label for="exampleInputEmail1">Confirme o e-mail</label>
                <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp"
                  placeholder="Seu email">
              </div>
              <div class="form-group">
                <label for="exampleInputEmail1">Data de Nascimento</label>
                <input type="date" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp"
                  placeholder="Seu email">
              </div>
              <div class="text-center">
                <button class="btn btn-primary mx-auto ">Enviar manifesto</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</main>

<section class="newsletter anime mt-3">
  <div class="container py-md-3">
    <div class="row">
      <div class="col-md-12 text-center mb-1 ">
        <h4 class="h4"><span class="title--green"> Receba nossas novidades</span><span class="title--blue"> por e-mail
          </span></h4>
      </div>
    </div>
    <form action="">
      <div class="row text-center">
        <div class="col-md-5 pr-md-1">
          <div class="form-group">
            <input type="text" class="form-control " placeholder="Nome">
          </div>
        </div>
        <div class="col-md-5 pl-md-1">
          <div class="form-group">
            <input type="email" class="form-control " placeholder="E-mail">
          </div>
        </div>
        <div class="col-md-2 mb-1 pl-md-1">
          <div class="form-group">
            <button type="submit" class="btn btn-primary w-100">Enviar</button>
          </div>
        </div>
      </div>
    </form>
  </div>
</section>



<?php $this->load->view('elements/footer');?>