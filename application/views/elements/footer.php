<style media="screen">
  footer a, .fa-whatsapp, .fa-instagram {color: #fff !important;}
</style>
<footer class="footer">
  <div class="container py-3" id="footer">
    <div class="row text-center">
      <div class="col-md">
        <h3>Links Úteis</h3>
        <!-- <a href="">Trabalhe conosco</a> -->
      </div>
      <div class="col-md">
        <h3>Fale conosco</h3>
        <a href="tel:(38)99263-1818">(38) 99263-1818</a>
        <a href="mailto:atendimento@transmootos.com.br">atendimento@transmootos.com.br</a>
        <p>Praça Americano Mendes, 143 - Taiobeiras - Minas Gerais</p>
      </div>
      <div class="col-md footer__icon">
        <h3>Redes sociais</h3>
        <a href="https://api.whatsapp.com/send?l=pt&amp;phone=5538992631818" class="mr-3" target="_blank" rel="noopener">
          <i class=" fab fa-whatsapp"></i> - WPP
        </a><br>
        <!-- <a class="" href="">
          <i class="fab fa-facebook-f text-green"></i>
        </a> -->
        <a href="https://www.instagram.com/transmootos/" target="_blank" rel="noopener">
          <i class="fab fa-instagram"></i> - Instagram
        </a>
      </div>
    </div>
  </div>
</footer>
<div class="row py-3" style="background-color: #444; color: #fff;">
  <div class="col-md"></div>
  <div class="col-md text-center">
    <span>&copy; Todos os direitos reservados - Transmootos</span>
  </div>
  <div class="col-md text-center">
    <span>Desenvolvido por <a href="https://jltecno.com.br/" target="_blank" rel="noopener" style="background-color: #444; color: #fff;">JL Tecno</a></span>
  </div>
</div>
<!-- <div class="footer_2  ">
  <div class="row">
    <div class="col-md-4">

    </div>
    <div class="col-md-4 text-center">
    </div>
    <div class="col-md-4 text-right">
    </div>
  </div>
</div> -->
<!--   Core JS Files   -->
<script src="<?php echo base_url()?>assets\build\js\concat\corePlugins.all.js"></script>
<script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
<script src="<?php echo base_url()?>assets/build/js/app.js"></script>





</body>

</html>
