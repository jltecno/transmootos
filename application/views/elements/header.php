<!DOCTYPE html>
<html lang="pt-br">

<head>
  <meta charset="utf-8" />
  <link rel="icon" type="image/png" href="<?php echo base_url()?>assets/build/img/favicon/favicon-50-50.png">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <title>
   Transmootos
  </title>
  <meta name="description" content="A Transmootos nasce em Taiobeiras com a missão de trazer agilidade e segurança em entregas. Nosso intuito é de solucionar de maneira profissional e eficaz a questão de entregas, encomendas e viagens rápidas de moto.">
  <meta name="keywords" content="Transmootos, Taiobeiras, Salinas, Serviço de entregas, Serviço de transporte de passageiros, Motos, Motoboy, Mototaxi, Mototaxi em Taiobeiras, Sistema logístico, Logística reversa, Encomenda programada Salinas, Distribuição, Avulso urgente, Dedicado Fixo.">
  <meta content='width=device-width, initial-scale=1.0, maximum-scale=5.0, user-scalable=0, shrink-to-fit=no'
    name='viewport' />
  <!--     Fonts and icons     -->
  <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet" />
  <!-- CSS Files -->

  <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>
  <link href="<?php echo base_url()?>assets/build/css/plugins/slick-theme.css" rel="stylesheet" />
  <link href="<?php echo base_url()?>assets\build\css\all.min.css" rel="stylesheet" />
  <link href="<?php echo base_url()?>assets\build\css\style.css" rel="stylesheet" />

</head>

<body class="">
