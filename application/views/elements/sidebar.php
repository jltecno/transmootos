<div class="bg-light navbar--border fixed-top">
  <nav class="navbar navbar-expand-lg  navbar-light container py-md-2">
    <a class="navbar-brand" href="<?php echo base_url()?>landing"><img src="<?php echo base_url()?>assets/build/img/logo/logo_sem_fundo.png"
        alt="Logo Transmootos"></a>
    <button class="navbar-toggler " type="button" data-toggle="collapse" data-target="#conteudoNavbarSuportado"
      aria-controls="conteudoNavbarSuportado" aria-expanded="false" aria-label="Alterna navegação">
      <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="conteudoNavbarSuportado">

      <ul class="navbar-nav ml-auto">
        <li class="nav-item active">
          <a class="nav-link" href="<?php echo base_url()?>">Inicio</a>
        </li>
        <li class="nav-item item-scroll">
          <a class="nav-link " href="#our_services">Serviços</a>
        </li>
        <li class="nav-item item-scroll">
          <a class="nav-link" href="#who_we_are">Quem somos</a>
        </li>
        <li class="nav-item item-scroll">
          <a class="nav-link" href="#partners">Parceiros</a>
        </li>
        <li class="nav-item item-scroll">
          <a class="nav-link " href="#footer">Contato</a>
        </li>
        <!-- <li class="nav-item ">
          <a class="nav-link nav-link--work btn  btn-outline-primary " href="<?php echo base_url()?>form_entregador">TRABALHE CONOSCO</a>
        </li> -->
      </ul>
    </div>
  </nav>
</div>
