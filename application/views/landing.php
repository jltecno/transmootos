<?php $this->load->view('elements/header');?>
<?php $this->load->view('elements/sidebar');?>
<br><br>
<section class="header__block">
	<div class="container">
		<div class="row">
			<div class="col-md">
				<h1 class="">A Empresa certa para <br> sua entrega!</h1>
				<p class="">Logística integrada para empresas,viagens rápidas e encomendas.<br>Agilize suas entregas
					com segurança!</p>
				<h3 class="">Contrate esse serviço</h3>
			</div>
		</div>

	</div>
</section>
<section class="our_services ">
	<div class="container py-5" id="our_services">
		<div class="row">
			<div class="col-md-12">
				<h2 class=" mb-5 h1"><span class="title--green">Nossos</span> serviços</h2>
			</div>
		</div>
		<div class="row pb-5">
			<div class="col-md-12 services_slick ">
				<div class="card  anime mr-4">
					<img class="card-img-top " src="<?php echo base_url()?>assets\build\img\services\distribuicao.jpeg"
						alt="Imagem de capa do card">
					<div class="card-body">
						<h5 class="card-title">Distribuição</h5>
						<p class="card-text">Entregas em grande quantidades: convites, brindes ou materiais de forma
							pulverizada.Gerando economia e proporcionando benefícios. </p>
					</div>
				</div>


				<div class="card  anime mr-4 ">
					<img class="card-img-top " src="<?php echo base_url()?>assets\build\img\services\avulso_urgente.jpeg"
						alt="Imagem de capa do card">
					<div class="card-body">
						<h5 class="card-title">Avulso Urgente</h5>
						<p class="card-text">Entregas e coletas rápidas de documentos, matérias e serviços burocráticos como bancos
							e cartórios, solicitando sempre quando houver necessidades</p>
					</div>
				</div>


				<div class="card  anime mr-4 ">
					<img class="card-img-top " src="<?php echo base_url()?>assets\build\img\services\terceirizacao.jpeg"
						alt="Imagem de capa do card">
					<div class="card-body">
						<h5 class="card-title">Dedicado fixo</h5>
						<p class="card-text">Terceirização de entregador regularizado e personalizado em período integral para sua
							empresa de acordo as especificações.</p>
					</div>
				</div>

				<div class="card  anime mr-4 ">
					<img class="card-img-top " src="<?php echo base_url()?>assets\build\img\services\troca_malotes.jpeg"
						alt="Imagem de capa do card">
					<div class="card-body">
						<h5 class="card-title">Troca de malotes</h5>
						<p class="card-text">Serviços de troca de malotes entre matriz, clientes ou fornecedores, que possuem
							rotinas diárias e necessidades especificas facilitando a transferencia de documentos</p>
					</div>
				</div>

				<div class="card  anime mr-4 ">
					<img class="card-img-top " src="<?php echo base_url()?>assets\build\img\services\entrega_programada.jpeg"
						alt="Imagem de capa do card">
					<div class="card-body">
						<h5 class="card-title">Entrega Progamada</h5>
						<p class="card-text">De acordo com sua programação, agendas sua coleta, podendo fazer o planejamento dos seu
							trabalhos, garantindo sua entrega de maneira precisa. </p>
					</div>
				</div>


				<div class="card  anime mr-4 ">
					<img class="card-img-top " src="<?php echo base_url()?>assets\build\img\services\sistema_logistico.jpeg"
						alt="Imagem de capa do card">
					<div class="card-body">
						<h5 class="card-title">Sistema logístico</h5>
						<p class="card-text">Solução completa, entregados dedicado com um sistema de controle fina para sua
							logística, com tecnologia digital, proporcionando todo controle da operação junto ao seu cliente</p>
					</div>
				</div>

				<div class="card  anime mr-4 ">
					<img class="card-img-top " src="<?php echo base_url()?>assets\build\img\services\logistica_reversa.jpeg"
						alt="Imagem de capa do card">
					<div class="card-body">
						<h5 class="card-title">Logística reversa</h5>
						<p class="card-text"> Coleta de produtos e materiais do local de consumo para origem de modo eficiente, com
							cronograma ou fazendo agendamento direto com o cliente, proporcionando a efetividade no processo.</p>
					</div>
				</div>

				<div class="card  anime mr-4 ">
					<img class="card-img-top " src="<?php echo base_url()?>assets\build\img\services\entrega_programada.jpeg"
						alt="Imagem de capa do card">
					<div class="card-body">
						<h5 class="card-title">Encomenda Progamada Salinas</h5>
						<p class="card-text">Envio de documentos, encomendas para a cidade de Salinas. Essa viagem é feita em dias
							definidos. </p>
					</div>
				</div>


			</div>
		</div>
	</div>


	</div>
</section>
<section class="who_we_are" id="who_we_are">
	<div class="container py-3">
		<div class="row py-5">
			<div class="col-md-6">
				<img class="img-fluid anime mt-md-4" src="<?php echo base_url()?>assets\build\img\piloto.png" alt="">
			</div>
			<div class="col-md-6 anime2">
				<h2 class="text-center mb-5 h1"><span class="title--green"> Quem</span> somos</h2>
				<p>Nascemos para trazer agilidade e segurança em entregas. Nosso intuito é de solucionar de maneira profissional
					e eficaz a questão de entregas, encomendas e viagens rápidas de moto.
					<p>
						Fazendo uma logística integrada, um sistema que monitora todas viagens e cada informação.
						Com qualidade e treinamento de pessoas para que o serviço seja prestado com excelência.
					</p>
					<p>
						Empresários do ramo de peças e manutenção de motos. Os sócios integram conhecimento e experiência no ramo
						motociclista a serviço de

						logística com a nova empresa, que nasce em 2020 em Taiobeiras MG.
					</p>
					<p>
						Com escritório centralizado na cidade, estamos preparados para o atendimento ao público e empresas.
						Reforçando a importância da parceria entre clientes e a Trans Mootos.
						Nossa equipe esta preparada para as atividades, com cuidado e a atenção aos detalhes.</p>
			</div>
		</div>
	</div>
</section>
<section class="partners py-5 " id="partners">
	<div class="container">
		<div class="row mb-2 anime">
			<div class="col-md text-center">
				<h2 class="h1"><span class="title--green">Nossos</span> parceiros</h2>
				<p>Empresas que apoiam o nosso trabalho</p>
			</div>
		</div>
		<div class="row anime2">
			<div class="col-md-12   mx-auto">
				<div class="row partners__bg  mx-auto my-3">
					<div class="col-md-12 mx-auto partners_slick d-flex align-itens-center justify-content-center my-auto">
						<div class="partners__bg__itens  ">
							<img class="w-100" src="<?php echo base_url()?>assets\build\img\patrocinadores\3.png" alt="">
						</div>
						<div class="partners__bg__itens">
							<img class="w-100" src="<?php echo base_url()?>assets\build\img\patrocinadores\4.png" alt="">
						</div>
						<div class="partners__bg__itens">
							<img src="<?php echo base_url()?>assets\build\img\patrocinadores\5.png" alt="" class="partners__img">
						</div>
						<div class="partners__bg__itens">
							<img src="<?php echo base_url()?>assets\build\img\patrocinadores\6.png" alt="" class="partners__img">
						</div>
						<div class="partners__bg__itens">
							<img src="<?php echo base_url()?>assets\build\img\patrocinadores\7.png" alt="" class="partners__img">
						</div>
						<div class="partners__bg__itens">
							<img src="<?php echo base_url()?>assets\build\img\patrocinadores\8.png" alt="" class="partners__img">
						</div>
						<div class="partners__bg__itens">
							<img src="<?php echo base_url()?>assets\build\img\patrocinadores\9.png" alt="" class="partners__img">
						</div>
						<div class="partners__bg__itens">
							<img src="<?php echo base_url()?>assets\build\img\patrocinadores\10.png" alt="" class="partners__img">
						</div>
						<div class="partners__bg__itens">
							<img src="<?php echo base_url()?>assets\build\img\patrocinadores\11.png" alt="" class="partners__img">
						</div>
						<div class="partners__bg__itens">
							<img src="<?php echo base_url()?>assets\build\img\patrocinadores\12.png" alt="" class="partners__img">
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="valores" id="who_we_are">
	<div class="container py-3">
		<div class="row py-5">
			<div class="col-md">
				<h2 class="title--blue">Missão</h2>
				<p>Prestar serviços em: entregas, encomendas e transporte com qualidade e confiabilidade, sempre visando a
					satisfação do cliente.</p>
			</div>

			<div class="col-md">
				<h2 class="title--blue">Visão</h2>
				<p>Ser referência em transportes, entregas e encomendas de motos do norte de Minas Gerais.</p>

			</div>
			<div class="col-md-5">
				<h2 class="title--blue">Valores</h2>
				<ul>
					<li>Seriedade e Honestidade com clientes, colaboradores e parceiros.</li>
					<li>Parceria e relações duradouras com o mercado.</li>
					<li>Qualidade e melhoria contínua nos serviços prestados.</li>
					<li>Transparência na administração.</li>
					<li>Sustentabilidade, saúde e segurança.   </li>
				</ul>


			</div>
		</div>
	</div>
</section>




<!-- <section class="depoimentos anime">
	<div class="container  py-5 text-center">
		<div class="row">
			<div class="col-md-12">
				<h2 class="h1 mb-5"> <span class="title--green"> Veja o que nossos </span> clientes estão falando </h2>
			</div>
		</div>
		<div class="row d-flex justify-content-center">
			<div class="col-md-12 mx-auto justify-content-center depoimentos_slick  ">
				<div class="depoimento__block  justify-content-center py-2  ">
					<img class="depoimento__block--img mx-auto mt-3"
						src="<?php echo base_url()?>assets/build/img/default-avatar.png" alt="">
					<blockquote class="blockquote text-center mt-2 depoimento__block--text mx-auto">
						<p class="mb-0">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.
						</p>
						<footer class="blockquote-footer">Cargo/Area<cite title="Nome do cliente">Nome do cliente</cite>
						</footer>
					</blockquote>
				</div>
				<div class="depoimento__block  justify-content-center py-2  ">
					<img class="depoimento__block--img mx-auto mt-3"
						src="<?php echo base_url()?>assets/build/img/default-avatar.png" alt="">
					<blockquote class="blockquote text-center mt-2 depoimento__block--text mx-auto">
						<p class="mb-0">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.
						</p>
						<footer class="blockquote-footer">Cargo/Area<cite title="Nome do cliente">Nome do cliente</cite>
						</footer>
					</blockquote>
				</div>
				<div class="depoimento__block  justify-content-center py-2  ">
					<img class="depoimento__block--img mx-auto mt-3"
						src="<?php echo base_url()?>assets/build/img/default-avatar.png" alt="">
					<blockquote class="blockquote text-center mt-2 depoimento__block--text mx-auto">
						<p class="mb-0">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.
						</p>
						<footer class="blockquote-footer">Cargo/Area<cite title="Nome do cliente">Nome do cliente</cite>
						</footer>
					</blockquote>
				</div>
			</div>
		</div>
	</div>
</section> -->




<!-- <section class="work_with_us ">
	<div class="container">
		<div class="row ">
			<div class="col-md-2">
				<img class="work_with_us--img anime" src="<?php echo base_url()?>assets/build/img/entregador2.jpg" alt="">
			</div>
			<div class="col-md text-center align-self-center  anime">
				<h2 class="h1 py-0 my-0"><span class="title--blue"> Trabalhe </span> conosco</h2>
				<p class=""><a class="" href="<?php echo base_url()?>form_entregador">Clique aqui</a> para saber mais</p>
			</div>
		</div>
	</div>
</section> -->


<!-- <section class="newsletter anime">
	<div class="container py-md-3">
		<div class="row">
			<div class="col-md-12 text-center mb-1 ">
				<h4 class="h4"><span class="title--green"> Receba nossas novidades</span><span class="title--blue"> por e-mail
					</span></h4>
			</div>
		</div>
		<form action="">
			<div class="row text-center">
				<div class="col-md-5 pr-md-1">
					<div class="form-group">
						<input type="text" class="form-control " placeholder="Nome">
					</div>
				</div>
				<div class="col-md-5 pl-md-1">
					<div class="form-group">
						<input type="email" class="form-control " placeholder="E-mail">
					</div>
				</div>
				<div class="col-md-2 mb-1 pl-md-1">
					<div class="form-group">
						<button type="submit" class="btn btn-primary w-100">Enviar</button>
					</div>
				</div>

			</div>
		</form>
	</div>
</section> -->
<?php $this->load->view('elements/footer');?>